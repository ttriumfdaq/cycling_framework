#ifndef DATA_SOURCE_LRS_H
#define DATA_SOURCE_LRS_H

#include "core/data_source.h"

#ifdef DUMMY_VME
#define MVME_INTERFACE int
#else
#include "vmicvme.h"
#endif

/*
 * Wraps the lrs1190.c driver in a DataSource object for
 * easy usage by the cycling framework.
 */
class DataSourceLRS : public DataSource {
  public:
    DataSourceLRS(MVME_INTERFACE* _myvme, DWORD _base);
    virtual ~DataSourceLRS();

    // Implementation of functions from DataSource interface.
    virtual INT begin_of_run(char* error) override;
    virtual INT end_of_run() override;
    virtual void new_cycle_starting(int cycle_count) override;
    virtual void this_cycle_finished() override;
    virtual DataStatus end_cycle_is_data_ready() override;
    virtual void write_data_to_banks(char *pevent) override;

  private:

    // VME connection.
    MVME_INTERFACE* myvme;

    // VME address base.
    DWORD VMCP_BASE;

    // Number of PPG cycles seen. Encoded as part of data format.
    DWORD cyc_count;

    // Number of words read from device.
    DWORD num_data;

    // Data read from device.
    DWORD* data;
};

#endif
