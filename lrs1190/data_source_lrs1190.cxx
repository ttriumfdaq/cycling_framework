#include "data_source_lrs1190.h"
#include "core/data_source.h"
#include <stdlib.h>
#include "lrs1190.h"

DataSourceLRS::DataSourceLRS(MVME_INTERFACE* _myvme, DWORD _base) {
  myvme = _myvme;
  VMCP_BASE = _base;
  cyc_count = 0;
  num_data = 0;
  data = NULL;
}

DataSourceLRS::~DataSourceLRS() {

}

INT DataSourceLRS::begin_of_run(char* error) {
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);
  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  lrs1190_Reset(myvme, VMCP_BASE);

  printf("vmcp: Count:0x%x\n", lrs1190_CountRead(myvme, VMCP_BASE));

  // Enable the module
  lrs1190_Enable(myvme, VMCP_BASE);

  return SUCCESS;
}

INT DataSourceLRS::end_of_run() {
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);
  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  // Disable the module
  lrs1190_Disable(myvme, VMCP_BASE);

  return SUCCESS;
}

void DataSourceLRS::new_cycle_starting(int cycle_count) {
  cyc_count = cycle_count;
}

void DataSourceLRS::this_cycle_finished() {}

DataStatus DataSourceLRS::end_cycle_is_data_ready() {
  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  num_data = lrs1190_CountRead(myvme, VMCP_BASE);

  if (num_data == 0) {
    data = NULL;
  } else {
    data = (DWORD*) malloc(num_data * sizeof(DWORD));
    lrs1190_I4Read(myvme, VMCP_BASE, data, num_data);
  }

  return DataReady;
}

void DataSourceLRS::write_data_to_banks(char *pevent) {
  DWORD* pmdata;

  bk_create(pevent, "MCPP", TID_DWORD, (void **) &pmdata);

  *pmdata++ = (DWORD) cyc_count;  // PPG cycle number

  // Data words
  for (DWORD i = 0; i < num_data; i++) {
    *pmdata++ = data[i];
  }

  bk_close(pevent, pmdata);

  if (data) {
    free(data);
    data = NULL;
  }
}

