# Builds:
# * Static library
#
# Required parameters:
# * VMIC_VME_OBJ (location of VME source)
# * VMIC_VME_INC (location of VME headers)
# * VME_UNIV (location of VME universe headers)
# * CORE_STATIC_NAME (exported by core subdirectory)
# * MIDASSYS (midas install location - set in parent CMakeLists.txt)
# * MIDAS_LIB (midas static library - set in parent CMakeLists.txt)
#
# Optional parameters:
# * LIB_SUFFIX (suffix for all lib names, e.g. if you want to distinguish 32- and 64-bit builds)
#
# Exported parameters:
# * LRS1190_STATIC_NAME (name of static lib that gets built)

set(libsrc_lrs1190
  lrs1190
  ${VMIC_VME_OBJ}
  data_source_lrs1190)

set(LRS1190_OBJLIB_NAME objlib_lrs1190${LIB_SUFFIX})
set(LRS1190_STATIC_NAME cycling_lrs1190${LIB_SUFFIX} CACHE INTERNAL "")

add_library(${LRS1190_OBJLIB_NAME} OBJECT ${libsrc_lrs1190})
set_property(TARGET ${LRS1190_OBJLIB_NAME} PROPERTY POSITION_INDEPENDENT_CODE 1)

add_library(${LRS1190_STATIC_NAME} STATIC $<TARGET_OBJECTS:${LRS1190_OBJLIB_NAME}>)

set(VME_LIBS -L${VME_UNIV}/lib -lvme)

target_include_directories(${LRS1190_OBJLIB_NAME} PUBLIC
  ${VME_UNIV}/include
  ${MIDASSYS}/include
  ${VMIC_VME_INC}
  ..)

target_link_libraries(${LRS1190_STATIC_NAME} PUBLIC ${MIDAS_LIB} ${VME_LIBS} ${CORE_STATIC_NAME})
