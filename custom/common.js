var cycling = cycling || {};

cycling.common = (function() {
  var create_run_state_div = function(div_name, explain_disabled=true, add_start_stop_buttons=false, callback=undefined) {
    let html = "";
    html += '<span id="runstate_text" style="padding-left:10px; padding-right:10px;"></span> <div id="runstate_extra" style="display:inline"></div>';
    html += '<span id="runstate_int" class="modbvalue" style="display:none" data-odb-path="/Runinfo/State" onchange="cycling.common.run_state_changed(' + explain_disabled + ', ' + add_start_stop_buttons + ', ' + callback + ')"></span>'
    $(div_name).html(html);
  };

  var run_state_changed = function(explain_disabled, add_start_stop_buttons, callback) {
    let state = parseInt($("#runstate_int").text());
    
    if (state == 1) {
      let extra = "";
      
      if (add_start_stop_buttons) {
        extra += '<input class="mbutton" id="startButton" type="button" value="Start" onclick="mhttpd_start_run();">';
      }
      
      $("#runstate_text").text("Stopped");
      $("#runstate_extra").html(extra);
      $("#runstate_text").removeClass("mgreen");
      $("#runstate_text").removeClass("myellow");
      $("#runstate_text").addClass("mred");
      disable_while_running(false);
    } else if (state == 2) {
      let extra = "";

      if (add_start_stop_buttons) {
        extra += '<input class="mbutton" id="resumeButton" type="button" value="Resume" onclick="mhttpd_resume_run();">';
        extra += '<input class="mbutton" id="stopButton" type="button" value="Stop" onclick="mhttpd_stop_run();">';
      }
      if (explain_disabled) {
        extra += "(Page functionality is disabled while run is in progress)";
      }
      
      $("#runstate_text").text("Paused");
      $("#runstate_extra").html(extra);
      $("#runstate_text").removeClass("mgreen");
      $("#runstate_text").addClass("myellow");
      $("#runstate_text").removeClass("mred");
      disable_while_running(true);
    } else if (state == 3) {
      let extra = "";

      if (add_start_stop_buttons) {
        extra += '<input class="mbutton" id="stopButton" type="button" value="Stop" onclick="mhttpd_stop_run();">';
      }
      if (explain_disabled) {
        extra += "(Page functionality is disabled while run is in progress)";
      }
      
      $("#runstate_text").text("Running");
      $("#runstate_extra").html(extra);
      $("#runstate_text").addClass("mgreen");
      $("#runstate_text").removeClass("myellow");
      $("#runstate_text").removeClass("mred");
      disable_while_running(true);
    }
    
    if (callback !== undefined) {
      callback(state);
    }
  };

  var disable_while_running = function(disable) {
    $(".disable_running").prop('disabled', disable);
    
    if (disable) {
      $(".disable_running").each(function(idx) {
        let ds = $(this).get(0).dataset;
        
        if ("odbEditable" in ds) {
          ds["wasEditable"] = "1";
          delete ds["odbEditable"];
          $(this).empty();
        }
      })
    } else {
      $(".disable_running").each(function(idx) {
        let ds = $(this).get(0).dataset;
        
        if ("wasEditable" in ds) {
          ds["odbEditable"] = "1";
          delete ds["wasEditable"];
          // Delete current value, so midas re-populates link
          $(this).empty();
        }
      })
    } 
  };

  var parse_rpc_response = function(rpc_result) {
    let status = rpc_result.status;
    let reply = "";
    
    if (status == 1) {
      // Only get a reply from mjsonrpc if status is 1
      let parsed = JSON.parse(rpc_result.reply);
      status = parsed["code"];
      reply = parsed["msg"];
    }
    
    return [status, reply];
  };
  
  var odbselect_html = function(odb_path, 
                                elem_id_base, 
                                allowed_options, 
                                extra_odb_change_callback, 
                                extra_classes, 
                                value_manipulator_sel_to_odb, 
                                value_manipulator_odb_to_sel) {
    // allowed_options: select val -> text
    let onchange_sel = "cycling.common.odbselect_select_changed('" + elem_id_base + "'";
    onchange_sel += ", " + value_manipulator_sel_to_odb + ");";
    let onchange_odb = "cycling.common.odbselect_odb_changed('" + elem_id_base + "'";
    onchange_odb += ", " + extra_odb_change_callback;
    onchange_odb += ", " + value_manipulator_odb_to_sel + ");";
    
    let classes = 'disable_running';
    
    if (extra_classes !== undefined) {
      classes += " " + extra_classes;
    }
    
    let html = "";
    html += '<select id="sel_' + elem_id_base + '" onchange="' + onchange_sel + '" class="' + classes + '">';
    
    for (let val in allowed_options) {
      let name = allowed_options[val];
      html += '<option value="' + val + '">' + name + '</option>';
    }

    html += '</select>';
    html += '<span style="display:none" class="modbvalue" data-odb-path="' + odb_path + '" id="span_' + elem_id_base + '" onchange="' + onchange_odb + '"></span>';
    return html;
  };
  
  var odbselect_odb_changed = function(elem_id_base, callback, value_manipulator) {
    let odb_val = $("#span_" + elem_id_base).html();
    let sel_opts = $("#sel_" + elem_id_base + " option").map(function() {return $(this).val();}).get();
    let sel_val = odb_val;
    
    if (value_manipulator !== undefined) {
      sel_val = value_manipulator(odb_val);
    }
    
    if (sel_opts.indexOf(odb_val) == -1) {
      // Handle any case-sensitivity if val not in list
      for (var i in sel_opts) {
        if (sel_opts[i].toLowerCase() == odb_val.toLowerCase()) {
          sel_val = sel_opts[i];
          break;
        }
      }
    } else {
      sel_val = odb_val;
    }
    
    if (sel_val !== undefined) {
      $("#sel_" + elem_id_base).val(sel_val);
    } else {
      // ODB set to invalid value. Set to first one.
      let odb_path = $("#span_" + elem_id_base).data("odb-path");
      let first_val = select.options[0].value;
      console.log("ODB path " + odb_path + " is set to " + odb_val + ", which isn't in allowed list of options for select box sel_" + elem_id_base + ". Resetting to first value (" + first_val + ")");
      $("#sel_" + elem_id_base).val(first_val);
      odbselect_select_changed(elem_id_base);
    }
    
    if (callback !== undefined) {
      callback();
    }
  };
  
  var odbselect_select_changed = function(elem_id_base, value_manipulator) {
    let new_val = $("#sel_" + elem_id_base).val();
    let odb_path = $("#span_" + elem_id_base).data("odb-path");
    
    if (value_manipulator !== undefined) {
      new_val = value_manipulator(new_val);
    }
    
    mjsonrpc_db_set_value(odb_path, new_val);  
  };
  
  return {
    create_run_state_div: create_run_state_div,
    run_state_changed: run_state_changed,
    parse_rpc_response: parse_rpc_response,
    odbselect_html: odbselect_html,
    odbselect_odb_changed: odbselect_odb_changed,
    odbselect_select_changed: odbselect_select_changed
  };
})();


