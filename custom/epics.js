var cycling = cycling || {};

cycling.epics = (function() {
  var show_epics_unused = false;
  var demand_set = {};
  
  var init = function() {
    cycling.common.create_run_state_div("#runstate");
    
    if (config.paths.epics_defs_base === undefined) {
      $(".show_unused_epics").hide();
      $("#epics").html("<tr><td colspan='5'>config.paths.epics_defs_base not defined</td></tr>");
      mhttpd_init('EPICS definitions');
    } else {
      mjsonrpc_db_get_values([config.paths.epics_defs_base + "/Demand device"]).then(function(rpc_prog) {
        if (rpc_prog.result.status[0] == 312) {
          $(".show_unused_epics").hide();
          $("#epics").html("<tr><td colspan='5'>No EPICS definitions found in ODB</td></tr>");
        } else {
          handle_epics_defs(rpc_prog.result.data[0].length);
        }
        mhttpd_init('EPICS definitions');
      });
    }
  };
  
  var set_show_epics_unused = function() {
    show_epics_unused = true;
    $(".epics_row").show();
    $(".show_unused_epics").hide();
  };
  
  var demand_changed = function(i) {
    let id_sfx = "_" + i;
    let odb_val = $("#demand" + id_sfx).text();
    
    if ((odb_val == "" || odb_val == "(empty)") && !show_epics_unused) {
      $("#epics_row" + id_sfx).hide();
      if (id_sfx in demand_set) {
        delete demand_set[id_sfx];
      }
    } else {
      $("#epics_row" + id_sfx).show();
      demand_set[id_sfx] = true;
    }
    
    $("#epics_count").text(Object.keys(demand_set).length);
  };
  
  var tol_odb_to_sel_manipulator = function(odb_val) {
    let sel_val = odb_val;
    
    if (odb_val == "" || odb_val == "(empty)") {
      sel_val = "default";
    }
    
    return sel_val;
  }
  
  var tol_sel_to_odb_manipulator = function(sel_val) {
    let odb_val = sel_val;
    
    if (sel_val == "default") {
      odb_val = "";
    }
    
    return odb_val;
  }
  
  var html_epics_row = function(i) {
    let id_sfx = "_" + i;
    let html = '<tr class="epics_row" id="epics_row' + id_sfx + '">';
    
    // ID
    html += "<th>" + i + "</th>";
    
    // Human name
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.epics_defs_base + '/Human name[' + i + ']"></span>';
  
    // Demand name
    html += '<td><span id="demand' + id_sfx + '" class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.epics_defs_base + '/Demand device[' + i + ']" onchange="cycling.epics.demand_changed(' + i + ')"></span>';
  
    // Measured name
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.epics_defs_base + '/Measured device[' + i + ']"></span>';
  
    // Tolerance function
    let odb_path = config.paths.epics_defs_base + '/Custom threshold check fn[' + i + ']';
    let human_to_func = {};
    
    if (config.epics.threshold_fns !== undefined && Object.keys(config.epics.threshold_fns).length > 0) {
      human_to_func = config.epics.threshold_fns;
    }
    
    let func_to_human = {};
    
    for (let key in human_to_func) {
      func_to_human[human_to_func[key]] = key;
    }
    html += '<td>';
    html += cycling.common.odbselect_html(odb_path, "tol_" + id_sfx, func_to_human, undefined, "tol_sel", "cycling.epics.tol_sel_to_odb_manipulator", "cycling.epics.tol_odb_to_sel_manipulator");
    html += '</td>';
      
    // Finished
    html += "</tr>";
    return html;
  };
  
  var handle_epics_defs = function(num_params) {
    let html = "";
    
    for (let i = 0; i < num_params; i++) {
      html += html_epics_row(i);
    }
  
    $("#epics").html(html);
  };
  
  return {
    init: init,
    set_show_epics_unused: set_show_epics_unused,
    tol_sel_to_odb_manipulator: tol_sel_to_odb_manipulator,
    tol_odb_to_sel_manipulator: tol_odb_to_sel_manipulator,
    demand_changed: demand_changed
  };
})();