var cycling = cycling || {};

cycling.ppg = (function() {
  var unique_id = 0;
  var ch_names = [];
  var ch_shortnames = [];
  var tref_shortnames = {};
  var tref_texts = {};
  var loop_name_of_block = {};
  var num_channels = 32;
  var all_block_names = [];
  var only_channels = undefined;
  var hide_move_button = false;
  var hide_block_number = false;
  
  var init = function() {
    setInterval(reload_image, 5000);
    cycling.common.create_run_state_div("#runstate");
    
    mjsonrpc_db_get_values([config.paths.ppg_prog_base, config.paths.scanning_base]).then(function(rpc_prog) {
      handle_ppg_structure(rpc_prog.result.data[0], rpc_prog.result.data[1]);
      mhttpd_init('PPG programming');
    });
  };
  
  var ppg_only_show_channels = function(chan_nums) {
    only_channels = chan_nums;
  }
  
  var should_show_channel = function(chan) {
    if (only_channels === undefined) {
      return true;
    } 
    
    return only_channels.indexOf(chan) > -1;
  };
  
  var populate_channel_names = function(ppg) {
    let odb_names = [];
    
    if (ppg.hasOwnProperty("names")) {
      odb_names = ppg["names"]["names"];
    }
    
    for (let i = 0; i < num_channels; i++) {
      let name = "";
      let shortname = "CH" + (i + 1);
      
      if (!should_show_channel(shortname)) {
        continue;
      }
      
      if (i < odb_names.length) {
        name += odb_names[i] + " (" + shortname + ")";
      } else {
        name = shortname;
      }
      
      ch_names.push(name);
      ch_shortnames.push(shortname);
    }
  };
  
  var block_name_to_loop_name = function(block_name) {
    let trim_len = 0;
    if (block_name.toLowerCase().indexOf("begin_") == 0) {
      trim_len = 6;
    } else if (block_name.toLowerCase().indexOf("begin") == 0) {
      trim_len = 5;
    } else if (block_name.toLowerCase().indexOf("end_") == 0) {
      trim_len = 4;
    } else if (block_name.toLowerCase().indexOf("end") == 0) {
      trim_len = 3;
    }
    return block_name.substr(trim_len);
  };
  
  var populate_tref_names = function(ppg) {
    let loop_list = [""];
    let curr_loop = "";
    
    tref_shortnames[curr_loop] = []
    tref_shortnames[curr_loop].push("");
    tref_texts[curr_loop] = []
    tref_texts[curr_loop].push("End of previous block");
    tref_shortnames[curr_loop].push("T0");
    tref_texts[curr_loop].push("Start of PPG sequence (T0)");
    
    for (let block_name in ppg) {
      if (block_name.indexOf("/name") != -1) {
        continue;
      }
      if (block_name.indexOf("/key") != -1) {
        continue;
      }
      if (["skip", "image", "names"].indexOf(block_name) != -1) {
        continue;
      }
      
      let block = ppg[block_name];
      let lower = block_name.toLowerCase();
      
      loop_name_of_block[block_name] = curr_loop;
      all_block_names.push(block_name);
      
      if (lower.indexOf("trans") == 0 || lower.indexOf("turn") == 0) {
        tref_shortnames[curr_loop].push("_T" + block_name);
        tref_texts[curr_loop].push("Transition " + block_name)
      } else if (lower.indexOf("pulse") == 0 || lower.indexOf("stdpulse") == 0) {
        tref_shortnames[curr_loop].push("_TSTART_" + block_name);
        tref_texts[curr_loop].push("Start of " + block_name)
        tref_shortnames[curr_loop].push("_TEND_" + block_name);
        tref_texts[curr_loop].push("End of " + block_name)
      } else if (lower.indexOf("pat") == 0) {
        tref_shortnames[curr_loop].push("_T" + block_name);
        tref_texts[curr_loop].push("Pattern " + block_name)
      } else if (lower.indexOf("del") == 0) {
        tref_shortnames[curr_loop].push("_T" + block_name);
        tref_texts[curr_loop].push("End of delay " + block_name)
      } else if (lower.indexOf("tim") == 0) {
        tref_shortnames[curr_loop].push("_T" + block_name);
        tref_texts[curr_loop].push("Time reference " + block_name)
      } else if (lower.indexOf("begin") == 0) {
        let loop_name = block_name_to_loop_name(block_name)
        
        loop_list.push(loop_name);
        curr_loop = loop_name;
        tref_texts[curr_loop] = []
        tref_texts[curr_loop].push("End of previous block");
        tref_shortnames[curr_loop] = [];
        tref_shortnames[curr_loop].push("")
        
        tref_shortnames[curr_loop].push("_TBEG" + loop_name);
        tref_texts[curr_loop].push("Start of each loop of " + loop_name)
      } else if (lower.indexOf("end") == 0) {
        let loop_name = block_name_to_loop_name(block_name)
        if (loop_name != curr_loop) {
          alert("Invalid PPG structure!")
        } else {
          loop_list.pop();
          curr_loop = loop_list[loop_list.length - 1];
          tref_shortnames[curr_loop].push("_TEND" + loop_name);
          tref_texts[curr_loop].push("End of all loops of " + loop_name)
        }
      } 
    }  
  };
  
  var handle_ppg_structure = function(ppg, scanning) {
    let html = "";
    let loop_depth = 0;
  
    let ppg_scan = {"x": [], "y": []};
  
    if (scanning !== null && scanning.hasOwnProperty("ppg")) {
      let ppg_sett = scanning["ppg"]["settings"];
      let check_x = scanning["global"]["settings"]["enable x"];
      let check_y = scanning["global"]["settings"]["enable y"];
      
      if (check_x) {
        ppg_scan["x"] = ppg_sett["x paths"];
      }
      
      if (check_y) {
        ppg_scan["y"] = ppg_sett["y paths"];
      }
    }
    
    populate_channel_names(ppg);
    populate_tref_names(ppg);
    
    let block_idx = 0;
    
    for (let rpc_block_name in ppg) {
      if (rpc_block_name.indexOf("/name") != -1) {
        continue;
      }
      if (rpc_block_name.indexOf("/key") != -1) {
        continue;
      }
      if (["skip", "image", "names"].indexOf(rpc_block_name) != -1) {
        continue;
      }
      let block_name = ppg[rpc_block_name + "/name"];
      let block = ppg[rpc_block_name];
      
      if (rpc_block_name.indexOf("end") == 0) {
        loop_depth -= 1;
      }
      
      html += block_html(block_name, block, loop_depth, ppg_scan, block_idx);
  
      if (rpc_block_name.indexOf("begin") == 0) {
        loop_depth += 1;
      }
      
      block_idx += 1;
    }
    
    $("#ppg").html(html);
  };
  
  var odb_ppg_span_html = function(block_name, param, format, editable) {
    let odb_path = config.paths.ppg_prog_base + "/" + block_name + "/" + param;
    return odb_span_html(odb_path, format, editable);
  };
  
  var odb_span_html = function(odb_path, format, editable, extra) {
    let odb_format = '';
    let odb_edit = '';
    let extra_html = '';
    if (format === undefined) {
      odb_format = 'data-format="' + format + '"';
    }
    if (editable === undefined || editable) {
      odb_edit = 'data-odb-editable="1"';
    }
    if (extra !== undefined) {
      extra_html = extra;
    }
    return '<span class="modbvalue disable_running" ' + odb_edit + ' data-odb-path="' + odb_path + '" ' + odb_format + ' ' + extra_html + '></span>';
  };
  
  var odb_scan_ppg_html = function(scan_xy, scan_idx, odb_path) {
    let retval = "";
    let odb_start = config.paths.scanning_ppg_base + "/" + scan_xy + " start[" + scan_idx + "]";
    let odb_end = config.paths.scanning_ppg_base + "/" + scan_xy + " end[" + scan_idx + "]";
    let extra = 'data-scanmin="' + odb_path + '"';
    
    return odb_span_html(odb_start, undefined, true, extra) + " to " + odb_span_html(odb_end);
  };
  
  var scan_min = function() {
    $("[data-scanmin]").each(function(idx) {
      let odb_path = $(this).data("scanmin");
      let link = $(this).children("a");
      let val = $(this).html();
      if (link.length) {
        val = link.html();
      }
      val = parseFloat(val);
      mjsonrpc_db_set_value(odb_path, val);
    });
  };
  
  var odb_select_html = function(block_name, param, values, texts) {
    let odb_path = config.paths.ppg_prog_base + "/" + block_name + "/" + param;
    let options = [];
    for (let i in values) {
      options[values[i]] = texts[i];
    }

    let html = cycling.common.odbselect_html(odb_path, unique_id, options, undefined, "max200px");
    
    unique_id += 1;
    return html;
  };
  
  var start_rename = function(block_name) {
    let prefix = block_name.split("_", 1) + "_";
    let suffix = block_name.replace(prefix, "");
    let form = 'Enter new block name:<br>&nbsp;<br>';
    form += prefix + '<input id="rename" maxlength="22" value="' + suffix + '">';
  
    
    let d = document.createElement("div");
    d.className = "dlgFrame";
    d.style.zIndex = "21";
  
    d.innerHTML = "<div class=\"dlgTitlebar\" id=\"dlgMessageTitle\">Rename PPG block</div>" +
       "<div class=\"dlgPanel\" style=\"padding: 30px;\">" +
       "<div id=\"dlgMessageString\">" + form + "</div>" +
       "<br /><br />" +
       "<button class=\"dlgButton\" id=\"dlgMessageButton\" type=\"button\" " +
       " onClick=\"cycling.ppg.rename_callback(true, '" + block_name + "', '" + prefix + "', this);\">Rename</button>" +
       "<button class=\"dlgButton\" id=\"dlgMessageButton\" type=\"button\" " +
       " onClick=\"cycling.ppg.rename_callback(false, '" + block_name + "', '" + prefix + "',  this);\">Cancel</button>" +
       "</div>";
  
    document.body.appendChild(d);
    
    dlgShow(d, true);
    return d;
  };
  
  var rename_callback = function(ok, block_name, prefix, elem) {
    let rename = $("#rename").val();
  
    dlgMessageDestroy(elem);
    
    if (!ok || rename === null || prefix + rename == block_name || rename == "") {
      return;
    }
    
    let params = Object()
    params.client_name = "PPGCompilerFrontend";
    params.cmd = "rename_block";
    params.args = JSON.stringify({"old_name": block_name, "new_name": prefix + rename});
    
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = cycling.common.parse_rpc_response(rpc.result);
      if (status == 1) {
        location.reload();
      } else {
        alert_rpc_error(status, reply);
      }
    }).catch(function(error) {
      mjsonrpc_error_alert(error);
    });
  };
  
  var start_delete = function(block_name) {
    let r = confirm("Are you sure you want to delete the '" + block_name + "' block?")
    
    if (!r) {
      return;
    }
    
    let params = Object()
    params.client_name = "PPGCompilerFrontend";
    params.cmd = "delete_block";
    params.args = JSON.stringify({"block_name": block_name});
  
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = cycling.common.parse_rpc_response(rpc.result);
      if (status == 1) {
        location.reload();
      } else {
        alert_rpc_error(status, reply);
      }
    }).catch(function(error) {
      mjsonrpc_error_alert(error);
    });
  };
  
  var start_reorder = function(block_name, current_idx) {
    let new_idx = prompt("Block is currently at position " + current_idx + ". Enter new position:", current_idx);
    
    if (new_idx === null) {
      return;
    }
    
    new_idx = parseInt(new_idx);
    
    let params = Object()
    params.client_name = "PPGCompilerFrontend";
    params.cmd = "reorder_block";
    params.args = JSON.stringify({"block_name": block_name, "new_idx": new_idx});
    
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = cycling.common.parse_rpc_response(rpc.result);
      if (status == 1) {
        location.reload();
      } else {
        alert_rpc_error(status, reply);
      }
    }).catch(function(error) {
      mjsonrpc_error_alert(error);
    });
  };
  
  var new_block_expected_prefix = function(block_type) {
    if (block_type == "transition") {
      return "trans_";
    }
    
    if (block_type == "turnon") {
      return "turnon_";
    }
  	  
    if (block_type == "turnoff") {
      return "turnoff_";
    }
  	  
    if (block_type == "pulse") {
      return "pulse_";
    }
    
    if (block_type == "begin") {
      return "begin_";
    }
    
    if (block_type == "delay") {
      return "del_";
    }
    
    return "";
  };
  
  var new_block_type_changed = function() {
    let block_type = $("#new_block_type").val();
  
    if (block_type == "pulse") {
      $("#new_width_div").show();
    } else {
      $("#new_width_div").hide();
    }
    
    if (block_type == "pulse" || block_type == "transition" || block_type == "turnon" || block_type == "turnoff") {
      $("#new_channel_div").show();
    } else {
      $("#new_channel_div").hide();
    }
    
    if (block_type == "begin") {
      $("#new_loop_count_div").show();
    } else {
      $("#new_loop_count_div").hide();
    }
    
    $("#new_timing_div").show();
    
    $("#new_name_prefix").text(new_block_expected_prefix(block_type));
  };
  
  var start_new_block = function(idx) {
    let form = '<table class="dialogTable" style="text-align:left">';
    form += '<tr><td>Block type:</td><td><select id="new_block_type" onchange="cycling.ppg.new_block_type_changed()">';
    form += '<option value="pulse">Pulse</option>';
    form += '<option value="transition">Toggle channel on/off (transition)</option>';
    form += '<option value="turnon">Turn channel on</option>';
    form += '<option value="turnoff">Turn channel off</option>';
    form += '<option value="delay">Delay</option>';
    form += '<option value="begin">Loop</option>';
    form += '</select></td></tr>';
  
    form += '<tr><td>Name:</td><td><span id="new_name_prefix"></span><input id="new_name" maxlength="22"></td>';
    form += '<tr><td id="new_timing_div">Timing:</td><td><input id="new_toff">ms after <select id="new_tref">';
    
    // Get time references from a relevant block
    let rel = idx;
    if (idx < 0) {
      rel = 0;
    }
    if (idx >= all_block_names.length) {
      rel = all_block_names.length - 1;
    }
    let ln = loop_name_of_block[all_block_names[rel]];
    for (let t = 0; t < tref_shortnames[ln].length; t++) {
      form += '<option value="' + tref_shortnames[ln][t] + '">' + tref_texts[ln][t] + '</option>';
    }
    
    if (tref_shortnames[ln].length == 0) {
      form += '<option value="">End of previous block</option>';
    }
    
    form += '</select></td></tr>';
  
    form += '<tr id="new_width_div"><td>Pulse width:</td><td><input id="new_width"> ms</td></tr>';
  
    form += '<tr id="new_channel_div"><td>PPG channel:</td><td><select id="new_channel">';
    
    for (let c = 0; c < ch_names.length; c++) {
      form += '<option value="' + ch_shortnames[c] + '">' + ch_names[c] + '</option>';
    }
    
    form += '</select></td></tr>';
    
  
    form += '<tr id="new_loop_count_div"><td>Loop count:</td><td><input type="number" value="1" id="new_loop_count"></td></tr>';
    
    form += '</table>';
    
    let d = document.createElement("div");
    d.className = "dlgFrame";
    d.style.zIndex = "21";
  
    d.innerHTML = "<div class=\"dlgTitlebar\" id=\"dlgMessageTitle\">Add new PPG block</div>" +
       "<div class=\"dlgPanel\" style=\"padding: 30px;\">" +
       "<div id=\"dlgMessageString\">" + form + "</div>" +
       "<br /><br />" +
       "<button class=\"dlgButton\" id=\"dlgMessageButton\" type=\"button\" " +
       " onClick=\"cycling.ppg.new_block_callback(true, " + idx + ", this);\">Add block</button>" +
       "<button class=\"dlgButton\" id=\"dlgMessageButton\" type=\"button\" " +
       " onClick=\"cycling.ppg.new_block_callback(false, " + idx + ", this);\">Cancel</button>" +
       "</div>";
  
    document.body.appendChild(d);
    new_block_type_changed();
  
    dlgShow(d, true);
    return d;
  };
  
  var new_block_callback = function(ok, new_position, elem) {
    if (!ok) {
      dlgMessageDestroy(elem);
      return;
    }
    
    let block_type = $("#new_block_type").val();
    let name = $("#new_name").val();
    let toff = $("#new_toff").val();
    let tref = $("#new_tref").val();
    let width = $("#new_width").val();
    let chan = $("#new_channel").val();
    let loop_count = $("#new_loop_count").val();
    dlgMessageDestroy(elem);
    
    try {
      toff = parseFloat(toff);
    } catch(err) {
      alert("Invalid time offset - should be a number");
      return;
    }
  
    if (block_type == "pulse") {
      try {
        width = parseFloat(width);
      } catch(err) {
        alert("Invalid pulse width - should be a number");
        return;
      }
      
      if (width <= 0) {
        alert("Invalid pulse width - should be > 0");
        return;
      }
    }
    
    try {
      loop_count = parseInt(loop_count);
    } catch(err) {
      loop_count = 1;
    }
  
    let prefix = new_block_expected_prefix(block_type);
    
    let args = {"block_type": block_type,
                "block_name": prefix + name,
                "toff": toff,
                "tref": tref,
                "position": new_position,
                "loop_count": loop_count
                }
    
    if (block_type == "pulse") {
      args["width"] = width;
    }
    
    if (block_type == "pulse" || block_type == "transition" || block_type == "turnon" || block_type == "turnoff") {
      args["channel"] = chan;
    }
    
    var params = new Object;
    params.client_name = "PPGCompilerFrontend";
    params.cmd = "add_block";
    params.args = JSON.stringify(args);
  
    mjsonrpc_call("jrpc", params).then(function(rpc) {
       let [status, reply] = cycling.common.parse_rpc_response(rpc.result);
       
       if (status == 1 && reply == "OK") {
         location.reload();
       } else {
         alert_rpc_error(status, reply);
       }
    }).catch(function(error) {
       mjsonrpc_error_alert(error);
    });
  };
  
  var alert_rpc_error = function(status, reply) {
    if (status == 103) {
      dlgAlert("The PPG compiler frontend must be running for this functionality to work."); 
    } else {
      dlgAlert("Failed to perform action!<div style='text-align:left'><br>Status code: " + status + "<br>Message: " + reply + "</div>"); 
    }  
  };
  
  var bg_col = function(loop_depth, block_name) {
    let ld = loop_depth;
    if (block_name.toLowerCase().indexOf("begin") == 0 || block_name.toLowerCase().indexOf("end") == 0) {
      ld += 1;
    }
    let cols = ["#EEE", "#CCC", "#AAA", "#888"];
    return cols[ld % cols.length]
  };
  
  var block_html = function(block_name, block, loop_depth, ppg_scan, block_idx) {
    let retval = "";
    
    if (only_channels !== undefined) {
      if (!block.hasOwnProperty("ppg signal name")) {
        return retval;
      }
      if (!should_show_channel(block["ppg signal name"])) {
        return retval;
      }
    }
    
    let lower = block_name.toLowerCase();
  
    let toff_path = config.paths.ppg_prog_base + "/" + block_name + "/time offset (ms)";
    let width_path = config.paths.ppg_prog_base + "/" + block_name + "/pulse width (ms)";
    let scan_toff = null;
    let scan_width = null;
  
    if (ppg_scan["x"].indexOf(toff_path) != -1) {
      scan_toff = ["X", ppg_scan["x"].indexOf(toff_path)];
    }
    if (ppg_scan["y"].indexOf(toff_path) != -1) {
      scan_toff = ["Y", ppg_scan["y"].indexOf(toff_path)];
    }
    if (ppg_scan["x"].indexOf(width_path) != -1) {
      scan_width = ["X", ppg_scan["x"].indexOf(width_path)];
    }
    if (ppg_scan["y"].indexOf(width_path) != -1) {
      scan_width = ["Y", ppg_scan["y"].indexOf(width_path)];
    }
    
    let t_offset = undefined;
    if (block.hasOwnProperty("time offset (ms)")) {
      t_offset = block["time offset (ms)"]
    }
    
    let t_ref = undefined;
    if (block.hasOwnProperty("time reference")) {
      t_ref = block["time reference"]
    }
    
    retval += "<tr style='background-color:" + bg_col(loop_depth, block_name) + "'>";
    if (!hide_block_number) {
      retval += "<td>" + block_idx + "</td>";
    }
    retval += "<td><i>" + block_name + "</i></td>" ;
    retval += '<td>';
    
    if (t_offset === undefined) {
      retval += "";
    } else if (scan_toff !== null) {
      retval += "(Scanned: " + odb_scan_ppg_html(scan_toff[0], scan_toff[1], toff_path) + "ms) <br>(currently: " + odb_ppg_span_html(block_name, "time offset (ms)", "f", false) + "ms)<br>";
    } else {
      retval += odb_ppg_span_html(block_name, "time offset (ms)", "f") + "ms<br>";
    }
    
    retval += "after ";
    
    if (t_ref === undefined) {
      retval += "end of previous block"; 
    } else {
      let ln = loop_name_of_block[block_name.toLowerCase()];
      retval += odb_select_html(block_name, "time reference", tref_shortnames[ln], tref_texts[ln]); 
    }
    
    retval += "</td><td>";
  
    if (lower.indexOf("trans") == 0) {
      retval += "toggle " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
    } else if (lower.indexOf("turnon") == 0) {
      retval += "turn on " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
    } else if (lower.indexOf("turnoff") == 0) {
      retval += "turn off " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
    } else if (lower.indexOf("pulse") == 0) {
      retval += "pulse " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
      if (scan_width === null) {
        retval += " for<br>" + odb_ppg_span_html(block_name, "pulse width (ms)", "f") + "ms";
      } else {
        retval += " for<br>(scanned: " + odb_scan_ppg_html(scan_width[0], scan_width[1], width_path) + "ms) <br>(currently: " + odb_ppg_span_html(block_name, "pulse width (ms)", "f", false) + "ms)";
      }
    } else if (lower.indexOf("stdpulse") == 0) {
      retval += "turn on " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
      retval += '<br>for standard time (<span class="modbvalue" data-odb-path="/Equipment/Titan_acq/Settings/ppg/input/standard pulse width (ms)"></span>ms)';
    } else if (lower.indexOf("pat") == 0) {
      retval += "set bit pattern to " + odb_ppg_span_html(block_name, "bit pattern");
    } else if (lower.indexOf("del") == 0) {
      retval += "do nothing (sleep/delay)"
    } else if (lower.indexOf("tim") == 0) {
      retval += "Documentation was missing for this...";
    } else if (lower.indexOf("begin") == 0) {
      retval += "start looping " + odb_ppg_span_html(block_name, "loop count") + " times";
    } else if (lower.indexOf("end") == 0) {
      retval += "end the loop"
    } else {
      retval += "Unhandled block type";
    }
    
    retval += "</td><td>";
    
    if (lower.indexOf("end") != 0) {
      // end-loop deleted/renamed automatically when begin-loop changed.
      retval += "<button role='button' class='mbutton disable_running' style='font-size:smaller' onclick='cycling.ppg.start_rename(\"" + block_name + "\")'>Rename</button>";
      retval += "<button role='button' class='mbutton disable_running' style='font-size:smaller' onclick='cycling.ppg.start_delete(\"" + block_name + "\")'>Delete</button>";
      retval += "<br>";
    }
  
    if (!hide_move_button) {
      retval += "<button role='button' class='mbutton disable_running' style='font-size:smaller' onclick='cycling.ppg.start_reorder(\"" + block_name + "\", " + block_idx + ")'>Move</button>";
    }
  
    retval += "<button role='button' class='mbutton disable_running' style='font-size:smaller' onclick='cycling.ppg.start_new_block(" + block_idx + ")'>Add above</button>";
    retval += "<button role='button' class='mbutton disable_running' style='font-size:smaller' onclick='cycling.ppg.start_new_block(" + (block_idx + 1) + ")'>Add below</button>";
    
    retval += "</td></tr>";
    
    return retval;
  };
  
  var new_plot = function() {
    $("#newplot").attr("disabled", true);
    var params = new Object;
    params.client_name = "PPGCompilerFrontend";
    params.cmd = "compile_now";
    params.args = '{"with_image": true}';
  
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = cycling.common.parse_rpc_response(rpc.result);
  
      if (status == 1) {
         setTimeout(reload_image, 100);
         setTimeout(reload_image, 1000);
         setTimeout(reload_image, 2000);
       } else {
         alert_rpc_error(status, reply);
       }
       $("#newplot").attr("disabled", false);
    }).catch(function(error) {
       mjsonrpc_error_alert(error);
       $("#newplot").attr("disabled", false);
    });
  };
  
  var reload_image = function() {
    $("#plot").attr("src", "ppgplot.png?" + new Date().getTime());
    $("#expanded_plot").attr("src", "expanded_ppgplot.png?" + new Date().getTime());
  };
  
  return {
    init: init,
    new_plot: new_plot,
    new_block_type_changed: new_block_type_changed,
    scan_min: scan_min,
    start_delete: start_delete,
    start_new_block: start_new_block,
    start_rename: start_rename,
    start_reorder: start_reorder,
    rename_callback: rename_callback,
    new_block_callback: new_block_callback
  };
})();