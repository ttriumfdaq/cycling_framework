var cycling = cycling || {};

cycling.scan = (function() {
  var has_ppg = false;
  var has_camp = false;
  var has_epics = false;
  var has_softdac = false;
  
  var show_ppg_unused = false;
  var show_camp_unused = false;
  var show_epics_unused = false;
  var show_softdac_unused = false;
  var ppg_set = {'X': [], 'Y': []};
  var camp_set = {'X': [], 'Y': []};
  var epics_set = {'X': [], 'Y': []};
  var softdac_set = {'X': [], 'Y': []};
  
  var init = function() {
    cycling.common.create_run_state_div("#runstate");
    
    mjsonrpc_db_get_values([config.paths.scanning_base, config.paths.ppg_prog_base]).then(function(rpc) {
      handle_scan_settings(rpc.result.data[0], rpc.result.data[1]);
      mhttpd_init('Scan settings');
    });
  };
  
  var x_sweep_changed = function() {
    mjsonrpc_db_set_value(config.paths.scanning_base + "/Global/Settings/X type", $("#x_sweep_style").val());
  };
  
  var y_sweep_changed = function() {
    mjsonrpc_db_set_value(config.paths.scanning_base + "/Global/Settings/Y type", $("#y_sweep_style").val());
  };
  
  var update_sweep_from_odb = function(xy) {
    let odb_val = $("#" + xy + "_sweep_style_span").html();
    let sel_obj_id = "#" + xy + "_sweep_style";
    $(sel_obj_id).val(odb_val);
  };
  
  var scan_type_changed = function() {
    let sel_val = $("#scan_type").val();
    let enable_x = (sel_val != "none");
    let enable_y = (sel_val == "2d");
    mjsonrpc_db_set_value(config.paths.scanning_base + "/Global/Settings/Enable X", enable_x);
    mjsonrpc_db_set_value(config.paths.scanning_base + "/Global/Settings/Enable Y", enable_y);
  };
  
  var update_enabled_from_odb = function() {
    let enable_x = $("#enable_x_span").html() == "y";
    let enable_y = $("#enable_y_span").html() == "y";
    
    if (enable_x && enable_y) {
      $("#scan_type").val("2d");
    } else if (enable_x) {
      $("#scan_type").val("1d");
    } else {
      $("#scan_type").val("none");
    }
    
    if (enable_x) {
      $(".scan_x").show();
    } else {
      $(".scan_x").hide();
    }
    
    if (enable_y) {
      $(".scan_y").show();
    } else {
      $(".scan_y").hide();
    }
    
    if (!has_ppg) {
      $(".ppg").hide();
    }
  
    if (!has_epics) {
      $(".epics").hide();
    }
  };
  
  var set_show_ppg_unused = function() {
    show_ppg_unused = true;
    $(".ppg_row").show();
    $(".show_unused_ppg").hide();
  };
  
  var ppg_path_changed = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_val = $("#path" + id_sfx).html();
    let bits;
    
    if (odb_val == "" || odb_val == "(empty)") {
      bits = ["", ""];
    } else {
      let stripped = odb_val.replace(config.paths.ppg_prog_base + "/", "");
      bits = stripped.split("/");  
    }
    
    if (bits.length != 2) {
      console.log("Failed to understand path " + odb_val);
    } else {
      $("#block" + id_sfx).val(bits[0]);
      $("#param" + id_sfx).val(bits[1]);
      
      
      if (bits[0] == "" && !show_ppg_unused) {
        $("#ppg_row" + id_sfx).hide();
        if (id_sfx in ppg_set[xy]) {
          delete ppg_set[xy][id_sfx];
        }
      } else {
        $("#ppg_row" + id_sfx).show();
        ppg_set[xy][id_sfx] = true;
      }
    }
    
    $("#ppg_count_" + xy).text($(".ppg_row_" + xy).length - $(".ppg_sel_" + xy).filter("[value=]").length);
  };
  
  var update_ppg_path = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_path = config.paths.scanning_ppg_base + "/" + xy + " paths[" + i + "]";
    
    let full_path = "";
    let blockname = $("#block" + id_sfx).val()
    
    if (blockname.length) {
      full_path = config.paths.ppg_prog_base + "/" + blockname + "/" + $("#param" + id_sfx).val();
    }
    
    mjsonrpc_db_set_value(odb_path, full_path);
  };
  
  var html_ppg_head = function(xy) {
    let html = '';
    html += '<table class="mtable scan_' + xy + ' ppg">';
    html += '<thead>';
    html += '  <tr>';
    html += '    <th colspan="5" class="mtableheader">PPG scanning - ' + xy.toUpperCase() + ' variables</th>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <td colspan="5" class="show_unused_ppg">';
    html += '      Showing the <span id="ppg_count_' + xy.toUpperCase() + '"></span> scanned parameters. ';
    html += '      <button class="mbutton" onclick="cycling.scan.set_show_ppg_unused()">Click to add more params</button>';
    html += '    </td>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <th>ID</th>';
    html += '    <th>PPG block</th>';
    html += '    <th>Parameter</th>';
    html += '    <th>Start value</th>';
    html += '    <th>End value</th>';
    html += '  </tr>';
    html += '</thead>';
    html += '<tbody id="ppg_scan_' + xy + '">';
    return html;
  };
  
  var html_ppg_row = function(xy, i, scan_ppg, blocklist) {
    let id_sfx = "_" + xy + "_" + i;
    let html = '<tr class="ppg_row ppg_row_' + xy + '" id="ppg_row' + id_sfx + '">';
    
    // ID
    html += "<th>" + xy + " [" + i + "]</th>";
    
    // PPG block
    html += '<td>';
    html += '<span class="modbvalue" id="path' + id_sfx + '" style="display:none" data-odb-path="' + config.paths.scanning_ppg_base + "/" + xy + ' paths[' + i + ']" onchange="cycling.scan.ppg_path_changed(\'' + xy + '\', ' + i + ')"></span>';
    html += '<select class="ppg_sel_' + xy + ' disable_running" id="block' + id_sfx + '" onchange="cycling.scan.update_ppg_path(\'' + xy + '\', ' + i + ')">';
    html += '<option value="">N/A</option>';
    for (let b = 0; b < blocklist.length; b++) {
      html += '<option value="' + blocklist[b] + '">' + blocklist[b] + '</option>';
    }
    html += '</select>';
    html += '</td>';
    
    // Parameter
    html += '<td>';
    html += '<select class="disable_running" id="param' + id_sfx + '" onchange="cycling.scan.update_ppg_path(\'' + xy + '\', ' + i + ')">';
    html += '  <option value="time offset (ms)">Time offset (ms)</option>';
    html += '  <option value="pulse width (ms)">Pulse width (ms)</option>';
    html += '</select>';
    html += '</td>';
    
    // Start
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_ppg_base + "/" + xy + ' start[' + i + ']"></span>';
    
    // End
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_ppg_base + "/" + xy + ' end[' + i + ']"></span>';
    
    html += "</tr>";
    return html;
  };
  
  var html_ppg_tail = function() {
    return "</tbody></table>";
  };
    
  var set_show_epics_unused = function() {
    show_epics_unused = true;
    $(".epics_row").show();
    $(".show_unused_epics").hide();
  };
  
  var epics_device_changed = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_val = parseInt($("#ep_dev" + id_sfx).html());
  
    $("#ep_devlist" + id_sfx).val(odb_val);
    
    if (odb_val == -1 && !show_epics_unused) {
      $("#epics_row" + id_sfx).hide();
      if (id_sfx in epics_set[xy]) {
        delete epics_set[xy][id_sfx];
      }
    } else {
      $("#epics_row" + id_sfx).show();
      epics_set[xy][id_sfx] = true;
    }
    
    $("#epics_count_" + xy).text(Object.keys(epics_set[xy]).length);
  };
  
  var update_epics_device = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_path = config.paths.scanning_epics_base + "/" + xy + " devices[" + i + "]";
    let val = $("#ep_devlist" + id_sfx).val();
    mjsonrpc_db_set_value(odb_path, val);
  };
  
  var html_epics_head = function(xy) {
    let html = "";
    html += '<table class="mtable scan_' + xy + ' epics">';
    html += '<thead>';
    html += '  <tr>';
    html += '    <th colspan="4" class="mtableheader">EPICS scanning - ' + xy.toUpperCase() + ' variables</th>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <td colspan="4" class="show_unused_epics">';
    html += '      Showing the <span id="epics_count_' + xy.toUpperCase() + '"></span> scanned parameters. ';
    html += '      <button class="mbutton" onclick="cycling.scan.set_show_epics_unused()">Click to add more params</button>';
    html += '    </td>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <th>ID</th>';
    html += '    <th>EPICS device</th>';
    html += '    <th>Start value</th>';
    html += '    <th>End value</th>';
    html += '  </tr>';
    html += '</thead>';
    html += '<tbody id="epics_scan_' + xy + '">';
    return html;
  };
  
  var html_epics_row = function(xy, i, scan_epics, devices) {
    let id_sfx = "_" + xy + "_" + i;
    let html = '<tr class="epics_row epics_row_' + xy + '" id="epics_row' + id_sfx + '">';
  
    // ID
    html += "<th>" + xy + " [" + i + "]</th>";
  
    // Device
    html += '<td>';
    html += '<span class="modbvalue" id="ep_dev' + id_sfx + '" style="display:none" data-odb-path="' + config.paths.scanning_epics_base + "/" + xy + ' devices[' + i + ']" onchange="cycling.scan.epics_device_changed(\'' + xy + '\', ' + i + ')"></span>';
    html += '<select class="epics_sel_' + xy + ' disable_running" id="ep_devlist' + id_sfx + '" onchange="cycling.scan.update_epics_device(\'' + xy + '\', ' + i + ')">';
    html += '<option value="-1">N/A</option>';
    for (let b = 0; b < devices.length; b++) {
      html += '<option value="' + devices[b][0] + '">' + devices[b][1] + '</option>';
    }
    html += '</select>';
    html += '</td>';
    
    // Start
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_epics_base + "/" + xy + ' start[' + i + ']"></span>';
    
    // End
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_epics_base + "/" + xy + ' end[' + i + ']"></span>';
    
    html += "</tr>";
    return html;
  };
  
  var html_epics_tail = function() {
    return "</tbody></table>";
  };
  
  var set_show_camp_unused = function() {
    show_camp_unused = true;
    $(".camp_row").show();
    $(".show_unused_camp").hide();
  };
  
  var camp_device_changed = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_val = parseInt($("#camp_dev" + id_sfx).html());
  
    $("#camp_devlist" + id_sfx).val(odb_val);
    
    if (odb_val == -1 && !show_camp_unused) {
      $("#camp_row" + id_sfx).hide();
      if (id_sfx in camp_set[xy]) {
        delete camp_set[xy][id_sfx];
      }
    } else {
      $("#camp_row" + id_sfx).show();
      camp_set[xy][id_sfx] = true;
    }
    
    $("#camp_count_" + xy).text(Object.keys(camp_set[xy]).length);
  };
  
  var update_camp_device = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_path = config.paths.scanning_camp_base + "/" + xy + " device index[" + i + "]";
    let val = $("#camp_devlist" + id_sfx).val();
    mjsonrpc_db_set_value(odb_path, val);
  };
  
  var html_camp_head = function(xy) {
    let html = "";
    html += '<table class="mtable scan_' + xy + ' camp">';
    html += '<thead>';
    html += '  <tr>';
    html += '    <th colspan="4" class="mtableheader">CAMP scanning - ' + xy.toUpperCase() + ' variables</th>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <td colspan="4" class="show_unused_camp">';
    html += '      Showing the <span id="epics_count_' + xy.toUpperCase() + '"></span> scanned parameters. ';
    html += '      <button class="mbutton" onclick="cycling.scan.set_show_camp_unused()">Click to add more params</button>';
    html += '    </td>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <th>ID</th>';
    html += '    <th>Setpoint path</th>';
    html += '    <th>Start value</th>';
    html += '    <th>End value</th>';
    html += '  </tr>';
    html += '</thead>';
    html += '<tbody id="epics_scan_' + xy + '">';
    return html;
  };
  
  var html_camp_row = function(xy, i, scan_camp, devices) {
    let id_sfx = "_" + xy + "_" + i;
    let html = '<tr class="camp_row camp_row_' + xy + '" id="camp_row' + id_sfx + '">';
  
    // ID
    html += "<th>" + xy + " [" + i + "]</th>";
  
    // Device
    html += '<td>';
    html += '<span class="modbvalue" id="camp_dev' + id_sfx + '" style="display:none" data-odb-path="' + config.paths.scanning_camp_base + "/" + xy + ' device index[' + i + ']" onchange="cycling.scan.camp_device_changed(\'' + xy + '\', ' + i + ')"></span>';
    html += '<select class="camp_sel_' + xy + ' disable_running" id="camp_devlist' + id_sfx + '" onchange="cycling.scan.update_camp_device(\'' + xy + '\', ' + i + ')">';
    html += '<option value="-1">N/A</option>';
    for (let b = 0; b < devices.length; b++) {
      html += '<option value="' + devices[b][0] + '">' + devices[b][1] + '</option>';
    }
    html += '</select>';
    html += '</td>';
    
    // Start
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_camp_base + "/" + xy + ' start[' + i + ']"></span>';
    
    // End
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_camp_base + "/" + xy + ' end[' + i + ']"></span>';
    
    html += "</tr>";
    return html;
  };
  
  var html_camp_tail = function() {
    return "</tbody></table>";
  };
  
  
  var set_show_softdac_unused = function() {
    show_softdac_unused = true;
    $(".softdac_row").show();
    $(".show_unused_softdac").hide();
  };
  
  var softdac_pulse_name_changed = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_val = $("#soft_pn" + id_sfx).html();
  
    if (odb_val == "(empty)") {
      odb_val = "";
    }
    
    $("#soft_pnlist" + id_sfx).val(odb_val);
    
    if (odb_val == "" && !show_softdac_unused) {
      $("#softdac_row" + id_sfx).hide();
      if (id_sfx in softdac_set[xy]) {
        delete softdac_set[xy][id_sfx];
      }
    } else {
      $("#softdac_row" + id_sfx).show();
      softdac_set[xy][id_sfx] = true;
    }
    
    $("#softdac_count_" + xy).text(Object.keys(softdac_set[xy]).length);
  };
  
  var update_softdac_pulse_name = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_path = config.paths.scanning_softdac_base + "/" + xy + " blocks[" + i + "]";
    let val = $("#soft_pnlist" + id_sfx).val();
    mjsonrpc_db_set_value(odb_path, val);
  };
  
  var softdac_channel_changed = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_val = parseInt($("#soft_cn" + id_sfx).html());
    $("#soft_cnlist" + id_sfx).val(odb_val);
  };
  
  var update_softdac_channel = function(xy, i) {
    let id_sfx = "_" + xy + "_" + i;
    let odb_path = config.paths.scanning_softdac_base + "/" + xy + " chan index[" + i + "]";
    let val = $("#soft_cnlist" + id_sfx).val();
    mjsonrpc_db_set_value(odb_path, val);
  };
  
  var html_softdac_head = function(xy) {
    let html = "";
    html += '<table class="mtable scan_' + xy + ' softdac">';
    html += '<thead>';
    html += '  <tr>';
    html += '    <th colspan="5" class="mtableheader">Trap voltage scanning - ' + xy.toUpperCase() + ' variables</th>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <td colspan="5" class="show_unused_softdac">';
    html += '      Showing the <span id="softdac_count_' + xy.toUpperCase() + '"></span> scanned parameters. ';
    html += '      <button class="mbutton" onclick="cycling.scan.set_show_softdac_unused()">Click to add more params</button>';
    html += '    </td>';
    html += '  </tr>';
    html += '  <tr>';
    html += '    <th>ID</th>';
    html += '    <th>Pulse</th>';
    html += '    <th>Channel</th>';
    html += '    <th>Start value</th>';
    html += '    <th>End value</th>';
    html += '  </tr>';
    html += '</thead>';
    html += '<tbody id="softdac_scan_' + xy + '">';
    return html;
  };
  
  var html_softdac_row = function(xy, i, scan_softdac, pulse_names, channel_names) {
    let id_sfx = "_" + xy + "_" + i;
    let html = '<tr class="softdac_row softdac_row_' + xy + '" id="softdac_row' + id_sfx + '">';
  
    // ID
    html += "<th>" + xy + " [" + i + "]</th>";
  
    // Pulse
    html += '<td>';
    html += '<span class="modbvalue" id="soft_pn' + id_sfx + '" style="display:none" data-odb-path="' + config.paths.scanning_softdac_base + "/" + xy + ' blocks[' + i + ']" onchange="cycling.scan.softdac_pulse_name_changed(\'' + xy + '\', ' + i + ')"></span>';
    html += '<select class="softdac_sel_' + xy + ' disable_running" id="soft_pnlist' + id_sfx + '" onchange="cycling.scan.update_softdac_pulse_name(\'' + xy + '\', ' + i + ')">';
    html += '<option value="">N/A</option>';
    for (let b = 0; b < pulse_names.length; b++) {
      if (pulse_names[b].length) {
        html += '<option value="' + pulse_names[b] + '">' + pulse_names[b].replace("pulse_", "").replace("Pulse_", "") + '</option>';
      }
    }
    html += '</select>';
    html += '</td>';
    
    // Channel
    html += '<td>';
    html += '<span class="modbvalue" id="soft_cn' + id_sfx + '" style="display:none" data-odb-path="' + config.paths.scanning_softdac_base + "/" + xy + ' chan index[' + i + ']" onchange="cycling.scan.softdac_channel_changed(\'' + xy + '\', ' + i + ')"></span>';
    html += '<select class="softdac_sel_' + xy + ' disable_running" id="soft_cnlist' + id_sfx + '" onchange="cycling.scan.update_softdac_channel(\'' + xy + '\', ' + i + ')">';
    for (let b = 0; b < channel_names.length; b++) {
      if (channel_names[b].length) {
        html += '<option value="' + b + '">' + channel_names[b] + '</option>';
      }
    }
    html += '</select>';
    html += '</td>';
    
    // Start
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_softdac_base + "/" + xy + ' start[' + i + ']"></span>';
    
    // End
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.scanning_softdac_base + "/" + xy + ' end[' + i + ']"></span>';
    
    html += "</tr>";
    return html;
  };
  
  var html_softdac_tail = function() {
    return "</tbody></table>";
  };
  
  var sort_epics_definitions = function(defs) {
    // Figure out the order we should display in.
    // We want to order alphabetically by device name,
    // but need to know the indexes in the original arrays.
    let devices = [];
    let devices_and_indices = [];
    
    for (let i = 0; i < defs["demand device"].length; i++) {
      if (defs["demand device"][i].length) {
        devices_and_indices.push([defs["demand device"][i], i]);
      }
    }
    
    devices_and_indices.sort(function(a, b) {return a[0] < b[0] ? -1 : 1;});
    
    let display_order = [];
    
    for (let i = 0; i < devices_and_indices.length; i++) {
      display_order.push(devices_and_indices[i][1]);
    }
    
    for (let order = 0; order < display_order.length; order++) {
      let i = display_order[order];
      let shown = "";
      
      if (defs["demand device"][i].length) {
        if (defs["human name"][i].length) {
          shown = defs["demand device"][i] + " (" + defs["human name"][i] + ")";
        } else {
          shown = defs["demand device"][i];
        }
      }
      devices.push([i, shown]);
    }
    
    return devices;
  };
  
  var sort_camp_definitions = function(defs) {
    // Figure out the order we should display in.
    // We want to order alphabetically by setpoint path
    // but need to know the indexes in the original arrays.
    let devices = [];
    let devices_and_indices = [];
    
    for (let i = 0; i < defs["setpoint path"].length; i++) {
      if (defs["setpoint path"][i].length) {
        devices_and_indices.push([defs["setpoint path"][i], i]);
      }
    }
    
    devices_and_indices.sort(function(a, b) {return a[0] < b[0] ? -1 : 1;});
    
    let display_order = [];
    
    for (let i = 0; i < devices_and_indices.length; i++) {
      display_order.push(devices_and_indices[i][1]);
    }
    
    for (let order = 0; order < display_order.length; order++) {
      let i = display_order[order];
      devices.push([i, defs["setpoint path"][i]]);
    }
    
    return devices;
  };
  
  var handle_scan_settings = function(scan, ppg_blocks) {
    if (scan === null) {
      $("#scan_tables").text("No scan parameters found; run the frontend to initialize ODB.");
      return;
    }
    
    let softdac_pulse_names = [];
    let html = "";
    
    if (scan.hasOwnProperty("ppg")) {
      has_ppg = true;
      
      let blocklist = [];
      let keys = Object.keys(ppg_blocks);
      
      for (let k = 0; k < keys.length; k++) {
        if (keys[k].indexOf("/") == -1 && keys[k] != "names") {
          blocklist.push(ppg_blocks[keys[k] + "/name"]);
          
          if (keys[k].startsWith("pulse") && config.softdac && config.softdac.ppg_channel) {
            if (ppg_blocks[keys[k]].hasOwnProperty("ppg signal name")) {
              let channel = ppg_blocks[keys[k]]["ppg signal name"];
              if (channel == config.softdac.ppg_channel) {
                softdac_pulse_names.push(keys[k])
              }
            }
          } 
        }
      }
      
      let nvars = 20;
      html += html_ppg_head('x');
          
      for (let i = 0; i < nvars; i++) {
        html += html_ppg_row('X', i, scan["ppg"]["settings"], blocklist);
      }
      
      html += html_ppg_tail();
      html += html_ppg_head('y');
      
      for (let i = 0; i < nvars; i++) {
        html += html_ppg_row('Y', i, scan["ppg"]["settings"], blocklist);
      }
  
      html += html_ppg_tail();
    }
  
    if (scan.hasOwnProperty("camp")) {
      has_epics = true;
      let devices = sort_camp_definitions(scan["camp"]["definitions"]);
      
      let nvars = 10;
      html += html_camp_head('x');
      
      for (let i = 0; i < nvars; i++) {
        html += html_camp_row('X', i, scan["camp"]["settings"], devices);
      }
      
      html += html_camp_tail();
      html += html_camp_head('y');
      
      for (let i = 0; i < nvars; i++) {
        html += html_camp_row('Y', i, scan["camp"]["settings"], devices);
      }
  
      html += html_camp_tail();
    }
    
    if (scan.hasOwnProperty("epics")) {
      has_epics = true;
      let devices = sort_epics_definitions(scan["epics"]["definitions"]);
      
      let nvars = 20;
      html += html_epics_head('x');
      
      for (let i = 0; i < nvars; i++) {
        html += html_epics_row('X', i, scan["epics"]["settings"], devices);
      }
      
      html += html_epics_tail();
      html += html_epics_head('y');
      
      for (let i = 0; i < nvars; i++) {
        html += html_epics_row('Y', i, scan["epics"]["settings"], devices);
      }
  
      html += html_epics_tail();
    }
    
    if (scan.hasOwnProperty("softdac")) {
      has_softdac = true;
      let channel_names = scan["softdac"]["settings"]["channel names"];
      
      let nvars = 24;
      html += html_softdac_head('x');
      
      for (let i = 0; i < nvars; i++) {
        html += html_softdac_row('X', i, scan["softdac"]["settings"], softdac_pulse_names, channel_names);
      }
      
      html += html_softdac_tail();
      html += html_softdac_head('y');
      
      for (let i = 0; i < nvars; i++) {
        html += html_softdac_row('Y', i, scan["softdac"]["settings"], softdac_pulse_names, channel_names);
      }
  
      html += html_softdac_tail();
    }
    
    $("#scan_tables").html(html);
  };
  
  return {
    init: init,
    x_sweep_changed: x_sweep_changed,
    y_sweep_changed: y_sweep_changed,
    update_sweep_from_odb: update_sweep_from_odb,
    scan_type_changed: scan_type_changed,
    update_enabled_from_odb: update_enabled_from_odb,
    set_show_ppg_unused: set_show_ppg_unused,
    ppg_path_changed: ppg_path_changed,
    update_ppg_path: update_ppg_path,
    set_show_epics_unused: set_show_epics_unused,
    epics_device_changed: epics_device_changed,
    update_epics_device: update_epics_device,
    set_show_camp_unused: set_show_camp_unused,
    camp_device_changed: camp_device_changed,
    update_camp_device: update_camp_device,
    set_show_softdac_unused: set_show_softdac_unused,
    softdac_pulse_name_changed: softdac_pulse_name_changed,
    update_softdac_pulse_name: update_softdac_pulse_name,
    softdac_channel_changed: softdac_channel_changed,
    update_softdac_channel: update_softdac_channel,
    sort_epics_definitions: sort_epics_definitions,
    sort_camp_definitions: sort_camp_definitions
  };
})();