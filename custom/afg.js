var cycling = cycling || {};

cycling.afg = (function() {
  var devs = [];
  var afg_ppg_loop_name;
  
  var init = function() {
    cycling.common.create_run_state_div("#runstate");
    
    mjsonrpc_db_get_values([config.paths.afg_sett_base]).then(function(rpc_sett) {
      handle_afg_settings(rpc_sett.result.data[0]);
      mhttpd_init('AFG frequencies');
    });
  };

  var handle_afg_settings = function(sett) {
    afg_ppg_loop_name = sett["ppg loop name"];
    $("#afg_ppg_loop_name").text(afg_ppg_loop_name);
    
    devs = [];
    let cal = sett["Calibration"];
    
    for (let k in sett) {
      if (typeof sett[k] === 'object' && sett[k] != null && k.toLowerCase() != "calibration") {
        devs.push(sett[k + "/name"]);
      }
    }

    let html = html_afg_table(devs);
    $("#afg_body").html(html);
  };
  
  var alert_rpc_error = function(status, reply) {
    if (status == 103) {
      dlgAlert("The AFG frontend must be running for this functionality to work."); 
    } else {
      dlgAlert("Failed to perform action!<div style='text-align:left'><br>Status code: " + status + "<br>Message: " + reply + "</div>"); 
    }  
  };

  var recompute = function() {
    let params = Object()
    params.client_name = "AFGFrontend";
    params.cmd = "compute_all";
    params.args = "";
    
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = parse_rpc_response(rpc.result);
      if (status == 1) {
        update_computed(JSON.parse(reply));
      } else {
        alert_rpc_error(status, reply);
      }
    }).catch(function(error) {
      mjsonrpc_error_alert(error);
    });
  };

  var update_computed = function(reply) {
    for (let k in reply) {
      let f = reply[k]["freq_list"];
      
      let html = "<ol>";
      
      for (let i in f) {
        html += "<li>" + f[i] + "</li>\n";
      }
      
      html += "</ol>";

      $("#computed_t_" + k).html(reply[k]["exc_time"]);
      $("#computed_v_" + k).html(reply[k]["voltage"]);
      $("#computed_f_" + k).html(html);
    }
  };

  var html_afg_table = function(dev_list) {
    let html = "";
    
    html += '<tr><td>Species</td>';
    html += '<td colspan="' + dev_list.length + '"><span class="modbvalue" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/Species"></span></td>';
    html += '</tr>';
      
    html += '<tr><td>Charge</td>';
    html += '<td colspan="' + dev_list.length + '"><span class="modbvalue" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/Charge"></span></td>';
    html += '</tr>';

    html += '<tr><td>Number of frequency points</td>';
    html += '<td colspan="' + dev_list.length + '"><span class="modbvalue" data-odb-editable="1" data-odb-path="' + config.paths.ppg_prog_base + '/' + afg_ppg_loop_name + '/loop count"></span></td>';
    html += '</tr>';

    
    html += "<tr><th></th>";
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<th>' + dev + '</th>';
    }
    html += "</tr>";
    
    html += '<tr><td>Enabled?</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td><input id="enabled_' + dev + '" type="checkbox" class="modbcheckbox disable_running" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/' + dev + '/Enabled" /></td>';
    }
    html += '</tr>';
    
    
    // TODO - support a ; list here!?!?!?! Maybe in a "pro" mode exposing the raw strings? 
    // Just make sure selection boxes have a "pro mode" option? 
    
    html += '<tr><td>Frequency list type</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      let odb_path_list = config.paths.afg_sett_base + '/' + dev + '/Use custom frequency list';
      let list_options = {"n": "Computed",
                          "y": "User-specified"};
      let callback = "cycling.afg.toggle_list_visible('" + dev + "')";
      
      html += '<td style="max-width:200px">';
      html += cycling.common.odbselect_html(odb_path_list, list_option, dev + "_list", callback);
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>User-specified frequencies (Hz)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td><span class="modbvalue l_custom_' + dev + '" id="list_' + dev + '" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/' + dev + '/Custom frequencies"></span><span class="l_auto_' + dev + '"></span></td>';
    }
    html += '</tr>';
      
    html += '<tr><td>Centre frequency type</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      let odb_path_freq = config.paths.afg_sett_base + '/' + dev + '/Centre frequency';
      html += '<td>';
      html += '<select class="l_auto_' + dev + '" id="sel_' + dev + '_freq" style="max-width:200px" class="disable_running" onchange="update_odb_from_sel_freq(\'' + odb_path_freq + '\', \'' + dev + '\')">';
      html += '<option value="freqc">Cyclotron frequencies</option>';
      html += '<option value="freqp">Dipole frequencies</option>';
      html += '<option value="custom">Custom centre frequencies</option>';
      html += '<option value="pro">Mixture</option>';
      html += '</select>';
      html += '<span style="display:none" class="modbvalue" data-odb-path="' + odb_path_freq + '" id="sel_span_' + dev + '_freq" onchange="update_sel_from_odb_freq(\'' + dev + '\')"></span>';
      html += '<span class="l_auto_' + dev + '"><span style="display:none" id="pro_freq_' + dev + '"><br>List like freqc,freqp,custom: <span class="modbvalue" data-odb-editable="1" data-odb-path="' + odb_path_freq + '"></span></span></span>';
      html += '<span class="l_custom_' + dev + '"></span>';
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>Custom centre frequencies (Hz)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td>';
      html += '<span class="l_auto_' + dev + '"><span class="modbvalue f_custom_' + dev + '" id="custom_f_' + dev + '" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/' + dev + '/Custom centre frequency"></span></span>';
      html += '<span class="f_auto_' + dev + '"><span class="l_custom_' + dev + '"></span></span>';
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>Frequency deviations (Hz)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td>';
      html += '<span class="modbvalue l_auto_' + dev + '" id="f_deviation_' + dev + '" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/' + dev + '/Frequency deviation"></span>';
      html += '<span class="l_custom_' + dev + '"></span>';
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>RF amplitude type</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      let odb_path_volt = config.paths.afg_sett_base + '/' + dev + '/Use custom amplitude';
      html += '<td style="max-width:200px">';

      if (config.afg.uses_calibration.indexOf(dev) > -1) {
        let volt_options = {"n": "Automatic (from calibration)",
                            "y": "Custom"};
        let callback = "cycling.afg.toggle_volt_visible('" + dev + "')";
        
        html += cycling.common.odbselect_html(odb_path_list, list_option, dev + "_volt", callback);
      } else {
        html += "Custom";
      }
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>Custom RF amplitude (V)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td>';
      html += '<span class="modbvalue v_custom_' + dev + '" id="custom_v_' + dev + '" data-odb-editable="1" data-odb-path="' + config.paths.afg_sett_base + '/' + dev + '/Custom amplitude (V)"></span>';
      if (config.afg.uses_calibration.indexOf(dev) > -1) {
        html += '<span class="v_auto_' + dev + '"></span>';
      }
      html += '</td>';
    }
    html += '</tr>';

    
    html += '<tr><td colspan="' + (dev_list.length + 1) + '" style="text-align:center">';
    html += '<input class="mbutton disable_running" id="recompute" value="Recompute now" type="button" onclick="recompute()">';
    html += '</td></tr>';
    
    html += '<tr><td>Computed excitation time (ms)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td id="computed_t_' + dev + '">';
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>Computed voltages (V)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td id="computed_v_' + dev + '">';
      html += '</td>';
    }
    html += '</tr>';

    html += '<tr><td>Computed frequencies (Hz)</td>';
    for (let d in dev_list) {
      let dev = dev_list[d];
      html += '<td id="computed_f_' + dev + '">';
      html += '</td>';
    }
    html += '</tr>';

    
    return html;
  };

  var update_sel_from_odb_freq = function(dev) {
    let sel_val = update_sel_from_odb(dev, "_freq");
    
    if (sel_val.indexOf(";") != -1 || sel_val.indexOf(",") != -1) {
      let sel_obj_id = "#sel_" + dev + "_freq";
      $(sel_obj_id).val("pro");
      $("#pro_freq_" + dev).show();
    } else {
      $("#pro_freq_" + dev).hide();
    }

    if (sel_val.indexOf("custom") != -1) {
      $(".f_custom_" + dev).show();
      $(".f_auto_" + dev).hide();
    } else {
      $(".f_custom_" + dev).hide();
      $(".f_auto_" + dev).show();
    }
  };

  var update_odb_from_sel_freq = function(odb_path, dev) {
    let sel_obj_id = "#sel_" + dev + "_freq";
    let val = $(sel_obj_id).val();
    
    if (val == "pro") {
      val = "freqc;custom"
    }
    
    mjsonrpc_db_set_value(odb_path, val);
  };

  var toggle_volt_visible = function(dev) {
    let sel_val = $("#sel_" + dev + "_volt").val();
    
    if (sel_val == "n") {
      $(".v_custom_" + dev).hide();
      $(".v_auto_" + dev).show();
    } else {
      $(".v_custom_" + dev).show();
      $(".v_auto_" + dev).hide();
    }
  };

  var toggle_list_visible = function(dev) {
    let sel_val = $("#sel_" + dev + "_volt").val();
    
    if (sel_val == "n") {
      $(".l_custom_" + dev).hide();
      $(".l_auto_" + dev).show();
    } else {
      $(".l_custom_" + dev).show();
      $(".l_auto_" + dev).hide();
    }
  };

  var update_sel_from_odb = function(dev, suffix) {
    let odb_val = $("#sel_span_" + dev + suffix).html();
    let sel_val = odb_val;
    let sel_obj_id = "#sel_" + dev + suffix;
    
    let sel_opts = $(sel_obj_id + " option").map(function() {return $(this).val();}).get();
    
    if (sel_opts.indexOf(odb_val) == -1) {
      // Handle any case-sensitivity if val not in list
      for (var i in sel_opts) {
        if (sel_opts[i].toLowerCase() == odb_val.toLowerCase()) {
          sel_val = sel_opts[i];
          break;
        }
      }
    }
    
    $(sel_obj_id).val(sel_val);
    return sel_val;
  };

  return {
    init: init,
    recompute: recompute,
    update_computed: update_computed,
    update_odb_from_sel_freq, update_odb_from_sel_freq,
    update_sel_from_odb_freq, update_sel_from_odb_freq,
    toggle_list_visible, toggle_list_visible,
    toggle_volt_visible, toggle_volt_visible
  };
})();




