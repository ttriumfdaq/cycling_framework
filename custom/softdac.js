var cycling = cycling || {};

cycling.softdac = (function() {

  var init = function() {
    cycling.common.create_run_state_div("#runstate");
    
    if (config.paths.softdac_defaults_base === undefined) {
      $("#softdac_tables").html("config.paths.softdac_defaults_base not defined!");
      mhttpd_init('Softdac');
    } else {
      mjsonrpc_db_get_values([config.paths.softdac_defaults_base, config.paths.scanning_softdac_base, config.paths.ppg_prog_base, config.paths.scanning_base + "/Global/Settings"]).then(function(rpc_prog) {
        if (rpc_prog.result.status[0] == 312 || rpc_prog.result.status[1] == 312 || rpc_prog.result.status[2] == 312 || rpc_prog.result.status[3] == 312) {
          $("#softdac_tables").html("Part of the ODB is missing.");
        } else {
          handle_softdac_odb(rpc_prog.result.data[0], rpc_prog.result.data[1], rpc_prog.result.data[2], rpc_prog.result.data[3]);
        }
        mhttpd_init('Softdac');
      });
    }
  };
  
  var html_cell = function(pulse_name, chan_idx, scanning, global_scanning) {
    let html = '<td>';
    
    let nvars = 24;
    let scan_xy = '';
    let scan_idx = -1;
    
    for (let i = 0; i < nvars; i++) {
      if (global_scanning["enable x"]) {
        if (scanning["x blocks"][i].toLowerCase() == pulse_name && scanning["x chan index"][i] == chan_idx) {
          scan_xy = 'X';
          scan_idx = i;
        }
      }
      if (global_scanning["enable y"]) {
        if (scanning["y blocks"][i].toLowerCase() == pulse_name && scanning["y chan index"][i] == chan_idx) {
          scan_xy = 'Y';
          scan_idx = i;
        }
      }
    }
    
    if (scan_idx > -1) {
      let odb_start = config.paths.scanning_softdac_base + "/" + scan_xy + " start[" + scan_idx + "]";
      let odb_end = config.paths.scanning_softdac_base + "/" + scan_xy + " end[" + scan_idx + "]";
      html += "Scanned from ";
      html += '<span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + odb_start + '"></span>';
      html += " to ";
      html += '<span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + odb_end + '"></span>';
    } else {
      let odb_key = config.paths.softdac_defaults_base + "/" + pulse_name + "[" + chan_idx + "]";
      html += '<span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + odb_key + '"></span>';
    }
    
    html += '</td>';
    return html;
  };
  
  var handle_softdac_odb = function(defaults, scanning, ppg_blocks, global_scanning) {
    let softdac_pulse_names = [];
    let keys = Object.keys(ppg_blocks);
    let defaults_names = Object.keys(defaults);
    
    let missing_from_defaults = [];
    
    for (let k = 0; k < keys.length; k++) {
      let keyname = keys[k];
      if (keyname.indexOf("/") == -1 && keyname.startsWith("pulse") && config.softdac && config.softdac.ppg_channel) {
        if (ppg_blocks[keyname].hasOwnProperty("ppg signal name")) {
          let channel = ppg_blocks[keyname]["ppg signal name"];
          if (channel == config.softdac.ppg_channel) {
            softdac_pulse_names.push(keyname);
            
            if (!defaults.hasOwnProperty(keyname)) {
              missing_from_defaults.push(keyname);
            }
          }
        }
      }
    }
    
    let settable_names = [];
    
    for (let i = 0; i < defaults_names.length; i++) {
      if (defaults_names[i].indexOf("/") == -1) {
        settable_names.push(defaults_names[i]);
      }
    }
      
    let html = "";
    
    for (let i = 0; i < missing_from_defaults.length; i++) {
      html += "<b>No defaults defined for " + missing_from_defaults[i] + "</b><br>";
    }
    
    html += '<table class="mtable">';
    html += '<thead>';
    html += '  <tr><th colspan="' + (1 + settable_names.length) + '" class="mtableheader">Softdac trap voltages</th></tr>'
    html += '  <tr>';
    html += '    <th></th>';
   
    for (let i = 0; i < settable_names.length; i++) {
      html += '    <th>' + settable_names[i].replace("pulse_", "") + ' (V)</th>';
    }
    
    html += '  </tr>';
    html += '</thead>';
    html += '<tbody>';
    
    for (let chan = 0; chan < 8; chan++) {
      html += '<tr>';
      html += '  <th>' + scanning["channel names"][chan] + '</th>';
      
      for (let i = 0; i < settable_names.length; i++) {
        html += html_cell(settable_names[i], chan, scanning, global_scanning);
      }
      
      html += '</tr>';
    }
    
    html += '</tbody>';
    html += '</table>';
  
    $("#softdac_tables").html(html);
  };
  
  return {
    init: init
  };
})();