var cycling = cycling || {};

cycling.saveload = (function() {
  var alert_rpc_error = function(status, reply) {
    if (status == 103) {
      dlgAlert("The save/load frontend must be running for this functionality to work."); 
    } else {
      dlgAlert("Failed to perform action!<div style='text-align:left'><br>Status code: " + status + "<br>Message: " + reply + "</div>"); 
    }  
  };
  
  var saveload_rpc = function(cmd, args, callback, maxlen) {
    let params = Object()
    params.client_name = config.saveload.client;
    params.cmd = cmd;
    params.args = JSON.stringify(args);
    
    if (maxlen !== undefined) {
      params.max_reply_length = maxlen;
    }
  
    mjsonrpc_call("jrpc", params).then(function(rpc) {
      let [status, reply] = cycling.common.parse_rpc_response(rpc.result);
      if (status == 1) {
        callback(reply);
      } else {
        alert_rpc_error(status, reply);
      }
    }).catch(function(error) {
      mjsonrpc_error_alert(error);
    });
  };
  
  var init = function() {
    $(".human_name").text(config.saveload.human_name);
    common_create_run_state_div("#runstate");
    let args = {"section": config.saveload.section};
    saveload_rpc("list", args, populate_list, 1e6);
    
    mhttpd_init('Save/load settings');
  };
  
  var populate_list = function(rpc_msg) {
    let files = JSON.parse(rpc_msg);
    let html = "";
    
    if (files.length) {
      
      for (let f in files) {
        let mod = files[f]["last_modified"];
        if (mod.indexOf(".") != -1) {
          // Strip any millisecs if present
          mod = mod.slice(0, mod.indexOf("."))
        }
        html += '<tr>';
        html += '<td>' + files[f]["comment"] + '</td><td>' + mod + '</td>';
        html += '<td><button role="button" class="mbutton disable_running" onclick="cycling.saveload.start_load(\'' + files[f]["filename"]  + '\')">Load these settings</button>';
        html += '<button role="button" class="mbutton" onclick="cycling.saveload.start_view(\'' + files[f]["filename"]  + '\')">View settings</button>';
        html += '<button role="button" class="mbutton" onclick="cycling.saveload.start_edit_comment(\'' + files[f]["filename"]  + '\')">Edit comment</button>';
        html += '<button role="button" class="mbutton" onclick="cycling.saveload.start_delete(\'' + files[f]["filename"]  + '\')">Delete</button>';
        html += '</td>';
        html += '</tr>';
      }
      
    } else {
      html += "<tr><td colspan='3'>No saved files found.</td></tr>";
    }  
    
    $("#settings_list").html(html);
  };
  
  var show_settings = function(rpc_reply) {
    $("#settings").text(rpc_reply);
  };
  
  var show_loaded = function(rpc_reply) {
    $("#settings").text("Loading saved settings: " + rpc_reply);
  };
  
  var refresh = function(rpc_reply) {
    location.reload();
  };
  
  var start_save = function() {
    let args = {"section": config.saveload.section, "comment": $("#comment").val()};
    saveload_rpc("save", args, refresh); 
  };
  
  var start_load = function(filename) {
    let args = {"section": config.saveload.section, "filename": filename};
    saveload_rpc("load", args, show_loaded); 
  };
  
  var start_view = function(filename) {
    let args = {"section": config.saveload.section, "filename": filename};
    saveload_rpc("view", args, show_settings, 1e6); 
  };
  
  var start_edit_comment = function(filename) {
    let comment = prompt("Enter new comment:");
    
    if (comment.length == 0) {
      return;
    }
    
    let args = {"section": config.saveload.section, "filename": filename, "comment": comment};
    saveload_rpc("edit_comment", args, refresh); 
  };
  
  var start_delete = function(filename) {
    if (!confirm("Are you sure you want to delete these saved settings from disk?")) {
      return;
    }
    
    let args = {"section": config.saveload.section, "filename": filename};
    saveload_rpc("delete", args, refresh); 
  };
  
  return {
    init: init,
    start_delete: start_delete,
    start_edit_comment: start_edit_comment,
    start_load: start_load,
    start_save: start_save,
    start_view: start_view
  };
})();