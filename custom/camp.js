var cycling = cycling || {};

cycling.camp = (function() {
  var show_camp_unused = false;
  var setpoint_path_set = {};
  
  var init = function() {
    cycling.common.create_run_state_div("#runstate");
    
    if (config.paths.camp_defs_base === undefined) {
      $(".show_unused_camp").hide();
      $("#camp").html("<tr><td colspan='5'>config.paths.camp_defs_base not defined</td></tr>");
      mhttpd_init('CAMP definitions');
    } else {
      mjsonrpc_db_get_values([config.paths.camp_defs_base + "/Setpoint path"]).then(function(rpc_prog) {
        if (rpc_prog.result.status[0] == 312) {
          $(".show_unused_camp").hide();
          $("#camp").html("<tr><td colspan='5'>No CAMP definitions found in ODB</td></tr>");
        } else {
          handle_camp_defs(rpc_prog.result.data[0].length);
        }
        mhttpd_init('CAMP definitions');
      });
    }
  };
  
  var set_show_camp_unused = function() {
    show_camp_unused = true;
    $(".camp_row").show();
    $(".show_unused_camp").hide();
  };
  
  var setpoint_path_changed = function(i) {
    let id_sfx = "_" + i;
    let odb_val = $("#setpoint_path" + id_sfx).text();
    
    if ((odb_val == "" || odb_val == "(empty)") && !show_camp_unused) {
      $("#camp_row" + id_sfx).hide();
      if (id_sfx in setpoint_path_set) {
        delete setpoint_path_set[id_sfx];
      }
    } else {
      $("#camp_row" + id_sfx).show();
      setpoint_path_set[id_sfx] = true;
    }
    
    $("#camp_count").text(Object.keys(setpoint_path_set).length);
  };
  
  var html_camp_row = function(i) {
    let id_sfx = "_" + i;
    let html = '<tr class="camp_row" id="camp_row' + id_sfx + '">';
    
    // ID
    html += "<th>" + i + "</th>";
  
    // Setpoint path
    html += '<td><span id="setpoint_path' + id_sfx + '" class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.camp_defs_base + '/Setpoint path[' + i + ']" onchange="cycling.camp.setpoint_path_changed(' + i + ')"></span>';
  
    // Instrument path
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.camp_defs_base + '/Instrument path[' + i + ']"></span>';
  
    // Instrument type
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.camp_defs_base + '/Instrument type[' + i + ']"></span>';
  
    // Units
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.camp_defs_base + '/Units[' + i + ']"></span>';
  
    // Integer conversion factor
    html += '<td><span class="modbvalue disable_running" data-odb-editable="1" data-odb-path="' + config.paths.camp_defs_base + '/Integer conversion factor[' + i + ']"></span>';
      
    // Finished
    html += "</tr>";
    return html;
  };
  
  var handle_camp_defs = function(num_params) {
    let html = "";
    
    for (let i = 0; i < num_params; i++) {
      html += html_camp_row(i);
    }
  
    $("#camp").html(html);
  };
  
  return {
    init: init,
    set_show_camp_unused: set_show_camp_unused,
    setpoint_path_changed: setpoint_path_changed
  };
})();