#include "epics.h"
#include "core/scan_utils.h"
#include <cmath>
#include <unistd.h>

#ifndef DUMMY_EPICS
#include "cadef.h"
#include "caerr.h"


void glob_connection_handler(struct connection_handler_args args) {
  EpicsMultiVar* var = (EpicsMultiVar*) ca_puser(args.chid);

  if (!var) {
    exit(1);
  }

  var->connection_handler(args);
}

void glob_read_event_handler(struct event_handler_args args) {
  EpicsMultiVar* var = (EpicsMultiVar*) args.usr;
  var->read_event_handler(args);
}

void glob_write_event_handler(struct event_handler_args args) {
  EpicsMultiVar* var = (EpicsMultiVar*) args.usr;
  var->write_event_handler(args);
}

#endif // DUMMY_EPICS

EpicsAccess::EpicsAccess() {
#ifndef DUMMY_EPICS
  int status = ca_task_initialize();

  if (status != ECA_NORMAL) {
    SEVCHK(status, ca_message(status));
    cm_msg(MERROR, __FUNCTION__, "Couldn't connect to EPICS: %s", ca_message(status));
    exit(0);
  }
#endif // DUMMY_EPICS
}

EpicsAccess::~EpicsAccess() {
#ifndef DUMMY_EPICS
  int status = ca_task_exit();

  if (status != ECA_NORMAL) {
    SEVCHK(status, ca_message(status));
    cm_msg(MERROR, __FUNCTION__, "Failed to disconnect from EPICS: %s", ca_message(status));
  }
#endif // DUMMY_EPICS
}


EpicsMultiVar::EpicsMultiVar(std::vector<std::string> _pv_names) {
  debug = false;

  for (auto it = _pv_names.begin(); it != _pv_names.end(); it++) {
    std::string& var = *it;

    if (var != "") {
      pv_states[var] = EpicsChanState();
      pv_states[var].channel_id = 0;
    }
  }
}

EpicsMultiVar::~EpicsMultiVar() {
#ifndef DUMMY_EPICS
  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    chid channel_id = it->second.channel_id;

    if (channel_id != 0) {
      ca_clear_channel(channel_id);
    }
  }
#endif
}

INT EpicsMultiVar::set(std::map<std::string, float> new_vals) {
  INT status = open_channels_if_needed();

  if (status != SUCCESS) {
    return status;
  }

  std::vector<std::string> unwritten;

  for (auto it = new_vals.begin(); it != new_vals.end(); it++) {
    EpicsChanState& state = pv_states[it->first];
    scan_utils::ts_printf("Setting EPICS variable %s to %f\n", it->first.c_str(), it->second);
    state.set_value = it->second;

#ifndef DUMMY_EPICS
    /* Get number of elements of PV */
    unsigned nelem = ca_element_count(state.channel_id);

    state.write_pending = true; /* set flag (cleared by callback) */
    unwritten.push_back(it->first);

    /* Make callback request */

    status = ca_array_put_callback( DBR_FLOAT,
                                    nelem, /* number of elements of database field */
                                    state.channel_id, /* channel identifier or chid */
                                    &(it->second), /* value to write */
                                    glob_write_event_handler, /* name of callback event handler */
                                    this);

    if (status != ECA_NORMAL) {
      SEVCHK(status, (char* )ca_message(status));
      printf("set: bad status after call to ca_array_put_callback\n");
      return -1;
    }
#endif
  }

#ifndef DUMMY_EPICS
  bool printed_warning = false;
  long unsigned int start = scan_utils::current_time_ms();

  while (unwritten.size()) {
    status = ca_poll();

    if (status != ECA_TIMEOUT && status != ECA_NORMAL) {
      printf("set: bad status after ca_poll:\n %s\n", ca_message(status));
      return -1;
    }

    long unsigned int now = scan_utils::current_time_ms();

    if (!printed_warning && (now - start) > 500) {
      printf("set: Waiting for callback from write_event_handler\n");
      printed_warning = true;
    }

    if ((now - start) > 30000) {
      /* wait up to 30s for callback to complete */
      printf("set: timed out waiting for write_event_handler \n");
      return -1;
    }

    std::vector<std::string> now_unwritten;

    for (auto it = unwritten.begin(); it != unwritten.end(); it++) {
      if (pv_states[*it].write_pending) {
        now_unwritten.push_back(*it);
      }
    }

    unwritten = now_unwritten;

    if (unwritten.size()) {
      if ((now - start) > 250) {
        cm_yield(1);
      } else {
        usleep(10);
      }
    }
  }
#endif // DUMMY_EPICS

  return SUCCESS;
}

INT EpicsMultiVar::read() {
  INT status = open_channels_if_needed();

  if (status != SUCCESS) {
    return status;
  }

#ifdef DUMMY_EPICS
  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    it->second.read_value = it->second.set_value;
  }
#else
  std::vector<std::string> unread;

  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    EpicsChanState& state = it->second;

    /* Get number of elements of PV */
    unsigned nelem = ca_element_count(state.channel_id);

    /* Make callback request */
    state.read_pending = true;

    unread.push_back(it->first);

    INT status = ca_array_get_callback( DBR_STS_FLOAT, nelem, /* number of elements of database field */
                                        state.channel_id, /* channel identifier or chid */
                                        glob_read_event_handler, /* name of callback event handler */
                                        this);

    if (status != ECA_NORMAL) {
      printf("read_chan: bad status after ca_array_get_callback : %s\n", ca_message(status));
      return -1;
    }
  }

  bool printed_warning = false;
  long unsigned int start = scan_utils::current_time_ms();

  while (unread.size()) {
    std::vector<std::string> now_unread;

    INT status = ca_poll();

    if (status != ECA_TIMEOUT && status != ECA_NORMAL) {
      printf("Bad status after ca_poll: %s\n", ca_message(status));
      return -1;
    }

    long unsigned int now = scan_utils::current_time_ms();

    if (!printed_warning && (now - start) > 200) {
      printf("read_chan: Waiting for callback from read_event_handler\n");
    }

    if ((now - start) > 30000) {
      printf("read_chan: timed out waiting to read\n");
      return -1;
    }

    for (auto it = unread.begin(); it != unread.end(); it++) {
      if (pv_states[*it].read_pending) {
        now_unread.push_back(*it);
      }
    }

    unread = now_unread;

    if (unread.size()) {
      if ((now - start) > 250) {
        cm_yield(1);
      } else {
        usleep(10);
      }
    }
  }
#endif

  return SUCCESS;
}

INT EpicsMultiVar::open_channels_if_needed() {
#ifndef DUMMY_EPICS
  INT status;

  std::vector<std::string> unopen_channels;

  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    EpicsChanState& state = it->second;
    if (state.channel_id == 0 || ca_state(state.channel_id) != cs_conn) {
      unopen_channels.push_back(it->first);
      state.connect_pending = true;

      status = ca_search_and_connect(it->first.c_str(), &(state.channel_id), glob_connection_handler, this);
      if (status != ECA_NORMAL) {
        SEVCHK(status, ca_message(status));
        printf("connect: bad status after ca_search_and_connect for %s\n", it->first.c_str());
        return -1;
      }
    }
  }

  bool printed_warning = false;
  long unsigned int start = scan_utils::current_time_ms();

  while (unopen_channels.size()) {
    status = ca_poll();

    if (status != ECA_TIMEOUT && status != ECA_NORMAL) {
      SEVCHK(status, ca_message(status));
      printf("connect: bad status after ca_poll: %s\n", ca_message(status));
      printf("connect: couldn't establish connection to %u channels\n", unopen_channels.size());
      return -1;
    }

    long unsigned int now = scan_utils::current_time_ms();

    /* print a message if we have to wait a long time */
    if (!printed_warning && (now - start) > 1500) {
      printf("connect: Waiting for callback from search_and_connect for %u channels\n", unopen_channels.size());
      printed_warning = true;
    }

    if ((now - start) > 60000) {
      printf("connect: timed out waiting for callback \n");
      printf("connect: couldn't establish connection to %u channels\n", unopen_channels.size());
      return -1;
    }

    std::vector<std::string> now_unopen;

    for (auto it = unopen_channels.begin(); it != unopen_channels.end(); it++) {
      EpicsChanState& state = pv_states[*it];
      if (state.channel_id == 0 || ca_state(state.channel_id) != cs_conn) {
        now_unopen.push_back(*it);
      }
    }

    unopen_channels = now_unopen;

    if (unopen_channels.size()) {
      if ((now - start) > 250) {
        cm_yield(1);
      } else {
        usleep(10);
      }
    }
  }

#endif // DUMMY_EPICS

  return SUCCESS;
}

std::map<std::string, float> EpicsMultiVar::get_set_values() {
  std::map<std::string, float> retval;

  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    retval[it->first] = it->second.set_value;
  }

  return retval;
}


std::map<std::string, float> EpicsMultiVar::get_read_values() {
  std::map<std::string, float> retval;

  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    retval[it->first] = it->second.read_value;
  }

  return retval;
}

#ifndef DUMMY_EPICS
void EpicsMultiVar::connection_handler(struct connection_handler_args args) {
  if (debug) {
    if (args.op == CA_OP_CONN_UP) {
      printf("Connect_handler: %s is reconnected \n", ca_name(args.chid));
    } else {
      printf("Connect_handler: %s is DISCONNECTED \n", ca_name(args.chid));
    }
  }

  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    if (it->second.channel_id == args.chid) {
      it->second.connect_pending = false;
    }
  }
}

void EpicsMultiVar::read_event_handler(struct event_handler_args args) {
  if (args.status == ECA_NORMAL) {
    switch (args.type) {
      case (DBR_STS_FLOAT): {
          struct dbr_sts_float *pvalue = (struct dbr_sts_float*) args.dbr;
          dbr_float_t *pfloat = &pvalue->value;

          for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
            if (it->second.channel_id == args.chid) {
              it->second.read_value = *pfloat;
              it->second.read_pending = false;
            }
          }

        }
        break;
      default:
        printf("Unexpected type %ld", args.type);
    }
  } else {
    /* if get operation failed, print channel name and message */

    SEVCHK(args.status, (char* ) ca_message(args.status));
    printf("read_event_handler: get operation failed on channel %s\n", ca_name(args.chid));
  }
}

void EpicsMultiVar::write_event_handler(struct event_handler_args args) {
  if (args.status != ECA_NORMAL) {
    printf("write_event_handler: channel %s: put operation FAILED due to:\n", ca_name(args.chid));
    SEVCHK(args.status, (char* ) ca_message(args.status));
  }

  for (auto it = pv_states.begin(); it != pv_states.end(); it++) {
    if (it->second.channel_id == args.chid) {
      it->second.write_pending = false;
    }
  }
}
#endif

EpicsSingleVar::EpicsSingleVar(std::string _pv_name) : EpicsMultiVar({_pv_name}) {
  pv_name = _pv_name;
}

EpicsSingleVar::~EpicsSingleVar() {
}

INT EpicsSingleVar::set(float new_val) {
  std::map<std::string, float> args;
  args[pv_name] = new_val;
  return EpicsMultiVar::set(args);
}

INT EpicsSingleVar::read() {
  return EpicsMultiVar::read();
}

float EpicsSingleVar::get_read_value() {
  std::map<std::string, float> read_vals = get_read_values();
  return read_vals[pv_name];
}


EpicsMeasDemandVar::EpicsMeasDemandVar(std::string _device_name, std::string _measured_device_name) :
    EpicsMultiVar({_device_name, _measured_device_name}) {
  device_name = _device_name;
  measured_device_name = _measured_device_name;
  custom_check_fn_pointer = NULL;
  check_setpoint_abs = -1;
  check_measured_abs = -1;
  check_setpoint_pct = -1;
  check_measured_pct = -1;
  stability_measured_abs = -1;
  stability_measured_pct = -1;
  check_timeout_secs = 30;
}

EpicsMeasDemandVar::~EpicsMeasDemandVar() {
}

std::string EpicsMeasDemandVar::get_name(bool demand) {
  return demand ? device_name : measured_device_name;
}

INT EpicsMeasDemandVar::set(float new_value) {
  std::map<std::string, float> args;
  args[device_name] = new_value;
  return EpicsMultiVar::set(args);
}


INT EpicsMeasDemandVar::read() {
  return EpicsMultiVar::read();
}

float EpicsMeasDemandVar::get_set_value() {
  return EpicsMultiVar::get_set_values()[device_name];
}

float EpicsMeasDemandVar::get_demand_value() {
  return EpicsMultiVar::get_read_values()[device_name];
}

float EpicsMeasDemandVar::get_measured_value() {
  return EpicsMultiVar::get_read_values()[measured_device_name];
}

void EpicsMeasDemandVar::set_custom_tolerance_check_function(epics_tolerance_check fn) {
  custom_check_fn_pointer = fn;
}

void EpicsMeasDemandVar::set_tolerances(double setpoint_abs, double measured_abs, double setpoint_pct, double measured_pct, double timeout_secs) {
  check_setpoint_abs = setpoint_abs;
  check_measured_abs = measured_abs;
  check_setpoint_pct = setpoint_pct;
  check_measured_pct = measured_pct;
  check_timeout_secs = timeout_secs;
  scan_utils::ts_printf("Tolerances for %s are: setpoint abs %f, setpoint pct %f, measured value abs %f, measured value pct %f\n", get_name().c_str(), check_setpoint_abs, check_setpoint_pct, check_measured_abs, check_measured_pct);
}

void EpicsMeasDemandVar::set_stability_tolerances(double abs, double pct) {
  stability_measured_abs = abs;
  stability_measured_pct = pct;
}

INT EpicsMeasDemandVar::set_value_and_wait_for_tolerance(float new_value, bool& ok) {
  INT status = set(new_value);

  if (status != SUCCESS) {
    scan_utils::ts_printf("Failed to set to %f\n", new_value);
    return status;
  }

  long unsigned int timeout = scan_utils::current_time_ms() + (check_timeout_secs * 1000);
  ok = false;

  while (scan_utils::current_time_ms() < timeout) {
    status = read();

    if (status != SUCCESS) {
      scan_utils::ts_printf("Failed to read\n");
      return status;
    }

    ok = is_within_tolerance();

    if (ok) {
      break;
    }

    ss_sleep(10);
  }

  return SUCCESS;
}

bool EpicsMeasDemandVar::has_measurable() {
  return measured_device_name != "";
}

bool EpicsMeasDemandVar::is_within_tolerance() {
  float set_value = get_set_value();
  float demand_value = get_demand_value();
  float measured_value = get_measured_value();

  if (custom_check_fn_pointer) {
    return custom_check_fn_pointer(set_value, demand_value, measured_value);
  } else {
    if (check_setpoint_abs > -0.5 && std::fabs(set_value - demand_value) > check_setpoint_abs) {
      printf("%s failed absolute setpoint test. %f - %f (= %f) > %f\n", get_name().c_str(), set_value, demand_value, std::fabs(set_value - demand_value), check_setpoint_abs);
      return false;
    }

    if (has_measurable() && check_measured_abs > -0.5 && std::fabs(set_value - measured_value) > check_measured_abs) {
      printf("%s failed absolute measured value test. %f - %f (= %f) > %f\n", get_name().c_str(), set_value, measured_value, std::fabs(set_value - measured_value), check_measured_abs);
      return false;
    }

    if (check_setpoint_pct > -0.5 && set_value != 0) {
      if (std::fabs(set_value - demand_value)/std::fabs(set_value) > (check_setpoint_pct/100.)) {
        printf("%s failed percentage setpoint test. %f - %f (= %f %%) > %f %%\n", get_name().c_str(), set_value, demand_value, (std::fabs(set_value - demand_value)/std::fabs(set_value)) * 100., check_setpoint_pct);
        return false;
      }
    }

    if (has_measurable() && check_measured_pct > -0.5 && set_value != 0) {
      if (std::fabs(set_value - measured_value)/std::fabs(set_value) > (check_measured_pct/100.)) {
        printf("%s failed percentage measured value test. %f - %f (= %f %%) > %f %%\n", get_name().c_str(), set_value, measured_value, (std::fabs(set_value - measured_value)/std::fabs(set_value)) * 100., check_measured_pct);
        return false;
      }
    }
  }

  return true;
}

bool EpicsMeasDemandVar::is_stable(float previous_measured_value) {
  if (has_measurable()) {
    float measured_value = get_measured_value();

    if (stability_measured_abs > -0.5 && std::fabs(previous_measured_value - measured_value) > stability_measured_abs) {
      printf("%s failed absolute measured value stability test. %f - %f (= %f) > %f\n", get_name().c_str(), previous_measured_value, measured_value, std::fabs(previous_measured_value - measured_value), stability_measured_abs);
      return false;
    }

    if (stability_measured_pct > -0.5 && measured_value != 0) {
      if (std::fabs(previous_measured_value - measured_value)/std::fabs(measured_value) > (stability_measured_pct/100.)) {
        printf("%s failed percentage measured value stability test. %f - %f (= %f %%) > %f %%\n", get_name().c_str(), previous_measured_value, measured_value, (std::fabs(previous_measured_value - measured_value)/std::fabs(measured_value)) * 100., stability_measured_pct);
        return false;
      }
    }
  }

  return true;
}
