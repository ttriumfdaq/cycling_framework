#include "epics.h"
#include <stdio.h>
#include <string>
#include <map>
#include <vector>
#include "core/scan_utils.h"

// Simple program to read/write from EPICS using the
// EpicsVar infrastructure in epics.cxx. Comparing this
// to low-level ca_get programs etc can be useful for
// debugging access/programming issues.

void usage() {
  printf("Usage: epics_test.exe <get|set> <param_name> [<value>]\n");
  exit(0);
}

int main(int argc, char* argv[]) {
  if (argc < 3 || argc > 4) {
    usage();
  }

  std::string cmd(argv[1]);

#ifdef DUMMY_EPICS
  printf("****************************************************************************\n");
  printf("EXECUTABLE WAS COMPILED IN DUMMY MODE - NOT CONNECTING TO REAL EPICS SERVER!\n");
  printf("****************************************************************************\n");
#endif

  if (cmd == "get") {
    if (argc != 3) {
      usage();
    }

    std::string dev(argv[2]);
    EpicsAccess epics;
    EpicsSingleVar var(dev);
    var.read();
    printf("%f\n", var.get_read_value());
  } else if (cmd == "set") {
    if (argc != 4) {
      usage();
    }

    EpicsAccess epics;
    EpicsSingleVar var(argv[2]);
    float new_value;
    sscanf(argv[3], "%f\n", &new_value);
    var.set(new_value);
    printf("Set %f\n", new_value);
    var.read();
    printf("Read %f\n", var.get_read_value());
  } else {
    usage();
  }
}
