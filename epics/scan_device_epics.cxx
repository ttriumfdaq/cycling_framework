#include "scan_device_epics.h"
#include "midas.h"
#include "epics.h"
#include "core/scannable_device.h"
#include <cmath>
#include <iostream>
#include <sstream>
#include <cstring>
#include "core/scan_utils.h"

#define EPICS_DEFINES_PATH "/Scanning/Epics/Definitions"
#define EPICS_SETTINGS_PATH "/Scanning/Epics/Settings"
#define EPICS_STATUS_PATH "/Scanning/Epics/Status"

bool tol_check_custom_example(float set, float readback, float measured) {
  if (std::fabs(set - readback) > 1e-7) {
    return false;
  }

  return true;
}

ScanDeviceEpics::ScanDeviceEpics(HNDLE _hDB) : ScannableDevice(_hDB) {
  tolerance_fns["tol_check_custom_example"] = {tol_check_custom_example, 0, 2};
}

void ScanDeviceEpics::add_bootstrap_definition(std::string human_name, std::string demand_device, std::string measured_device, std::string tol_check) {
  BootstrapDefinition def;
  def.name = human_name;
  def.demand = demand_device;
  def.measured = measured_device;
  def.tol_check = tol_check;
  bootstrap_defs.push_back(def);
}

INT ScanDeviceEpics::check_records() {
  char settings_path[256], status_path[256], defs_path[256];
  sprintf(settings_path, EPICS_SETTINGS_PATH);
  sprintf(status_path, EPICS_STATUS_PATH);
  sprintf(defs_path, EPICS_DEFINES_PATH);

  INT status;

  status = db_check_record(hDB, 0, settings_path, EPICS_SCAN_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", settings_path);
    return status;
  }

  status = db_check_record(hDB, 0, status_path, EPICS_SCAN_STATUS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", status_path);
    return status;
  }

  HNDLE hdefs;
  bool do_bootstrap = (db_find_key(hDB, 0, defs_path, &hdefs) == DB_NO_KEY);

  status = db_check_record(hDB, 0, defs_path, EPICS_DEFINES_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", defs_path);
    return status;
  }

  if (do_bootstrap) {
    db_find_key(hDB, 0, defs_path, &hdefs);

    for (DWORD i = 0; i < bootstrap_defs.size(); i++) {
      char val[64];
      sprintf(val, "%s", bootstrap_defs[i].name.c_str());
      db_set_value_index(hDB, hdefs, "Human name", val, sizeof(val), i, TID_STRING, FALSE);
      sprintf(val, "%s", bootstrap_defs[i].demand.c_str());
      db_set_value_index(hDB, hdefs, "Demand device", val, sizeof(val), i, TID_STRING, FALSE);
      sprintf(val, "%s", bootstrap_defs[i].measured.c_str());
      db_set_value_index(hDB, hdefs, "Measured device", val, sizeof(val), i, TID_STRING, FALSE);
      sprintf(val, "%s", bootstrap_defs[i].tol_check.c_str());
      db_set_value_index(hDB, hdefs, "Custom threshold check fn", val, sizeof(val), i, TID_STRING, FALSE);
    }
  }

  return SUCCESS;
}

INT ScanDeviceEpics::open_records() {
  char settings_path[256], status_path[256], defs_path[256];
  sprintf(settings_path, EPICS_SETTINGS_PATH);
  sprintf(status_path, EPICS_STATUS_PATH);
  sprintf(defs_path, EPICS_DEFINES_PATH);

  HNDLE h_settings, h_status, h_defs;
  INT size_settings = sizeof(user_settings);
  INT size_status = sizeof(scan_status);
  INT size_defs = sizeof(epics_defines);
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);
  db_find_key(hDB, 0, defs_path, &h_defs);

  status = db_get_record(hDB, h_settings, &user_settings, &size_settings, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", settings_path);
    return status;
  }

  status = db_get_record(hDB, h_status, &scan_status, &size_status, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", status_path);
    return status;
  }

  status = db_get_record(hDB, h_defs, &epics_defines, &size_defs, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", defs_path);
    return status;
  }

  status = db_open_record(hDB, h_settings, &user_settings, size_settings, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open EPICS settings record");
    return status;
  }

  status = db_open_record(hDB, h_status, &scan_status, size_status, MODE_WRITE, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open EPICS status record");
    return status;
  }

  status = db_open_record(hDB, h_defs, &epics_defines, size_defs, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open EPICS defs record");
    return status;
  }

  return status;
}

INT ScanDeviceEpics::close_records() {
  char settings_path[256], status_path[256], defs_path[256];
  sprintf(settings_path, EPICS_SETTINGS_PATH);
  sprintf(status_path, EPICS_STATUS_PATH);
  sprintf(defs_path, EPICS_DEFINES_PATH);

  HNDLE h_settings, h_status, h_defs;
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);
  db_find_key(hDB, 0, defs_path, &h_defs);

  status = db_close_record(hDB, h_settings);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close EPICS settings record");
    return status;
  }

  status = db_close_record(hDB, h_status);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close EPICS status record");
    return status;
  }

  status = db_close_record(hDB, h_defs);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close EPICS defs record");
    return status;
  }

  return status;
}

EpicsMeasDemandVar* ScanDeviceEpics::build_epics_var(int idx) {
  EpicsMeasDemandVar* var = NULL;

  if (strlen(epics_defines.demand_device[idx])) {
    var = new EpicsMeasDemandVar(epics_defines.demand_device[idx], epics_defines.measured_device[idx]);

    if (strlen(epics_defines.threshold_fn_name[idx])) {
      if (tolerance_fns.find(epics_defines.threshold_fn_name[idx]) != tolerance_fns.end()) {
        // Custom tolerance check
        EpicsToleranceCheck custom = tolerance_fns[epics_defines.threshold_fn_name[idx]];
        var->set_custom_tolerance_check_function(custom.fn);
        var->set_stability_tolerances(custom.stability_abs, custom.stability_pct);
      } else if (strcmp(epics_defines.threshold_fn_name[idx], "no_check") == 0) {
        // Nothing to do - default is not to check anything.
      } else if (strstr(epics_defines.threshold_fn_name[idx], "parse_") != NULL) {
        // Parse the setpoint/measured tolerances
        double set_abs = -1;
        double meas_abs = -1;
        double stab_abs = -1;
        double set_pct = -1;
        double meas_pct = -1;
        double stab_pct = -1;
        double timeout_secs = -1;

        INT status = parse_tolerances(epics_defines.threshold_fn_name[idx], set_abs, meas_abs, set_pct, meas_pct, stab_abs, stab_pct, timeout_secs);

        if (status == SUCCESS) {
          var->set_tolerances(set_abs, meas_abs, set_pct, meas_pct, timeout_secs);
          var->set_stability_tolerances(stab_abs, stab_pct);
        } else {
          cm_msg(MERROR, __FUNCTION__, "Unable to parse tolerance check function '%s'", epics_defines.threshold_fn_name[idx]);
          delete var;
          var = NULL;
        }

      } else {
        cm_msg(MERROR, __FUNCTION__, "Unknown tolerance check function %s", epics_defines.threshold_fn_name[idx]);
        delete var;
        var = NULL;
      }
    }
  } else {
    cm_msg(MERROR, __FUNCTION__, "Undefined EPICS variable %i", idx);
  }

  return var;
}

INT ScanDeviceEpics::begin_of_run(char* error) {
  // Set up the variables
  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (epics_vars_x[i]) {
      delete epics_vars_x[i];
      epics_vars_x[i] = NULL;
      human_names_x[i] = "";
    }

    if (epics_vars_y[i]) {
      delete epics_vars_y[i];
      epics_vars_y[i] = NULL;
      human_names_y[i] = "";
    }

    if (x_enabled && user_settings.x_devices[i] > -1) {
      if (debug) {
        scan_utils::ts_printf("Building Epics variable for X device %i\n", i);
      }

      epics_vars_x[i] = build_epics_var(user_settings.x_devices[i]);
      human_names_x[i] = epics_defines.name[user_settings.x_devices[i]];

      if (epics_vars_x[i] == NULL) {
        snprintf(error, 255, "Failed to build EPICS X variable %i. See Messages for more.", i);
        return FE_ERR_ODB;
      }
    }

    if (y_enabled && user_settings.y_devices[i] > -1) {
      if (debug) {
        scan_utils::ts_printf("Building Epics variable for X device %i\n", i);
      }

      epics_vars_y[i] = build_epics_var(user_settings.y_devices[i]);
      human_names_y[i] = epics_defines.name[user_settings.y_devices[i]];

      if (epics_vars_y[i] == NULL) {
        snprintf(error, 255, "Failed to build EPICS Y variable %i. See Messages for more.", i);
        return FE_ERR_ODB;
      }
    }
  }

  // Make sure we can read all the variables
  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (epics_vars_x[i]) {
      if (debug) {
        scan_utils::ts_printf("Doing initial read for X var %i\n", i);
      }

      INT status = epics_vars_x[i]->read();

      if (status != SUCCESS) {
        snprintf(error, 255, "Failed to read EPICS X var %i", i);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_HW;
      }
    }

    if (epics_vars_y[i]) {
      if (debug) {
        scan_utils::ts_printf("Doing initial read for X var %i\n", i);
      }

      INT status = epics_vars_y[i]->read();

      if (status != SUCCESS) {
        snprintf(error, 255, "Failed to read EPICS Y var %i", i);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_HW;
      }
    }
  }

  return SUCCESS;
}

INT ScanDeviceEpics::setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) {
  INT status = SUCCESS;
  bool ok = false;

  // Compute new values and set in ODB
  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (x_enabled && new_x && epics_vars_x[i]) {
      scan_status.x_demand[i] = sweep_x[i][x_step];
      status = epics_vars_x[i]->set_value_and_wait_for_tolerance(scan_status.x_demand[i], ok);

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Failed to set %s to %f", epics_vars_x[i]->get_name().c_str(), scan_status.x_demand[i]);
        return status;
      }

      if (!ok) {
        cm_msg(MERROR, __FUNCTION__, "Failed tolerance check setting %s to %f", epics_vars_x[i]->get_name().c_str(), scan_status.x_demand[i]);
        return FE_ERR_DRIVER;
      }
    } else {
      scan_status.x_demand[i] = 0;
    }

    if (y_enabled && new_y && epics_vars_y[i]) {
      scan_status.y_demand[i] = sweep_y[i][y_step];
      status = epics_vars_y[i]->set_value_and_wait_for_tolerance(scan_status.y_demand[i], ok);

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Failed to set %s to %f", epics_vars_y[i]->get_name().c_str(), scan_status.y_demand[i]);
        return status;
      }

      if (!ok) {
        cm_msg(MERROR, __FUNCTION__, "Failed tolerance check setting %s to %f", epics_vars_y[i]->get_name().c_str(), scan_status.y_demand[i]);
        return FE_ERR_DRIVER;
      }
    } else {
      scan_status.y_demand[i] = 0;
    }

  }

  return status;
}

INT ScanDeviceEpics::is_next_step_ready(bool& ok) {
  INT status = SUCCESS;
  ok = true;

  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (x_enabled && epics_vars_x[i]) {
      status = epics_vars_x[i]->read();

      if (status != SUCCESS) {
        return status;
      }

      scan_status.x_measured[i] = epics_vars_x[i]->get_measured_value();

      if (!user_settings.validate_readback) {
        continue;
      }

      ok = epics_vars_x[i]->is_within_tolerance();

      if (!ok) {
        // Return SUCCESS as we managed to do the check;
        // calling code will then inspect the "ok" variable
        // (which is false) to know that we're not ready.
        return SUCCESS;
      }
    }

    if (y_enabled && epics_vars_y[i]) {
      status = epics_vars_y[i]->read();

      if (status != SUCCESS) {
        return status;
      }

      scan_status.y_measured[i] = epics_vars_y[i]->get_measured_value();

      if (!user_settings.validate_readback) {
        continue;
      }

      ok = epics_vars_y[i]->is_within_tolerance();

      if (!ok) {
        return SUCCESS;
      }
    }
  }

  return status;
}

INT ScanDeviceEpics::scanned_vars_are_stable(bool& ok) {
  INT status = SUCCESS;
  ok = true;

  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (x_enabled && epics_vars_x[i]) {
      float prev = epics_vars_x[i]->get_measured_value();
      status = epics_vars_x[i]->read();

      if (status != SUCCESS) {
        return status;
      }

      ok = epics_vars_x[i]->is_stable(prev);

      if (!ok) {
        // Return SUCCESS as we managed to do the check;
        // calling code will then inspect the "ok" variable
        // (which is false) to know that we're out of tolerance.
        return SUCCESS;
      }
    }

    if (y_enabled && epics_vars_y[i]) {
      float prev = epics_vars_y[i]->get_measured_value();
      status = epics_vars_y[i]->read();

      if (status != SUCCESS) {
        return status;
      }

      ok = epics_vars_y[i]->is_stable(prev);

      if (!ok) {
        return SUCCESS;
      }
    }
  }

  return status;
}

std::string ScanDeviceEpics::explain_unreadiness() {
  std::stringstream s;

  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (x_enabled && epics_vars_x[i]) {
      if (!epics_vars_x[i]->is_within_tolerance()) {
        s << "Epics device X[" << i << "] (" << epics_vars_x[i]->get_name() << ") is out of tolerance:";
        s << " set " << epics_vars_x[i]->get_set_value();
        s << ", readback " << epics_vars_x[i]->get_demand_value();
        if (epics_vars_x[i]->has_measurable()) {
          s << ", measured " << epics_vars_x[i]->get_measured_value();
        }
        return s.str();
      }
    }

    if (y_enabled && epics_vars_y[i]) {
      if (!epics_vars_y[i]->is_within_tolerance()) {
        s << "Epics device Y[" << i << "] (" << epics_vars_y[i]->get_name() << ") is out of tolerance:";
        s << " set " << epics_vars_y[i]->get_set_value();
        s << ", readback " << epics_vars_y[i]->get_demand_value();
        if (epics_vars_y[i]->has_measurable()) {
          s << ", measured " << epics_vars_y[i]->get_measured_value();
        }
        return s.str();
      }
    }
  }

  return "";
}

void ScanDeviceEpics::compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) {
  for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
    if (direction_type_x != DirNoRecomputation) {
      if (x_enabled && epics_vars_x[i]) {
        sweep_x[i] = compute_sweep_float(direction_type_x, user_settings.x_start[i], user_settings.x_end[i], nX);
        print_sweep(sweep_x[i], epics_vars_x[i], true, i);
      } else {
        sweep_x[i].clear();
      }
    }

    if (direction_type_y != DirNoRecomputation) {
      if (y_enabled && epics_vars_y[i]) {
        sweep_y[i] = compute_sweep_float(direction_type_y, user_settings.y_start[i], user_settings.y_end[i], nY);
        print_sweep(sweep_y[i], epics_vars_y[i], false, i);
      } else {
        sweep_y[i].clear();
      }
    }
  }
}

INT ScanDeviceEpics::fill_end_of_cycle_banks(char* pevent) {
  if (x_enabled) {
    float* pdata_xdem;
    bk_create(pevent, "XEPD", TID_FLOAT, (void**) &pdata_xdem);

    for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
      *pdata_xdem++ = scan_status.x_demand[i];
    }

    bk_close(pevent, pdata_xdem);

    float* pdata_xmeas;
    bk_create(pevent, "XEPM", TID_FLOAT, (void**) &pdata_xmeas);

    for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
      *pdata_xmeas++ = scan_status.x_measured[i];
    }

    bk_close(pevent, pdata_xmeas);
  }

  if (y_enabled) {
    float* pdata_ydem;
    bk_create(pevent, "YEPD", TID_FLOAT, (void**) &pdata_ydem);

    for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
      *pdata_ydem++ = scan_status.y_demand[i];
    }

    bk_close(pevent, pdata_ydem);

    float* pdata_ymeas;
    bk_create(pevent, "YEPM", TID_FLOAT, (void**) &pdata_ymeas);

    for (int i = 0; i < EPICS_SCAN_MAXVARS; i++) {
      *pdata_ymeas++ = scan_status.y_measured[i];
    }

    bk_close(pevent, pdata_ymeas);
  }

  return SUCCESS;

}

void ScanDeviceEpics::print_sweep(std::vector<float>& values, EpicsMeasDemandVar* var, bool is_x, int idx) {
  std::cout << "Sweep values for EPICS ";

  if (is_x) {
    std::cout << "X";
  } else {
    std::cout << "Y";
  }

  std::cout << " param #" << idx << " (" << var->get_name() << ") are: ";

  for (unsigned int i = 0; i < values.size(); i++) {
    if (i > 0) {
      std::cout << ", ";
    }

    std::cout << values[i];
  }

  std::cout << std::endl;
}

INT ScanDeviceEpics::parse_tolerances(char* tol_check, double& set_abs, double& meas_abs, double& set_pct, double& meas_pct, double& stab_abs, double& stab_pct, double& timeout_secs) {
  INT retval = 0;
  set_abs = -1;
  meas_abs = -1;
  stab_abs = -1;
  set_pct = -1;
  meas_pct = -1;
  stab_pct = -1;
  timeout_secs = 10;

  char* pch = strtok(tol_check, "_");

  while (pch != NULL) {
    if (strstr(pch, "parse") != NULL) {
      pch = strtok(NULL, "_");
      continue;
    }

    std::string s(pch);
    int colon = s.find(':');

    if (colon == std::string::npos) {
      cm_msg(MERROR, __FUNCTION__, "Couldn't parse token '%s' in EPICS tolerance check '%s' (no colon)", s.c_str(), tol_check);
      return FE_ERR_ODB;
    }

    std::string what = s.substr(0, colon);
    std::string val_str = s.substr(colon + 1, std::string::npos);
    double val_num = 0;
    int pc = val_str.find("pc");
    bool is_pct = false;

    if (pc != std::string::npos) {
      is_pct = true;
      val_str = val_str.substr(0, pc);
    }

    if (val_str.compare("no_check") == 0) {
      pch = strtok(NULL, "_");
      continue;
    }

    if (val_str.compare("exact") == 0) {
      val_num = 0;
    } else {
      int scan_retval = sscanf(val_str.c_str(), "%lf", &val_num);

      if (scan_retval != 1) {
        cm_msg(MERROR, __FUNCTION__, "Couldn't parse token '%s' in EPICS tolerance check '%s' (sscanf failure for '%s')", s.c_str(), tol_check, val_str.c_str());
        return FE_ERR_ODB;
      }
    }

    if (what.compare("setpoint") == 0) {
      if (is_pct) {
        set_pct = val_num;
      } else {
        set_abs = val_num;
      }
    } else if (what.compare("meas") == 0) {
      if (is_pct) {
        meas_pct = val_num;
      } else {
        meas_abs = val_num;
      }
    } else if (what.compare("stability") == 0) {
      if (is_pct) {
        stab_pct = val_num;
      } else {
        stab_abs = val_num;
      }
    } else if (what.compare("timeout") == 0) {
      if (is_pct) {
        cm_msg(MERROR, __FUNCTION__, "Couldn't parse token '%s' in EPICS tolerance check '%s' (timeout written as pct)", s.c_str(), tol_check);
      } else {
        timeout_secs = val_num;
      }
    } else {
      cm_msg(MERROR, __FUNCTION__, "Couldn't parse token '%s' in EPICS tolerance check '%s' ('%s' didn't contain setpoint/meas/timeout)", s.c_str(), tol_check, what.c_str());
      return FE_ERR_ODB;
    }

    pch = strtok(NULL, "_");
  }

  return SUCCESS;
}

std::string ScanDeviceEpics::get_x_epics_var_human_name(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_x[index] == NULL) {
    return "UNKNOWN";
  }

  return human_names_x[index];
}

float ScanDeviceEpics::get_x_epics_var_set(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_x[index] == NULL) {
    return -99999;
  }

  return epics_vars_x[index]->get_set_value();
}

float ScanDeviceEpics::get_x_epics_var_demand(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_x[index] == NULL) {
    return -99999;
  }

  return epics_vars_x[index]->get_demand_value();
}

float ScanDeviceEpics::get_x_epics_var_measured(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_x[index] == NULL) {
    return -99999;
  }

  return epics_vars_x[index]->get_measured_value();
}

std::string ScanDeviceEpics::get_y_epics_var_human_name(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_y[index] == NULL) {
    return "UNKNOWN";
  }

  return human_names_y[index];
}

float ScanDeviceEpics::get_y_epics_var_set(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_y[index] == NULL) {
    return -99999;
  }

  return epics_vars_y[index]->get_set_value();
}

float ScanDeviceEpics::get_y_epics_var_demand(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_y[index] == NULL) {
    return -99999;
  }

  return epics_vars_y[index]->get_demand_value();
}

float ScanDeviceEpics::get_y_epics_var_measured(int index) {
  if (index < 0 || index > EPICS_SCAN_MAXVARS || epics_vars_y[index] == NULL) {
    return -99999;
  }

  return epics_vars_y[index]->get_measured_value();
}

