#ifndef EPICS_H
#define EPICS_H

#include "midas.h"
#include <string>
#include <map>
#include <vector>

// The DUMMY_EPICS flag can be set if you want to test the
// logic of this class without actually connecting to EPICS.
#ifdef DUMMY_EPICS
typedef int chid;
#else
#include "cadef.h"
#endif

// Helper typedef for a function that can be called to check if a
// measured value is within tolerance of a set value.
typedef bool (*epics_tolerance_check)(float set_value, float readback_value, float measured_value);

/*
 * Trivial class that wraps connecting/disconnecting from EPICS.
 */
class EpicsAccess {
  public:
    // Connect to EPICS.
    EpicsAccess();

    // Disconnect from EPICS.
    ~EpicsAccess();
};

/* Object-based interface for interacting with a device via EPICS.
 * This version allows you to specify a list of variables that will
 * have their values read/set at the same time.
 *
 * Talking to EPICS can introduce significant latency, so batching all
 * requests into one poll() can dramatically improve performance
 * compared to individual requests for each channel.
 *
 * This class abstracts away all the pain of talking to EPICS into a
 * simple interface.
 *
 * There are more specialized classes further down this file for specific
 * scenarios:
 * * EpicsSingleVar - if you only care about a single variable, you can
 *    avoid dealing with vectors/maps.
 * * EpicsMeasDemandVar - if you have two related variables for setting
 *    (demanding) a value and reading back a true measured value.
 */
class EpicsMultiVar {
  public:
    EpicsMultiVar(std::vector<std::string> pv_names);
    virtual ~EpicsMultiVar();

    // Set new values in EPICS. Key is the PV name,
    // values are the new values.
    INT set(std::map<std::string, float> new_vals);

    // Read new values. Call get_read_values() later
    // to actually get the values.
    INT read();

    // Call read() first.
    // Keyed by PV name.
    std::map<std::string, float> get_read_values();

    // Get the values we have sent to EPICS.
    // Keyed by PV name.
    std::map<std::string, float> get_set_values();

    // Whether to print debug statements to screen or not.
    void set_debug(bool _debug) {debug = _debug;}

#ifndef DUMMY_EPICS
    // Function called when we've connected to an EPICS channel.
    void connection_handler(struct connection_handler_args args);

    // Function called when we've read a value from EPICS.
    void read_event_handler(struct event_handler_args args);

    // Function called when we've written a value to EPICS.
    void write_event_handler(struct event_handler_args args);
#endif

  protected:
    // Open any channels that are not currently open.
    INT open_channels_if_needed();

    bool debug;

    // Internal state of each channel we're talking to.
    struct EpicsChanState {
      float set_value;
      float read_value;
      chid channel_id;
      bool connect_pending;
      bool read_pending;
      bool write_pending;
    };

    std::map<std::string, EpicsChanState> pv_states;
};

/*
 * A specialized class to simplify interacting with a single
 * EPICS variable.
 */
class EpicsSingleVar : public EpicsMultiVar {
  public:
    EpicsSingleVar(std::string _pv_name);
    virtual ~EpicsSingleVar();

    INT set(float new_val);
    INT read();
    float get_read_value();

  protected:
    std::string pv_name;
};

/*
 * A specialized class for interacting with a pair of EPICS PVs that
 * represent the requested (demand) value of a device and the true
 * readback (measured value).
 *
 * You can also specify tolerances to decide when the measured value is
 * acceptably close to the set value.
 */
class EpicsMeasDemandVar : public EpicsMultiVar {
  public:

    // Constructor. Provide the EPICS channel/device name to be set.
    // Optionally provide a related name that contains the measured value
    // of the same physical device.
    EpicsMeasDemandVar(std::string _device_name, std::string _measured_device_name = "");
    virtual ~EpicsMeasDemandVar();

    // Whether a "measured" device has been configured.
    bool has_measurable();

    // Get the name of this device (either the set/demand device or the measured device).
    std::string get_name(bool demand=true);

    // Set a new value, and wait until the measured value is within tolerance.
    // The timeout for giving up waiting is set by `check_timeout_secs` member variable.
    //
    // There are 2 codes to look at:
    // - The return value states whether midas/EPICS had a problem.
    // - ok states whether the device is within tolerance.
    INT set_value_and_wait_for_tolerance(float new_value, bool& ok);

    // Simply set a new demand value.
    INT set(float new_value);

    // Simply read the current set value (and measured value if appropriate).
    // Call get_set_value() etc to actually see the values that were read.
    INT read();

    float get_set_value(); // Call read() first
    float get_demand_value(); // Call read() first
    float get_measured_value(); // Call read() first
    bool is_within_tolerance(); // Call read() first
    bool is_stable(float previous_measured_value); // Call read() first

    // Set absolute or percentage tolerances that should be met to consider
    // a measured value as being within tolerance of a set value.
    void set_tolerances(double setpoint_abs, double measured_abs, double setpoint_pct, double measured_pct, double timeout_secs);

    // Set absolute or percentage tolerances that should be met to consider
    // a measured value as being stable (when comparing one readback value
    // to the previous one).
    void set_stability_tolerances(double abs, double pct);

    // Set a custom function for checking whether a measured value is within
    // tolerance of a set value, or cases where a simple absolute/percentage
    // value just isn't sufficient.
    void set_custom_tolerance_check_function(epics_tolerance_check fn);

  private:

    // EPICS name of main device.
    std::string device_name;

    // EPICS name of readback/measured device.
    std::string measured_device_name;

    // Custom tolerance check (a function pointer or NULL).
    epics_tolerance_check custom_check_fn_pointer;

    // Absolute/percentage thresholds for checking demand device.
    double check_setpoint_abs;
    double check_setpoint_pct;

    // Absolute/percentage thresholds for checking measured device.
    double check_measured_abs;
    double check_measured_pct;

    // Timeout for waiting to be in tolerance.
    double check_timeout_secs;

    // Absolute/percentage thresholds for comparing measured value
    // to a previously-measured value.
    double stability_measured_abs;
    double stability_measured_pct;
};


#endif
