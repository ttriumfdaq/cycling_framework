#ifndef SCAN_DEVICE_EPICS_H
#define SCAN_DEVICE_EPICS_H

#include "midas.h"
#include "epics.h"
#include "core/scannable_device.h"
#include <map>
#include <string>

#define EMPTY_STR_10 "[64] \n\
[64] \n\
[64] \n\
[64] \n\
[64] \n\
[64] \n\
[64] \n\
[64] \n\
[64] \n\
[64] \n"

#define EMPTY_STR_100 EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10

#define EMPTY_STR_200 EMPTY_STR_100 \
EMPTY_STR_100

#define EPICS_DEF_MAXVARS 200

#define EPICS_DEFINES_STR \
"Human name = STRING[200] : \n" \
EMPTY_STR_200 "\
Demand device = STRING[200] : \n" \
EMPTY_STR_200 "\
Measured device = STRING[200] : \n" \
EMPTY_STR_200 "\
Custom threshold check fn = STRING[200] : \n" \
EMPTY_STR_200 "\
"

typedef struct {
  char name[EPICS_DEF_MAXVARS][64];
  char demand_device[EPICS_DEF_MAXVARS][64];
  char measured_device[EPICS_DEF_MAXVARS][64];
  char threshold_fn_name[EPICS_DEF_MAXVARS][64];
} EPICS_DEFINES_STRUCT;

#define EPICS_SCAN_MAXVARS 20

#define EPICS_SCAN_SETTINGS_STR "\
Validate readback = BOOL : y\n\
X devices = INT[20] : \n\
[0] -1 \n\
[1] -1 \n\
[2] -1 \n\
[3] -1 \n\
[4] -1 \n\
[5] -1 \n\
[6] -1 \n\
[7] -1 \n\
[8] -1 \n\
[9] -1 \n\
[10] -1 \n\
[11] -1 \n\
[12] -1 \n\
[13] -1 \n\
[14] -1 \n\
[15] -1 \n\
[16] -1 \n\
[17] -1 \n\
[18] -1 \n\
[19] -1 \n\
Y devices = INT[20] : \n\
[0] -1 \n\
[1] -1 \n\
[2] -1 \n\
[3] -1 \n\
[4] -1 \n\
[5] -1 \n\
[6] -1 \n\
[7] -1 \n\
[8] -1 \n\
[9] -1 \n\
[10] -1 \n\
[11] -1 \n\
[12] -1 \n\
[13] -1 \n\
[14] -1 \n\
[15] -1 \n\
[16] -1 \n\
[17] -1 \n\
[18] -1 \n\
[19] -1 \n\
X start = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y start = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
X end = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y end = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
"

typedef struct {
  BOOL validate_readback;
  INT x_devices[EPICS_SCAN_MAXVARS];
  INT y_devices[EPICS_SCAN_MAXVARS];
  float x_start[EPICS_SCAN_MAXVARS];
  float y_start[EPICS_SCAN_MAXVARS];
  float x_end[EPICS_SCAN_MAXVARS];
  float y_end[EPICS_SCAN_MAXVARS];
} EPICS_SCAN_SETTINGS_STRUCT;


#define EPICS_SCAN_STATUS_STR "\
X demand values = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
X measured values = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y demand values = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y measured values = FLOAT[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
"

typedef struct {
  float x_demand[EPICS_SCAN_MAXVARS];
  float x_measured[EPICS_SCAN_MAXVARS];
  float y_demand[EPICS_SCAN_MAXVARS];
  float y_measured[EPICS_SCAN_MAXVARS];
} EPICS_SCAN_STATUS_STRUCT;


typedef struct {
  epics_tolerance_check fn;
  double stability_abs;
  double stability_pct;
} EpicsToleranceCheck;

class ScanDeviceEpics: public ScannableDevice {
  public:
    ScanDeviceEpics(HNDLE _hDB);

    virtual ~ScanDeviceEpics() {};

    void add_custom_tolerance_function(std::string name, EpicsToleranceCheck details) {
      tolerance_fns[name] = details;
    }

    // If creating ODB records for the first time, add this defintion.
    void add_bootstrap_definition(std::string human_name, std::string demand_device, std::string measured_device, std::string tol_check);

    virtual INT check_records() override;
    virtual INT open_records() override;
    virtual INT close_records() override;

    virtual INT begin_of_run(char* error) override;

    virtual INT setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) override;
    virtual INT is_next_step_ready(bool& ok) override;
    virtual std::string explain_unreadiness() override;

    virtual void compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) override;

    virtual INT fill_end_of_cycle_banks(char* pevent) override;

    // Special EPICS-only function for checking if readback values are stable
    INT scanned_vars_are_stable(bool& ok);

    std::string get_x_epics_var_human_name(int index);
    float get_x_epics_var_set(int index);
    float get_x_epics_var_demand(int index);
    float get_x_epics_var_measured(int index);
    std::string get_y_epics_var_human_name(int index);
    float get_y_epics_var_set(int index);
    float get_y_epics_var_demand(int index);
    float get_y_epics_var_measured(int index);

  private:
    INT parse_tolerances(char* tol_check, double& set_abs, double& meas_abs, double& set_pct, double& meas_pct, double& stab_abs, double& stab_pct, double& timeout_secs);
    void print_sweep(std::vector<float>& values, EpicsMeasDemandVar* var, bool is_x, int idx);
    EpicsMeasDemandVar* build_epics_var(int idx);

    std::map<std::string, EpicsToleranceCheck> tolerance_fns;

    EPICS_DEFINES_STRUCT epics_defines;
    EPICS_SCAN_SETTINGS_STRUCT user_settings;
    EPICS_SCAN_STATUS_STRUCT scan_status;

    std::vector<float> sweep_x[EPICS_SCAN_MAXVARS];
    std::vector<float> sweep_y[EPICS_SCAN_MAXVARS];

    EpicsAccess epics_access;
    EpicsMeasDemandVar* epics_vars_x[EPICS_SCAN_MAXVARS];
    EpicsMeasDemandVar* epics_vars_y[EPICS_SCAN_MAXVARS];
    std::string human_names_x[EPICS_SCAN_MAXVARS];
    std::string human_names_y[EPICS_SCAN_MAXVARS];

    typedef struct {
      std::string name;
      std::string demand;
      std::string measured;
      std::string tol_check;
    } BootstrapDefinition;

    std::vector<BootstrapDefinition> bootstrap_defs;
};

#endif
