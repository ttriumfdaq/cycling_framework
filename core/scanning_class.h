#ifndef SCANNING_H
#define SCANNING_H

#include "midas.h"
#include "data_source.h"
#include "scannable_device.h"
#include "flippable_device.h"
#include "cycle_controller.h"
#include "begin_of_run_device.h"
#include <map>

#define STATUS_ALL_LOOPS_COMPLETE 980001

// Global scan settings.
#define SCAN_SETTINGS_STR "\
Enable X = BOOL : n\n\
Enable Y = BOOL : n\n\
nX steps = INT : 0\n\
nY steps = INT : 0\n\
Type comment = STRING : [128] 0: Sawtooth, 1: Triangle, 2: Random\n\
X type = INT : 0\n\
Y type = INT : 0\n\
Step setup timeout (s) = FLOAT : 10\n\
Stop after N loops = INT : 0\n\
Stop after this loop = BOOL : n\n\
N cycles per step = INT : 1\n\
Debug = BOOL : n\n\
Enable constant time mode = BOOL : n\n\
Failure action comment = STRING : [256] 0: continue, 1: repeat cycle, 2: repeat step, 3: rollback step, 4: repeat scan\n\
Failure action (bad flip) = INT : 1\n\
Failure action (bad cnst time) = INT : 1\n\
"

// Global scan status.
#define SCAN_STATUS_STR "\
Loop count = INT : 0\n\
X step = INT : 0\n\
Y step = INT : 0\n\
Step repetition = INT : 0\n\
Total cycle count = INT : 0\n\
Num bad flips = INT : 0\n\
Num constant time failures = INT : 0\n\
"

// Global scan settings.
typedef struct {
  BOOL enable_x;
  BOOL enable_y;
  INT nX;
  INT nY;
  char type_comment[128];
  INT sweep_type_x;
  INT sweep_type_y;
  float setup_timeout_s;
  INT stop_after_n_loops;
  BOOL stop_after_this_loop;
  INT cycles_per_supercycle;
  BOOL debug;
  BOOL constant_time_mode;
  char next_cycle_logic_comment[256];
  INT next_cycle_logic_if_bad_flip;
  INT next_cycle_logic_if_const_time_failure;
} SCAN_SETTINGS_STRUCT;

// Global scan status.
typedef struct {
  int loop_count;
  int x_step;
  int y_step;
  int step_repetition;
  int cycle_count;
  int num_bad_flip;
  int num_const_time_failure;
} SCAN_STATUS_STRUCT;

typedef enum {
  FSEveryCycle,
  FSEveryOtherCycle,
  FSEverySuperCycle,
  FSEveryOtherSuperCycle,
  FSEveryScan,
  FSEveryOtherScan
} FlipStrategy;

typedef enum {
  FEWAlways,
  FEWOnlyTrue,
  FEWOnlyFalse
} FlipEventWritingStrategy;

typedef void (*ExtraMetadataBankFn)(char*);

/*
 * Main class that is at the heart of a PPG-based DAQ that can scan/change
 * certain parameters between cycles.
 *
 * The general concept is that the main logic for each experiment is the
 * exactly the same (this class), you just define the things to be scanned and
 * the data to be read out by calling functions in this class.
 *
 * It is currently single-threaded, and is not designed for reading out devices
 * with lots of data. If such a device is needed, you should probably create a
 * separate FE for that device, tell it when cycles are over via ODB hotlinks
 * or RPC calls, and add an event builder to combine the data.
 *
 * It supports no scanning, 1-D scanning, and 2-D scanning. In a 2-D scan, a
 * group of variables are scanned through, then another group are increment,
 * and the first group are scanned through again.
 */
class Scanner {
  public:
    Scanner(HNDLE _hDB, CycleController* _controller);

    // Add a device that handles scanning. Examples would be a PPG, EPICS,
    // waveform generator etc.
    void add_scan_device(std::string name, ScannableDevice* device);

    // Add a data source that should be read out at the end of
    // each cycle. The state of the scan and the data from each
    // data source are all written to the same event.
    void add_data_source(std::string name, DataSource* data_source);

    // Add a device that just does stuff at begin/end of a run.
    // If bor_first is true, this device will get the begin_of_run()
    // call before all the other; otherwise it will get it last.
    void add_begin_of_run_device(std::string name, BeginOfRunDevice* device, bool bor_first);

    // Add a device that is toggled between two states during a run.
    void add_flippable_device(std::string name, FlippableDevice* device);

    // Set how a flippable device affects the scanning behaviour.
    void set_flippable_device_timing(std::string name, FlipStrategy flip_strategy, FlipEventWritingStrategy event_strategy);

    // Set whether we should write each cycle in a supercycle individually,
    // or combine all into one readout.
    void set_combine_data_from_all_cycles_in_supercycle(bool combine) {
      combine_supercycle = combine;
    }

    // Delay sending data to midas banks until the next cycle has already
    // started. Can reduce deadtime if sending data is a bottleneck.
    // Sometimes you may want to split the data across multiple events with
    // different trigger masks; num_chunks specifies the number of events.
    void enable_delay_banks_until_next_cycle(bool enable, int num_chunks=1);

    // Call db_check_record() for this class and all scan devices / data
    // sources.
    INT check_records();

    // Call db_open_record() for this class and all scan devices / data
    // sources.
    INT open_records(char* error);

    // Call db_close_record() for this class and all scan devices / data
    // sources.
    INT close_records();

    // Open ODB records and prepare for first cycle.
    INT begin_of_run();
    INT begin_of_run(char* error);

    // Stop the current cycle and close any open records.
    INT end_of_run();

    // Called frequently to see if we should send an event to midas.
    // Checks whether the PPG cycle is over; if so, checks whether
    // all the data sources have finished their acquisition.
    bool is_cycle_finished_and_data_acquired(BOOL for_mfe_test);

    // Cause the PPG to start.
    INT start_cycle();

    // Cause the PPG to stop.
    INT stop_cycle();

    // Create a midas event containing the scan state and any data from data
    // sources, then make any changes needed for the next cycle.
    INT readout_and_setup_next_cycle(char *pevent);

    // Number of cycles we'll do at each scan point before incrementing.
    // aka length of a "super cycle".
    INT get_num_reps_per_scan_point() {
      return user_settings.cycles_per_supercycle;
    }

    void set_repeat_first_cycle(bool repeat) {
      repeat_first_cycle = repeat;
    }

    void set_num_flip_states_in_supercycle(int n) {
      num_flip_states_in_supercycle = n;
    }

    // Set function that can fill more banks in the event.
    void set_extra_metadata_bank_fn(ExtraMetadataBankFn fn) {
      extra_metadata_bank_fn = fn;
    }

    SCAN_STATUS_STRUCT get_status() {
      return scan_status;
    }

    bool is_x_scan_enabled() {
      return user_settings.enable_x;
    }

    bool is_y_scan_enabled() {
      return user_settings.enable_y;
    }

    bool is_x_scan_randomized() {
      return user_settings.sweep_type_x == SweepRandom;
    }

    bool is_y_scan_randomized() {
      return user_settings.sweep_type_y == SweepRandom;
    }

    bool is_x_scan_triangle() {
      return user_settings.sweep_type_x == SweepUpThenDown;
    }

    bool is_y_scan_triangle() {
      return user_settings.sweep_type_y == SweepUpThenDown;
    }

  private:
    // Helper function to increment/rollover X/Y step counters.
    // begin_of_run flag handles initial setup.
    // new_x/new_y flags populated based on whether we're starting a new X/Y
    // step.
    //
    // Sometimes x/y steps need to be preset/reset to certain values.
    // You can indicate that you've done this with the xysteps_already_set
    // flag. We will still set new_x/new_y, but not do any ++ing.
    INT increment_step_counters(bool begin_of_run, bool& new_x, bool& new_y, bool xysteps_already_set);

    // Main function that sets up equipment for the next cycle. Doesn't
    // actually start the cycle, just makes any changes to PPG loadfile, EPICS
    // variables etc.
    INT setup_next_step(bool begin_of_run);

    // Create midas event containing scan state and data from data sources for
    // the cycle that just finished.
    INT create_event(char *pevent, bool skip_data_sources=false);

    INT check_if_event_should_be_written(bool& write_event);

    INT check_flip_device_writing_strategies(bool& write_event);

    INT flip_flippable_devices_if_needed();

    // ODB handle.
    HNDLE hDB;

    // Special logic between begin-of-run and starting first cycle.
    bool first_call_of_run;

    // Whether we're currently waiting for data from the data sources
    // (i.e. cycle is finished, but data collection not yet finished).
    bool waiting_for_data;

    // Whether we want to stop the run.
    bool request_stop_run;

    // Whether we've asked for the run to stop. Together with the above flag,
    // this system helps us to not call stop_run() multiple times.
    bool run_stopping;

    // Whether we've called db_open_record(), and thus should call
    // db_close_record() later.
    bool records_opened;

    // Whether we need to repeat the current step.
    NextCycleSetupLogic next_cycle_setup_logic;

    //
    INT num_cycles_to_skip_writing;

    // Object that can report/edit cycle state.
    CycleController* cycle_controller;

    // Objects that can change scan parameters (and report cycle state).
    std::map<std::string, ScannableDevice*> scan_devices;

    // Objects that can toggle things on and off.
    std::map<std::string, FlippableDevice*> flip_devices;

    // Objects that just do stuff at begin of run.
    std::map<std::string, BeginOfRunDevice*> bor_devices;
    std::map<std::string, bool> bor_device_tr_first;

    // How the flip devices affect scanning operation.
    std::map<std::string, FlipStrategy> flip_when;
    std::map<std::string, FlipEventWritingStrategy> flip_event_writing;

    // Objects that provide data.
    std::map<std::string, DataSource*> data_sources;

    // Global scan settings.
    SCAN_SETTINGS_STRUCT user_settings;

    // Global scan status.
    SCAN_STATUS_STRUCT scan_status;

    // Whether we previously were scanning the X variables in increasing or
    // decreasing order.
    DirectionType previous_direction_type_x;

    // Whether we previously were scanning the Y variables in increasing or
    // decreasing order.
    DirectionType previous_direction_type_y;

    // Don't wait for banks to be sent to midas before starting the
    // next cycle. Instead, send them while the next cycle is running.
    bool delay_data_banks_until_next_cycle;

    // Number of data chunks we'll send in total (normally just 1)
    int num_data_chunks;

    // Current data chunk index if we're chunking the data.
    int curr_data_chunk;

    // If delay_banks_until_next_cycle is true, whether we still need
    // to send the data from the previous cycle.
    bool delayed_data_banks_to_send;

    // Whether to repeat the first cycle.
    bool repeat_first_cycle;

    // Whether this is the first cycle of the run.
    bool first_cycle;

    bool combine_supercycle;

    int num_flip_states_in_supercycle;

    bool is_new_supercycle;
    bool is_new_loop;

    bool stop_after_final_delayed_banks_sent;

    ExtraMetadataBankFn extra_metadata_bank_fn;
};


#endif
