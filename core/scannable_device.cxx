#include "scannable_device.h"
#include "midas.h"
#include <algorithm>

ScannableDevice::ScannableDevice(HNDLE _hDB) {
  hDB = _hDB;
  x_enabled = false;
  y_enabled = false;
  nX = 0;
  nY = 0;
  debug = false;
}

void ScannableDevice::set_xy_enabled(bool x, bool y, int nx, int ny) {
  x_enabled = x;
  y_enabled = y;
  nX = nx;
  nY = ny;
}

std::vector<double> ScannableDevice::compute_sweep_double(DirectionType direction_type, double start, double end, int n_steps) {
  double incr = (end - start) / n_steps;
  std::vector<double> retval(n_steps+1);

  for (int i = 0; i <= n_steps; i++) {
    retval[i] = start + (i * incr);
  }

  reorder_sweep(direction_type, retval);

  return retval;
}

std::vector<float> ScannableDevice::compute_sweep_float(DirectionType direction_type, float start, float end, int n_steps) {
  float incr = (end - start) / n_steps;
  std::vector<float> retval(n_steps+1);

  for (int i = 0; i <= n_steps; i++) {
    retval[i] = start + (i * incr);
  }

  reorder_sweep(direction_type, retval);

  return retval;
}

std::vector<int> ScannableDevice::compute_sweep_int(DirectionType direction_type, int start, int end, int n_steps) {
  float incr = (end - start) / n_steps;
  std::vector<int> retval(n_steps+1);

  for (int i = 0; i <= n_steps; i++) {
    retval[i] = start + (i * incr);
  }

  reorder_sweep(direction_type, retval);

  return retval;
}

void ScannableDevice::set_debug_enabled(bool _debug) {
  debug = _debug;
}

template <class T> void ScannableDevice::reorder_sweep(DirectionType direction_type, std::vector<T>& increasing_vals) {
  if (direction_type == DirDecreasing) {
    std::reverse(increasing_vals.begin(), increasing_vals.end());
  } else if (direction_type == DirRandom) {
    std::random_shuffle(increasing_vals.begin(), increasing_vals.end());
  }
}
