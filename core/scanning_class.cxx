#include "scanning_class.h"
#include "midas.h"
#include "scan_utils.h"
#include "scannable_device.h"
#include <cstdio>
#include <stdlib.h>
#include <sys/time.h>
#include <vector>


#define GLOBAL_SETTINGS_PATH "/Scanning/Global/Settings"
#define GLOBAL_STATUS_PATH "/Scanning/Global/Status"


Scanner::Scanner(HNDLE _hDB, CycleController* _controller) {
  hDB = _hDB;
  waiting_for_data = false;
  first_call_of_run = false;
  previous_direction_type_x = DirDecreasing;
  previous_direction_type_y = DirDecreasing;
  request_stop_run = false;
  run_stopping = false;
  records_opened = false;
  delay_data_banks_until_next_cycle = false;
  delayed_data_banks_to_send = false;
  num_data_chunks = 1;
  curr_data_chunk = 0;
  next_cycle_setup_logic = NCLContinue;
  num_flip_states_in_supercycle = 1;
  first_cycle = true;
  repeat_first_cycle = false;
  is_new_loop = true;
  is_new_supercycle = true;
  num_cycles_to_skip_writing = 0;
  extra_metadata_bank_fn = NULL;
  cycle_controller = _controller;
  combine_supercycle = false;
  stop_after_final_delayed_banks_sent = false;
}

void Scanner::enable_delay_banks_until_next_cycle(bool enable, int num_chunks) {
  delay_data_banks_until_next_cycle = enable;
  num_data_chunks = num_chunks;
  curr_data_chunk = 0;
}

INT Scanner::check_records() {
  if (user_settings.debug) {
    scan_utils::ts_printf("Check records\n");
  }

  INT status;
  char settings_path[256], status_path[256];
  sprintf(settings_path, GLOBAL_SETTINGS_PATH);
  sprintf(status_path, GLOBAL_STATUS_PATH);

  status = db_check_record(hDB, 0, settings_path, SCAN_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", settings_path);
    return status;
  }

  status = db_check_record(hDB, 0, status_path, SCAN_STATUS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", status_path);
    return status;
  }

  status = cycle_controller->check_records();

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "%s", "Failed to check records for cycle controller");
    return status;
  }

  for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
    status = it->second->check_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check records for scan device %s", it->first.c_str());
      return status;
    }
  }

  for (auto it = bor_devices.begin(); it != bor_devices.end(); it++) {
    status = it->second->check_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check records for begin-of-run device %s", it->first.c_str());
      return status;
    }
  }

  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    status = it->second->check_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check records for flip device %s", it->first.c_str());
      return status;
    }
  }

  for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
    status = it->second->check_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check records for data source %s", it->first.c_str());
      return status;
    }
  }

  return SUCCESS;
}

INT Scanner::open_records(char* error) {
  char settings_path[256], status_path[256];
  sprintf(settings_path, GLOBAL_SETTINGS_PATH);
  sprintf(status_path, GLOBAL_STATUS_PATH);
  HNDLE h_settings, h_status;
  INT size_settings = sizeof(user_settings);
  INT size_status = sizeof(scan_status);
  INT status;

  status = check_records();

  if (status != SUCCESS) {
    return status;
  }

  records_opened = true;
  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_get_record(hDB, h_settings, &user_settings, &size_settings, 0);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to get record for %s", settings_path);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  if (user_settings.debug) {
    scan_utils::ts_printf("Opening records\n");
  }

  status = db_get_record(hDB, h_status, &scan_status, &size_status, 0);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to get record for %s", status_path);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  status = db_open_record(hDB, h_settings, &user_settings, size_settings, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to open Global settings record");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  status = db_open_record(hDB, h_status, &scan_status, size_status, MODE_WRITE, NULL, NULL);

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to open Global status record");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  cycle_controller->set_debug_enabled(user_settings.debug);
  status = cycle_controller->open_records();

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to open records for cycle controller");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
    it->second->set_debug_enabled(user_settings.debug);
    status = it->second->open_records();

    if (status != SUCCESS) {
      snprintf(error, 255, "Failed to open records for scan device %s", it->first.c_str());
      cm_msg(MERROR, __FUNCTION__, "%s", error);
      return status;
    }
  }

  for (auto it = bor_devices.begin(); it != bor_devices.end(); it++) {
    status = it->second->open_records();

    if (status != SUCCESS) {
      snprintf(error, 255, "Failed to open records for begin-of-run device %s", it->first.c_str());
      cm_msg(MERROR, __FUNCTION__, "%s", error);
      return status;
    }
  }

  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    it->second->set_debug_enabled(user_settings.debug);

    status = it->second->open_records();

    if (status != SUCCESS) {
      snprintf(error, 255, "Failed to open records for flip device %s", it->first.c_str());
      cm_msg(MERROR, __FUNCTION__, "%s", error);
      return status;
    }
  }

  for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
    status = it->second->open_records();

    if (status != SUCCESS) {
      snprintf(error, 255, "Failed to open records for data source %s", it->first.c_str());
      cm_msg(MERROR, __FUNCTION__, "%s", error);
      return status;
    }
  }

  if (user_settings.debug) {
    scan_utils::ts_printf("Opened records\n");
  } else {
    scan_utils::ts_printf("Opened records (but debug mode is currently disabled)\n");
  }

  return status;
}

INT Scanner::close_records() {
  if (user_settings.debug) {
    scan_utils::ts_printf("Closing any open records\n");
  }

  if (!records_opened) {
    return SUCCESS;
  }

  char settings_path[256], status_path[256];
  sprintf(settings_path, GLOBAL_SETTINGS_PATH);
  sprintf(status_path, GLOBAL_STATUS_PATH);
  HNDLE h_settings, h_status;
  INT status;

  records_opened = false;
  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_close_record(hDB, h_settings);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close Global settings record");
    return status;
  }

  status = db_close_record(hDB, h_status);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close Global status record");
    return status;
  }

  status = cycle_controller->close_records();

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close records for cycle controller");
    return status;
  }

  for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
    status = it->second->close_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to close records for scan device %s", it->first.c_str());
      return status;
    }
  }

  for (auto it = bor_devices.begin(); it != bor_devices.end(); it++) {
    status = it->second->close_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to close records for begin-of-run device %s", it->first.c_str());
      return status;
    }
  }

  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    status = it->second->close_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to close records for flip device %s", it->first.c_str());
      return status;
    }
  }

  for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
    status = it->second->close_records();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to close records for data source %s", it->first.c_str());
      return status;
    }
  }

  return status;
}

void Scanner::add_scan_device(std::string name, ScannableDevice* device) {
  scan_devices[name] = device;
}

void Scanner::add_begin_of_run_device(std::string name, BeginOfRunDevice* device, bool bor_first) {
  bor_devices[name] = device;
  bor_device_tr_first[name] = bor_first;
}

void Scanner::add_flippable_device(std::string name, FlippableDevice* device) {
  flip_devices[name] = device;

  if (flip_when.find(name) == flip_when.end()) {
    flip_when[name] = FSEveryCycle;
  }

  if (flip_event_writing.find(name) == flip_event_writing.end()) {
    flip_event_writing[name] = FEWAlways;
  }
}

void Scanner::set_flippable_device_timing(std::string name, FlipStrategy flip_strategy, FlipEventWritingStrategy readout_strategy) {
  flip_when[name] = flip_strategy;
  flip_event_writing[name] = readout_strategy;

  std::string flip_strat_str;

  if (flip_strategy == FSEveryCycle) {
    flip_strat_str = "every cycle";
  } else if (flip_strategy == FSEveryOtherCycle) {
    flip_strat_str = "every other cycle";
  } else if (flip_strategy == FSEverySuperCycle) {
    flip_strat_str = "every super-cycle";
  } else if (flip_strategy == FSEveryOtherSuperCycle) {
    flip_strat_str = "every other super-cycle";
  } else if (flip_strategy == FSEveryScan) {
    flip_strat_str = "every scan";
  } else if (flip_strategy == FSEveryOtherScan) {
    flip_strat_str = "every other scan";
  } else {
    flip_strat_str = "????";
  }

  std::string readout_strat_str;

  if (readout_strategy == FEWAlways) {
    readout_strat_str = "every cycle";
  } else {
    readout_strat_str = "every other cycle";
  }

  cm_msg(MINFO, __FUNCTION__, "Will flip %s %s and readout data %s", name.c_str(), flip_strat_str.c_str(), readout_strat_str.c_str());
}

void Scanner::add_data_source(std::string name, DataSource* data_source) {
  data_sources[name] = data_source;
}

INT Scanner::begin_of_run() {
  // Some older FE's call begin_of_run without an "error"
  // argument. We'll make a dummy error string for them.
  char error[256];
  return begin_of_run(error);
}

INT Scanner::begin_of_run(char* error) {
  if (user_settings.debug) {
    scan_utils::ts_printf("Begin of run\n");
  }

  INT status = open_records(error);

  if (status != SUCCESS) {
    return status;
  }

  if (user_settings.enable_y && !user_settings.enable_x) {
    snprintf(error, 255, "%s", "Can't enable Y scan without also enabling X scan");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  if (user_settings.enable_x && user_settings.nX <= 0) {
    snprintf(error, 255, "%s", "Specify > 0 X steps");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  if (user_settings.enable_y && user_settings.nY <= 0) {
    snprintf(error, 255, "%s", "Specify > 0 Y steps");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  if (user_settings.cycles_per_supercycle == 0) {
    snprintf(error, 255, "%s", "Specify > 0 cycles per step");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  if (num_flip_states_in_supercycle <= 0) {
    num_flip_states_in_supercycle = 1;
  }

  // BOR devices getting setup before all others
  for (auto it = bor_devices.begin(); it != bor_devices.end(); it++) {
    if (bor_device_tr_first[it->first]) {
      if (user_settings.debug) {
        scan_utils::ts_printf("Calling begin of run for begin-of-run device %s\n", it->first.c_str());
      }

      status = it->second->begin_of_run(error);

      if (status != SUCCESS) {
        return status;
      }
    }
  }

  // Cycle controller
  if (user_settings.debug) {
    scan_utils::ts_printf("Calling begin of run for cycle controller\n");
  }

  if (user_settings.constant_time_mode && !cycle_controller->supports_constant_time_mode()) {
    snprintf(error, 255, "%s", "Cycle controller doesn't support constant time mode!");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_DRIVER;
  }

  cycle_controller->enable_constant_time_mode(user_settings.constant_time_mode);

  status = cycle_controller->begin_of_run(error);

  if (status != SUCCESS) {
    return status;
  }

  // Scan devices
  for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling begin of run for scan device %s\n", it->first.c_str());
    }

    it->second->set_xy_enabled(user_settings.enable_x, user_settings.enable_y, user_settings.nX, user_settings.nY);
    status = it->second->begin_of_run(error);

    if (status != SUCCESS) {
      return status;
    }
  }

  // Flippable devices
  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling begin of run for flip device %s\n", it->first.c_str());
    }

    status = it->second->begin_of_run(error);

    if (status != SUCCESS) {
      return status;
    }
  }

  // Data sources
  for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling begin of run for data source %s\n", it->first.c_str());
    }

    status = it->second->begin_of_run(error);

    if (status != SUCCESS) {
      return status;
    }
  }

  // BOR devices getting setup after all others
  for (auto it = bor_devices.begin(); it != bor_devices.end(); it++) {
    if (!bor_device_tr_first[it->first]) {
      if (user_settings.debug) {
        scan_utils::ts_printf("Calling begin of run for begin-of-run device %s\n", it->first.c_str());
      }

      status = it->second->begin_of_run(error);

      if (status != SUCCESS) {
        return status;
      }
    }
  }

  if (user_settings.debug) {
    scan_utils::ts_printf("Setting up initial step\n");
  }

  scan_status.num_bad_flip = 0;
  scan_status.num_const_time_failure = 0;

  status = setup_next_step(true);

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to set up first step. See Messages for details.");
    return status;
  }

  first_call_of_run = true;
  request_stop_run = false;
  run_stopping = false;
  stop_after_final_delayed_banks_sent = false;
  first_cycle = true;

  if (user_settings.debug) {
    scan_utils::ts_printf("Begin-of-run complete\n");
  }

  if (repeat_first_cycle) {
    next_cycle_setup_logic = NCLRepeatCycle;
  }

  return SUCCESS;
}

INT Scanner::end_of_run() {
  if (user_settings.debug) {
    scan_utils::ts_printf("End of run\n");
  }

  INT status_stop = stop_cycle();
  INT status_close = close_records();

  INT status_eor = SUCCESS;

  // Scan devices
  for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling begin of run for scan device %s\n", it->first.c_str());
    }

    it->second->set_xy_enabled(user_settings.enable_x, user_settings.enable_y, user_settings.nX, user_settings.nY);
    INT status = it->second->end_of_run();

    if (status != SUCCESS) {
      status_eor = status;
    }
  }

  // Flippable devices
  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling begin of run for flip device %s\n", it->first.c_str());
    }

    INT status = it->second->end_of_run();

    if (status != SUCCESS) {
      status_eor = status;
    }
  }

  // Data sources
  for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling end of run for data source %s\n", it->first.c_str());
    }

    INT status = it->second->end_of_run();

    if (status != SUCCESS) {
      status_eor = status;
    }
  }

  // BOR devices
  for (auto it = bor_devices.begin(); it != bor_devices.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Calling end of run for end-of-run device %s\n", it->first.c_str());
    }

    INT status = it->second->end_of_run();

    if (status != SUCCESS) {
      status_eor = status;
    }
  }

  if (status_stop != SUCCESS) {
    return status_stop;
  }

  if (status_close != SUCCESS) {
    return status_close;
  }

  return status_eor;
}


INT Scanner::create_event(char *pevent, bool skip_data_sources) {
  if (user_settings.debug) {
    scan_utils::ts_printf("Creating readout event\n");
  }

  bk_init32(pevent);

  if (delayed_data_banks_to_send) {
    TRIGGER_MASK(pevent) = 1<<curr_data_chunk;
  } else {
    // Metadata
    INT* pdata_scan;
    bk_create(pevent, "SCAN", TID_INT, (void**) &pdata_scan);
    *pdata_scan++ = scan_status.loop_count;
    *pdata_scan++ = scan_status.x_step;
    *pdata_scan++ = scan_status.y_step;
    *pdata_scan++ = scan_status.step_repetition;
    *pdata_scan++ = scan_status.cycle_count;
    bk_close(pevent, pdata_scan);

    if (flip_devices.size()) {
      BOOL* pdata_flip;
      bk_create(pevent, "FLIP", TID_BOOL, (void**) &pdata_flip);
      for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
        bool state = false;
        it->second->get_state(state);
        *pdata_flip++ = state;
      }
      bk_close(pevent, pdata_flip);
    }

    for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
      it->second->fill_end_of_cycle_banks(pevent);
    }

    if (extra_metadata_bank_fn) {
      extra_metadata_bank_fn(pevent);
    }
  }

  if (!skip_data_sources) {
    // Actual data
    for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
      if (num_data_chunks > 1 && it->second->supports_chunking()) {
        // Data chunking is enabled, and this data source supports it
        it->second->write_data_chunk_to_banks(pevent, curr_data_chunk, num_data_chunks);
      } else if (curr_data_chunk == 0) {
        // No chunking. Only write unchunkable data in the first chunk.
        it->second->write_data_to_banks(pevent);
      }
    }
  }

  return bk_size(pevent);
}

bool Scanner::is_cycle_finished_and_data_acquired(BOOL for_mfe_test) {
  if (for_mfe_test) {
    // MFE calls poll_event during init process; don't do
    // any actual work at that point.
    return false;
  }

  if (user_settings.debug) {
    ss_sleep(1);
  }

  if (first_call_of_run) {
    // Special logic at start of run. We do the initial
    // setup in BOR, but returning true here will cause
    // the PPG cycle to actually be started.
    return true;
  }

  if (request_stop_run) {
    if (!run_stopping) {
      scan_utils::ts_printf("Need to stop the run\n");
      run_stopping = true;
      scan_utils::stop_the_run(TR_ASYNC);
    }
    return false;
  }

  if (delay_data_banks_until_next_cycle && delayed_data_banks_to_send) {
    // Special logic for case where we need to call "readout" again
    // so that delayed data is sent.
    if (user_settings.debug) {
      scan_utils::ts_printf("Waiting for delayed banks to be sent\n");
    }

    return true;
  }

  CycleState cycle_state = cycle_controller->get_cycle_progress();

  if (cycle_state == NotApplicable) {
    cm_msg(MERROR, __FUNCTION__, "Cycle controller didn't report the cycle state!");
    stop_cycle();
    exit(1);
  }

  if (user_settings.debug) {
    //scan_utils::ts_printf("cycle_state is currently %d\n", cycle_state);
  }

  if (cycle_state == NotStarted) {
    return false;
  }

  if (cycle_state == Running) {
    for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
      it->second->mid_cycle_read_data_if_needed();
    }

    return false;
  }

  bool need_to_start_readout = false;

  if (user_settings.constant_time_mode) {
    // Constant time mode
    if (cycle_state == ConstantTimeReadyForReadoutAndInDaqServiceTime) {
      need_to_start_readout = true;
    }
  } else {
    // Normal mode
    if (cycle_state == Finished) {
      need_to_start_readout = true;
    }
  }

  if (need_to_start_readout && !waiting_for_data) {
    for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
      it->second->this_cycle_finished();
    }

    waiting_for_data = true;
  }

  if (waiting_for_data) {
    bool all_ready = true;

    for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
      DataStatus data_status = it->second->end_cycle_is_data_ready();

      if (data_status == DataFatalError) {
        scan_utils::ts_printf("data source %s has fatal error - stop the run\n", it->first.c_str());
        request_stop_run = true;
        return false;
      } else if (data_status == DataNotReady) {
        all_ready = false;
      }
    }

    if (user_settings.debug && all_ready) {
      scan_utils::ts_printf("All data sources are now ready.\n");
    }

    waiting_for_data = !all_ready;
  }

  if (user_settings.constant_time_mode && !waiting_for_data && (cycle_state == ConstantTimeReadyForReadoutButNotInDaqServiceTime || cycle_state == Finished)) {
    cm_msg(MERROR, __FUNCTION__, "Need to repeat cycle, as DAQ was not finished processing when next cycle was due to start.");
    next_cycle_setup_logic = (NextCycleSetupLogic)user_settings.next_cycle_logic_if_const_time_failure;
    scan_status.num_const_time_failure++;
  }

  return !waiting_for_data;
}

INT Scanner::check_if_event_should_be_written(bool& write_event) {
  INT status = SUCCESS;
  bool intentional_skip = false;
  bool for_combining_supercycle = false;
  write_event = true;

  if (num_cycles_to_skip_writing) {
    num_cycles_to_skip_writing--;
    write_event = false;
    intentional_skip = true;
    scan_utils::ts_printf("Not writing this cycle. %d more cycles to skip.\n", num_cycles_to_skip_writing);
  }

  if (combine_supercycle && scan_status.step_repetition < user_settings.cycles_per_supercycle * num_flip_states_in_supercycle - 1) {
    write_event = false;
    for_combining_supercycle = true;
    scan_utils::ts_printf("Not writing yet, as this is rep %d and we're merging all reps into one readout.\n", scan_status.step_repetition);
  }

  // See if we should skip writing event at the moment, as we want
  // to combine data from multiple flip states into one event.
  status = check_flip_device_writing_strategies(write_event);

  if (status != SUCCESS) {
    return status;
  }

  status = cycle_controller->check_if_event_should_be_written(write_event, next_cycle_setup_logic, intentional_skip, for_combining_supercycle);

  if (status != SUCCESS) {
    return status;
  }

  if (user_settings.debug) {
    if (write_event) {
      scan_utils::ts_printf("This event should be written to banks.\n");
    } else {
      scan_utils::ts_printf("This event should NOT be written to banks.\n");
    }
  }

  return status;
}

INT Scanner::check_flip_device_writing_strategies(bool& write_event) {
  INT status = SUCCESS;

  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    if (!it->second->is_enabled()) {
      continue;
    }

    FlipEventWritingStrategy strat = flip_event_writing[it->first];

    if (strat == FEWOnlyTrue || strat == FEWOnlyFalse) {
      bool curr_state = false;
      status = it->second->get_state(curr_state);

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Invalid status trying to read state of %s: %d", it->first.c_str(), status);
        return status;
      }

      if (strat == FEWOnlyTrue && !curr_state) {
        if (user_settings.debug) {
          scan_utils::ts_printf("Skipping readout event as %s is currently false\n", it->first.c_str());
        }

        write_event = false;
      }

      if (strat == FEWOnlyFalse && curr_state) {
        if (user_settings.debug) {
          scan_utils::ts_printf("Skipping readout event as %s is currently true\n", it->first.c_str());
        }

        write_event = false;
      }
    }

    bool correct_state = false;
    status = it->second->is_in_expected_state(correct_state);

    if (status != SUCCESS || !correct_state) {
      cm_msg(MERROR, __FUNCTION__, "%s is in an unexpected state when trying to write data! status=%d, correct_state=%d", it->first.c_str(), status, correct_state);

      write_event = false;
      next_cycle_setup_logic = (NextCycleSetupLogic)user_settings.next_cycle_logic_if_bad_flip;
      scan_status.num_bad_flip++;
      break;
    }
  }

  return status;
}

INT Scanner::increment_step_counters(bool begin_of_run, bool& new_x, bool& new_y, bool xysteps_already_set) {
  if (user_settings.debug) {
    scan_utils::ts_printf("Incrementing step counters. Begin of run logic? %d\n", begin_of_run);
  }

  bool compute_sweep_x = false;
  bool compute_sweep_y = false;

  // Initialize / increment step counts
  if (begin_of_run) {
    compute_sweep_x = true;
    compute_sweep_y = true;
    new_x = true;
    new_y = true;
    scan_status.loop_count = 0;
    scan_status.x_step = user_settings.enable_x ? 0 : -1;
    scan_status.y_step = user_settings.enable_y ? 0 : -1;
    scan_status.step_repetition = 0;
    scan_status.num_const_time_failure = 0;
    scan_status.num_bad_flip = 0;
    previous_direction_type_x = DirDecreasing;
    previous_direction_type_y = DirDecreasing;
  } else {
    bool full_loop_finished = false;
    new_x = false;
    new_y = false;

    if (!xysteps_already_set) {
      scan_status.step_repetition++;
    }

    if (user_settings.cycles_per_supercycle * num_flip_states_in_supercycle > 1 && scan_status.step_repetition < user_settings.cycles_per_supercycle * num_flip_states_in_supercycle) {
      if (user_settings.debug) {
        scan_utils::ts_printf("Repeating same step for repetition %d (will do %d cycles total)\n", scan_status.step_repetition, user_settings.cycles_per_supercycle * num_flip_states_in_supercycle);
      }
    } else {
      scan_status.step_repetition = 0;
      is_new_supercycle = true;

      // Y is inner loop - go through all those values before incrementing X
      if (user_settings.enable_x) {
        if (user_settings.enable_y) {
          if (!xysteps_already_set) {
            scan_status.y_step++;
          }

          new_y = true;

          if (scan_status.y_step == user_settings.nY + 1) {
            scan_status.y_step = 0;
            scan_status.x_step++;
            new_x = true;
            compute_sweep_y = true;
          }
        } else {
          if (!xysteps_already_set) {
            scan_status.x_step++;
          }

          new_x = true;
        }
      } else {
        full_loop_finished = true;
      }

      if (scan_status.x_step == user_settings.nX + 1) {
        scan_status.x_step = 0;
        full_loop_finished = true;
        compute_sweep_x = true;
      }
    }

    if (full_loop_finished) {
      scan_status.loop_count++;
      is_new_loop = true;

      if (user_settings.stop_after_this_loop) {
        cm_msg(MINFO, __FUNCTION__, "Stopping the run, as user requested we stop after this loop");
        return STATUS_ALL_LOOPS_COMPLETE;
      }

      if (user_settings.stop_after_n_loops > 0 && scan_status.loop_count == user_settings.stop_after_n_loops) {
        cm_msg(MINFO, __FUNCTION__, "Stopping the run, as we've completed all %d loops", user_settings.stop_after_n_loops);
        return STATUS_ALL_LOOPS_COMPLETE;
      }
    }
  }

  // Explicitly calculate the cycle count.
  // We don't just do scan_status.cycle_count++, as we may have rolled-back / repeated
  // some cycles.
  int loop_len = 1;
  int offset_x = 0;
  int offset_y = 0;

  if (user_settings.enable_x) {
    loop_len *= (user_settings.nX + 1);
    offset_x = scan_status.x_step;
  }
  if (user_settings.enable_y) {
    loop_len *= (user_settings.nY + 1);
    offset_x *= (user_settings.nY + 1);
    offset_y = scan_status.y_step;
  }

  scan_status.cycle_count = (scan_status.loop_count * loop_len + offset_x + offset_y) * user_settings.cycles_per_supercycle * num_flip_states_in_supercycle;
  scan_status.cycle_count += scan_status.step_repetition;

  if (user_settings.debug) {
    scan_utils::ts_printf("X step: %d, Y step: %d, compute new X sweep: %d, compute new Y sweep: %d\n", scan_status.x_step, scan_status.y_step, compute_sweep_x, compute_sweep_y);
  }

  if (compute_sweep_x || compute_sweep_y) {
    DirectionType direction_type_x = DirNoRecomputation;
    DirectionType direction_type_y = DirNoRecomputation;

    if (compute_sweep_x) {
      direction_type_x = DirIncreasing;

      if (user_settings.sweep_type_x == SweepUpThenDown && previous_direction_type_x == DirIncreasing) {
        direction_type_x = DirDecreasing;
      }

      if (user_settings.sweep_type_x == SweepRandom) {
        direction_type_x = DirRandom;
      }

      previous_direction_type_x = direction_type_x;

      // Start over so all first Y of each X are in same direction in up-then-down mode.
      previous_direction_type_y = DirDecreasing;
    }

    if (compute_sweep_y) {
      direction_type_y = DirIncreasing;

      if (user_settings.sweep_type_y == SweepUpThenDown && previous_direction_type_y == DirIncreasing) {
        direction_type_y = DirDecreasing;
      }

      if (user_settings.sweep_type_y == SweepRandom) {
        direction_type_y = DirRandom;
      }

      previous_direction_type_y = direction_type_y;
    }

    for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
      it->second->compute_values_for_next_sweep(direction_type_x, direction_type_y);
    }
  }

  return SUCCESS;
}

INT Scanner::setup_next_step(bool begin_of_run) {
  INT status = SUCCESS;
  bool xysteps_already_set = false;

  if (next_cycle_setup_logic == NCLRepeatCycle) {
    num_cycles_to_skip_writing = 0;
    next_cycle_setup_logic = NCLContinue;
    return SUCCESS;
  } else if (next_cycle_setup_logic == NCLRepeatStep) {
    xysteps_already_set = true;
    scan_status.step_repetition = 0;
    num_cycles_to_skip_writing = 0;
  } else if (next_cycle_setup_logic == NCLRollbackStepNoWriteRepeatedStep) {
    // Complicated one - roll back one step...
    xysteps_already_set = true;
    scan_status.step_repetition = 0;

    if (user_settings.enable_y && user_settings.enable_x) {
      scan_status.y_step--;

      if (scan_status.y_step < 0) {
        scan_status.y_step = user_settings.nY;
        scan_status.x_step -= 1;

        if (scan_status.x_step < 0) {
          scan_status.x_step = user_settings.nX + 1;
        }
      }
    } else if (user_settings.enable_x) {
      scan_status.x_step--;

      if (scan_status.x_step < 0) {
        scan_status.x_step = user_settings.nX + 1;
      }
    }

    // ... and skip writing the repeated step
    num_cycles_to_skip_writing = user_settings.cycles_per_supercycle * num_flip_states_in_supercycle;

    if (user_settings.enable_x) {
      num_cycles_to_skip_writing *= user_settings.nX + 1;
    }

    if (user_settings.enable_y) {
      num_cycles_to_skip_writing *= user_settings.nY + 1;
    }

  } else if (next_cycle_setup_logic == NCLRepeatScan) {
    xysteps_already_set = true;
    scan_status.step_repetition = 0;

    if (user_settings.enable_x) {
      scan_status.x_step = 0;
    }

    if (user_settings.enable_y) {
      scan_status.y_step = 0;
    }

    num_cycles_to_skip_writing = 0;
  }

  next_cycle_setup_logic = NCLContinue;

  // Increment the actual step counters
  bool new_x = false;
  bool new_y = false;

  status = increment_step_counters(begin_of_run, new_x, new_y, xysteps_already_set);

  if (status != SUCCESS) {
    return status;
  }

  // See if we need to flip something
  status = flip_flippable_devices_if_needed();

  if (status != SUCCESS) {
    return status;
  }

  // Setup each scannable device
  for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Setting up next step (if needed) on scan device %s (new_x=%d, new_y=%d)\n", it->first.c_str(), new_x, new_y);
    }

    status = it->second->setup_next_step(new_x, new_y, scan_status.x_step, scan_status.y_step, scan_status.step_repetition, begin_of_run);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to set up next step for scan device %s", it->first.c_str());
      return status;
    }
  }

  // Check scannable devices are ready
  long int timeout = scan_utils::current_time_ms() + (user_settings.setup_timeout_s * 1000);
  std::string unready = "";

  while (scan_utils::current_time_ms() < timeout) {
    unready = "";

    for (auto it = scan_devices.begin(); it != scan_devices.end(); it++) {
      bool this_dev_ok = true;
      status = it->second->is_next_step_ready(this_dev_ok);

      if (status != SUCCESS) {
        return status;
      }

      if (!this_dev_ok) {
        unready = it->first;

        if (user_settings.debug) {
          scan_utils::ts_printf("Scan device %s isn't ready yet\n", unready.c_str());
        }
      }
    }

    if (unready == "") {
      // Everything is ready.
      break;
    }

    ss_sleep(100);
  }

  if (unready != "") {
    cm_msg(MERROR, __FUNCTION__, "Need to stop run as scan device %s isn't ready after waiting %f seconds", unready.c_str(), user_settings.setup_timeout_s);
    cm_msg(MERROR, __FUNCTION__, "%s", scan_devices[unready]->explain_unreadiness().c_str());
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT Scanner::flip_flippable_devices_if_needed() {
  INT status = SUCCESS;

  for (auto it = flip_devices.begin(); it != flip_devices.end(); it++) {
    if (!it->second->is_enabled()) {
      continue;
    }

    FlipStrategy strat = flip_when[it->first];
    bool state = false;
    status = it->second->get_state(state);

    if (status != SUCCESS) {
      return status;
    }

    bool default_state = it->second->get_default_state();
    bool opposite = false;

    if (strat == FSEveryCycle) {
      opposite = (scan_status.cycle_count % 2 == 1);
    }

    if (strat == FSEveryOtherCycle) {
      opposite = (scan_status.cycle_count % 4 >= 2);
    }

    if (strat == FSEverySuperCycle || strat == FSEveryOtherSuperCycle) {
      int cyc_per_super = user_settings.cycles_per_supercycle * num_flip_states_in_supercycle;

      if (strat == FSEverySuperCycle) {
        opposite = (scan_status.cycle_count % (cyc_per_super * 2) >= cyc_per_super);
      }

      if (strat == FSEveryOtherSuperCycle) {
        opposite = (scan_status.cycle_count % (cyc_per_super * 4) >= cyc_per_super * 2);
      }
    }

    if (strat == FSEveryScan || strat == FSEveryOtherScan) {
      int cyc_per_scan = user_settings.cycles_per_supercycle * num_flip_states_in_supercycle;

      if (user_settings.enable_x) {
        cyc_per_scan *= (user_settings.nX + 1);
      }

      if (user_settings.enable_y) {
        cyc_per_scan *= (user_settings.nY + 1);
      }

      if (strat == FSEveryScan) {
        opposite = (scan_status.cycle_count % (cyc_per_scan * 2) >= cyc_per_scan);
      }

      if (strat == FSEveryOtherScan) {
        opposite = (scan_status.cycle_count % (cyc_per_scan * 4) >= cyc_per_scan * 2);
      }
    }

    bool new_state = opposite ? !default_state : default_state;

    if (new_state != state) {
      if (user_settings.debug) {
        scan_utils::ts_printf("Changing state of %s to %d. Cycle count %d.\n", it->first.c_str(), new_state, scan_status.cycle_count);
      }

      status = it->second->set(new_state);

      if (status != SUCCESS) {
        return status;
      }
    }
  }

  return status;
}

INT Scanner::start_cycle() {
  INT status = SUCCESS;
  waiting_for_data = false;

  if (!delayed_data_banks_to_send) {
    if (user_settings.debug) {
      scan_utils::ts_printf("Telling data sources that a new cycle is starting\n");
    }

    for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
      it->second->new_cycle_starting(scan_status.cycle_count);
    }
  }

  if (user_settings.debug) {
    scan_utils::ts_printf("Telling cycle controller to start the cycle\n");
  }

  status = cycle_controller->start_cycle(is_new_supercycle, is_new_loop);

  if (status != SUCCESS) {
    return status;
  }

  return status;
}

INT Scanner::stop_cycle() {
  if (user_settings.debug) {
    scan_utils::ts_printf("Telling cycle controller to stop the cycle\n");
  }

  waiting_for_data = false;
  return cycle_controller->stop_cycle();
}

INT Scanner::readout_and_setup_next_cycle(char *pevent) {
  INT ev_size = 0;

  if (delay_data_banks_until_next_cycle && delayed_data_banks_to_send) {
    // Just send data that we didn't send last time.
    ev_size = create_event(pevent);

    curr_data_chunk++;

    if (curr_data_chunk >= num_data_chunks) {
      delayed_data_banks_to_send = false;
      curr_data_chunk = 0;

      if (stop_after_final_delayed_banks_sent) {
        scan_utils::ts_printf("Stopping run, as final delayed banks have been sent.\n");
        request_stop_run = true;
      } else {
        if (user_settings.debug) {
          scan_utils::ts_printf("Telling data sources that a new cycle is starting (after delayed sending of data)\n");
        }

        for (auto it = data_sources.begin(); it != data_sources.end(); it++) {
          it->second->new_cycle_starting(scan_status.cycle_count);
        }
      }
    }
  } else {
    // Send data (if appropriate), set up next step, and start cycle.
    INT setup_status;
    bool was_first_call = first_call_of_run;
    is_new_supercycle = false;
    is_new_loop = false;

    if (first_call_of_run) {
      // Just starting first cycle - no event to write yet.
      // Initial setup was done during BOR procedure
      first_call_of_run = false;
      setup_status = SUCCESS;
      is_new_supercycle = true;
      is_new_loop = true;
    } else {
      bool write_event = false;
      setup_status = check_if_event_should_be_written(write_event);

      if (write_event) {
        if (delay_data_banks_until_next_cycle) {
          // Write everything except the data sources
          ev_size = create_event(pevent, true);

          // Make a note that we still need to send the data sources
          // (unless we skipped writing data at this point for some
          // reason).
          if (ev_size > 0) {
            delayed_data_banks_to_send = true;
            curr_data_chunk = 0;
          }
        } else {
          // Write data for cycle that just finished
          ev_size = create_event(pevent);
        }
      }

      // Set up next cycle.
      setup_status = setup_next_step(false);
    }

    if (setup_status == STATUS_ALL_LOOPS_COMPLETE) {
      if (delayed_data_banks_to_send) {
        scan_utils::ts_printf("Stopping run soon, after last set of delayed data banks have been sent.\n");
        stop_after_final_delayed_banks_sent = true;
      } else {
        scan_utils::ts_printf("Stopping run, as all loops are complete and no delayed data banks to be sent.\n");
        request_stop_run = true;
      }
    } else if (setup_status == SUCCESS) {
      // Start the next cycle
      start_cycle();

      if (!was_first_call) {
        // Started the second cycle (or the first cycle for the second attempt)
        first_cycle = false;
      }
    } else {
      scan_utils::ts_printf("setup_next_step returned %d - stop the run\n", setup_status);
      request_stop_run = true;
    }
  }


  return ev_size;
}
