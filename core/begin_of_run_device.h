#ifndef BOR_DEVICE_H
#define BOR_DEVICE_H

#include "midas.h"
#include "scan_utils.h"

/*
 * This class defines an interface for a device that just does some work at
 * the beginning of a run, and never provides any data or scans variables during a run.
 *
 * All functions that return an INT expect a midas status code to be returned
 * (e.g. SUCCESS if there were no issues).
 */
class BeginOfRunDevice {
  public:
    BeginOfRunDevice() {}
    virtual ~BeginOfRunDevice() {};

    // In this function you should call db_check_record() for any settings/status
    // entries you want in the ODB.
    virtual INT check_records() { return SUCCESS; }

    // In this function you should call db_open_record() for any settings/status
    // entries you want in the ODB.
    virtual INT open_records() { return SUCCESS; }

    // In this function you should call db_close_record() for any settings/status
    // entries you want in the ODB.
    virtual INT close_records() { return SUCCESS; }

    // This function will be called at the beginning of a run.
    virtual INT begin_of_run(char* error) { return SUCCESS;}

    // This function will be called at the end of a run.
    virtual INT end_of_run() { return SUCCESS;}
};

#endif
