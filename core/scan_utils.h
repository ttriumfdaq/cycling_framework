#ifndef TITAN_UTILS
#define TITAN_UTILS

#include "midas.h"

namespace scan_utils {
  // Stop the current midas run.
  // sync_flag can be TR_SYNC or TR_ASYNC.
  // Returns whether request was successful.
  INT stop_the_run(INT sync_flag);

  // When the given file was last modified.
  // Returns DB_FILE_ERROR if file doesn't exist.
  // Returns SUCCESS if file exists, and populate unixtime.
  INT file_modified_time(char* path, unsigned int& unixtime);

  // Get the current unix time in milliseconds.
  long unsigned int current_time_ms();

  // Whether the given file exists on disk.
  bool file_exists(char* path);

  // Wrapper around printf to automatically prepend the current date and time.
  void ts_printf(const char* format, ...);

  // Check if the current host this program is running on matches
  // the expected hostname.
  // If unqualified_okay is true, we'll compare qualified and unqualified
  // hostnames; if false, we'll require an exact match between gethostname()
  // and the expected hostname.
  // If msg_on_error is true, we'll emit a midas cm_msg MERROR on failure.
  bool check_hostname(char* expected_hostname, bool unqualified_okay=true, bool msg_on_error=true);

  // Extract the return code and message from a JSON response of the form
  // `{"code": 1, "msg": "my_message"}`.
  // Requires that the message itself does not contain any double-quotes.
  // Returns SUCCESS or FE_ERR_DRIVER.
  INT extract_msg_from_rpc_response(char* input, int& return_code, char* msg, bool unescape_newlines=true);

  INT run_cmd_and_search_for_output(char* cmd, char* search, bool& contains);
}

#endif
