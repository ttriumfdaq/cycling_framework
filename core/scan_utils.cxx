#include "scan_utils.h"
#include "midas.h"
#include <sys/stat.h>
#include <sys/time.h>
#include <ctime>
#include <stdarg.h>
#include <unistd.h>
#include <cstring>

namespace scan_utils {
  INT stop_the_run(INT sync_flag) {
    ts_printf("We're stopping the run\n");
    char errstr[256];
    return cm_transition(TR_STOP, 0, errstr, sizeof(errstr), sync_flag, FALSE);
  }

  bool file_exists(char* path) {
    struct stat stbuf;
    return (stat(path, &stbuf) == 0);
  }

  INT file_modified_time(char* path, unsigned int& unixtime) {
    struct stat stbuf;
    int status = stat(path, &stbuf);

    if (status != 0 || stbuf.st_size < 0) {
      return DB_FILE_ERROR;
    }

    unixtime = stbuf.st_mtime;
    return SUCCESS;
  }

  long unsigned int current_time_ms() {
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((long unsigned int)tp.tv_sec * 1000) + ((long unsigned int)tp.tv_usec / 1000);
  }

  void ts_printf(const char* format, ...) {
    // Handle va args for message
    va_list argptr;
    char message[10000];
    va_start(argptr, format);
    vsprintf(message, (char *) format, argptr);
    va_end(argptr);

    // Handle timestamp
    time_t rawtime;
    struct tm * timeinfo;
    char ts[80];

    long unsigned int milli = current_time_ms() % 1000;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(ts, 80, "%Y-%m-%d %H:%M:%S", timeinfo);
    printf("%s.%lu %s", ts, milli, message);
  }

  bool check_hostname(char* expected_hostname, bool unqualified_okay, bool msg_on_error) {
    char our_hostname[256];
    gethostname(our_hostname, 256);

    if (strcmp(our_hostname, expected_hostname) == 0) {
      return true;
    }

    if (unqualified_okay) {
      // Get the first bit of the expected/our hostnames.
      // E.g. 'lxabcd.triumf.ca' -> 'lxabcd'
      char x_unqual[256];
      char* x_dot = strchr(expected_hostname, '.');
      int x_len = (x_dot == NULL) ? strlen(expected_hostname) : (x_dot - expected_hostname);
      if (x_len > 255) x_len = 255;
      strncpy(x_unqual, expected_hostname, x_len);
      x_unqual[x_len] = '\0';

      char o_unqual[256];
      char* o_dot = strchr(our_hostname, '.');
      int o_len = (o_dot == NULL) ? strlen(our_hostname) : (o_dot - our_hostname);
      if (o_len > 255) o_len = 255;
      strncpy(o_unqual, our_hostname, o_len);
      o_unqual[o_len] = '\0';

      if (strcmp(o_unqual, x_unqual) == 0) {
        return true;
      }
    }

    if (msg_on_error) {
      cm_msg(MERROR, __FUNCTION__, "This program is designed to run on host '%s', not '%s'", expected_hostname, our_hostname);
    }

    return false;
  }

  INT extract_msg_from_rpc_response(char* input, int& return_code, char* msg, bool unescape_newlines) {
    // C's sscanf "%s" format splits at the next whitespace, which would include
    // the trailing quote mark and comma/curly-brace for us. So we look for
    // anything that isn't a quote mark.
    // If we ever need to return strings that include quaote marks as part of
    // the message, this function must be improved (probably using a proper JSON
    // parser).
    int filled = sscanf(input, "{\"code\": %d, \"msg\": \"%[^\"]\"}", &return_code, msg);

    if (filled != 2) {
      filled = sscanf(input, "{\"msg\": \"%[^\"]\", \"code\": %d}", msg, &return_code);

      if (filled != 2) {
        cm_msg(MERROR, __FUNCTION__, "Unexpected return value from RPC call: '%s'", input);
        return FE_ERR_DRIVER;
      }
    }

    if (unescape_newlines) {
      std::string new_msg_str;
      std::string msg_str(msg);
      std::string::const_iterator it = msg_str.begin();

      while (it != msg_str.end()) {
        char c = *it++;

        if (c == '\\' && it != msg_str.end()) {
          switch (*it++) {
          case '\\': c = '\\'; break;
          case 'n': c = '\n'; break;
          case 't': c = '\t'; break;
          // all other escapes
          default:
            // invalid escape sequence - skip it. alternatively you can copy it as is, throw an exception...
            continue;
          }
        }

        new_msg_str += c;
      }

      sprintf(msg, "%s", new_msg_str.c_str());
    }

    return SUCCESS;
  }

  INT run_cmd_and_search_for_output(char* cmd, char* search, bool& contains) {
    char buffer[128];

    std::string result = "";
    FILE *pipe = popen(cmd, "r");

    if (!pipe) {
      cm_msg(MERROR, __FUNCTION__, "Failed to open pipe");
      return FE_ERR_DRIVER;
    }

    while (fgets(buffer, sizeof(buffer), pipe) != NULL) {
      result += buffer;
    }

    INT retval = pclose(pipe);

    if (retval != 0) {
      return FE_ERR_DRIVER;
    }

    contains = (result.find(search) != std::string::npos);

    return SUCCESS;
  }
}
