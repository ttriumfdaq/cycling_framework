#ifndef FLIPPABLE_DEVICE_H
#define FLIPPABLE_DEVICE_H

#include "midas.h"
#include <string>
#include <vector>

/*
 * Base class for things that can be "flipped" between 2 states during a run.
 */
class FlippableDevice {
  public:
    FlippableDevice(HNDLE _hDB) {
      hDB = _hDB;
      debug = false;
      last_set_state = get_default_state();
    }

    virtual ~FlippableDevice() {};

    // Call db_check_record() for the settings/status.
    virtual INT check_records() { return SUCCESS; }

    // Call db_open_record() for the settings/status.
    virtual INT open_records() { return SUCCESS; }

    // Call db_close_record() for the settings/status.
    virtual INT close_records() { return SUCCESS; }

    // Perform any begin-of-run actions.
    virtual INT begin_of_run(char* error) { return SUCCESS; }

    // Perform any end-of-run actions.
    virtual INT end_of_run() { return SUCCESS; }

    virtual bool is_enabled() = 0;

    // Whether to print debug messages to screen.
    void set_debug_enabled(bool _debug) {
      debug = _debug;
    }

    // Reset to the default value.
    virtual INT reset() {
      return set(get_default_state());
    }

    // Set to a certain value.
    virtual INT set(bool state) = 0;

    // What state is set when reset() is called.
    virtual bool get_default_state() {return true;}

    // Get the current state.
    virtual INT get_state(bool& state) = 0;

    // Whether we're currently in the state we expect to be in.
    virtual INT is_in_expected_state(bool& okay) {
      bool state = false;
      INT status = get_state(state);

      if (status != SUCCESS) {
        okay = false;
        return status;
      }

      okay = (state == last_set_state);
      return SUCCESS;
    }

  protected:

    // Midas ODB handle.
    HNDLE hDB;

    // Whether to print debug messages.
    bool debug;

    // What we set to most recently.
    bool last_set_state;
};

// Dummy flippable device for testing logic of scanner.
class DummyFlippableDevice : public FlippableDevice {
  public:
    DummyFlippableDevice(HNDLE hDB) : FlippableDevice(hDB) {
      curr_state = false;
    };
    virtual ~DummyFlippableDevice() {};

    bool is_enabled() {
      return true;
    }

    bool get_default_state() {
      return false;
    }

    INT set(bool state) {
      curr_state = state;
      return SUCCESS;
    }

    INT flip() {
      curr_state = !curr_state;
      return SUCCESS;
    }

    INT get_state(bool& state) {
      state = curr_state;
      return SUCCESS;
    }

    INT is_in_expected_state(bool& okay) {
      okay = true;
      return SUCCESS;
    }

  private:
    bool curr_state;
};

#endif
