#ifndef DATA_SOURCE_BASE_H
#define DATA_SOURCE_BASE_H

#include "midas.h"
#include "scan_utils.h"

enum DataStatus {
  DataReady,
  DataNotReady,
  DataFatalError
};

/*
 * This class defines an interface for a device that provides data that should
 * be read out at the end of a cycle. You should implement a class that inherits
 * from this one, and implement the virtual functions you need.
 *
 * All functions that return an INT expect a midas status code to be returned
 * (e.g. SUCCESS if there were no issues).
 */
class DataSource {
  public:
    DataSource() {}
    virtual ~DataSource() {};

    // In this function you should call db_check_record() for any settings/status
    // entries you want in the ODB.
    virtual INT check_records() { return SUCCESS; }

    // In this function you should call db_open_record() for any settings/status
    // entries you want in the ODB.
    virtual INT open_records() { return SUCCESS; }

    // In this function you should call db_close_record() for any settings/status
    // entries you want in the ODB.
    virtual INT close_records() { return SUCCESS; }

    // This function will be called at the beginning of a run.
    virtual INT begin_of_run(char* error) { return SUCCESS;}

    // This function will be called at the end of a run.
    virtual INT end_of_run() { return SUCCESS;}

    // This will be called once when a new PPG cycle starts.
    virtual void new_cycle_starting(int cycle_count) {}

    // This will be called once when a PPG cycle ends.
    virtual void this_cycle_finished() {}

    // This will be called periodically and allows you
    // to flush data from devices if needed.
    virtual void mid_cycle_read_data_if_needed() {}

    // This should return whether all the data is available.
    // It will be called frequently after the end of a cycle
    // until you return DataReady.
    virtual DataStatus end_cycle_is_data_ready() { return DataReady; }

    // Whether this data source supports "chunking" of data across
    // multiple midas events.
    virtual bool supports_chunking() { return false; }

    // This should fill banks in the event.
    // bk_init32() will already have been called for the event.
    virtual void write_data_to_banks(char *pevent) {}

    // Like write_data_to_banks, but for cases where the data
    // source supports "chunking" data into multiple events.
    virtual void write_data_chunk_to_banks(char *pevent, int chunk_num, int total_num_chunks) {}
};

/*
 * This class is an example data source used for testing the logic
 * of the main scanning class.
 */
class DataSourceDummy: public DataSource {
  public:
    // _dummy_delay_ms - how long to pretend it takes for data to be
    //   acquired at the end of each cycle.
    DataSourceDummy(int _dummy_delay_ms) {
      dummy_delay_ms = _dummy_delay_ms;
      delay_start = 0;
    }
    void new_cycle_starting(int cycle_count) {
      delay_start = 0;
    }
    void this_cycle_finished() {
      delay_start = scan_utils::current_time_ms();
    }
    DataStatus end_cycle_is_data_ready() {
      long int now = scan_utils::current_time_ms();
      bool ready = (now - delay_start) > dummy_delay_ms;
      return ready ? DataReady : DataNotReady;
    }
    void write_data_to_banks(char *pevent) {
      DWORD* pdata_dummy;
      bk_create(pevent, "DUMY", TID_DWORD, (void**) &pdata_dummy);
      *pdata_dummy++ = 123;
      *pdata_dummy++ = 456;
      *pdata_dummy++ = 789;
      bk_close(pevent, pdata_dummy);
    }

  private:
    int dummy_delay_ms;
    long int delay_start;
};

#endif
