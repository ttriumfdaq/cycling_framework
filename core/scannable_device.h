#ifndef SCANNABLE_DEVICE_H
#define SCANNABLE_DEVICE_H

#include "midas.h"
#include <string>
#include <vector>

enum SweepType {
  SweepUp,
  SweepUpThenDown,
  SweepRandom
};

enum DirectionType {
  DirNoRecomputation,
  DirIncreasing,
  DirDecreasing,
  DirRandom
};

/*
 * Base class for things that can be "scanned" during a run.
 *
 * Scanning means that a variable is changed at the end of a cycle, before
 * the next cycle starts.
 *
 * Concrete examples are editing PPG delays, changing EPICS variables and more.
 * Each derived class is responsible for its own ODB settings and implementation.
 * In general we expect settings to appear in /Scanning/XYZ/Settings, and the status
 * to be reported in /Scanning/XYZ/Status.
 */
class ScannableDevice {
  public:
    ScannableDevice(HNDLE _hDB);
    virtual ~ScannableDevice() {};

    // Call db_check_record() for the settings/status.
    virtual INT check_records() { return SUCCESS; }

    // Call db_open_record() for the settings/status.
    virtual INT open_records() { return SUCCESS; }

    // Call db_close_record() for the settings/status.
    virtual INT close_records() { return SUCCESS; }

    // Perform and begin-of-run actions.
    virtual INT begin_of_run(char* error) { return SUCCESS; }

    // Perform and begin-of-run actions.
    virtual INT end_of_run() { return SUCCESS; }

    // Compute values for the next sweep through. For example, we'll ask that
    // a new Y sweep is computed after each X step completes. This is because
    // the user may request that values are randomly scanned (rather than
    // sequentially) to reduce systematic errors, or are scanned up-then-down
    // to reduce the deadtime between sweeps.
    // Derived class is responsible for storing the values for the next sweep
    // in their own member variables.
    virtual void compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) {}

    // Actually set up the device for the next step. The values to set should
    // be taken from the values computed by `compute_values_for_next_sweep()`.
    // new_x/y say whether to change the X/Y values; x/y_step say which step
    // we're now at.
    virtual INT setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) { return SUCCESS; }

    // It may take some time for devices to get to the desired state after
    // `setup_next_step()` is called (e.g. if there's a slow ramp rate for HV).
    // This function should populate `ok` with whether we're ready to proceed.
    virtual INT is_next_step_ready(bool& ok) { ok = true;  return SUCCESS; }

    // If `is_next_step_ready()` returns false, you may be asked to explain
    // what the problem is.
    virtual std::string explain_unreadiness() { return ""; };

    // Add banks to the event for this cycle. Derived classes should fill the
    // X and Y values in separate banks (e.g. "XPPG" and "YPPG" when scanning
    // the PPG).
    virtual INT fill_end_of_cycle_banks(char* pevent)  { return SUCCESS; }

    // Set the scan parameters - whether X/Y scanning is enabled, and the
    // number of X/Y scan steps.
    void set_xy_enabled(bool x, bool y, int nx, int ny);

    // Set whether debug messages should be printed or not.
    void set_debug_enabled(bool _debug);

    // Helper functions to compute next values for scan sweep.
    // If we add any more types, we should probably move to a templated function.
    std::vector<double> compute_sweep_double(DirectionType direction_type, double start, double end, int n_steps);
    std::vector<float> compute_sweep_float(DirectionType direction_type, float start, float end, int n_steps);
    std::vector<int> compute_sweep_int(DirectionType direction_type, int start, int end, int n_steps);

    // Reorder a set of values that are initially ordered in increasing order.
    template <class T> void reorder_sweep(DirectionType direction_type, std::vector<T>& increasing_vals);

    // Whether x scanning is currently enabled
    bool is_x_enabled() {
      return x_enabled;
    }

    // Whether y scanning is currently enabled
    bool is_y_enabled() {
      return y_enabled;
    }

  protected:

    // Midas ODB handle.
    HNDLE hDB;

    // Whether any scanning is enabled.
    bool x_enabled;

    // Whether a 2D scan is enabled.
    bool y_enabled;

    // Number of X scan points.
    int nX;

    // Number of Y scan points.
    int nY;

    // Whether to print debug messages.
    bool debug;

};

#endif
