#ifndef CYCLE_CONTROLLER_H
#define CYCLE_CONTROLLER_H

#include "midas.h"

enum CycleState {
  NotStarted,
  Running,
  Finished,
  ConstantTimeReadyForReadoutAndInDaqServiceTime,
  ConstantTimeReadyForReadoutButNotInDaqServiceTime,
  NotApplicable
};

typedef enum {
  NCLContinue,
  NCLRepeatCycle,
  NCLRepeatStep,
  NCLRollbackStepNoWriteRepeatedStep,
  NCLRepeatScan
} NextCycleSetupLogic;

/*
 * This class defines an interface for a device that controls/reports the state
 * of the cycle in an experiment. Generally this will be a PPG, but it could be
 * any other device. You should implement a class that inherits from this one,
 * and implement the virtual functions you need.
 *
 * The scanning class will frequently call get_cycle_progress() to determine what
 * action it should take.
 *
 * All functions that return an INT expect a midas status code to be returned
 * (e.g. SUCCESS if there were no issues).
 */
class CycleController {
  public:
    CycleController(HNDLE _hDB) {
      hDB = _hDB;
      constant_time_mode = false;
      debug = false;
    }

    virtual ~CycleController() {};

    // In this function you should call db_check_record() for any settings/status
    // entries you want in the ODB.
    virtual INT check_records() { return SUCCESS; }

    // In this function you should call db_open_record() for any settings/status
    // entries you want in the ODB.
    virtual INT open_records() { return SUCCESS; }

    // In this function you should call db_close_record() for any settings/status
    // entries you want in the ODB.
    virtual INT close_records() { return SUCCESS; }

    // In this function you should do any begin-of-run actions (e.g. loading a
    // script into the PPG).
    virtual INT begin_of_run(char* error) { return SUCCESS; }

    // If this device is responsible for reporting the cycle state, return what
    // the cycle state is.
    virtual CycleState get_cycle_progress() {
      return NotApplicable;
    }

    // Perform any actions needed to start the cycle (e.g. starting the PPG).
    // In case you need to do extra things for special cycles, you are told
    // whether this is a new supercycle (moving onto the next scan step) or
    // a completely new loop (finished all X and Y steps)
    virtual INT start_cycle(bool is_new_supercycle, bool is_new_loop) {
      return SUCCESS;
    }

    // Perform any actions needed to stop the cycle (e.g. stopping the PPG).
    virtual INT stop_cycle() {
      return SUCCESS;
    }

    // Return true is your cycle controller supports constant time mode
    // (can return ConstantTimeReadyForReadoutAndInDaqServiceTime etc
    // when get_cycle_progress() is called).
    virtual bool supports_constant_time_mode() {
      return false;
    }

    // Enable constant time mode.
    virtual void enable_constant_time_mode(bool enable) {
      constant_time_mode = enable;
    }

    // At end of cycle, perform any checks needed to see if event
    // should be written to disk.
    // This is IN ADDITION to automatic checks that "flippable devices"
    // are in the correct state for writing data.
    virtual INT check_if_event_should_be_written(bool& write_event, NextCycleSetupLogic& next_logic, bool intentional_skip, bool for_combining_supercycle) {;
      return SUCCESS;
    }

    // Whether to print debug info to screen.
    virtual void set_debug_enabled(bool _debug) {
      debug = _debug;
    }

  protected:

    // Midas ODB handle.
    HNDLE hDB;

    // Whether constant time mode is enabled.
    bool constant_time_mode;

    // Whether to print debug info to screen.
    bool debug;
};

#endif
