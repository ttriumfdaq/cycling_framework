"""
Frontend that can save/load bits of the ODB to/from text files on disk.

We store JSON files on disk, and provide a webpage (in custom/saveload.html) that
allows the user to save/load settings as desired.

We allow the user to configure multiple sections of the ODB to be saved. This would
allow the user to save/load detector settings separately from PPG settings, for example.
This configuration is done in /Saveload/Sections. Each entry in that directory is a list
of strings.

The on-disk directory in which files are stored is specified by the ODB key /Saveload/Path.
"""

import midas
import midas.client
import json
import datetime
import os.path
import glob
import collections

base_odb_dir = "/Saveload"

def parse(filename):
    """
    Load the specified file as JSON.
    
    Args:
        
    * filename (str)
    
    Returns:
        `collections.OrderedDict`
    """
    with open(filename) as f:
        return json.load(f, object_pairs_hook=collections.OrderedDict)

def get_section_disk_dir(section, client):
    """
    Get where on disk a section's saved files will be found.
    
    Args:
        
    * section (str)
    * client (`midas.client.MidasClient`)
    """
    return client.odb_get(base_odb_dir + "/Path") + "/" + section

def create_filename(section):
    """
    Create a new filename for saving settings. We encode the data/time to
    guarantee uniqueness.
    
    Args:
        
    * section (str)
    
    Returns:
        str - Just the filename, not the full path.
    """
    now = datetime.datetime.now()
    return section + "__" + datetime.datetime.strftime(now, "%Y_%m_%d-%H%M%S.json")

def save(filename, comment, odb_dir_list, client):
    """
    Save the current ODB content to disk.
    
    Args:
        
    * filename (str) - Where to save (full path).
    * comment (str) - Comment to add to the file.
    * odb_dir_list (list of str) - Which bits of the ODB to save. Empty 
        strings are ignored.
    * client (`midas.client.MidasClient`)
    """
    data = {"comment": comment,
            "odb": {}}
    
    for odb_dir in odb_dir_list:
        data["odb"][odb_dir] = client.odb_get(odb_dir)
    
    pardir = os.path.dirname(filename)
    
    if not os.path.exists(pardir):
        os.mkdir(pardir)
    
    with open(filename, "w") as f:
        json.dump(data, f)

def edit_comment(filename, comment):
    """
    Edit the comment metadata in a saved file.
    
    Args:
        
    * filename (str) - File to edit (full path).
    * comment (str) - New comment.
    """
    data = parse(filename)
    data["comment"] = comment
    
    with open(filename, "w") as f:
        json.dump(data, f)

def force_floats(odb):
    """
    Force certain ODB keys to be returned as floats rather than ints.
    """
    names = ["time offset (ms)", "pulse width (ms)"]
    
    for k, v in odb.items():
        if isinstance(v, dict):
            odb[k] = force_floats(v)
        elif k in names:
            odb[k] = float(v)
            
    return odb

def load(filename, odb_dir_list, client):
    """
    Load ODB settings from a file into the live ODB.
    
    Args:
        
    * filename (str) - File to load (full path).
    * odb_dir_list (list of str) - Bits of the ODB to load.
    * client (`midas.client.MidasClient`)
    """
    odb_bits = get_odb_bits(filename, odb_dir_list)
    
    for odb_dir, content in odb_bits.items():
        print("Updating %s" % odb_dir)
        content = force_floats(content)
        client.odb_set(odb_dir, content)

def get_odb_bits(filename, odb_dir_list):
    """
    Get the specified parts of the ODB from a file. If a required
    bit is missing, we raise an error. This slightly helps prevent
    cases of loading very old saved files where the ODB structure is
    now very different.
    
    Args:
        
    * filename (str) - File to load (full path).
    * odb_dir_list (list of str) - Bits of the ODB to load.
    
    Returns:
        dict of {str: `collections.OrderedDict`}, keyed by ODB path.
    """
    js_data = parse(filename)
    retval = {}
    
    for odb_dir in odb_dir_list:
        if odb_dir == "":
            continue
        
        if odb_dir not in js_data["odb"]:
            raise ValueError("ODB directory '%s' not present in saved file!" % odb_dir)
        
        retval[odb_dir] = js_data["odb"][odb_dir]
    
    return retval

def filelist(disk_dir):
    """
    Get the list of JSON files in the specified directory.
    
    Args:
        
    * disk_dir (str)
    
    Returns:
        list of dict. One dict per file, ordered by most recently-edited file.
        Dict keys are filename, comment, and last_modified.
    """
    files = []
    
    if os.path.exists(disk_dir):
        filenames = glob.glob(disk_dir + "/*.json")
        
        for filename in filenames:
            js_data = parse(filename)
            meta = {"filename": filename, 
                    "comment": js_data["comment"], 
                    "last_modified": str(datetime.datetime.fromtimestamp(os.path.getmtime(filename)))}
            files.append(meta)
    
    return sorted(files, key=lambda x: x["last_modified"], reverse=True)

def rpc_handler(client, cmd, args, max_len):
    """
    JRPC handler we register with midas. This will generally be called when
    a user clicks a button on the associated webpage.
    
    Args:
        
    * client (`midas.client.MidasClient`)
    * cmd (str) - The command the user wants us to do.
    * args (str) - Stringified JSON of any arguments for this command.
    * max_len (int) - Maximum length of response the user will accept.
    
    Returns:
        2-tuple of (int, str) for (status code, message)
        
    Accepted commands:
        
    * save - Save live ODB to disk
    * edit_comment - Edit the comment in a file
    * delete - Delete settings from disk
    * load - Load settings from disk into live ODB
    * view - Return the content of a file
    * list - List the files available
    """
    jargs = json.loads(args)
    section = jargs["section"]
    settings = client.odb_get(base_odb_dir)
    
    if section not in settings["Sections"]:
        raise ValueError("Unknown section '%s' - configure it in ODB at %s/Sections/%s" % (section, base_odb_dir, section))
    
    print("Handling RPC function %s" % cmd)
    base_disk_dir = get_section_disk_dir(section, client)
    retstr = ""
    
    if cmd == "save":
        comment = jargs["comment"]
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        filename = os.path.join(base_disk_dir, jargs.get("filename", create_filename(section)))
        save(filename, comment, odb_dir_list, client)
    elif cmd == "edit_comment":
        comment = jargs["comment"]
        filename = os.path.join(base_disk_dir, jargs["filename"])
        edit_comment(filename, comment)
    elif cmd == "delete":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        os.unlink(filename)
    elif cmd == "load":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        load(filename, odb_dir_list, client)
        retstr = "Loaded successfully"
    elif cmd == "view":
        filename = os.path.join(base_disk_dir, jargs["filename"])
        odb_dir_list = [d for d in settings["Sections"][section] if d != ""]
        odb = get_odb_bits(filename, odb_dir_list)
        retstr = json.dumps(odb, indent=2)
    elif cmd == "list":
        retstr = json.dumps(filelist(base_disk_dir))
    else:
        raise ValueError("Unknown command '%s'" % cmd)
    
    if len(retstr) > max_len:
        raise ValueError("Return string too long (want %s chars but buffer is size %s)" % (len(retstr), max_len))
    
    return (midas.status_codes["SUCCESS"], retstr)

def main():
    """
    Main function that starts a midas client and runs forever.
    """
    client = midas.client.MidasClient("saveload")
    
    default_settings = {"Path": client.odb_get("/Logger/Data dir"),
                        "Sections": {"PPGScan": ["/Equipment/PPGCompiler/Settings",
                                                 "/Equipment/PPGCompiler/Programming",
                                                 "/Scanning",
                                                 "",
                                                 "",
                                                 "",
                                                 ""]}}
    
    client.odb_set(base_odb_dir, default_settings, update_structure_only=True)
    client.register_jrpc_callback(rpc_handler, True)
    
    while True:
        client.communicate(1000)

if __name__ == "__main__":
    main()
