# Your parent CMakeLists.txt should configure which bits of
# the cycling framework should be compiled.
# 
# Some features may need more variables to be set - see their
# respective CMakeLists.txt for details.
# 
# Example from parent:
# ```
# set(BUILD_CAMP 1)
# set(BUILD_PPG 1)
# add_subdirectory(cycling_framework)
# ```

add_subdirectory(core)

if (BUILD_PPG)
 add_subdirectory(ppg)
endif()

if (BUILD_EPICS)
 add_subdirectory(epics)
endif()

if (BUILD_VT4)
 add_subdirectory(vt4)
endif()

if (BUILD_LRS1190)
 add_subdirectory(lrs1190)
endif()

if (BUILD_SOFTDAC)
 add_subdirectory(softdac)
endif()

if (BUILD_CAMP)
 add_subdirectory(camp)
endif()

if (BUILD_SCALERS)
 add_subdirectory(scalers)
endif()

if (BUILD_PSM)
 add_subdirectory(psm)
endif()

if (BUILD_NIMIO32)
 add_subdirectory(nimio32)
endif()