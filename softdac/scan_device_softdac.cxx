#include "midas.h"
#include "mrpc.h"
#include "core/scannable_device.h"
#include "scan_device_softdac.h"
#include "core/scan_utils.h"
#include <algorithm>
#include <iostream>
#include <cstring>

#define SOFTDAC_DEFAULTS_PATH "/Scanning/Softdac/Defaults"
#define SOFTDAC_SETTINGS_PATH "/Scanning/Softdac/Settings"
#define SOFTDAC_STATUS_PATH "/Scanning/Softdac/Status"

INT ScanDeviceSoftdac::check_records() {
  char settings_path[256], status_path[256];
  sprintf(settings_path, SOFTDAC_SETTINGS_PATH);
  sprintf(status_path, SOFTDAC_STATUS_PATH);

  INT status;

  status = db_check_record(hDB, 0, settings_path, SOFTDAC_SCAN_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", settings_path);
    return status;
  }

  status = db_check_record(hDB, 0, status_path, SOFTDAC_SCAN_STATUS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", status_path);
    return status;
  }

  return SUCCESS;
}

INT ScanDeviceSoftdac::open_records() {
  char settings_path[256], status_path[256];
  sprintf(settings_path, SOFTDAC_SETTINGS_PATH);
  sprintf(status_path, SOFTDAC_STATUS_PATH);

  HNDLE h_settings, h_status;
  INT size_settings = sizeof(user_settings);
  INT size_status = sizeof(scan_status);
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_get_record(hDB, h_settings, &user_settings, &size_settings, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", settings_path);
    return status;
  }

  status = db_get_record(hDB, h_status, &scan_status, &size_status, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", status_path);
    return status;
  }

  status = db_open_record(hDB, h_settings, &user_settings, size_settings, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open Softdac settings record");
    return status;
  }

  status = db_open_record(hDB, h_status, &scan_status, size_status, MODE_WRITE, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open Softdac status record");
    return status;
  }

  return status;
}


INT ScanDeviceSoftdac::close_records() {
  char settings_path[256], status_path[256];
  sprintf(settings_path, SOFTDAC_SETTINGS_PATH);
  sprintf(status_path, SOFTDAC_STATUS_PATH);

  HNDLE h_settings, h_status;
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_close_record(hDB, h_settings);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close Softdac settings record");
    return status;
  }

  status = db_close_record(hDB, h_status);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close Softdac status record");
    return status;
  }

  return status;
}

INT ScanDeviceSoftdac::begin_of_run(char* error) {
  INT status;

  status = populate_list_of_step_ppg_pulses(error);

  if (status != SUCCESS) {
    return status;
  }

  status = validate_softdac_scan_settings(error);

  if (status != SUCCESS) {
    return status;
  }

  status = read_default_voltages(error);

  if (status != SUCCESS) {
    return status;
  }

  status = init_softdac_device(error);

  if (status != SUCCESS) {
    return status;
  }

  return status;
}

INT ScanDeviceSoftdac::init_softdac_device(char* error) {
  if (debug) {
    scan_utils::ts_printf("Opening softdac\n");
  }

  INT status;
  status = softdac_Open(&softdac);
  softdac_open = true;

  if (status < 0) {
    snprintf(error, 255, "Cannot open PMC-SOFTDAC device! (status %d)", status);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_HW;
  }

  softdac_Reset(softdac);

  status = softdac_Setup(softdac, 0, 3);

  if (status < 0) {
    snprintf(error, 255, "Failed to set softdac mode! (status %d)", status);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_HW;
  }

  // Load Voltage range
  std::vector<std::string> voltage_ranges;
  voltage_ranges.push_back("P5V");
  voltage_ranges.push_back("P10V");
  voltage_ranges.push_back("PM5V");
  voltage_ranges.push_back("PM10V");
  voltage_ranges.push_back("PM2P5V");
  voltage_ranges.push_back("M2P5P7P5V");

  std::string user_range(user_settings.voltage_range_str);

  int range_idx;
  INT range_reg_val = 0;

  for (range_idx = 0; range_idx <= SOFTDAC_RANGE_MAX; range_idx++) {
    if (voltage_ranges[range_idx].compare(user_range) == 0) {
      range_reg_val = SOFTDAC_RANGE_P5V + range_idx;
      break;
    }
  }

  if (range_idx >= SOFTDAC_RANGE_MAX) {
    snprintf(error, 255, "%s", "Unknown softdac voltage range. Should be a string like 'PM10V'");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  softdac_ScaleSet(softdac, range_reg_val, 0., 0.);

  cm_msg(MINFO, __FUNCTION__, "Softdac range is %s", voltage_ranges[range_idx].c_str());

  softdac_Status(softdac, 1);

  return SUCCESS;
}

INT ScanDeviceSoftdac::end_of_run() {
  if (softdac_open) {
    softdac_Close(softdac);
    softdac_open = false;
  }

  return SUCCESS;
}

INT ScanDeviceSoftdac::populate_list_of_step_ppg_pulses(char* error) {
  if (debug) {
    scan_utils::ts_printf("Getting list of PPG pulses\n");
  }

  pulse_name_to_voltage_vector_idx.clear();
  default_chan_voltages.clear();
  this_step_chan_voltages.clear();

  INT client_handle;
  INT status = cm_connect_client("PPGCompilerFrontend", &client_handle);

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to connect to PPGCompilerFrontend");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return CM_NO_CLIENT;
  }

  int bufsize = 10000;
  char retstr_buf[bufsize];
  char args[256];
  sprintf(args, "{\"channel\":\"%s\"}", user_settings.ppg_channel_name);
  status = rpc_client_call(client_handle, RPC_JRPC, "list_pulses_for_channel", args, retstr_buf, bufsize);
  cm_disconnect_client(client_handle, FALSE);

  if (status != SUCCESS) {
    snprintf(error, 255, "Got return value %d from PPGCompilerFrontend", status);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return DB_TIMEOUT;
  }

  if (debug) {
    scan_utils::ts_printf("PPG compiler returned '%s'\n", retstr_buf);
  }

  // Expect return value to be of form {"code": 1, "msg": "pulse_a,pulse_b"}
  char pulse_list[bufsize];
  int code = 0;
  status = scan_utils::extract_msg_from_rpc_response(retstr_buf, code, pulse_list);

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to extract message from RPC response. See Messages for more.");
    return status;
  }

  if (code != 1) {
    snprintf(error, 255, "Unexpected return value from PPGCompilerFrontend: %s", retstr_buf);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_DRIVER;
  }

  char* pulse_name = strtok(pulse_list, ",");

  while (pulse_name != NULL) {
    if (debug) {
      scan_utils::ts_printf("Found pulse name '%s'\n", pulse_name);
    }

    pulse_name_to_voltage_vector_idx[pulse_name] = default_chan_voltages.size();
    std::vector<double> empty;
    empty.resize(SOFTDAC_NUM_CHANS);
    default_chan_voltages.push_back(empty);
    this_step_chan_voltages.push_back(empty);
    pulse_name = strtok(NULL, ",");
  }

  if (default_chan_voltages.size() == 0) {
    // Not a fatal error, as maybe the user isn't even scanning anything.
    cm_msg(MERROR, __FUNCTION__, "Warning: didn't find any PPG pulses that step the softdac trap voltages");
  }

  return SUCCESS;
}

INT ScanDeviceSoftdac::validate_softdac_scan_settings(char* error) {
  if (debug) {
    scan_utils::ts_printf("Validating softdac scan settings\n");
  }

  INT status;
  HNDLE key;

  if (x_enabled) {
    for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
      x_voltage_idx[i] = -1;

      if (strlen(user_settings.x_blocks[i]) == 0) {
        continue;
      }

      if (user_settings.x_chan_idx[i] >= SOFTDAC_NUM_CHANS) {
        snprintf(error, 255, "Invalid X channel index %d", user_settings.x_chan_idx[i]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      if (pulse_name_to_voltage_vector_idx.find(user_settings.x_blocks[i]) == pulse_name_to_voltage_vector_idx.end()) {
        snprintf(error, 255,"Unknown X pulse block '%s'", user_settings.x_blocks[i]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      x_voltage_idx[i] = pulse_name_to_voltage_vector_idx[user_settings.x_blocks[i]];
    }
  }

  if (y_enabled) {
    for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
      y_voltage_idx[i] = -1;

      if (strlen(user_settings.y_blocks[i]) == 0) {
        continue;
      }

      if (user_settings.y_chan_idx[i] >= SOFTDAC_NUM_CHANS) {
        snprintf(error, 255, "Invalid Y channel index %d", user_settings.y_chan_idx[i]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      if (pulse_name_to_voltage_vector_idx.find(user_settings.y_blocks[i]) == pulse_name_to_voltage_vector_idx.end()) {
        snprintf(error, 255, "Unknown Y pulse block '%s'", user_settings.y_blocks[i]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      y_voltage_idx[i] = pulse_name_to_voltage_vector_idx[user_settings.y_blocks[i]];
    }
  }

  return SUCCESS;
}

INT ScanDeviceSoftdac::read_default_voltages() {
  if (debug) {
    scan_utils::ts_printf("Reading default voltages\n");
  }

  INT status;
  char defaults_path[256];
  sprintf(defaults_path, SOFTDAC_DEFAULTS_PATH);

  HNDLE hkey;

  status = db_find_key(hDB, 0, defaults_path, &hkey);

  if (status != DB_SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to find defaults directory %s", defaults_path);
    return status;
  }

  for (auto it = pulse_name_to_voltage_vector_idx.begin(); it != pulse_name_to_voltage_vector_idx.end(); it++) {
    std::string pulse_name = it->first;
    int v_idx = it->second;

    if (v_idx < 0 || v_idx >= (int)default_chan_voltages.size()) {
      cm_msg(MERROR, __FUNCTION__, "Programming error - see v_idx of %i for pulse %s", v_idx, pulse_name);
      return FE_ERR_DRIVER;
    }

    HNDLE hsubkey;
    status = db_find_key(hDB, hkey, pulse_name.c_str(), &hsubkey);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to find defaults in %s/%s (code %d)", defaults_path, pulse_name.c_str(), status);
      return status;
    }

    double read_vals[SOFTDAC_NUM_CHANS];
    INT size = sizeof(read_vals);

    status = db_get_data(hDB, hsubkey, read_vals, &size, TID_DOUBLE);

    for (int i = 0; i < SOFTDAC_NUM_CHANS; i++) {
      default_chan_voltages[v_idx][i] = read_vals[i];
    }

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to read defaults in %s/%s (error %i)", defaults_path, pulse_name.c_str(), status);
      return status;
    }
  }

  return SUCCESS;
}

INT ScanDeviceSoftdac::fill_end_of_cycle_banks(char* pevent) {
  if (x_enabled) {
    float* pdata_x;
    bk_create(pevent, "XSFT", TID_FLOAT, (void**) &pdata_x);

    for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
      *pdata_x++ = scan_status.x_values[i];
    }

    bk_close(pevent, pdata_x);
  }

  if (y_enabled) {
    float* pdata_y;
    bk_create(pevent, "YSFT", TID_FLOAT, (void**) &pdata_y);

    for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
      *pdata_y++ = scan_status.y_values[i];
    }

    bk_close(pevent, pdata_y);
  }

  return SUCCESS;
}

INT ScanDeviceSoftdac::load_softdac() {
  softdac_SMDisable(softdac);

  for (unsigned int i = 0; i < this_step_chan_voltages.size(); i++) {
    scan_utils::ts_printf("Loading softdac with voltages: ");

    for (int c = 0; c < SOFTDAC_NUM_CHANS; c++) {
      printf("%f", this_step_chan_voltages[i][c]);

      int pts = i;
      softdac_LinLoad(softdac, this_step_chan_voltages[i][c], this_step_chan_voltages[i][c], 1, &pts, 0, c);
    }

    printf("\n");
  }

  softdac_SampleSet(softdac, 0, this_step_chan_voltages.size()); //  number of set points
  softdac_SMEnable(softdac);

  return SUCCESS;
}

void ScanDeviceSoftdac::print_sweep(std::vector<double>& values, char* block_name, DWORD chan_idx, bool is_x, int idx) {
  std::cout << "Sweep values for Softdac ";

  if (is_x) {
    std::cout << "X";
  } else {
    std::cout << "Y";
  }

  std::cout << " param #" << idx << " (" << block_name << "[" << chan_idx << "]" << ") are: ";

  for (unsigned int i = 0; i < values.size(); i++) {
    if (i > 0) {
      std::cout << ", ";
    }

    std::cout << values[i];
  }

  std::cout << std::endl;
}

void ScanDeviceSoftdac::compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) {
  for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
    if (direction_type_x != DirNoRecomputation) {
      if (strlen(user_settings.x_blocks[i]) == 0 || !x_enabled) {
        sweep_x[i].clear();
      } else {
        sweep_x[i] = compute_sweep_double(direction_type_x, user_settings.x_start[i], user_settings.x_end[i], nX);
        print_sweep(sweep_x[i], user_settings.x_blocks[i], user_settings.x_chan_idx[i], true, i);
      }
    }

    if (direction_type_y != DirNoRecomputation) {
      if (strlen(user_settings.y_blocks[i]) == 0 || !y_enabled) {
        sweep_y[i].clear();
      } else {
        sweep_y[i] = compute_sweep_double(direction_type_y, user_settings.y_start[i], user_settings.y_end[i], nY);
        print_sweep(sweep_y[i], user_settings.y_blocks[i], user_settings.y_chan_idx[i], false, i);
      }
    }
  }
}

INT ScanDeviceSoftdac::setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) {
  INT status = SUCCESS;
  bool load = false;

  if (bor) {
    // Start off with the default voltages
    int num_steps = default_chan_voltages.size();

    for (int i = 0; i < num_steps; i++) {
      for (int c = 0; c < SOFTDAC_NUM_CHANS; c++) {
        this_step_chan_voltages[i][c] = default_chan_voltages[i][c];
      }
    }

    load = true;
  }

  if (new_x) {
    for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
      if (sweep_x[i].size() == 0 || !x_enabled) {
        scan_status.x_values[i] = 0;
      } else {
        double voltage = sweep_x[i][x_step];
        scan_status.x_values[i] = voltage;

        int v_idx = x_voltage_idx[i];
        int c_idx = user_settings.x_chan_idx[i];
        this_step_chan_voltages[v_idx][c_idx] = voltage;
        load = true;
      }
    }
  }

  if (new_y) {
    for (int i = 0; i < SOFTDAC_SCAN_MAXVARS; i++) {
      if (sweep_y[i].size() == 0 || !y_enabled) {
        scan_status.y_values[i] = 0;
      } else {
        double voltage = sweep_y[i][x_step];
        scan_status.y_values[i] = voltage;

        int v_idx = y_voltage_idx[i];
        int c_idx = user_settings.y_chan_idx[i];
        this_step_chan_voltages[v_idx][c_idx] = voltage;
        load = true;
      }
    }
  }

  if (load) {
    status = load_softdac();

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to load softdac");
      return status;
    }
  }

  return status;
}

