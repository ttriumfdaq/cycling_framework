/*
 * Wrapper around softdac.c driver, exposing midas interface
 * for cycling framework.
 *
 * Softdac is a device that provides voltages (+/- 10V) on 8
 * channels. It can be programmed with lists of voltages to
 * be applied, and the index in the list stepped via a TTL
 * pulse (generally provided by a PPG).
 */

#ifndef SCAN_DEVICE_SOFTDAC_H
#define SCAN_DEVICE_SOFTDAC_H

#include "softdac.h"
#include "midas.h"
#include "core/scannable_device.h"
#include <map>
#include <string>
#include <vector>

#define SOFTDAC_SCAN_MAXVARS 24
#define SOFTDAC_NUM_CHANS 8

#define SOFTDAC_SCAN_SETTINGS_STR "\
PPG channel name = STRING : [32] CH1\n\
Softdac voltage range = STRING : [32] PM10V\n\
Channel names = STRING[8] :\n\
[32] Guards\n\
[32] Inj. Cap\n\
[32] Ring\n\
[32] Ext. Cap\n\
[32] Ext. Tube\n\
[32] Inj. Tube\n\
[32] broken\n\
[32] XDT\n\
X blocks = STRING[24] : \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
Y blocks = STRING[24] : \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
X chan index = DWORD[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
Y chan index = DWORD[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
X start = DOUBLE[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
Y start = DOUBLE[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
X end = DOUBLE[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
Y end = DOUBLE[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
"

#define SOFTDAC_SCAN_STATUS_STR "\
X values = DOUBLE[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
[23] 0\n\
Y values = DOUBLE[24] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
[20] 0\n\
[21] 0\n\
[22] 0\n\
"

// Struct that maps to /Scanning/Softdac/Settings in the ODB.
// The arrays of length SOFTDAC_SCAN_MAXVARS are used for configuring
// which voltages get scanned. E.g. element 4 in the arrays says the
// start/end voltages for a specific channel in a specific PPG block.
typedef struct {
  char ppg_channel_name[32]; // PPG channel name for stepping voltages (so we know how many voltages we expect to set)
  char voltage_range_str[32]; // Voltage range configured by user (e.g. PM10V)
  char softdac_channel_names[SOFTDAC_NUM_CHANS][32]; // Names of each softdac channel
  char x_blocks[SOFTDAC_SCAN_MAXVARS][200]; // Which PPG blocks have voltage scanning
  char y_blocks[SOFTDAC_SCAN_MAXVARS][200]; // Which PPG blocks have voltage scanning
  DWORD x_chan_idx[SOFTDAC_SCAN_MAXVARS]; // Which channel in each PPG block is being scanned
  DWORD y_chan_idx[SOFTDAC_SCAN_MAXVARS]; // Which channel in each PPG block is being scanned
  double x_start[SOFTDAC_SCAN_MAXVARS]; // Start voltage
  double y_start[SOFTDAC_SCAN_MAXVARS]; // Start voltage
  double x_end[SOFTDAC_SCAN_MAXVARS]; // End voltage
  double y_end[SOFTDAC_SCAN_MAXVARS]; // End voltage
} SOFTDAC_SCAN_SETTINGS_STRUCT;

// Struct that maps to /Scanning/Softdac/Status in the ODB.
// Contains the current voltage set for each scanned parameter.
typedef struct {
  double x_values[SOFTDAC_SCAN_MAXVARS];
  double y_values[SOFTDAC_SCAN_MAXVARS];
} SOFTDAC_SCAN_STATUS_STRUCT;

class ScanDeviceSoftdac: public ScannableDevice {
  public:
    ScanDeviceSoftdac(HNDLE _hDB) : ScannableDevice(_hDB) {
      softdac = NULL;
      softdac_open = false;
    }

    virtual ~ScanDeviceSoftdac() {};

    // Functions from ScannableDevice interface
    virtual INT check_records() override;
    virtual INT open_records() override;
    virtual INT close_records() override;
    virtual INT begin_of_run(char* error) override;
    virtual INT end_of_run() override;
    virtual INT setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) override;
    virtual void compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) override;
    virtual INT fill_end_of_cycle_banks(char* pevent) override;

  private:

    // Actually load the lists of voltages onto the softdac.
    INT load_softdac();

    // Check that all the PPG blocks the user has specified are
    // actually present in the current PPG program. Also check that
    // channels indices are acceptable.
    INT validate_softdac_scan_settings(char* error);

    // When debugging, print the current values chosen for a sweep.
    void print_sweep(std::vector<double>& values, char* block_name, DWORD chan_idx, bool is_x, int idx);

    // Get a list of PPG pulse names that will step the softdac voltages.
    // Works by calling the PPG frontend via RPC and asking it for the list
    // of pulses that act on our PPG channel (specified in user_settings.ppg_channel_name).
    // Fills pulse_name_to_voltage_vector_idx.
    INT populate_list_of_step_ppg_pulses(char* error);

    // Read the default voltages for each block/channel (i.e. the voltages
    // to use if something is not being scanned). Read from /Scanning/Softdac/Defaults
    // in the ODB.
    INT read_default_voltages(char* error);

    // Connect to the softdac and set the voltage range we want to use.
    INT init_softdac_device(char* error);

    // Settings from the ODB.
    SOFTDAC_SCAN_SETTINGS_STRUCT user_settings;

    // Status written to ODB.
    SOFTDAC_SCAN_STATUS_STRUCT scan_status;

    // Voltages we'll apply this loop.
    std::vector<double> sweep_x[SOFTDAC_SCAN_MAXVARS];
    std::vector<double> sweep_y[SOFTDAC_SCAN_MAXVARS];

    // PPG pulse name -> index in default_chan_voltages/
    // this_step_chan_voltages where the voltages for that
    // block are found.
    std::map<std::string, int> pulse_name_to_voltage_vector_idx;

    // For each voltage being scanned, the index in
    // default_chan_voltages/this_step_chan_voltages where the
    // voltage is found. Element i is the same as doing
    // pulse_name_to_voltage_vector_idx[user_settings.x_blocks[i]]
    INT x_voltage_idx[SOFTDAC_SCAN_MAXVARS];
    INT y_voltage_idx[SOFTDAC_SCAN_MAXVARS];

    // Default voltages for each block/channel.
    // First index is block number (i.e. order in which voltages
    // are stepped through by PPG sequence). Second index is
    // channel number.
    std::vector<std::vector<double> > default_chan_voltages;

    // Voltages to be applied this X/Y step for each block/channel.
    // First index is block number (i.e. order in which voltages
    // are stepped through by PPG sequence). Second index is
    // channel number.
    std::vector<std::vector<double> > this_step_chan_voltages;

    // Low-level driver.
    ALPHISOFTDAC* softdac;

    // Whether we've connected to the softdac.
    bool softdac_open;
};

#endif
