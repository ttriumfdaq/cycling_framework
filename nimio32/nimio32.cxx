/*********************************************************************

 Name:         nimio.c
 Created by:   Pierre-Andre Amaudruz

 Contents:      Routines for accessing the VMEIO Triumf board
 *********************************************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include "nimio32.h"

/********************************************************************/

uint32_t nimio_CommandReg(MVME_INTERFACE *myvme, DWORD base, DWORD data) {
  uint32_t my_data;
  //data = 0 (no-op) 1 (reset) etc.
  nimio_write32(myvme, base, IO32_COMMAND, data & 0xF);
  my_data = nimio_read32(myvme, base, IO32_COMMAND);
  return (my_data & 0xF);
}

uint32_t nimio_Reset(MVME_INTERFACE *myvme, DWORD base) {
  uint32_t data;
  data = nimio_CommandReg(myvme, base, IO32_CMD_RESET); // General Reset = 1
  return data;
}

void nimio_OutputSet(MVME_INTERFACE *myvme, DWORD base, DWORD data) {
  DWORD data1, data2;
  data2 = data & 0xFFFF; // get just lower bits 0-16 NIM outputs

  data1 = nimio_read32(myvme, base, IO32_NIMOUT); // read whole word 32 bits
  data1 = data1 & 0xFFFF0000; // get upper output control bits 16-31

  printf("nimio_OutputSet: currently output control bits (16-31) are 0x%4.4x\n", data1);
  data1 = data2 | data1; // OR
  printf("nimio_OutputSet: writing NIM output word as  0x%8.8x\n", data1);
  nimio_write32(myvme, base, IO32_NIMOUT, data1);
}

uint32_t nimio_OutputRead(MVME_INTERFACE *myvme, DWORD base) {
  uint32_t data;
  data = nimio_read32(myvme, base, IO32_NIMOUT);
  return (data & 0xFFFF);
}

uint32_t nimio_OutputControlRead(MVME_INTERFACE *myvme, DWORD base) {
  uint32_t data;
  data = nimio_read32(myvme, base, IO32_NIMOUT);
  return ((data & 0xFFFF0000) >> 16);
}

void nimio_OutputControlSet(MVME_INTERFACE *myvme, DWORD base, DWORD data) {
  uint32_t my_data, data1;
  my_data = (data & 0xFFFF) << 16;

  data1 = nimio_read32(myvme, base, IO32_NIMOUT); // read whole word
  data1 = data1 & 0xFFFF; // get lower bits NIM outputs 0-15
  printf("Currently NIM outputs 0-15 are 0x%4.4x\n", data1);
  data1 = my_data | data1; // OR
  printf("Writing 0x%x to NIM output reg\n", data1);
  nimio_write32(myvme, base, IO32_NIMOUT, data1);
}

void nimio_SetOneOutput(MVME_INTERFACE *myvme, DWORD base, DWORD bit) {
  uint32_t data;

  data = nimio_read32(myvme, base, IO32_NIMOUT);
  data = nimio_bitset(bit, data);
  nimio_write32(myvme, base, IO32_NIMOUT, data);
}

void nimio_ClrOneOutput(MVME_INTERFACE *myvme, DWORD base, DWORD bit) {
  uint32_t data;

  data = nimio_read32(myvme, base, IO32_NIMOUT);
  data = nimio_bitclear(bit, data);
  nimio_write32(myvme, base, IO32_NIMOUT, data);
}

uint32_t nimio_ReadOneInput(MVME_INTERFACE *myvme, DWORD base, DWORD bit) {
  uint32_t data;
  uint32_t ival;
  data = nimio_read32(myvme, base, IO32_NIMIN);
  ival = 1 << bit;
  return (data & ival);
}

uint32_t nimio_ReadOneLatchedInput(MVME_INTERFACE *myvme, DWORD base, DWORD bit) {
  uint32_t data;
  uint32_t ival;
  data = nimio_read32(myvme, base, IO32_NIMIN);
  ival = 1 << bit;
  data = data >> 16;  // latched bits in upper 16 bits;
  return (data & ival);
}

void nimio_ResetLatch(MVME_INTERFACE *myvme, DWORD base, DWORD data) {
  // mvme_write_value(myvme, base+IO32_NIMOUT, data & 0xFFFFFF);
  nimio_write32(myvme, base, IO32_NIMIN, data & 0xFFFF);
}

uint32_t nimio_InputRead(MVME_INTERFACE *myvme, DWORD base) {
  uint32_t data;
  data = nimio_read32(myvme, base, IO32_NIMIN);
  return data;
}

uint32_t nimio_FirmwareRead(MVME_INTERFACE *myvme, DWORD base) {
  uint32_t data;
  data = nimio_read32(myvme, base, IO32_REVISION);
  return data;
}

/********************************************************************/

/********************************************************************/
/**
 Read the NIMIO32
 @param myvme vme structure
 @param base  VMEIO base address
 @return register value
 */

uint32_t nimio_read32(MVME_INTERFACE *myvme, DWORD base, DWORD offset) {
  mvme_set_am(myvme, MVME_AM_A24);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  return mvme_read_value(myvme, base + offset);
}

void nimio_write32(MVME_INTERFACE *myvme, DWORD base, DWORD offset, uint32_t data) {
  mvme_set_am(myvme, MVME_AM_A24);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  mvme_write_value(myvme, base + offset, data);
}

uint32_t nimio_bitset(int bit, uint32_t data) {
  uint32_t ival;
  ival = 1 << bit;
  data = data | ival;
  return data;
}

uint32_t nimio_bitclear(int bit, uint32_t data) {
  uint32_t ival;
  ival = 1 << bit;
  data = data & ~ival;
  return data;
}

void read_bnmrinputs(MVME_INTERFACE *myvme, DWORD base) {
  uint32_t data, data1, data2;
  // Input 0 BEAM
  //       1 HEL DWN
  //       2 HEL UP
  //       3 HEL DWN Latched by beam
  //       4 HEL UP Latched by beam 

  int beam, held, helu, lheld, lhelu;
  data = nimio_InputRead(myvme, base);
  data1 = data & 0xFFFF0000;
  data2 = data & 0x0000FFFF;
  printf(" data=0x%8.8x; Latched NIM inputs 0x%4.4x, NIM inputs=0x%4.4x\n", data, (data1 >> 16), data2);

  beam = held = helu = lheld = lhelu = 0;
  if (data & 1)
    beam = 1;
  if (data & 2)
    held = 1;
  if (data & 4)
    helu = 1;
  if (data & 8)
    lheld = 1;
  if (data & 16)
    lhelu = 1;

  printf("Beam %d   hel down = %d hel up=%d    -->  latched  hel down = %d hel up = %d \n", beam, held, helu, lheld, lhelu);
  return;
}

