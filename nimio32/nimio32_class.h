#ifndef NIMIO32_CLASS_H
#define NIMIO32_CLASS_H

#include "midas.h"
#include <map>
#include <vector>
#include <string>

#ifndef DUMMY_VME
#include "mvmestd.h"
#endif

// Object-oriented interface for the NIMIO32, which is a VME module
// that allows a input/output LEMO connectors to be queried/set via
// register manipulation.
//
// This OO interface allows you to specify names for each input/output channel.
class NIMIO32 {
  public:

#ifdef DUMMY_VME
    NIMIO32() {};
#else
    NIMIO32(MVME_INTERFACE* _myvme, DWORD _vme_base);
#endif

    ~NIMIO32() {};

    // Set input/output channel names. Index of the vector is the channel number.
    void set_channel_names(std::vector<std::string> inputs, std::vector<std::string> outputs);

    void set_output_state(DWORD channel, bool on_off);
    void set_output_state(std::string channel, bool on_off);

    bool get_input_state(DWORD channel);
    bool get_input_state(std::string channel);

    bool get_latched_input_state(DWORD channel);
    bool get_latched_input_state(std::string channel);

  private:

#ifndef DUMMY_VME
    MVME_INTERFACE* myvme;
    DWORD VME_BASE;
#endif

    std::map<std::string, DWORD> input_names;
    std::map<std::string, DWORD> output_names;
};


#endif
