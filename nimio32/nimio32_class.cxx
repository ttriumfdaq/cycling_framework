#include "nimio32_class.h"
#include "nimio32.h"
#include "midas.h"

#ifndef DUMMY_VME
#include "mvmestd.h"

NIMIO32::NIMIO32(MVME_INTERFACE* _myvme, DWORD _vme_base = 0x100000) {
  myvme = _myvme;
  VME_BASE = _vme_base;
  nimio_Reset(myvme, VME_BASE);
}
#endif

void NIMIO32::set_channel_names(std::vector<std::string> inputs, std::vector<std::string> outputs) {
  for (int i = 0; i < inputs.size(); i++) {
    input_names[inputs[i]] = i;
  }

  for (int i = 0; i < outputs.size(); i++) {
    output_names[outputs[i]] = i;
  }
}

void NIMIO32::set_output_state(std::string channel, bool on_off) {
  return set_output_state(output_names.at(channel), on_off);
}

void NIMIO32::set_output_state(DWORD channel, bool on_off) {
#ifndef DUMMY_VME
  if (on_off) {
    nimio_SetOneOutput(myvme, VME_BASE, channel);
  } else {
    nimio_ClrOneOutput(myvme, VME_BASE, channel);
  }
#endif
}

bool NIMIO32::get_input_state(std::string channel) {
  return get_input_state(input_names.at(channel));
}

bool NIMIO32::get_input_state(DWORD channel) {
#ifdef DUMMY_VME
  return false;
#else
  return nimio_ReadOneInput(myvme, VME_BASE, channel) != 0;
#endif
}

bool NIMIO32::get_latched_input_state(std::string channel) {
  return get_latched_input_state(input_names.at(channel));
}

bool NIMIO32::get_latched_input_state(DWORD channel) {
#ifdef DUMMY_VME
  return false;
#else
  return nimio_ReadOneLatchedInput(myvme, VME_BASE, channel) != 0;
#endif
}


