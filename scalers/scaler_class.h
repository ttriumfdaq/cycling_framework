#ifndef MCS_CLASS_H
#define MCS_CLASS_H

#include "midas.h"
#include <string>
#include "core/data_source.h"

// Helper struct for getting data from a scaler. Some fields
// may not be relevant for all scaler models.
struct ScalerDataPoint {
  bool exists; // Set to false when there are no more data points to read.
  int chan_idx; // Channel ID
  int user_bits; // Extra metadata (e.g. user bits from SIS scalers)
  int count; // The actual count in this bin
};

// Base class for all scalers, to help integration with the cycling framework.
// Each instance creates an in-memory buffer for reading data from the scaler
// for processing.
class Scaler : public DataSource {
  public:

    // Constructor. Initiates a buffer of buffer_size bytes.
    // The buffer should be at least as big as the internal buffer of the scaler.
    Scaler(INT buffer_size);
    virtual ~Scaler();

    // Sanity check that you're talking to the right model of scaler.
    // E.g. if it's a VME scaler, check the base address makes sense.
    virtual INT check_module_id() { return SUCCESS; };

    // Reset the scaler to a known state.
    virtual void reset() {};

    // Start the scaler.
    virtual void start() {};

    // Stop the scaler.
    virtual void stop() {};

    // Set which channels the scaler should enable, if it supports this.
    virtual void set_enabled_channels_bitmask(DWORD mask) = 0;

    // Set how many bins the scaler should expect.
    virtual void set_num_bins(int num_bins) = 0;

    // Return the number of bins the scaler has seen so far.
    virtual int num_bins_seen() = 0;

    // Get the index of the first channel the scaler is reading out.
    virtual int get_first_enabled_chan_idx() = 0;

    // Whether the scaler's internal buffer is over half-full, and
    // should be read out now to avoid it getting completely full.
    virtual bool is_over_half_full() { return false; }

    // Whether the scaler's internal buffer is completely full.
    virtual bool is_full() { return false; }

    // Whether the scaler's internal buffer is empty.
    virtual bool is_empty() { return false; }

    // Do any begin-of-run initialization.
    // Populate error message if needed.
    virtual INT begin_of_run(char* error) override;

    // Do any end-of-run tear-down.
    virtual INT end_of_run() override;

    // Note that a new cycle is starting.
    // By default this just calls start().
    virtual void new_cycle_starting(int cycle_count) override;

    // Note that this cycle is finished.
    // By default this does nothing.
    virtual void this_cycle_finished() override;

    // By default this checks if the scaler's internal buffer
    // is over half full, and if so reads it out.
    virtual void mid_cycle_read_data_if_needed() override;

    // By default this reads out all the data that remains in
    // the scaler's internal buffer.
    virtual DataStatus end_cycle_is_data_ready() override;

    // If desired, you can convert the data from the in-memory buffer
    // to a midas bank. This may not be necessary if you will be using
    // the get_next_data_point() interface to manipulate the data instead.
    virtual void write_data_to_banks(char *pevent) override {};

    // Get the number of words that have been read into our in-memory
    // buffer.
    virtual DWORD get_num_words_read() { return num_words_read; }

    // Provide an interface for iterating over the in-memory buffer, returning
    // the data as a struct. This may not be necessary if you will just be
    // populating a midas bank.
    virtual ScalerDataPoint get_next_data_point() = 0;

  protected:
    // Read data from the scaler's internal buffer into our
    // in-memory buffer.
    //
    // * num_words will be filled with the number of words we read.
    // * data points to where we'll write to.
    // * buffer_size_bytes is how much space we have left in `data`.
    // * flush_everything means to ensure the internal buffer becomes completely empty.
    virtual void read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything=true) = 0;

    int num_words_read;
    DWORD* buffer;
    int buffer_pos;
    DWORD buffer_size_bytes;
};

#endif
