#ifndef MCS_SIS3820
#define MCS_SIS3820

#include "scalers/scaler_class.h"
#include <string>

#ifndef DUMMY_VME
#include "mvmestd.h"

// Interface for a Struck SIS3820 module.
// See the Scaler base class header for most documentation.
// This module allows reading an arbitrary combination of channels.
class ScalerSIS3820 : public Scaler {
  public:
    ScalerSIS3820(MVME_INTERFACE* _vme, DWORD _base);
    virtual ~ScalerSIS3820();

    virtual INT check_module_id() override;

    virtual void reset() override;
    virtual void start() override;
    virtual void stop() override;

    virtual void set_enabled_channels_bitmask(DWORD mask) override;
    virtual void set_num_bins(int num_bins) override;
    virtual int get_first_enabled_chan_idx() override { return first_chan; }
    virtual int num_bins_seen() override;

    virtual bool is_over_half_full() override;
    virtual bool is_full() override;
    virtual bool is_empty() override;

    virtual ScalerDataPoint get_next_data_point() override;

    // Set the first channel to record the 25MHz reference frequency.
    virtual void enable_ref1(bool enable);

    // Instead of immediately enabling the scaler when calling start(),
    // arm it instead (so it won't start until the first "LNE" pulse is seen.
    // This behaviour was used in the old BNQR frontend.
    // When arming, the acquisition count isn't reset until the first LNE pulse
    // is seen either.
    virtual void use_arm_instead_of_enable(bool arm);

  protected:
    virtual void read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything=true) override;

    virtual DWORD fifo_fill_level();

    MVME_INTERFACE* myvme;
    DWORD VME_BASE;
    int first_chan;
    bool use_arm;
    INT num_preset_bins;

    // When using ARM instead of ENABLE, the acquisition count isn't reset until
    // the first LNE pulse is seen. So you can't trivially tell whether an acq
    // count == num_preset_bins means that the scaler hasn't seen the first LNE pulse
    // yet, or has seen the LNE pulse and has seen all the bins of this cycle too.
    // We use this boolean to track whether we've seen a non-full count at any point during
    // our rapid polling of the acquisition count, so we therefore can distinguish the cases.
    bool seen_partial_count_since_arm;
};

#endif // DUMMY_VME

// Dummy version of an SIS3820 that can be used for testing
// and doesn't require a VME crate.
class DummyScalerSIS3820 : public Scaler {
  public:
    // Specify how long you would like the Scaler to pretend
    // to take to acquire all the bins.
    DummyScalerSIS3820(float _acq_time_ms);
    virtual ~DummyScalerSIS3820() {}

    virtual INT check_module_id() override { return SUCCESS; }

    virtual void reset() override {};
    virtual void start() override;
    virtual void stop() override {};

    virtual void set_enabled_channels_bitmask(DWORD mask) override;
    virtual void set_num_bins(int n_bins) override { num_bins = n_bins; }
    virtual int get_first_enabled_chan_idx() override { return first_chan; }
    virtual int num_bins_seen() override;

    virtual bool is_over_half_full() override { return false; }
    virtual bool is_full() override { return false; }
    virtual bool is_empty() override { return !have_read_this_cycle; }

    virtual ScalerDataPoint get_next_data_point() override;

    virtual void enable_ref1(bool enable) {};

  protected:
    virtual void read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything=true) override;

    INT num_bins;
    DWORD chan_mask;
    int first_chan;
    bool have_read_this_cycle;
    float acq_time_ms;
    double cycle_start_time_ms;
};

#endif
