#include "core/scan_utils.h"
#include "scaler_sis3820.h"

#ifndef DUMMY_VME
#include "sis3820drv.h"
#endif

ScalerDataPoint sis3820_get_next_data_point(DWORD* buffer, int& buffer_pos, DWORD num_words_read) {
  ScalerDataPoint data;

  if (buffer_pos >= num_words_read) {
    data.exists = false;
  } else {
    /*
     UUFCCCCCddddDDDDddddDDDDddddDDDD
     where U: user bits (front panel inputs)
     F: latch bit (not used)
     C: channel bit (C4..C0)
     dD: data bit  (24bit)
     */
    data.exists = true;
    DWORD raw = buffer[buffer_pos];
    data.user_bits = (raw >> 30);
    data.chan_idx = ((raw >> 24) & 0x1F);
    data.count = (raw & 0xFFFFFF);
    buffer_pos++;
  }

  return data;
}
#ifndef DUMMY_VME

ScalerSIS3820::ScalerSIS3820(MVME_INTERFACE *_vme, DWORD _base) : Scaler(256*1024) {
  myvme = _vme;
  VME_BASE = _base;
  first_chan = 0;
  use_arm = false;
  seen_partial_count_since_arm = false;
  num_preset_bins = 0;

  reset();
  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_LNE_PRESCALE, 1);

  DWORD input_mode = 3 << 16;
  DWORD output_mode = 2 << 20;

  DWORD opbits = SIS3820_CLEARING_MODE |      //   bit 0    set clearing mode
    SIS3820_MCS_DATA_FORMAT_24BIT |           //   24 bits of data to match 3801 data format
    SIS3820_LNE_SOURCE_CONTROL_SIGNAL|        //   bit 4-6  external LNE signal
    SIS3820_ARM_ENABLE_CONTROL_SIGNAL|        //   LNE as enable
    SIS3820_FIFO_MODE|                        //   bit 12,13 FIFO mode
    SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|     //   bit 28-31  MCS mode
    input_mode |                  //   bit 16-18 Input mode
    output_mode;             //   bit 20,21 Output mode

  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_OPERATION_MODE, opbits);
  sis3820_Status(myvme, VME_BASE);
}

ScalerSIS3820::~ScalerSIS3820() {
  stop();
}

INT ScalerSIS3820::check_module_id() {
  DWORD full_modid = sis3820_RegisterRead(myvme, VME_BASE, SIS3820_MODID);
  DWORD firmware = (full_modid & 0xFFFF) >> 12;
  DWORD module_id = full_modid >> 16;

  scan_utils::ts_printf("Base Address 0x%x  Module ID: 0x%x and Firmware: 0x%x\n", VME_BASE, module_id, firmware);

  if (module_id != 0x3820) {
    cm_msg(MERROR, __FUNCTION__, "Wrong VME module (0x%x) is installed at Base Address 0x%x . Expecting a SIS3820", module_id, VME_BASE);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

void ScalerSIS3820::reset() {
  sis3820_Reset(myvme, VME_BASE);
}

void ScalerSIS3820::start() {
  if (use_arm) {
    sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_KEY_OPERATION_ARM, 0);
    seen_partial_count_since_arm = false;
  } else {
    sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_KEY_OPERATION_ENABLE, 0);
  }
}

void ScalerSIS3820::stop() {
  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_KEY_OPERATION_DISABLE, 0);
  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_KEY_COUNTER_CLEAR,0);
}

void ScalerSIS3820::set_enabled_channels_bitmask(DWORD enabled_chans) {
  DWORD disabled_chans = ~enabled_chans;
  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_COPY_DISABLE, disabled_chans);

  first_chan = 0;

  for (int i = 0; i < 32; i++) {
    if (enabled_chans & (1<<i)) {
      first_chan = i;
      break;
    }
  }
}

void ScalerSIS3820::set_num_bins(int num_bins) {
  num_preset_bins = num_bins;
  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_ACQUISITION_PRESET, num_bins);
}

void ScalerSIS3820::enable_ref1(bool enable) {
  DWORD csrbits = CTRL_COUNTER_TEST_25MHZ_DISABLE |  CTRL_COUNTER_TEST_MODE_DISABLE;

  if (enable) {
    scan_utils::ts_printf("Enabling 3820 Ref ch1\n");
    csrbits |= CTRL_REFERENCE_CH1_ENABLE;
  } else {
    scan_utils::ts_printf("Disabling 3820 Ref ch1\n");
    csrbits |= CTRL_REFERENCE_CH1_DISABLE;
  }

  sis3820_RegisterWrite(myvme, VME_BASE, SIS3820_CONTROL_STATUS, csrbits );
}

int ScalerSIS3820::num_bins_seen() {
  int bins = sis3820_RegisterRead(myvme, VME_BASE, SIS3820_ACQUISITION_COUNT);

  if (!seen_partial_count_since_arm && bins >= num_preset_bins) {
    // -1 waiting for first bin to even start ARM.
    return -1;
  } else {
    seen_partial_count_since_arm = true;
    return bins;
  }
}

bool ScalerSIS3820::is_over_half_full() {
  // Not really half full, just a reasonable amount of data to read.
  // TODO - old DAQ had a 1s cooldown too.
  return fifo_fill_level() > 32*1024;
}

bool ScalerSIS3820::is_full() {
  return false;
}

bool ScalerSIS3820::is_empty() {
  return sis3820_DataReady(myvme, VME_BASE) > 0;
}

DWORD ScalerSIS3820::fifo_fill_level() {
  return sis3820_RegisterRead(myvme, VME_BASE, SIS3820_FIFO_WORDCOUNTER);
}

void ScalerSIS3820::read(int &num_words, DWORD *data, DWORD buffer_size_bytes, bool flush_everything) {
  DWORD start_level = fifo_fill_level();

  if (start_level == 0) {
    num_words = 0;
    return;
  }

  int max_dma_bytes = 64*1024;
  int free_bytes = buffer_size_bytes;  // bytes (actually the fifo array size)

  DWORD this_read = start_level;
  DWORD to_read = this_read;
  num_words = 0;

  while (to_read > 0 && free_bytes > 0) {
    if (this_read * sizeof(DWORD) > free_bytes) {
      this_read = free_bytes/sizeof(DWORD);
    }

    if (this_read * sizeof(DWORD) > max_dma_bytes) {
      this_read = max_dma_bytes/sizeof(DWORD);
    }

    sis3820_FifoRead(myvme, VME_BASE, (char *)data, this_read);

    num_words += this_read;
    to_read -= this_read;
    free_bytes -= this_read * sizeof(DWORD);
  }
}

ScalerDataPoint ScalerSIS3820::get_next_data_point() {
  return sis3820_get_next_data_point(buffer, buffer_pos, num_words_read);
}

#endif // DUMMY_VME

DummyScalerSIS3820::DummyScalerSIS3820(float _acq_time_ms) : Scaler(256*1024) {
  num_bins = 0;
  chan_mask = 0;
  first_chan = 0;
  have_read_this_cycle = false;
  acq_time_ms = _acq_time_ms;
  cycle_start_time_ms = 0;
}

void DummyScalerSIS3820::start() {
  have_read_this_cycle = false;
  cycle_start_time_ms = scan_utils::current_time_ms();
}

void DummyScalerSIS3820::set_enabled_channels_bitmask(DWORD mask) {
  chan_mask = mask;

  first_chan = 0;

  for (int i = 0; i < 32; i++) {
    if (chan_mask & (1<<i)) {
      first_chan = i;
      break;
    }
  }

}

int DummyScalerSIS3820::num_bins_seen() {
  if (num_bins == 0) {
    return 0;
  }

  double now = scan_utils::current_time_ms();

  int retval = num_bins * ((now - cycle_start_time_ms) / acq_time_ms);

  if (retval > num_bins) {
    return num_bins;
  } else {
    return retval;
  }
}

void DummyScalerSIS3820::read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything) {
  /*
       UUFCCCCCddddDDDDddddDDDDddddDDDD
       where U: user bits (front panel inputs)
       F: latch bit (not used)
       C: channel bit (C4..C0)
       dD: data bit  (24bit)

             data.user_bits = (raw >> 30);
      data.chan_idx = ((raw >> 24) & 0x1F);
      data.count = (raw & 0xFFFFFF);

 */
  have_read_this_cycle = true;
  num_words = 0;

  for (DWORD bin = 0; bin < (DWORD)num_bins; bin++) {
    for (DWORD chan = 0; chan < 32; chan++) {
      if (! (chan_mask & (1<<chan))) {
        continue;
      }

      num_words++;

      if (num_words * sizeof(DWORD) >= buffer_size_bytes) {
        return;
      }

      // For now, no setting of user bits, and just set data
      // content to bin number.
      DWORD user_bits = 0;
      DWORD latch_bit = 0;
      DWORD scaler_count = bin + 1;
      DWORD this_word = ((user_bits & 0x3) << 30) | ((latch_bit & 0x1) << 29) | ((chan & 0x1F) << 24) | (scaler_count & 0xFFFFFF);
      *data++ = this_word;
    }
  }
}

ScalerDataPoint DummyScalerSIS3820::get_next_data_point() {
  return sis3820_get_next_data_point(buffer, buffer_pos, num_words_read);
}

