
/*********************************************************************

  Name:         sis3820.c
  Created by:   K.Olchanski

  Contents:     SIS3820 32-channel 32-bit multiscaler
                
  $Log: sis3820.c,v $
  Revision 1.1  2013/01/21 21:32:17  suz
  Initial version for bnqr (VMIC)

  Revision 1.1  2006/05/25 05:53:42  alpha
  First commit

*********************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h> // pow
#include "sis3820drv.h"
#include "sis3820.h"
//#include "mvmestd.h"
#include "unistd.h" // for sleep
/*****************************************************************/


uint32_t write_reg (MVME_INTERFACE *myvme,  DWORD SIS3820_BASE,  uint32_t register, uint32_t data );
void sis3820_CSR0(MVME_INTERFACE *mvme, DWORD base);

#ifndef MAIN_ENABLE
extern int dsis; // debug
#else
int dsis = 0;
#endif

/*
Read sis3820 register value
*/
static uint32_t regRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  return mvme_read_value(mvme, base + offset);
}

/*****************************************************************/
/*
Write sis3820 register value
*/
static void regWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  mvme_set_am(mvme, MVME_AM_A32);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  mvme_write_value(mvme, base + offset, value);
}

/*****************************************************************/
uint32_t sis3820_RegisterRead(MVME_INTERFACE *mvme, DWORD base, int offset)
{
  return regRead(mvme, base, offset);
}

/*****************************************************************/
void     sis3820_RegisterWrite(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value)
{
  regWrite(mvme,base,offset,value);
}

/*****************************************************************/
uint32_t sis3820_ScalerRead(MVME_INTERFACE *mvme, DWORD base, int ch)
{
  if (ch == 0)
    return regRead(mvme, base, SIS3820_COUNTER_CH1+ch*4);
  else
    return regRead(mvme, base, SIS3820_COUNTER_SHADOW_CH1+ch*4);
}


/*****************************************************************/
/*
Read nentry of data from the data buffer. Will use the DMA engine
if size is larger then 127 bytes. 
*/
int sis3820_FifoRead(MVME_INTERFACE *mvme, DWORD base, void *pdest, int wcount)
{
  int rd;
  int save_am;

    mvme_get_am(mvme,&save_am);
    // mvme_set_blt(  mvme, MVME_BLT_MBLT64);
    // mvme_set_am(   mvme, MVME_AM_A32_D64);

    mvme_set_blt(  mvme, MVME_BLT_NONE); // SD  DMA not working

  rd = mvme_read(mvme, pdest, base + SIS3820_FIFO_BASE, wcount*4);
  //  printf("fifo read wcount: %d, rd: %d\n", wcount, rd);
  mvme_set_am(mvme, save_am);


  return wcount;
}

int sis3820_sdramRead(MVME_INTERFACE *mvme, DWORD base, void *pdest, int wcount)
{
  int rd=0;
  int save_am;

    mvme_get_am(mvme,&save_am);
    mvme_set_blt(  mvme, MVME_BLT_MBLT64);
    mvme_set_am(   mvme, MVME_AM_A32_D64);
  printf("sdram wcount: %d\n", wcount);
  rd = mvme_read(mvme, pdest, base + SIS3820_SDRAM_BASE, wcount*4);
  printf("sdram read wcount: %d, rd: %d\n", wcount, rd);
  mvme_set_am(mvme, save_am);

  if(rd < 0) 
    return -1;
  else
    return wcount;
}


/*****************************************************************/
void sis3820_Reset(MVME_INTERFACE *mvme, DWORD base)
{
  regWrite(mvme,base,SIS3820_KEY_RESET,0);
}

/*****************************************************************/
int  sis3820_DataReady(MVME_INTERFACE *mvme, DWORD base)
{
  return regRead(mvme,base,SIS3820_FIFO_WORDCOUNTER);
}

/*****************************************************************/
void  sis3820_Status(MVME_INTERFACE *mvme, DWORD base)
{
  printf("SIS3820 at A32 0x%x\n", (int)base);
  printf("ModuleID and Firmware: 0x%x\n", regRead(mvme,base,SIS3820_MODID));
  // printf("CSR: 0x%x\n", regRead(mvme,base,SIS3820_CONTROL_STATUS));

  sis3820_CSR0(mvme,base);


  printf("Operation mode: 0x%x\n", regRead(mvme,base,SIS3820_OPERATION_MODE));
  printf("Inhibit/Count disable: 0x%x\n", regRead(mvme, base, SIS3820_COUNTER_INHIBIT));
  printf("Counter Overflow: 0x%x\n", regRead(mvme, base, SIS3820_COUNTER_OVERFLOW));
}

void sis3820_CSR0(MVME_INTERFACE *mvme, DWORD base)
{
  uint32_t bitpat;
  char onof[2][4]={"off","on"};
  char enab[2][10]={"disabled","enabled"};
  char acti[2][11]={"NOT active","active"};
  char arm[2][10]={"NOT armed","Armed"}; 
  char set[2][6]={"set","clear"}; 
  char tmp[30];

  bitpat = regRead(mvme,base,SIS3820_CONTROL_STATUS);
  printf("CSR: 0x%x\n", bitpat);
  
  sprintf (tmp, "User LED %s",onof[((bitpat & 0x1 ) ? 1:0)]); // bit 0
  printf ("%-25s",tmp);
  sprintf (tmp,"25MHz Pulses %s", enab[((bitpat & 0x10) ? 1:0)]); // bit 4
  printf ("%-25s",tmp);
  sprintf (tmp,"Cntr TestMode %s",enab[((bitpat & 0x20) ? 1:0)]); // bit 5
  printf ("%-25s",tmp);
  sprintf (tmp,"50MHz RefCh1 %s", enab[((bitpat & 0x40) ? 1:0)]); // bit 6
  printf ("%-25s\n",tmp);
  sprintf (tmp,"Sclr enable %s", acti[((bitpat & 0x10000) ? 1:0)]); // bit 16
  printf ("%-25s",tmp);
  sprintf (tmp,"MCS enable %s", acti[((bitpat & 0x40000) ? 1:0)]); // bit 18
  printf ("%-25s",tmp);
  sprintf (tmp,"SDRAM/FIFO test %s", enab[((bitpat & 0x800000) ? 1:0)]); // bit 23
  printf ("%-25s",tmp);
  printf ("%-10s\n", arm[((bitpat & 0x1000000) ? 1:0)]); // bit 24
  sprintf (tmp,"HISCAL %s", enab[((bitpat & 0x2000000) ? 1:0)]); // bit 25
  printf ("%-25s",tmp);  sprintf (tmp,"Overflow %s", set[((bitpat & 0x8000000) ? 1:0)]); // bit 27
  sprintf (tmp,"Input bits %d", (bitpat & 0x30000000 >> 28) ); // bits 28,29
  printf ("%-25s",tmp);
  sprintf (tmp,"Ext Latch bits %d\n\n", (bitpat & 0xC0000000 >> 30) ); // bits 30,31
  printf ("%-25s\n",tmp);  return;
}

#ifdef TEST1
/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {
  int i,j,k;
  uint32_t SIS3820_BASE  = 0x38000000;
  uint32_t dest[32], scaler;
  uint32_t bits;
  uint32_t data,reg;
  MVME_INTERFACE *myvme;
  int status,wcount,wc;
  int scancount,oldscancount;
  uint32_t data_buffer [100];
  
  dsis=0; // debug

  int nbins =  3; // number of channel advances

  if (argc>1) {
    sscanf(argv[1],"%x", &SIS3820_BASE);
  }

  // Test under vmic
  status = mvme_open(&myvme, 0);
  sis3820_Reset(myvme,  SIS3820_BASE);

  data = 0xFFF0FFFF;// disable all except channels 17-21
  reg  =  SIS3820_COPY_DISABLE;
  write_reg(myvme, SIS3820_BASE, reg, data);

  reg = SIS3820_ACQUISITION_PRESET;  // set number of acquistions (bins)
  write_reg(myvme, SIS3820_BASE, reg, nbins);

  reg =  SIS3820_LNE_PRESCALE; // enable LNE prescaler
  data = 9999999;
  write_reg(myvme, SIS3820_BASE, reg, data);
  int t=10*pow(10,6); // 10MHz 
  printf ("Frequency of LNE pulses will be %d sec\n", t/(data+1) );



  // Select internal 10 MHz clock as LNE source, select MCS + SDRAM mode
  //  bits =  SIS3820_LNE_SOURCE_INTERNAL_10MHZ |  SIS3820_OP_MODE_MULTI_CHANNEL_SCALER |  SIS3820_SDRAM_MODE ;
  bits = SIS3820_CLEARING_MODE | SIS3820_MCS_DATA_FORMAT_32BIT | 
    SIS3820_LNE_SOURCE_INTERNAL_10MHZ |  SIS3820_OP_MODE_MULTI_CHANNEL_SCALER |
    SIS3820_SDRAM_MODE ;

  reg =   SIS3820_OPERATION_MODE;
  write_reg(myvme, SIS3820_BASE, reg, bits);

// enable counting, S LED should come on 
  printf ("\nWriting 0x%x to reg 0x%x (S LED should come on)\n", 
	  SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE);
  sis3820_RegisterWrite(myvme, SIS3820_BASE,SIS3820_KEY_OPERATION_ENABLE, SIS3820_KEY_OPERATION_ENABLE); 

  // read status
  sis3820_Status(myvme, SIS3820_BASE);

  printf ("Waiting 1s...\n");
  sleep(1);

  scancount=0;
  oldscancount=0;
  while (scancount!=nbins) {
    scancount =   sis3820_RegisterRead(myvme, SIS3820_BASE, SIS3820_ACQUISITION_COUNT);
       if(scancount!=oldscancount){
           oldscancount=scancount;
           printf("\nMCS mode, scan %d completed\n",scancount);
       }
   }
  if(0)
    {
     /* readout of scaler data */
  wc = sis3820_sdramRead(myvme,  SIS3820_BASE, (void *)data_buffer, nbins);
     printf("read wc %d\n",wc);
     if(wc>0) {

    

    printf("scaler[%i] = %d\n", i, scaler);
         printf("scaler data\n");
         k=0;
         for (j=1;j<=nbins;j++) {
            printf("scan: %3.3d ",j);
            for (i=1;i<=4;i++) {
               printf("ch%2.2d %8.8x  ",i,data_buffer[k]);
               k++;
            }
            printf("\n");
         }
	  }
    } 

  if(1)
    {
  // try single word reads
      int nchan = 32;
  int nread = nbins * nchan ; 
    for (i=0;i<nread;i++) {
      data_buffer[i]= sis3820_ScalerRead(myvme, SIS3820_BASE, i); // FIFO read from same place; SRAM advance ptr
    printf("data_buffer[%i] = %d\n", i, data_buffer[i]);
    }

  return 1;
    }
}
uint32_t write_reg (  MVME_INTERFACE *mvme, DWORD base, uint32_t reg, uint32_t data )
{

  printf ("\nWriting 0x%x to reg 0x%x\n", data,  reg); 
  sis3820_RegisterWrite(mvme, base, reg, data);
  data = sis3820_RegisterRead(mvme, base, reg);
  printf ("Read back  0x%x from reg 0x%x\n", data,  reg);
  return data;
}
#endif  // MAIN
#endif  // TEST1

#define TEST2
#ifdef TEST2
/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef SIS3820_MAIN

#define MAX 100000

int main (int argc, char* argv[]) {
  int i,j,k;
  uint32_t SIS3820_BASE  = 0x38000000;
  //  uint32_t dest[32]; 
  uint32_t scaler;
  MVME_INTERFACE *myvme;
  int status, count;
  uint32_t opbits, csrbits, scancount;
  uint32_t pdata32[MAX];
  int n_bins = 500; // Number of time bins (LNE)
  int dump=0; // don't dump the data
  if (argc>1) {
    //    sscanf(argv[1],"%x", &SIS3820_BASE);
sscanf(argv[1],"%d", &dump);
  }
  
  // Test under vmic
  status = mvme_open(&myvme, 0);
  sis3820_Reset(myvme, SIS3820_BASE);
  sis3820_Status(myvme, SIS3820_BASE);

  // Set SIS3820 into test mode
  
   regWrite(myvme, SIS3820_BASE,  SIS3820_COPY_DISABLE,  0xFFF0FFFF); // disable all except channels 17-21


  sis3820_RegisterWrite(myvme,SIS3820_BASE,SIS3820_LNE_PRESCALE, 100000); // 0x18  prescale LNE pulse rate
  printf("Setting preset to %d\n",n_bins);
  sis3820_RegisterWrite(myvme,SIS3820_BASE , SIS3820_ACQUISITION_PRESET, n_bins);
   opbits =
	SIS3820_CLEARING_MODE|                           //   bit 0    set clearing mode
	SIS3820_MCS_DATA_FORMAT_24BIT   |               //   bits 2,3 8 bit data 
     //data_format |                                    //   input param
	SIS3820_LNE_SOURCE_INTERNAL_10MHZ|               //   bit 4-6  LNE source 10MHz pulser
	SIS3820_FIFO_MODE|                               //   bit 12,13 FIFO mode
	SIS3820_OP_MODE_MULTI_CHANNEL_SCALER|            //   bit 28-31  MCS mode
	SIS3820_CONTROL_INPUT_MODE1|                     //   bit 16-18 Input mode 1
	SIS3820_CONTROL_OUTPUT_MODE2 ;                   //   bit 20,21 Output mode 2

 printf("Writing 0x%x to Operation Mode Register\n",opbits);
 sis3820_RegisterWrite(myvme,SIS3820_BASE,SIS3820_OPERATION_MODE, opbits); // 0x100 

  csrbits =  CTRL_COUNTER_TEST_25MHZ_ENABLE |  CTRL_COUNTER_TEST_MODE_ENABLE;

 printf("Enabling test pulses\n");
      sis3820_RegisterWrite(myvme,SIS3820_BASE , SIS3820_CONTROL_STATUS, csrbits );

 sis3820_RegisterWrite(myvme,SIS3820_BASE , SIS3820_TEST_PULSE_MASK, 0); // no mask ch(s) from test pulses

  sis3820_Status(myvme, SIS3820_BASE);
  printf("\nCntrl/Status reg 0x%x  =  0x%x\n", SIS3820_CONTROL_STATUS, regRead (myvme, SIS3820_BASE,  SIS3820_CONTROL_STATUS) );
  printf("\nOperation reg 0x%x  =  0x%x\n", SIS3820_OPERATION_MODE, regRead (myvme, SIS3820_BASE,  SIS3820_OPERATION_MODE) );
  printf("\nCopy/Disable reg 0x%x  =  0x%x\n", SIS3820_COPY_DISABLE, regRead (myvme, SIS3820_BASE,  SIS3820_COPY_DISABLE) );
  printf("\nTest Pulse Mask reg 0x%x  =  0x%x\n", SIS3820_TEST_PULSE_MASK, regRead (myvme, SIS3820_BASE,  SIS3820_TEST_PULSE_MASK) );
  printf("\nPreset reg 0x%x  =  0x%x \n", SIS3820_ACQUISITION_PRESET, regRead (myvme, SIS3820_BASE,  SIS3820_ACQUISITION_PRESET) );
  printf("\nPrescale reg 0x%x  =  0x%x\n", SIS3820_LNE_PRESCALE, regRead (myvme, SIS3820_BASE, SIS3820_LNE_PRESCALE ) );

  printf("now Enabling scaler\n");
 
  regWrite(myvme, SIS3820_BASE, SIS3820_KEY_OPERATION_ENABLE, 0x00);

  
  scancount=0;
  j=0; // overall data count
  while (scancount < n_bins)
    {
      sleep(1);
      scancount =   sis3820_RegisterRead(myvme, SIS3820_BASE, SIS3820_ACQUISITION_COUNT);
      printf("acquisition count= %d \n",scancount);
      if (scancount == n_bins)
	printf("Preset has been reached\n");
      else
	printf("\n");


      count =  (int)sis3820_RegisterRead(myvme,SIS3820_BASE , SIS3820_FIFO_WORDCOUNTER);
 
      int toRead32=0;
      if(count == 0) 
	printf("Module is empty\n"); 
      else
	{
	  printf("Reading out the %d words of data (max size of data chunk is %d): \n",count, MAX);
	  while (count > 0)
	    {
	      if(count > MAX)
		toRead32= MAX;
	      else
		toRead32=count;
	      
	      int rd = sis3820_FifoRead(myvme,SIS3820_BASE, &pdata32[0],toRead32);
	      printf("Reading toRead32= %d words; returned rd=%d\n",toRead32,rd);
	      if(dump)
		{
		  i=0;
		  while (i<rd)
		    {
		      if (i % 10 ==0 )
			printf("\n%10.10d %6.6d  ",i+j, i);
		      for (k=0; k < 10; k++) 
			{
			  printf("0x%8.8x ", pdata32[i]);
			  i++;
			}
		    }
		  j+=rd;
		}
	      count=count-toRead32;
	    } // while count
	  printf("\n");
	}
      
    } // while scancount 
  printf("\n Reached preset of %d time bins\n",n_bins);
 
  printf("Now disabling scaler\n");
  regWrite(myvme, SIS3820_BASE, SIS3820_KEY_OPERATION_DISABLE, 0x00);

  if(0)
    {
 scancount=0;
 scancount =   sis3820_RegisterRead(myvme, SIS3820_BASE, SIS3820_ACQUISITION_COUNT);
 printf("scancount= %d\n",scancount);
 sleep(5);
 scancount=0;
 scancount =   sis3820_RegisterRead(myvme, SIS3820_BASE, SIS3820_ACQUISITION_COUNT);
   printf("scancount= %d\n",scancount);

  printf("now disabling scaler\n");
  regWrite(myvme, SIS3820_BASE, SIS3820_KEY_OPERATION_DISABLE, 0x00);
  
  regWrite(myvme, SIS3820_BASE, SIS3820_CONTROL_STATUS, (csrbits | 0x10000)); // switch off user LED
  
  sis3820_Status(myvme, SIS3820_BASE);
  
  //sis3820_RegisterWrite(myvme, SIS3820_BASE, SIS3820_COUNTER_CLEAR, 0xFFFFFFFF);
  // sis3820_FifoRead(myvme, SIS3820_BASE, &dest[0], 32);
  for (i=0;i<32;i++) {
    scaler = sis3820_ScalerRead(myvme, SIS3820_BASE, i);
    printf("scaler[%i] = %d\n", i, scaler);
  }
 scancount=0;
 scancount =   sis3820_RegisterRead(myvme, SIS3820_BASE, SIS3820_ACQUISITION_COUNT);
   printf("scancount= %d\n",scancount);

  } // if (0)
  return 1;

  
}  


#endif 
#endif


