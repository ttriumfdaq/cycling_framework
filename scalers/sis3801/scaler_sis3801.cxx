#include "scaler_sis3801.h"
#include "sis3801.h"
#include "core/scan_utils.h"

ScalerDataPoint sis3801_get_next_data_point(DWORD* buffer, int& buffer_pos, DWORD num_words_read) {
  ScalerDataPoint data;

  if (buffer_pos >= num_words_read) {
    data.exists = false;
  } else {
    /*
     UUFCCCCCddddDDDDddddDDDDddddDDDD
     where U: user bits (front panel inputs)
     F: latch bit (not used)
     C: channel bit (C4..C0)
     dD: data bit  (24bit)
     */
    data.exists = true;
    DWORD raw = buffer[buffer_pos];
    data.user_bits = (raw >> 30);
    data.chan_idx = ((raw >> 24) & 0x1F);
    data.count = (raw & 0xFFFFFF);
    buffer_pos++;
  }

  return data;
}

#ifndef DUMMY_VME
ScalerSIS3801::ScalerSIS3801(MVME_INTERFACE *_vme, DWORD _base) : Scaler(SIS_FIFO_SIZE) {
  myvme = _vme;
  VME_BASE = _base;

  reset();
  sis3801_Setup(myvme, VME_BASE, 5);
  SIS3801_Status(myvme, VME_BASE);
}

ScalerSIS3801::~ScalerSIS3801() {
  stop();
}

INT ScalerSIS3801::check_module_id() {
  DWORD full_modid = sis3801_module_ID(myvme, VME_BASE);
  DWORD firmware = (full_modid & 0xFFFF) >> 12;
  DWORD module_id = full_modid >> 16;

  scan_utils::ts_printf("Base Address 0x%x  Module ID: 0x%x and Firmware: 0x%x\n", VME_BASE, module_id, firmware);

  if (module_id != 0x3801) {
    cm_msg(MERROR, __FUNCTION__, "Wrong VME module (0x%x) is installed at Base Address 0x%x . Expecting a SIS3801", module_id, VME_BASE);
    return FE_ERR_HW;
  }

  if (firmware == 0xE) {
    scan_utils::ts_printf("A SIS3801 with Firmware 'E' has been detected at base address 0x%x\n", VME_BASE);
  } else {
    cm_msg(MERROR, __FUNCTION__, "SIS3801 at base address 0x%x DOES NOT HAVE Firmware 'E'", VME_BASE);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

void ScalerSIS3801::reset() {
  sis3801_module_reset(myvme, VME_BASE);
}

void ScalerSIS3801::start() {
  sis3801_next_logic(myvme, VME_BASE, SIS3801_ENABLE_NEXT_CLK_WO);
}

void ScalerSIS3801::stop() {
  sis3801_next_logic(myvme, VME_BASE, SIS3801_DISABLE_NEXT_CLK_WO);
  sis3801_FIFO_clear(myvme, VME_BASE);
}

void ScalerSIS3801::set_enabled_channels_bitmask(DWORD mask) {
  int max_chan = 0;

  for (int i = 0; i < 32; i++) {
    if (mask & (1 << i)) {
      max_chan = i;
    }
  }

  sis3801_channel_enable(myvme, VME_BASE, max_chan + 1);
}

void ScalerSIS3801::set_num_bins(int num_bins) {
  sis3801_write_preset(myvme, VME_BASE, num_bins);
}

void ScalerSIS3801::enable_ref1(bool enable) {
  if (enable) {
    scan_utils::ts_printf("Enabling 3801 Ref ch1\n");
    sis3801_ref1(myvme, VME_BASE, SIS3801_ENABLE_REF_CH1_WO);
  } else {
    scan_utils::ts_printf("Disabling 3801 Ref ch1\n");
    sis3801_ref1(myvme, VME_BASE, SIS3801_DISABLE_REF_CH1_WO);
  }
}

int ScalerSIS3801::num_bins_seen() {
  return sis3801_read_acquisition_count(myvme, VME_BASE);
}

bool ScalerSIS3801::is_over_half_full() {
  return sis3801_CSR_read(myvme, VME_BASE, IS_FIFO_HALF_FULL);
}

bool ScalerSIS3801::is_full() {
  return sis3801_CSR_read(myvme, VME_BASE, CSR_FULL);
}

bool ScalerSIS3801::is_empty() {
  return sis3801_CSR_read(myvme, VME_BASE, IS_FIFO_EMPTY);
}

void ScalerSIS3801::read(int &num_words, DWORD *data, DWORD buffer_size_bytes, bool flush_everything) {
  if (is_full() || is_empty()) {
    num_words = 0;
  } else if (is_over_half_full() && !flush_everything) {
    num_words = sis3801_HFIFO_read(myvme, VME_BASE, data);
  } else {
    num_words = sis3801_FIFO_flush(myvme, VME_BASE, data);
  }
}

ScalerDataPoint ScalerSIS3801::get_next_data_point() {
  return sis3801_get_next_data_point(buffer, buffer_pos, num_words_read);
}

#endif

DummyScalerSIS3801::DummyScalerSIS3801(float _acq_time_ms) : Scaler(SIS_FIFO_SIZE * 100) {
  num_bins = 0;
  num_chans = 0;
  have_read_this_cycle = false;
  acq_time_ms = _acq_time_ms;
  cycle_start_time_ms = 0;
}

void DummyScalerSIS3801::start() {
  have_read_this_cycle = false;
  cycle_start_time_ms = scan_utils::current_time_ms();
}

void DummyScalerSIS3801::set_enabled_channels_bitmask(DWORD mask) {
  num_chans = 0;

  for (int i = 0; i < 32; i++) {
    if (mask & (1 << i)) {
      num_chans = i + 1;
    }
  }
}

int DummyScalerSIS3801::num_bins_seen() {
  if (num_bins == 0) {
    return 0;
  }

  double now = scan_utils::current_time_ms();

  int retval = num_bins * ((now - cycle_start_time_ms) / acq_time_ms);

  if (retval > num_bins) {
    return num_bins;
  } else {
    return retval;
  }
}

void DummyScalerSIS3801::read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything) {
  /*
       UUFCCCCCddddDDDDddddDDDDddddDDDD
       where U: user bits (front panel inputs)
       F: latch bit (not used)
       C: channel bit (C4..C0)
       dD: data bit  (24bit)

             data.user_bits = (raw >> 30);
      data.chan_idx = ((raw >> 24) & 0x1F);
      data.count = (raw & 0xFFFFFF);

 */
  have_read_this_cycle = true;
  num_words = 0;

  for (DWORD bin = 0; bin < (DWORD)num_bins; bin++) {
    for (DWORD chan = 0; chan < num_chans; chan++) {
      num_words++;

      if (num_words * sizeof(DWORD) >= buffer_size_bytes) {
        scan_utils::ts_printf("Ran out of space in dummy SIS3801 buffer!\n");
        return;
      }

      // For now, no setting of user bits, and just set data
      // content to bin number.
      DWORD user_bits = 0;
      DWORD latch_bit = 0;
      DWORD scaler_count = bin + 1;
      DWORD this_word = ((user_bits & 0x3) << 30) | ((latch_bit & 0x1) << 29) | ((chan & 0x1F) << 24) | (scaler_count & 0xFFFFFF);
      *data++ = this_word;
    }
  }
}

ScalerDataPoint DummyScalerSIS3801::get_next_data_point() {
  return sis3801_get_next_data_point(buffer, buffer_pos, num_words_read);
}
