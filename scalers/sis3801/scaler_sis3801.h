#ifndef MCS_SIS3801
#define MCS_SIS3801

#include "scalers/scaler_class.h"
#include <string>
#include <time.h>

#ifndef DUMMY_VME
#include "mvmestd.h"

// Interface for a Struck SIS3801 module.
// See the Scaler base class header for most documentation.
// This module can't read out an arbitrary set of channels - it must
// read out the first N channels.
class ScalerSIS3801 : public Scaler {
  public:
    ScalerSIS3801(MVME_INTERFACE* _vme, DWORD _base);
    virtual ~ScalerSIS3801();

    virtual INT check_module_id() override;

    virtual void reset() override;
    virtual void start() override;
    virtual void stop() override;

    virtual void set_enabled_channels_bitmask(DWORD mask) override;
    virtual void set_num_bins(int num_bins) override;
    virtual int get_first_enabled_chan_idx() override { return 0; }
    virtual int num_bins_seen() override;

    virtual bool is_over_half_full() override;
    virtual bool is_full() override;
    virtual bool is_empty() override;

    virtual ScalerDataPoint get_next_data_point() override;

    // Set the first channel to record the 25MHz reference frequency.
    virtual void enable_ref1(bool enable);

  protected:
    virtual void read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything=true) override;

    MVME_INTERFACE* myvme;
    DWORD VME_BASE;
};
#endif

// Dummy version of an SIS3801 that can be used for testing
// and doesn't require a VME crate.
class DummyScalerSIS3801 : public Scaler {
  public:
    // Specify how long you would like the Scaler to pretend
    // to take to acquire all the bins.
    DummyScalerSIS3801(float _acq_time_ms);
    virtual ~DummyScalerSIS3801() {}

    virtual INT check_module_id() override { return SUCCESS; }

    virtual void reset() override {};
    virtual void start() override;
    virtual void stop() override {};

    virtual void set_enabled_channels_bitmask(DWORD mask) override;
    virtual void set_num_bins(int n_bins) override { num_bins = n_bins; }
    virtual int get_first_enabled_chan_idx() override { return 0; }
    virtual int num_bins_seen() override;

    virtual bool is_over_half_full() override { return false; }
    virtual bool is_full() override { return false; }
    virtual bool is_empty() override { return !have_read_this_cycle; }


    virtual ScalerDataPoint get_next_data_point() override;

    virtual void enable_ref1(bool enable) {};

  protected:
    virtual void read(int& num_words, DWORD* data, DWORD buffer_size_bytes, bool flush_everything=true) override;

    INT num_bins;
    DWORD num_chans;
    bool have_read_this_cycle;
    float acq_time_ms;
    double cycle_start_time_ms;
};

#endif
