#include "core/data_source.h"
#include "scalers/scaler_class.h"
#include "midas.h"

Scaler::Scaler(INT buffer_size) {
  num_words_read = 0;
  buffer_pos = 0;
  buffer = (DWORD*)calloc(buffer_size, sizeof(DWORD));
  buffer_size_bytes = buffer_size;
}

Scaler::~Scaler() {
  free(buffer);
}

INT Scaler::begin_of_run(char* error) {
  return SUCCESS;
}

INT Scaler::end_of_run() {
  stop();
  return SUCCESS;
}

void Scaler::new_cycle_starting(int cycle_count) {
  start();
  num_words_read = 0;
}

void Scaler::this_cycle_finished() {
}

void Scaler::mid_cycle_read_data_if_needed() {
  if (is_over_half_full()) {
    buffer_pos = 0;
    read(num_words_read, buffer, buffer_size_bytes, true);
  }
}

DataStatus Scaler::end_cycle_is_data_ready() {
  buffer_pos = 0;
  read(num_words_read, buffer, buffer_size_bytes);
  return DataReady;
}
