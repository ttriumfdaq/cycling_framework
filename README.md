# Cycling framework

This repository houses a common framework for experiments that use a PPG or similar, and that want to scan/increment values at the end of each cycle. It is currently used for several TITAN traps, and is currently being extended to support BNxR experiments.

This repository defines common interfaces for different roles that hardware can play in an experiment - e.g. something that needs to have values scanned at the end of a cycle, something that provides data we want to read out each cycle etc. It also provides concrete implementations for common devices (e.g. PPG, EPICS etc).

The main "scanning class" code handles all the logic about setting up each cycle and reading data at appropriate times. The only experiment-specific code that needs to be written is to define which devices are being used, and which optional features of the scanning class should be enabled.

This structure means that if a new feature is needed for one experiment (e.g. we want to have a random ordering of scan values, rather than just from lowest to highest each time), it only needs to be written once, and all other experiments can use it "for free".

This module should generally be installed as a git submodule of the experiment-specific repository.
