#ifndef DATA_SOURCE_VT4_H
#define DATA_SOURCE_VT4_H

#include "core/data_source.h"

#ifdef DUMMY_VME
#define MVME_INTERFACE int
#else
#include "vmicvme.h"
#endif

/*
 * Wrapper around the VT4 TDC driver that works with the
 * cycling framework interface.
 */
class DataSourceVT4 : public DataSource {
  public:
    DataSourceVT4(MVME_INTERFACE* _myvme, DWORD _base);

    virtual ~DataSourceVT4();

    // Implementation of functions from the DataSource interface.
    virtual INT begin_of_run(char* error) override;
    virtual INT end_of_run() override;
    virtual void new_cycle_starting(int cycle_count) override;
    virtual void this_cycle_finished() override;
    virtual DataStatus end_cycle_is_data_ready() override;
    virtual void write_data_to_banks(char *pevent) override;

  private:
    // VME connection
    MVME_INTERFACE* myvme;

    // VME base address
    DWORD VT4_BASE;

    // Number of PPG cycles we've run. Written as part of data format.
    int cyc_count;

    // Number of data words read.
    DWORD num_data;

    // The data read from the device.
    DWORD* data;
};

#endif
