/*********************************************************************

  Name:         vt4.c
  Created by:   Pierre-Andre Amaudruz

  Contents:      Routines for accessing the VT4 
                 which is a modification of the NIMIO32 Triumf board
                
*********************************************************************/
#include "vt4.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include "mvmestd.h"




/********************************************************************/

uint32_t vt4_reset(MVME_INTERFACE *myvme, DWORD base, DWORD data)
{
  uint32_t my_data;
  //data = 0 (no-op) 1 (reset) etc.
   vt4_write32(myvme,  base,  VT4_CSR ,  data & 0x1);
   my_data = vt4_read32(myvme, base,   VT4_CSR);
   return (my_data & 0x3FF);
}

uint32_t vt4_read_CSR(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t my_data;
   my_data = vt4_read32(myvme, base,   VT4_CSR);
   printf("Status Register: 0x%x\n",my_data);
   if( my_data & VT4_DATA_EMPTY) 
     printf("Data Empty: Y ");
   else
     printf("Data Empty: N ");
   if( my_data & VT4_DATA_FULL)
     printf("Data Full: Y ");
   else
     printf("Data Full: N ");

   printf("\n");
   return (my_data);
}


uint32_t vt4_FirmwareRead(MVME_INTERFACE *myvme, DWORD base)
{
  uint32_t data;
  data = vt4_read32(myvme,  base, VT4_IO32_REVISION);
  return data;
}


/********************************************************************/


/********************************************************************/
/**
Read the VT432
@param myvme vme structure
@param base  VMEIO base address
@return register value
*/

uint32_t vt4_read32(MVME_INTERFACE *myvme, DWORD base, DWORD offset)
{
  mvme_set_am(myvme, MVME_AM_A32);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  return mvme_read_value(myvme, base+offset);
}

void vt4_write32(MVME_INTERFACE *myvme, DWORD base, DWORD offset, uint32_t data)
{
  mvme_set_am(myvme, MVME_AM_A32);
  mvme_set_dmode(myvme, MVME_DMODE_D32);
  mvme_write_value(myvme, base + offset, data);
}

uint32_t vt4_bitset(int bit, uint32_t data)
{
  uint32_t ival;
  ival = 1<<bit;
  data=data | ival;
  return data;
}

uint32_t vt4_bitclear(int bit, uint32_t data)
{
  uint32_t ival;
  ival = 1<<bit;
  data = data & ~ival;
  return data;
}

void vt4_test(MVME_INTERFACE *myvme, DWORD base, int poll_time_ms)
{
  uint32_t data;
  int i,eflag;
  char *dot=".";
  double time_zero=0;
  int cycle_counter=0;
  char cmd[]="hi";
  FILE *fout=NULL;
  char file[]="vt4.dat";

  printf("Write data to file %s ? y/n ? ",file);
  scanf("%s",cmd);
  cmd[0]=toupper(cmd[0]);
  if(strncmp(cmd,"Y",1)==0)
    {
      fout = fopen(file,"w");
      if(fout == NULL){
	printf("vt4_test: output data file \"%s\" could not be opened.\n", file); 
	return;    
      }
      else
	printf("vt4_test: output data file \"%s\" has been opened.\n", file); 
    }
  printf("Reset VT4 module? y/n ? ");
  scanf("%s",cmd);
  cmd[0]=toupper(cmd[0]);
  if(strncmp(cmd,"Y",1)==0)
    {
      printf("Resetting VT4 module\n");
      if(fout)  fprintf(fout,"Resetting VT4 module\n");
      vt4_reset(myvme,base,1);
      usleep(500 * 1000); // sleep
    }
  printf("Reading data from VT4 in a loop at %d ms intervals\n",poll_time_ms);
  if(fout)  fprintf(fout,"Reading data from VT4 in a loop at %d ms intervals\n",poll_time_ms);
  eflag=0;
  for (i=0; i<100; i++)
    {
      data = vt4_read32(myvme, base,   VT4_CSR);
      usleep(100 * 1000); // sleep
      //  printf("i=%d data=0x%x eflag=%d\n",i,data, eflag);
      if (data & VT4_DATA_EMPTY) 
	{
	  //	    printf("empty.. i=%d eflag=%d\n",i,eflag);
	  if(eflag)
	    printf("%s ",dot);
	  else
	    {
	      printf("\nempty.. i=%d eflag=%d  setting eflag\n",i,eflag);
	      eflag=1;
	      printf("CSR:  data buffer is empty");
	      if(fout)fprintf(fout,"CSR:  data buffer is empty");
	    }
	}
      else
	{
	  printf("NOT empty.. i=%d eflag=%d \n",i,eflag);
	  if(fout)fprintf(fout,"NOT empty.. i=%d eflag=%d \n",i,eflag);
	  if(eflag)
	    {
	      printf("\n");
	      if(fout)fprintf(fout,"\n");
	      eflag=0;  // clear empty flag
	    }
	  
	  if(data & VT4_DATA_FULL)
	    {
	      printf("*** Error - CSR: data buffer is full ***\n");
	      if(fout)fprintf(fout,"*** Error - CSR: data buffer is full ***\n");
	    }
	}
    
      if(!eflag)
	{
	  data =  vt4_read32(myvme, base , VT4_NWORDS);
	  printf("Number of data words to read : %d",data);
	  if(fout)fprintf(fout,"Number of data words to read : %d",data);
	  if(data > 0)
	    {
	      printf("\n");
	      if(fout)fprintf(fout,"\n");
	      vt4_read_data(myvme, base, data, &time_zero, &cycle_counter, fout);
	      
	      printf("sleeping...\n");
	      if(fout)fprintf(fout,"sleeping...\n");
	    }
	  else
	    {
	      printf("\n*** Error - data buffer not empty but num words > 0 ***\n");
	      if(fout)fprintf(fout,"\n*** Error - data buffer not empty but num words > 0 ***\n");
	    }
	}

      usleep(poll_time_ms * 1000); // sleep
    }
  return;
}

void vt4_simple_test(MVME_INTERFACE *myvme, DWORD base, int poll_time_ms)
{
  uint32_t data;
  double time_zero=-1;
  int cycle_counter=0;
  char cmd[]="hi";
  FILE *fout=NULL;



  printf("Reset VT4 module? y/n ? ");
  scanf("%s",cmd);
  cmd[0]=toupper(cmd[0]);
  if(strncmp(cmd,"Y",1)==0)
    {
      printf("Resetting VT4 module\n");
      if(fout)  fprintf(fout,"Resetting VT4 module\n");
      vt4_reset(myvme,base,1);
      usleep(500 * 1000); // sleep
    }
  printf("Start PPG program.... type any character when finished ? ");
  scanf("%s",cmd);
    
  printf("Reading data from VT4\n");
 
  data = vt4_read32(myvme, base,   VT4_CSR);
  usleep(100 * 1000); // sleep
  if (data & VT4_DATA_EMPTY)
    {
      printf("CSR:  data buffer is empty");
      return;
    }

	  
  if(data & VT4_DATA_FULL)
    {
      printf("*** Error - CSR: data buffer is full ***\n");
      return;
    }
       
  data =  vt4_read32(myvme, base , VT4_NWORDS);
  printf("Number of data words to read : %d",data);

  if(data > 0)
    {
      printf("\n");
      vt4_simple_read_data(myvme, base, data, &time_zero, &cycle_counter, fout);
    }
  return;
}

void vt4_simple_read_data(MVME_INTERFACE *myvme, DWORD base, int num, double *time_zero, int *cycle_counter, FILE *fout )
{
  uint32_t      data1,data2;
  uint32_t hitime,lotime;
  int count,id,chanbp,cycle=0;
  int i,j;
  char chan[5];
  char ctrl[5];
  double tottime, diff_ms, tottime_s; // total time
  double last=-1;
  printf("vt4_simple_read_data: number of words to read is %d \n\n",num);

  printf("i     data_hi    data_lo  id   cycle# gate# ctrl chbp   chan   tottime(cnts)      tottime(s)        diff(ms) \n");
  for(i=0; i<num; i++)
    {
      data1 =  vt4_read32(myvme, base , VT4_DATA_LO);
      data2 =  vt4_read32(myvme, base , VT4_DATA_HI);
      hitime  = data2 & 0xFFFF; // mask to get hitime
      count = (data2 & VT4_DHI_COUNT) >> 16;
      chanbp = (data2 & VT4_DHI_CHAN) >> 26;
      id    = (data2 & VT4_DHI_ID) >> 30;

      // printf("hi data2=0x%x  shifted count=0x%x shifted id=0x%x  shifted bp=0x%x\n",data2, count,id,chanbp);
      if(id & VT4_DHI_CYCL)
	{
	  sprintf(ctrl,"%s","cycl");
	  cycle=count;
	}

      
      else if(id & VT4_DHI_GATE)
	sprintf(ctrl,"%s","gate");
      
      else
	sprintf(ctrl,"%s","endg");
      
      sprintf(chan,"none"); // chanbp=0;

      for(j=0;j<4; j++)
	{
	  if(chanbp & 1<<j)
	    {
	      // printf ("i=%d  ch=%d bit set\n",j,(j+1));
	      sprintf(chan,"ch%d",(j+1));
	      break;
	    }
	}
      if((strncmp(ctrl,"gate",4)==0) && (strncmp(chan,"none",4))==0)
	sprintf(ctrl,"%s","begg"); // begin gate

      lotime = data1;
      tottime = (double)hitime *  pow(2,32); // shifted ihitime
      tottime += (double)lotime;
      tottime_s = tottime  * pow(10,-8);  // 100MHz clock

      if(last==-1) 
	diff_ms=0; // first time through
      else
	diff_ms=(double)(tottime-last) * pow(10,-5);  // ms ;
      //      double ip; // integer part
      //  double fp; // fractional part
      //  fp=modf(tottime_s,&ip);
      //  j=(int)ip;
      if(id & VT4_DHI_CYCL)
	printf("%3.3d 0x%8.8x 0x%8.8x  %1.1d   %4.4d       %5.5s    %1.1d    %5.5s %15g %15.6f %15g \n",
	       i,data2,data1,id,cycle,ctrl, chanbp, chan, tottime, tottime_s,diff_ms);
      else
	printf("%3.3d 0x%8.8x 0x%8.8x  %1.1d   %4.4d %4.4d  %5.5s    %1.1d    %5.5s %15g %15.6f %15g \n",
	       i,data2,data1,id,cycle,count,ctrl, chanbp, chan, tottime, tottime_s,diff_ms);

      last=tottime;
    }
  return;


}


void vt4_read_data(MVME_INTERFACE *myvme, DWORD base, int num, double *time_zero, int *cycle_counter, FILE *fout )
{
  int i,j,count,id,chanbp;
  uint32_t      data1,data2;


  double diff_s,diff_g; // difference from start
  //  uint32_t idiff_g; // difference from last gate (counts)
  static  uint32_t igsl,igsh;
  char chan[5];
  char ctrl[5];
  double tottime; // total time
  double gate_diff_ms,gate_prev;
  double cycle_diff_ms, cycle_prev;
  uint32_t hitime,lotime;

  //  long int idiff;
  static double gate_start=0;    // time
  static double cycle_start=0;   // time
  printf("vt4_read_data: number of words to read is %d  last gate_start = %f\n",num,gate_start);
  if(fout)fprintf(fout,"vt4_read_data: number of words to read is %d  last gate_start = %f\n",num,gate_start);

  
  for(i=0; i<num; i++)
    {
      data1 =  vt4_read32(myvme, base , VT4_DATA_LO);
      data2 =  vt4_read32(myvme, base , VT4_DATA_HI);
      hitime  = data2 & 0xFFFF; // mask to get hitime
      count = (data2 & VT4_DHI_COUNT) >> 16;
      chanbp = (data2 & VT4_DHI_CHAN) >> 26;
      id    = (data2 & VT4_DHI_ID) >> 30;

      //  printf("hi data2=0x%x  shifted count=0x%x shifted id=0x%x  shifted bp=0x%x\n",data2, count,id,chanbp);
      if(id & VT4_DHI_CYCL)
	sprintf(ctrl,"%s","cycl");
      
      else if(id & VT4_DHI_GATE)
	sprintf(ctrl,"%s","gate");

      else
	sprintf(ctrl,"%s","endg");

      sprintf(chan,"none"); // chanbp=0;
      
      for(j=0;j<4; j++)
	{
	  if(chanbp & 1<<j)
	    {
	      //  printf ("i=%d  ch=%d bit set\n",j,(j+1));
	      sprintf(chan,"ch%d",(j+1));
	      break;
	    }
	}
      lotime = data1;
      tottime = (double)hitime *  pow(2,32); // shifted ihitime
      tottime += (double)lotime;

      if((id & VT4_DHI_GATE) && chanbp==0)
	{ // new gate
	  gate_prev = gate_start; // previous gate_start
	  gate_start = tottime;
	  gate_diff_ms = (gate_start - gate_prev) * pow(10,-5); //ms ; Module clocked at 100MHz
          igsl = lotime; // remember lotime
          igsh = hitime; // and hitime
	  sprintf(ctrl,"%s","newg");
	  //printf("New gate... gate_start = %g sec. Time from previous gate_start = %g ms\n",tottime, gate_diff_ms);
	  //if(fout)fprintf(fout,"New gate... gate_start = %g sec.  Time from previous gate_start = %g ms\n",tottime, gate_diff_ms);

	  /*  Module fixed - now the first word is cycl (new cycle)	  */
	  diff_s = (gate_start - *time_zero) * pow(10,-8);

	  printf("%3.3d 0x%8.8x 0x%8.8x %3d %5d %5d %3d %5.5s %5.5s %5u %10u %15g %15g\n",
		 i,data2,data1,id,*cycle_counter,count,chanbp,ctrl,chan, hitime,lotime,tottime,diff_s);
	  if(fout)fprintf(fout,"%3.3d 0x%8.8x 0x%8.8x %3d %5d %5d %3d %5.5s %5.5s %5u %10u %15g %15g\n",
		 i,data2,data1,id,*cycle_counter,count,chanbp,ctrl,chan, hitime,lotime,tottime,diff_s);

	}

      else if (id & VT4_DHI_CYCL)
	{ // new cycle
	  // if(count != (*cycle_counter + 1))
	  // {   // Mismatch if reset not done 
	      //  printf("\n** Mismatch in cycle count. Last cycle count was %d, new cycle is %d ** \n",*cycle_counter,count);
	      // if(fout)fprintf(fout,"\n** Error in cycle count. Last cycle was %d, new cycle is %d ** \n",*cycle_counter,count);
	  // }
          cycle_prev=cycle_start;
	  cycle_diff_ms = (tottime - cycle_start) * pow(10,-5); //ms ;
	  printf("New cycle %d at %f seconds. Time from last new cycle = %f ms\n",count,tottime, cycle_diff_ms);
	  if(fout)fprintf(fout,"New cycle %d at %f seconds. Time from last new cycle = %f ms\n",count,tottime, cycle_diff_ms);

  printf(" i     data_hi    data_lo  id cycle# gate# bit ctrl chan hitime    lotime       totaltime        time(sec)  diff(ms) from gate start\n");
  if(fout)fprintf(fout," i     data_hi    data_lo  id cycle count  bit  ctrl chan hitime    lotime       totaltime        time(sec)  diff(ms) from gate start\n");

	  cycle_start= tottime;
	  *cycle_counter = count;
	  diff_s =  (tottime - *time_zero) * pow(10,-8); // time in seconds
        
	
	  printf("%3.3d 0x%8.8x 0x%8.8x %3d %5d       %3d %5.5s %5.5s %5u %10u %15g %15g\n",
		 i,data2,data1,id,*cycle_counter,chanbp,ctrl,chan, hitime,lotime,tottime,diff_s);
	  if(fout)fprintf(fout,"%3.3d 0x%8.8x 0x%8.8x %3d %5d       %3d %5.5s %5.5s %5u %10u %15g %15g \n",
			  i,data2,data1,id,*cycle_counter,chanbp,ctrl,chan, hitime,lotime,tottime,diff_s);

	}
      else if (strncmp(ctrl,"endg",4)==0)
	{ // end gate
	  // idiff_g = lotime - igsl;
	  diff_g = (tottime - gate_start) * pow(10,-5); //ms
	  diff_s = (tottime - *time_zero) * pow(10,-8);
	  printf("%3.3d 0x%8.8x 0x%8.8x %3d %5d %5d %3d %5.5s %5.5s %5u %10u %15g %15g %15g gate width",
		 i,data2,data1,id,*cycle_counter,count,chanbp,ctrl,chan, hitime,lotime,tottime, diff_s, diff_g);
	  if(fout)fprintf(fout,"%3.3d 0x%8.8x 0x%8.8x %3d %5d %5d %3d %5.5s %5.5s %5u %10u %15g %15g %15g gate width",
		 i,data2,data1,id,*cycle_counter,count,chanbp,ctrl,chan, hitime,lotime,tottime, diff_s, diff_g);

         if (hitime != igsh)
	   {
	     printf("%s\n","*");// hi bits changed
	     if(fout)fprintf(fout,"%s\n","*");// hi bits changed
	   }
	 else
	   {
	     printf("\n");
	     if(fout)fprintf(fout,"\n");
	   }

	}
      else
	{ // channels
	  // idiff_g =  lotime - igsl;
	  diff_g = (tottime - gate_start) * pow(10,-5); //ms
	  diff_s = (tottime - *time_zero) * pow(10,-8);
	  printf("%3.3d 0x%8.8x 0x%8.8x %3d %5d %5d %3d %5.5s %5.5s %5u %10u %15g %15g %15g  ",
		 i,data2,data1,id,*cycle_counter,count,chanbp,ctrl,chan, hitime,lotime,tottime,diff_s,diff_g);
	  if(fout)fprintf(fout,"%3.3d 0x%8.8x 0x%8.8x %3d %5d %5d %3d %5.5s %5.5s %5u %10u %15g %15g %15g ",
			  i,data2,data1,id,*cycle_counter,count,chanbp,ctrl,chan, hitime,lotime,tottime,diff_s,diff_g);

	  if (hitime != igsh)
	    {
	      printf("%s\n","*");// hi bits changed
	      if(fout)fprintf(fout,"%s\n","*");// hi bits changed
	    }
	  else
	    {
	      printf("\n");
	      if(fout)fprintf(fout,"\n");
	    }
	}
    }
  printf("end\n");
  if(fout)fprintf(fout,"end\n");
  return;
}
  



#ifdef MAIN_ENABLE
void vt4(void)
{
   printf("       VT4 command list\n");
   printf("A Read Ctrl/Status Reg   B Read Firmware Timestamp\n"); 
   printf("C Read num data words    D Read data from buffer \n"); 
   printf("E Read/Write Test Reg \n");
   printf("P print this list        R Reset \n");
   printf("S simple test (needs input signals,reads out data)\n");
   printf("T test (needs input signals,reads out data in a loop)\n");
   printf("X exit \n");
   printf("\n");
}


int main (int argc, char* argv[]) 
{

#define NUM_OUTPUTS           0        /*  number of output LEMOS fitted  */
#define NUM_INPUTS            8        /*  number of input LEMOS fitted  */
  MVME_INTERFACE *myvme;
  DWORD VT4_BASE = 0x00A00000 ;


  int status, csr;
  uint32_t      data,data1;
  char cmd[]="hallo";
  int s;
  int ival;
  int polling_sleep_time_ms=100;
 
 if (argc>1) {
    sscanf(argv[1],"%lx",& VT4_BASE);
  }
 //  if (argc>2) {
 //  sscanf(argv[2],"%d",&ddd);
 //  }

// printf("VT4 base: %lx   debug=%d\n", VT4_BASE,ddd); 
 printf("VT4 base: 0x%x \n", (unsigned int)VT4_BASE); 

  // Test under vmic   
  status = mvme_open(&myvme, 0);
  printf("\n");
  csr = vt4_FirmwareRead(myvme, VT4_BASE);
  printf("Firmware : 0x%x\n", csr);

  while ( isalpha(cmd[0]) )
    {
      printf("\nEnter command (a-Y) X to exit?  ");
      scanf("%s",cmd);
      printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];
      
      switch(s)
	{ 

	case('A'):
	  data =  vt4_read_CSR (myvme, VT4_BASE);
	  break;

	case('B'):
	  data =  vt4_read32(myvme, VT4_BASE , VT4_TSTAMP);
	  printf("Firmware Timestamp Reg : 0x%x or %d\n",data,data);
	  break;

	case('C'):
	  data =  vt4_read32(myvme, VT4_BASE , VT4_NWORDS);
	  printf("Read number of data words : 0x%x or %d\n",data,data);
	  break;

	case('D'):
	  printf("\nEnter number of 64-bit words to read ?");	    
	  scanf("%d",&ival);
	  if(ival<=0)
	    break; // nothing to do
	  double time_zero=-1;
	  int cycle_counter=0;
	  //	  FILE *file=NULL;
	  vt4_read_data(myvme,VT4_BASE,ival,&time_zero, &cycle_counter, NULL);
	  break; 



	case('E'):
	  data =  vt4_read32(myvme, VT4_BASE , VT4_TEST);
	  printf("Test Reg reads : 0x%x or %d\n",data,data);
	  printf("\nWrite to test register ? (Y/N)");
	  scanf("%s",cmd);
	  cmd[0]=toupper(cmd[0]);
	  s=cmd[0];
	  if(strncmp(cmd,"Y",1)!=0)
	    break;

	  printf("\nEnter data to write 0x");	    
	  scanf("%x",&data1);
	  vt4_write32(myvme, VT4_BASE , VT4_TEST, data1);
	  data = vt4_read32(myvme, VT4_BASE , VT4_TEST);
	  printf("\nWrote 0x%x to test register, read back 0x%x\n",
		 data1,data);
	  break;

	case('S'):
	  vt4_simple_test(myvme, VT4_BASE, polling_sleep_time_ms);
	  break;


	case('T'):
	  // printf("\nEnter polling sleep time (ms): ");	    
	  //  scanf("%d",&t);
	  polling_sleep_time_ms=100;
	  printf("Polling sleep time will be %d ms\n", polling_sleep_time_ms);
	  vt4_test(myvme, VT4_BASE, polling_sleep_time_ms);
	  break;


	case ('P'): // print command list
	  vt4();
	  break;

	case ('R'): // Reset
	
	  data = vt4_reset(myvme, VT4_BASE , 1); 
	  printf("after reset, data=0x%x\n",data);
	    
	  break;

	case ('X'): // exit
	  goto exit;
	  break;


	}
    }



  printf("\n");

 exit:
  status = mvme_close(myvme);
  return 1;
}	
#endif
