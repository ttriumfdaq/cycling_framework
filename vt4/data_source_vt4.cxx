#include "data_source_vt4.h"

extern "C" {
#include "vt4.h"
}

DataSourceVT4::DataSourceVT4(MVME_INTERFACE *_myvme, DWORD _base) {
  myvme = _myvme;
  VT4_BASE = _base;
  vt4_reset(myvme, VT4_BASE, 1);  // initialize and stop clock
  ss_sleep(500);
  printf("VT4 Status: \n");
  vt4_read_CSR(myvme, VT4_BASE);
  cyc_count = 0;
  num_data = 0;
  data = NULL;
}

DataSourceVT4::~DataSourceVT4() {};

INT DataSourceVT4::begin_of_run(char* error) {
  printf("begin_of_run: resetting VT4\n");
  vt4_reset(myvme, VT4_BASE, 1); // mvme_set_am,dm done in driver
  printf("begin_of_run: initialized VT4 clock. Clock will start at first PPG cycle pulse\n");

  return SUCCESS;
}

INT DataSourceVT4::end_of_run() {
  printf("end_of_run: resetting VT4 to stop the clock \n");
  vt4_reset(myvme, VT4_BASE, 1);  // initialize and stop clock
  return SUCCESS;
}

// This will be called once when a new PPG cycle starts.
void DataSourceVT4::new_cycle_starting(int cycle_count) {
  cyc_count = cycle_count;
}

// This will be called once when a PPG cycle ends.
void DataSourceVT4::this_cycle_finished() {
}

// This should return whether all the data is available.
// It will be called periodically until you return DataReady/DataFatalError.
DataStatus DataSourceVT4::end_cycle_is_data_ready() {
  DWORD csr = vt4_read32(myvme, VT4_BASE, VT4_CSR);

  if (csr & VT4_DATA_EMPTY) {
    // No data
    num_data = 0;
    data = NULL;
  } else if (csr & VT4_DATA_FULL) {
    // Fatal error - data loss!
    cm_msg(MERROR, __FUNCTION__, "VT4 data buffers are full. Cannot continue.");
    return DataFatalError;
  } else {
    // Read the data
    num_data = vt4_read32(myvme, VT4_BASE, VT4_NWORDS);
    data = (DWORD*)malloc(num_data * 2 * sizeof(DWORD));

    for (DWORD i = 0; i < num_data * 2; i += 2) {
      data[i] = vt4_read32(myvme, VT4_BASE, VT4_DATA_LO); // read lo data
      data[i + 1] = vt4_read32(myvme, VT4_BASE, VT4_DATA_HI); // then hi data
    }
  }

  return DataReady;
}

// This should fill banks in the event.
void DataSourceVT4::write_data_to_banks(char *pevent) {
  DWORD *pdata;
  bk_create(pevent, "DVT4", TID_DWORD, (void**) &pdata);

  *pdata++ = cyc_count;  // PPG cycle number

  // lo then hi data per word.
  for (DWORD i = 0; i < num_data * 2; i++) {
    *pdata++ = data[i];
  }

  bk_close(pevent, pdata);
  free(data);
}
