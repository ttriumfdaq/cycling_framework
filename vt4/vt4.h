//
// Name: vt4.h
//
#ifndef VT4_H
#define VT4_H

#include <stdio.h>
#include <stdint.h>
#include <vmicvme.h>

#define VT4_CSR     0x3C
#define VT4_TEST    0x40
#define VT4_TSTAMP  0x44
#define VT4_NWORDS  0x48
#define VT4_DATA_LO 0x4C
#define VT4_DATA_HI 0x50

// Masks
#define VT4_DATA_EMPTY      0x100  // CSR
#define VT4_DATA_FULL       0x200  // CSR
#define VT4_DHI_ID      0xC0000000 // DATA_HI
#define VT4_DHI_COUNT   0x03FF0000 // DATA_HI
#define VT4_DHI_TIME    0x0000FFFF // DATA_HI
#define VT4_DHI_CHAN    0x3C000000 // DATA_HI

// Acting on shifted ID bits
#define VT4_DHI_CYCL    0x2  // ID Cycle bit
#define VT4_DHI_GATE    0x1  // ID Gate bit
 // C
// IO32 registers

#define VT4_IO32_REVISION (4*0)
uint32_t vt4_read32(MVME_INTERFACE *myvme, DWORD base, DWORD offset);
void vt4_write32(MVME_INTERFACE *myvme, DWORD base, DWORD offset, uint32_t data);
uint32_t vt4_read_CSR(MVME_INTERFACE *myvme, DWORD base);
uint32_t vt4_reset(MVME_INTERFACE *myvme, DWORD base, DWORD data);


uint32_t vt4_FirmwareRead(MVME_INTERFACE *myvme, DWORD base);
uint32_t vt4_bitset(int bit, uint32_t data);
uint32_t vt4_bitclear(int bit, uint32_t data);

void vt4_read_data(MVME_INTERFACE *myvme, DWORD base, int num, double *time_zero, int *cycle_counter, FILE *fout);
void vt4_test(MVME_INTERFACE *myvme, DWORD base, int poll_time_ms);
void vt4_simple_read_data(MVME_INTERFACE *myvme, DWORD base, int num, double *time_zero, int *cycle_counter, FILE *fout );
void vt4_simple_test(MVME_INTERFACE *myvme, DWORD base, int poll_time_ms);
#endif
// end file
