#include "midas.h"
#include "psm/psm1_class.h"

extern "C" {
  #include "psm/trPSM.h"
}

PSM1::PSM1(MVME_INTERFACE* _myvme, DWORD _vme_base) {
  myvme = _myvme;
  vme_base = _vme_base;

  reset();

  chan_names.push_back("1f");
  chan_names.push_back("3f");
  chan_names.push_back("5f");
  chan_names.push_back("fRef");

  /* offsets from BASE_CONTROL_REGS */
  chan_name_control_addr["1f"] = BASE_1F;
  chan_name_control_addr["3f"] = BASE_3F;
  chan_name_control_addr["5f"] = BASE_5F;
  chan_name_control_addr["fRef"] =  BASE_FREF;

  /* offsets from MEMORY_BASE */
  chan_name_base_iq_addr["1f"] = BASE_DM_1F_IQ;   // 0x00000
  chan_name_base_iq_addr["3f"] = BASE_DM_3F_IQ;   // 0x02000
  chan_name_base_iq_addr["5f"] = BASE_DM_5F_IQ;   // 0x04000
  chan_name_base_iq_addr["fRef"] = BASE_DM_FREF_IQ; // 0x06000
}

PSM1::~PSM1() {
}

void PSM1::reset() {
  psmVMEReset(myvme, vme_base);
}

INT PSM1::init(bool use_cw_mode) {
  reset_enabled_channel_list();

  if (use_cw_mode) {
    cm_msg(MERROR, __FUNCTION__, "PSM1 does not support CW mode");
    return FE_ERR_ODB;
  }

  INT data = psmRegRead8(myvme, vme_base, 0);

  if (data != 0x84) {
    ss_sleep(250); // sleep 0.25s (may still be resetting)
    data = psmRegRead8(myvme, vme_base, 0);

    if (data != 0x84) {
      cm_msg(MERROR, __FUNCTION__, "Invalid read from PSM - expected 0x84, read 0x%x", data);
      return FE_ERR_HW;
    }
  }


  // Initialize the PSM and set 1f freq to 1MHz, ref freq to 0.5MHz
  INT status =  psmSetOneFreq_Hz(myvme, vme_base, 10000000, 5000000);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Error return from psmSetOneFreq_Hz; cannot setup PSM");
    return FE_ERR_HW;
  }

  // Disable PSM initially
  status = disable();

  if (status != SUCCESS) {
    return status;
  }

  return SUCCESS;
}

int PSM1::get_max_buffer_factor() {
  return psmReadMaxBufFactor(myvme, vme_base);
}

bool PSM1::is_using_cw_mode() {
  return false;
}

INT PSM1::disable() {
  DWORD mode = FULL_SLEEP_MODE | SINGLE_TONE_MODE;

  psmWriteProfileReg(myvme, vme_base, "all", PROFILE_REG2, mode);
  psmWriteGateControl(myvme, vme_base, "all", 0); /* disable external gates; ppg sends spurious gates when loaded */
  psmClearRFpowerTrip(myvme, vme_base);
  psmSetScaleFactor(myvme, vme_base, "all", 0);  /* set amplitude to zero */

  return SUCCESS;
}

bool PSM1::channel_exists(std::string chan_name) {
  return std::find(chan_names.begin(), chan_names.end(), chan_name) != chan_names.end();
}

INT PSM1::set_channel_config(std::string chan_name, INT gate_control, INT scale_factor, BOOL end_iq_sweep_jump_to_idle) {
  note_channel_enabled(chan_name);

  switch (gate_control) {
    case FP_GATE_DISABLED:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Front Panel Gate is Disabled. Use only for testing");
      break;
    case GATE_NORMAL_MODE:
      break;
    case GATE_PULSE_INVERTED:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Front Panel Gate Pulse Inverted. Use only for testing");
      break;
    case INTERNAL_GATE:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Front Panel Gate Pulse Ignored and Gates are ALWAYS ON. Use only for testing");
      break;
    default:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Illegal gate parameter for channel %s (%d)", chan_name, gate_control);
      return FE_ERR_ODB;
  }

  if (scale_factor < 0 || scale_factor > 255) {
    cm_msg(MERROR, __FUNCTION__, "Invalid scale factor %d", scale_factor);
    return FE_ERR_ODB;
  }

  if (!channel_exists(chan_name)) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  char chan_name_c[10];
  sprintf(chan_name_c, "%s", chan_name.c_str());

  psmWriteGateControl(myvme, vme_base, chan_name_c, gate_control);
  psmIQWriteEndSweepMode(myvme, vme_base, chan_name_c, end_iq_sweep_jump_to_idle);

  INT  read_scale_factor = psmSetScaleFactor(myvme, vme_base, chan_name_c, scale_factor);

  if (read_scale_factor != scale_factor) {
    cm_msg(MERROR, __FUNCTION__, "Failed to set correct scale factor for chan %s. Set %d, read %d", chan_name.c_str(), scale_factor, read_scale_factor);
    return FE_ERR_DRIVER;
  }

  return SUCCESS;
}

INT PSM1::set_buffer_factor(std::string chan_name, INT buf_factor) {
  if (!channel_exists(chan_name)) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  char chan_name_c[10];
  sprintf(chan_name_c, "%s", chan_name.c_str());

  psmWriteProfileBufFactor(myvme, vme_base, chan_name_c, buf_factor);
  return SUCCESS;
}

bool PSM1::supports_cic_rate() {
  return true;
}

INT PSM1::set_cic_rate(std::string chan_name, INT Ncic) {
  if (!channel_exists(chan_name)) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  char chan_name_c[10];
  sprintf(chan_name_c, "%s", chan_name.c_str());

  psmSetCIC_Rate(myvme, vme_base, chan_name_c, Ncic);
  return SUCCESS;
}

INT PSM1::set_rf_power_trip_threshold(float thresh) {
  psmWriteRFpowerTripThresh(myvme, vme_base, thresh);
  return SUCCESS;
}

bool PSM1::is_rf_tripped() {
  return psmReadRFpowerTrip(myvme, vme_base) & 1;
}

void PSM1::reset_rf_trip() {
  psmClearRFpowerTrip(myvme, vme_base);
}

INT PSM1::load_iq_pairs(std::string channel_name, int num_iq, int* i_arr, int* q_arr) {
  if (!channel_exists(channel_name)) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", channel_name.c_str());
    return FE_ERR_DRIVER;
  }

  char chan_name_c[10];
  sprintf(chan_name_c, "%s", channel_name.c_str());

  // Set the channel to modulation mode
  psmWriteProfileReg(myvme, vme_base, chan_name_c, PROFILE_REG2, 0);

  // Load the actual I, Q pairs
  int base_iq_addr = chan_name_base_iq_addr[channel_name];

  DWORD my_offset = MEMORY_BASE + base_iq_addr;

  for (int n = 0; n < num_iq; n++) {
    // 2's complement and mask to 10 bits
    INT i_load = (~i_arr[n] + 1) & I_Q_DATA_MASK;
    INT q_load = (~q_arr[n] + 1) & I_Q_DATA_MASK;

    INT data = psmRegWriteDM(myvme, vme_base, my_offset, i_load);

    if (data != i_load) {
      cm_msg(MERROR, __FUNCTION__, "Incorrect i readback for position %d. Set %d, read %d", n, i_load, data);
      return FE_ERR_DRIVER;
    }

    my_offset += 2;

    data = psmRegWriteDM(myvme, vme_base, my_offset, i_load);

    if (data != q_load) {
      cm_msg(MERROR, __FUNCTION__, "Incorrect q readback for position %d. Set %d, read %d", n, q_load, data);
      return FE_ERR_DRIVER;
    }

    my_offset += 2;
  }

  // Set the IQ length register
  my_offset = chan_name_control_addr[channel_name] + PROFILE_IQ_DM_LENGTH;
  psmRegWrite(myvme, vme_base, my_offset, num_iq);

  return SUCCESS;
}

INT PSM1::load_idle_iq(std::string chan_name, int i, int q) {
  if (!channel_exists(chan_name)) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  char chan_name_c[10];
  sprintf(chan_name_c, "%s", chan_name.c_str());

  // Load the I,Q pair
  INT status = psmLoadIdleIQ(myvme, vme_base, chan_name_c, i, q, false);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Error writing idle I,Q pair as %d,%d for channel %s", i, q, chan_name.c_str());
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT PSM1::load_single_iq(std::string chan_name, int i, int q) {
  if (!channel_exists(chan_name)) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  char chan_name_c[10];
  sprintf(chan_name_c, "%s", chan_name.c_str());

  // Load the IQ
  INT status = load_idle_iq(chan_name, i, q);

  if (status != SUCCESS) {
    return status;
  }

  // Set the channel to single-tone mode
  psmWriteProfileReg(myvme, vme_base, chan_name_c, PROFILE_REG2, SINGLE_TONE_MODE);

  // Set the IQ length register to Zero
  DWORD my_offset = chan_name_control_addr[chan_name] + PROFILE_IQ_DM_LENGTH;
  psmRegWrite(myvme, vme_base, my_offset, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Error writing IQ length = 0 for Idle I,Q pair for channel %s", chan_name.c_str());
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT PSM1::load_frequency_list(std::string channel_name, std::vector<float> freqs_Hz) {
  if (is_reference_channel(channel_name)) {
    cm_msg(MERROR, __FUNCTION__, "Can't load a list of reference frequencies");
    return FE_ERR_DRIVER;
  }

  int n_freq = freqs_Hz.size();

  if (n_freq == 0) {
    cm_msg(MERROR, __FUNCTION__, "Can't load a list of frequencies of length 0");
    return FE_ERR_DRIVER;
  }

  int freq_arr[n_freq];

  for (int i = 0; i < n_freq; i++) {
    freq_arr[i] = freqs_Hz[i];
  }

  DWORD first_freq = 0;

  // Load the actual frequencies.
  // For PSM1, this sets the list length automatically in driver.
  INT status = psmLoadFreqDM_ptr(myvme, vme_base, freq_arr, n_freq, &first_freq);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to load frequency list");
    return FE_ERR_DRIVER;
  }

  reset_freq_sweep_addr();

  return SUCCESS;
}

INT PSM1::load_single_frequency(std::string channel_name, float freq_Hz, bool arm) {
  INT status = SUCCESS;

  if (is_reference_channel(channel_name)) {
    psmWrite_fREF_freq_Hz(myvme, vme_base, freq_Hz);
  } else {
    psmLoadIdleFreq_Hz(myvme, vme_base, freq_Hz);
  }

  if (arm) {
    // Send internal strobe
    reset_freq_sweep_addr();
    psmFreqSweepStrobe(myvme, vme_base);
  }

  return SUCCESS;
}

INT PSM1::reset_freq_sweep_addr() {
  psmFreqSweepMemAddrReset(myvme, vme_base);
  return SUCCESS;
}

std::string PSM1::get_reference_channel_name() {
  return "fRef";
}

std::vector<std::string> PSM1::get_channel_names() {
  return chan_names;
}

std::vector<std::string> PSM1::get_channel_names_for_loading_freqs() {
  std::vector<std::string> retval;
  retval.push_back(PSM_ALL_CHANS);
  retval.push_back(get_reference_channel_name());
  return retval;
}

bool PSM1::is_reference_channel(std::string channel_name) {
  return channel_name == get_reference_channel_name();
}
