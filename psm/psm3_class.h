#ifndef PSM3_CLASS_H
#define PSM3_CLASS_H

#include "psm/psm_class.h"
#include <map>
#include <string>

#ifndef DUMMY_VME
#include "mvmestd.h"
#endif

// Object-oriented interfce for the PSM version 3.
// See the PSM base class for most documentation.
//
// Specific features of this version:
// * Channel names are f0ch1/f0ch2/f0ch3/f0ch4/f1
// * Supports CW mode
// * Does not support a CIC rate
// * In CW mode, individual frequencies can be specified for each channel
//
// Below, there are derived classes for a real PSM and a dummy PSM (which
// can be used for testing and doesn't require a VME crate).
class PSM3Base : public PSM {
  public:
    PSM3Base();
    virtual ~PSM3Base() {};

    int get_version() {
      return 3;
    }

    int get_max_buffer_factor();
    bool is_using_cw_mode();
    std::vector<std::string> get_channel_names();
    std::vector<std::string> get_channel_names_for_loading_freqs();
    std::string get_reference_channel_name();
    bool is_reference_channel(std::string channel_name);
    bool supports_cic_rate() {return false;}
    INT set_cic_rate(std::string chan_name, INT cic_rate) {return FE_ERR_DRIVER;}

  protected:
    int channel_name_to_driver_index(std::string name);
    int channel_name_to_offset_index(std::string name);

    bool using_cw_mode;

    std::map<std::string, int> chan_name_driver_idx;
    std::map<std::string, int> chan_name_offset_idx;
    std::map<std::string, int> chan_name_base_iq_addr;
};

#ifndef DUMMY_VME
// Implementation for real PSM3.
class PSM3 : public PSM3Base {
  public:
    PSM3(MVME_INTERFACE* _myvme, DWORD _vme_base);
    virtual ~PSM3();

    // Functions for interface
    void reset();
    INT init(bool use_cw_mode);
    INT disable();
    INT set_rf_power_trip_threshold(float thresh);
    bool is_rf_tripped();
    void reset_rf_trip();
    INT load_iq_pairs(std::string channel_name, int num_iq, int* i_arr, int* q_arr);
    INT load_idle_iq(std::string channel_name, int i, int q);
    INT load_single_iq(std::string channel_name, int i, int q);
    INT load_frequency_list(std::string channel_name, std::vector<float> freqs_Hz);
    INT load_single_frequency(std::string channel_name, float freq_Hz, bool arm=false);
    INT reset_freq_sweep_addr();
    INT set_channel_config(std::string chan_name, INT gate_control, INT scale_factor, BOOL end_iq_sweep_jump_to_idle);
    INT set_buffer_factor(std::string chan_name, INT buf_factor);

  private:

    MVME_INTERFACE* myvme;
    DWORD vme_base;

};
#endif // DUMMY_VME

// Implementation for dummy PSM3 that doesn't require a VME crate.
class DummyPSM3 : public PSM3Base {
  public:
    DummyPSM3() : PSM3Base() {};
    virtual ~DummyPSM3() {};

    void reset() {};

    INT init(bool use_cw_mode) {
      reset_enabled_channel_list();
      using_cw_mode = use_cw_mode;
      return SUCCESS;
    }

    INT disable() {
      return SUCCESS;
    }

    INT set_rf_power_trip_threshold(float thresh) {
      return SUCCESS;
    }

    bool is_rf_tripped() {
      return false;
    }

    void reset_rf_trip() {}

    INT load_iq_pairs(std::string channel_name, int num_iq, int* i_arr, int* q_arr) {
      return sanity_check_channel_name(channel_name, __FUNCTION__);
    }

    INT load_idle_iq(std::string channel_name, int i, int q) {
      return sanity_check_channel_name(channel_name, __FUNCTION__);
    }

    INT load_single_iq(std::string channel_name, int i, int q) {
      return sanity_check_channel_name(channel_name, __FUNCTION__);
    }

    INT load_frequency_list(std::string channel_name, std::vector<float> freqs_Hz) {
      return sanity_check_channel_name(channel_name, __FUNCTION__);
    }

    INT load_single_frequency(std::string channel_name, float freq_Hz, bool arm=false) {
      return sanity_check_channel_name(channel_name, __FUNCTION__);
    }

    INT reset_freq_sweep_addr() {
      return SUCCESS;
    }

    INT set_channel_config(std::string chan_name, INT gate_control, INT scale_factor, BOOL end_iq_sweep_jump_to_idle) {
      note_channel_enabled(chan_name);
      return sanity_check_channel_name(chan_name, __FUNCTION__);
    }

    INT set_buffer_factor(std::string chan_name, INT buf_factor) {
      return sanity_check_channel_name(chan_name, __FUNCTION__);
    }

  protected:
    INT sanity_check_channel_name(std::string chan_name, const char* caller) {
      if (chan_name != PSM_ALL_CHANS) {
        int selected_channel = channel_name_to_driver_index(chan_name);

        if (selected_channel < 0) {
          cm_msg(MERROR, caller, "Unknown channel %s", chan_name.c_str());
          return FE_ERR_DRIVER;
        }
      }

      return SUCCESS;
    }
};

#endif
