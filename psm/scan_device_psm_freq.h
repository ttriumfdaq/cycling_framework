#ifndef SCAN_DEVICE_PSM_FREQ_H
#define SCAN_DEVICE_PSM_FREQ_H

#include "midas.h"
#include "core/scannable_device.h"
#include "psm/psm_class.h"
#include <map>
#include <string>
#include <vector>

#define MAX_PARAMETERIC_FUNCTIONS 5

#define EMPTY_STR_100 EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10 \
EMPTY_STR_10

#define PSM_CHANNEL_SETTINGS_STR "\
Enabled = BOOL : n\n\
Amplitude = INT : 181\n\
Gate control = INT : 0\n\
Modulation mode = INT : 0\n\
Modulation bandwidth Hz = INT : 200\n\
Modulation idle iq at end = BOOL : n\n\
Modulation idle i = INT : 0\n\
Modulation idle q = INT : 0\n\
Modulation use constant i = BOOL : n\n\
Modulation use constant q = BOOL : n\n\
Modulation constant i = INT : 0\n\
Modulation constant q = INT : 0\n\
Modulation wurst phase corr deg = FLOAT : 0\n\
"

#define PSM_CHANNEL_COMPUTED_STR "\
Buffer factor = INT : 0\n\
Num iq = INT : 0\n\
CIC rate = INT : 0\n\
i = INT[4096] \n\
q = INT[4096] \n\
"

#define PSM_GLOBAL_SETTINGS_STR "\
Enabled = BOOL : n\n\
PSM version = INT : -1\n\
Max buffer factor = INT : -1\n\
Use CW mode = BOOL : n\n\
Use wurst params = BOOL : n\n\
Wurst q0 = FLOAT : 0\n\
Wurst N freq scans (40 or 80) = INT : 0\n\
Wurst Tp ms = FLOAT : 0\n\
Wurst freq resolution Nf = INT : 0\n\
Wurst precession freq Hz = FLOAT : 0\n\
"

#define PSM_HOTLINKS_STR "\
Check RF power trip = BOOL : n\n\
RF power trip threshold V = FLOAT : 0.5\n\
"

#define PSM_FREQ_SETTINGS_STR "\
Scan within ppg loop = BOOL : n\n\
Scan X = BOOL : n\n\
Scan Y = BOOL : n\n\
No scan only idle freq = BOOL : n\n\
Start Hz = DOUBLE : 0\n\
End Hz = DOUBLE : 0\n\
Idle Hz = DOUBLE : 0\n\
Reference freq Hz = DOUBLE : 0\n\
In PPG loop N steps = INT : 0\n\
In PPG loop randomize order = BOOL : y\n\
In PPG end sweep load idle = BOOL : n\n\
In PPG idle is first sweep = BOOL : n\n\
Use parametric fns = BOOL : n\n\
Parametric fns = STRING[5] : \n\
[255] \n\
[255] \n\
[255] \n\
[255] \n\
[255] \n\
Parametric Y Hz = DOUBLE : 0\n\
Parametric computation FE name = STRING : [255] RFCalculatorFrontend\n\
"

#define PSM_STATUS_STR "\
RF power tripped = BOOL : n\n\
Scanned frequency Hz = FLOAT : 0\n\
Scanned parametric Hz = FLOAT[5] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
"

typedef struct {
  BOOL enabled;
  INT  amplitude;
  INT  gate_control;
  INT  modulation_mode;
  INT  modulation_bandwidth_Hz;
  BOOL modulation_idle_iq_at_end;
  INT  modulation_idle_i;
  INT  modulation_idle_q;
  BOOL modulation_use_constant_i;
  BOOL modulation_use_constant_q;
  INT  modulation_constant_i;
  INT  modulation_constant_q;
  float modulation_wurst_phase_corr_deg;
} PSM_CHANNEL_SETTINGS;

typedef struct {
  INT buf_factor;
  INT num_iq;
  INT cic_rate;
  INT i[4096];
  INT q[4096];
} PSM_CHANNEL_COMPUTED;

typedef struct {
  BOOL enabled;
  INT  psm_version;
  INT  max_buffer_factor;
  BOOL use_cw_mode;
  BOOL use_wurst_params;
  float wurst_q0;
  int  wurst_N;
  float wurst_tp_ms;
  int  wurst_freq_resolution_Nf;
  float wurst_precession_freq_Hz;
} PSM_GLOBAL_SETTINGS;

typedef struct {
  bool check_rf_power_trip;
  float rf_power_trip_threshold_V;
} PSM_HOTLINKS_SETTINGS;

typedef struct {
  BOOL scan_within_ppg_loop; // Special logic that doesn't match normal scanning behaviour
  BOOL scan_x;
  BOOL scan_y;
  BOOL no_scan_only_idle_freq;
  double start_Hz;
  double end_Hz;
  double idle_Hz;
  double reference_freq_Hz;
  INT in_ppg_loop_n_steps;
  BOOL in_ppg_loop_randomize_order;
  BOOL in_ppg_end_sweep_load_idle;
  BOOL in_ppg_load_first_sweep_val_as_idle;
  BOOL use_parametric_fns;
  char parametric_fns[MAX_PARAMETERIC_FUNCTIONS][255];
  double parametric_y_Hz;
  char parametric_computation_fe_name[255];
} PSM_FREQ_SETTINGS;

typedef struct {
  BOOL rf_tripped;
  float scan_freq_Hz;
  float scan_parametric_Hz[MAX_PARAMETERIC_FUNCTIONS];
} PSM_STATUS;

// A ScannableDevice that can scan through RF frequencies on a PSM.
// Also handles loading I,Q pairs at the beginning of a run.
//
// The PSM is used in a special way, in that it may be used as a normal
// scannable device (incrementing the frequency after each PPG cycle), but
// can also be used to load a list of frequencies that are scanned WITHIN a
// PPG cycle (by loading a list of frequencies onto the PSM and having the PPG
// send a signal to the PSM each time the next frequency should be loaded).
//
// As the logic for computing the frequency lists is the same for either
// type of scan, they are both handled here, with a boolean setting
// (scan_within_ppg_loop) determining which type of scan to implement.
//
// It is also possible for the user to define frequencies as a set of parametric
// functions, rather than the regular linear increase. A separate (python)
// client is called to do these computations. We scan an 'x' variable, and compute
// frequencies for each PSM channel as a function of 'x' and 'y' (a user-specified
// constant). Separate functions can be specified for each channel.
//
// I,Q pairs are computed by a python frontend that should run before this one during
// the begin-of-run transition. That frontend writes the I,Q pair lists to
// /Scanning/PSM/Computed/<chan_name> in the ODB.
//
// This class can handle both PSM1 and PSM3 hardware.
class ScanDevicePSMFreq : public ScannableDevice {
  public:
    ScanDevicePSMFreq(HNDLE _hDB, PSM* _psm);
    virtual ~ScanDevicePSMFreq();

    // Functions from the ScannableDevice interface
    virtual INT check_records() override;
    virtual INT open_records() override;
    virtual INT close_records() override;

    virtual INT begin_of_run(char* error) override;
    virtual INT end_of_run() override;

    virtual INT setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) override;
    virtual void compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) override;

    virtual INT fill_end_of_cycle_banks(char* pevent) override;

    // Special functions for this device

    // Whether we're scanning frequencies inside each loop or between loops.
    bool is_scanning_freq_in_loop() {
      return freq_settings.scan_within_ppg_loop;
    }

    // Convert from current (random freq order) to increasing order idx.
    // This may be useful for "de-randomizing" the frequencies later
    // (especially if scanning within a loop, and you want to histogram
    // data as a function of true de-randomized frequency).
    INT get_sorted_freq_order_idx(std::vector<int>& sorted_idxs, char* error);

    // Whether the RF power has tripped.
    bool is_rf_tripped();

    // Reset the flag saying that RF has tripped.
    void reset_rf_trip();

    // Callback function for when an entry in /Scanning/PSM/Hotlinks is changed
    void hotlinks_touched();

    // Whether we're using parametric equations to compute the frequencies rather
    // than a regular linear increase.
    // Parametric functions use x and y as variables. The user specifies a range
    // for x (incremented linearly) and a constant y.
    bool is_using_parametric_fns() {
      return freq_settings.use_parametric_fns;
    }

    // If using parametric functions for calculating frequencies, the current
    // value of 'x' in Hz.
    float get_current_parametric_x_Hz();

    // The current frequency (if scanning between loops rather than withing a PPG loop).
    float get_current_freq_Hz();

  private:
    // Call an external python program to compute the parameteric frequencies to load
    // (if using parametric functions).
    INT precompute_parametric_values();

    // Compute the frequencies for this sweep, handling normal and parametric cases.
    void compute_sweep(DirectionType direction_type, int n_steps);

    // Helper function to load I,Q pairs from the ODB.
    INT load_iq_pairs(std::string chan_name);

    HNDLE hDB;
    PSM* psm;
    PSM_HOTLINKS_SETTINGS hotlink_settings;
    PSM_FREQ_SETTINGS freq_settings;
    PSM_GLOBAL_SETTINGS global_settings;
    PSM_STATUS rf_status;
    std::map<std::string, PSM_CHANNEL_SETTINGS> channel_settings;
    std::map<std::string, PSM_CHANNEL_COMPUTED> channel_computed;

    int most_recent_x_step;
    int most_recent_y_step;

    std::map<std::string, std::vector<float> > chan_freqs;

    // We use the same 'x' for all channels when computing the parametric
    // functions.
    typedef struct {
      float x_Hz;
      std::map<std::string, float> chan_freq;
    } ParametricFreqPoint;

    std::vector<ParametricFreqPoint> parametric_precomputed_freqs;
    std::vector<float> parametric_sorted_x;
};

#endif
