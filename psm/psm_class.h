#ifndef PSM_CLASS_H
#define PSM_CLASS_H

#include "midas.h"
#include <vector>
#include <string>
#include <algorithm>

// Sometime parameters apply to all non-reference channels on the PSM.
// This definition is used as a pseudo channel name for those cases.
#define PSM_ALL_CHANS "__all__"

// Base class for interacting with PSM (Pol Synth Module) as used
// for generating RF in BNxR-type experiments.
//
// Provides an object-oriented interface consistent for PSM1-3.
class PSM {
  public:
    PSM() {};
    virtual ~PSM() {};

    // PSM version (1, 2, 3 etc).
    virtual int get_version() = 0;

    // Reset the module.
    virtual void reset() = 0;

    // Initialize the module, in either normal mode or CW mode.
    // If the PSM version does not support CW mode, this function should
    // return an error.
    // This function should reset the module to a known state, and in particular
    // should ensure that all channels are initially disabled.
    virtual INT init(bool use_cw_mode) = 0;

    // Whether the PSM is currently configured to use CW mode.
    virtual bool is_using_cw_mode() = 0;

    // Disable the PSM.
    virtual INT disable() = 0;

    // Set the RF power trip threshold, in V.
    virtual INT set_rf_power_trip_threshold(float thresh) = 0;

    // Get whether the RF power has tripped.
    virtual bool is_rf_tripped() = 0;

    // Reset the flag the notes that RF power has tripped.
    virtual void reset_rf_trip() = 0;

    // Enable a channel and set some specific parameters.
    // You should call `note_channel_enabled(chan_name)` from your implementation
    // of this function.
    virtual INT set_channel_config(std::string chan_name, INT gate_control, INT scale_factor, BOOL end_iq_sweep_jump_to_idle) = 0;

    // Return the full list of channel names, including the reference channel.
    virtual std::vector<std::string> get_channel_names() = 0;

    // Return the full list of channel names to be used when loading frequencies,
    // including the reference channel if appropriate.
    // Depending on the PSM version and operating mode, you may not be able to specify
    // independent frequencies for each channel. Therefore, this list may include
    // the `PSM_ALL_CHANS` defined above instead of individual channel names.
    virtual std::vector<std::string> get_channel_names_for_loading_freqs() = 0;

    // Get the name of the reference channel.
    virtual std::string get_reference_channel_name() = 0;

    // Whether this is the reference channel.
    virtual bool is_reference_channel(std::string channel_name) = 0;


    // Load I,Q pairs list for a given channel.
    virtual INT load_iq_pairs(std::string channel_name, int num_iq, int* i_arr, int* q_arr) = 0;

    // Load a single I,Q pair for a given channel, rather than a list.
    virtual INT load_single_iq(std::string channel_name, int i, int q) = 0;

    // Load the idle I,Q pair for a given channel.
    virtual INT load_idle_iq(std::string channel_name, int i, int q) = 0;

    // Get the maximum buffer factor that can be used when computing I,Q pairs
    // for this PSM version.
    virtual int get_max_buffer_factor() = 0;

    // Set the buffer factor that should actually be used for the I,Q pairs for
    // the given channel.
    virtual INT set_buffer_factor(std::string chan_name, INT buf_factor) = 0;

    // Whether this PSM version supports setting a CIC rate when using I,Q pairs.
    virtual bool supports_cic_rate() = 0;

    // Set the CIC rate to use with the I,Q pairs for a given channel.
    virtual INT set_cic_rate(std::string chan_name, INT cic_rate) = 0;


    // Load a frequency list for a given channel.
    // Note channel_name may be from the `PSM_ALL_CHANS` defined above if
    // relevant for a PSM version.
    virtual INT load_frequency_list(std::string channel_name, std::vector<float> freqs_Hz) = 0;

    // Load a single frequency for a given channel, rather than a list.
    // Note channel_name may be from the `PSM_ALL_CHANS` defined above if
    // relevant for a PSM version.
    virtual INT load_single_frequency(std::string channel_name, float freq_Hz, bool arm=false) = 0;

    // Reset frequency sweeps to the first value.
    virtual INT reset_freq_sweep_addr() = 0;


    // Reset our record of which channels are enabled.
    virtual void reset_enabled_channel_list() {
      enabled_channels.clear();
    }

    // Call this from `set_channel_config()`
    virtual void note_channel_enabled(std::string chan_name) {
      enabled_channels.push_back(chan_name);
    }

    // Return whether a specific channel is enabled or not.
    // If chan_name matches the `PSM_ALL_CHANS` defined above, then it
    // will return true if any non-reference channel is enabled.
    virtual bool is_channel_enabled(std::string chan_name) {
      if (chan_name == PSM_ALL_CHANS) {
        // See if any non-reference channel is enabled.
        for (auto it = enabled_channels.begin(); it != enabled_channels.end(); it++) {
          if (!is_reference_channel(*it)) {
            return true;
          }
        }

        return false;
      } else {
        // See if the specific channel is enabled.
        return std::find(enabled_channels.begin(), enabled_channels.end(), chan_name) != enabled_channels.end();
      }
    }

  protected:
    std::vector<std::string> enabled_channels;
};

#endif
