/*-----------------------------------------------------------------------------
 * Copyright (c) 1996      TRIUMF Data Acquistion Group
 * Please leave this header in any reproduction of that distribution
 *
 * TRIUMF Data Acquisition Group, 4004 Wesbrook Mall, Vancouver, B.C. V6T 2A3
 * Email: online@triumf.ca           Tel: (604) 222-1047  Fax: (604) 222-1074
 *        amaudruz@triumf.ca
 * ----------------------------------------------------------------------------
 *
 * Description	: Header file for TRIUMF VME FREQ SYNTH Module
 *
 * Author: Suzannah Daviel, TRIUMF
 *      based on trPSM.h
 *---------------------------------------------------------------------------*
 * CVS log information:
 *$Log: trPSMIII.h,v $
 *
 *
 *---------------------------------------------------------------------------*/
#ifndef _PSMIII_INCLUDE_
#define _PSMIII_INCLUDE_
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define EXTERNAL extern

#ifndef MIDAS_TYPE_DEFINED
#define MIDAS_TYPE_DEFINED
typedef unsigned short int WORD;
typedef int                INT;
typedef char               BYTE;
typedef unsigned int       DWORD;
typedef unsigned int  BOOL;
#define SUCCESS 1
#endif /* MIDAS_TYPE_DEFINED */

// For some reason need this
//typedef int                INT;
//typedef unsigned int       DWORD;
#include "mvmestd.h"


/* PSMIII register offset defines

          register        offset        bits     access
            name                        used     type        */
#define MEMORY_BASE          0x00000
/* Data Memory (DM) : offsets from MEMORY_BASE */
#define BASE_DM_F0_CH1_IQ             0x00000   /* 10     word    RW  */
#define BASE_DM_F0_CH2_IQ             0x02000   /* 10     word    RW  */
#define BASE_DM_F0_CH3_IQ             0x04000   /* 10     word    RW  */
#define BASE_DM_F0_CH4_IQ             0x06000   /* 10     word    RW  */
#define BASE_DM_F1_IQ                0x08000   /* 10     word    RW  */
#define BASE_DM_FREQ_SWEEP           0x0A000   /* 32     2*word  RW  */
#define IDLE_IQ_OFFSET               0x01FFC   /* 10     word    RW  */
#define IDLE_FREQ_OFFSET             0x01FFC   /* 32     2*word  RW  */

#define NUM_CHANNELS             /* 5 if F1 counts? */

/* Base value of Register sets */
#define BASE_CONTROL_REGS    0x0C000
/* The following Offsets are added to BASE_CONTROL_REGS :

   Channel offsets (5 channels) */
#define BASE_F0_C1                 0x00  /* base address offset of 1F channel */
#define BASE_F0_C2                 0x0A  /* base address offset of 3F channel */
#define BASE_F0_C3                 0x14  /* base address offset of 5F channel */
#define BASE_F0_C4                 0x1E  /* base address offset of freq channel */
#define BASE_F1                    0x28  /* base address offset of freq channel */

/* Offsets within a channel */
#define PHASE_MOD                  0x0   /* offset from channel base to Phase Mod Reg */
#define OP_SCALE_FAC               0x2   /* offset from channel base to Scale Factor Reg */
#define NC_BUF_FAC                 0x4   /* offset from channel base to Nc Buffer Factor Reg */
#define IQ_DM_LEN                  0x6   /* offset from channel base to IQ Data Mem length */
#define IQ_DM_ADDR                 0x8   /* offset from channel base to IQ Data Mem address */

/* F1 Frequency Tuning Register */
#define F1_TUNING_FREQ             0x34      /* 32    dword    RW  */

#define FC01_SCALE_FACTOR          0x38      /*  8     byte   RW   */
#define FC01_OUTPUT_SELECT         0x39      /*  1     byte   RW   */
/* FC0 Tuning Frequency Register */
#define FC0_TUNING_FREQ            0x3C      /* 16     word    RW  */
/* FC1 Tuning Frequency Register */
#define FC1_TUNING_FREQ            0x40      /* 16     word    RW  */


#define FREQ_SWEEP_LENGTH          0x44      /* 16     word    RW  */
#define FREQ_SWEEP_ADDRS           0x46      /* 16     word    R   */
#define FREQ_SWEEP_ADDR_PRESET     0x48      /*  8     byte    RW  */
#define FREQ_SWEEP_ADDR_RESET      0x49      /*  8     byte    RW  */
#define FREQ_SWEEP_INT_STROBE      0x4A      /*  8     byte    RW  */
#define END_SWEEP_CONTROL          0x4B      /*  8     byte    RW  */
#define GATE_CONTROL_REG           0x4C      /*  16    word    RW  */

/* Ancillary Control */
#define ANCILLARY_INPUT            0x4E      /*  8     byte    R   */
#define ANCILLARY_OUTPUT           0x4F      /*  8     byte    RW  */
#define ANCILLARY_IO_CONTROL       0x50      /*  8     byte    RW  */

#define RF_PRE_GATED_OUTPUT_SELECT 0x51      /*  8     byte    RW  */
#define RF_GATED_OUTPUT_SELECT     0x52      /*  8     byte    RW  */
#define RF_TRIP_THRESHOLD          0x53      /*  8     byte    RW  */
#define RF_TRIP_STATUS_RESET       0x54      /*  8     byte    RW  */

#define MODULE_OPERATING_MODE      0x55      /*  8     byte    RW  */
#define PSM_VME_RESET              0x56      /*  8     byte    RW  */
#define FPGA_TEMP                  0x58      /*  8     byte    RW  */
#define FCW_SCALE_FACTOR           0x5C      /*  8     byte    RW  */
#define FCW_TUNING_FREQ            0x60      /* 32     dword   RW  */

/* bits and masks */
#define SIX_BIT_MASK 0x3F
#define EIGHT_BIT_MASK  0xFF
#define TEN_BIT_MASK  0x3FF
#define ELEVEN_BIT_MASK  0x7FF

#define I_Q_DATA_MASK 0x3FF /* 10 bits */

// Define Channel numbers etc
#define CH1 1
#define CH2 2
#define CH3 3
#define CH4 4
#define F1  5
#define ALLCH 6
#define SETALL 99 // used by getbitpat


/* Gate Control Register */
#define FP_GATE_DISABLED 0
#define GATE_NORMAL_MODE 1
#define GATE_PULSE_INVERTED 2
#define INTERNAL_GATE 3

/* Module Operating Mode */
#define BNMR_OPERATING_MODE 0
#define BNQR_OPERATING_MODE 1
#define CW_OPERATING_MODE 2

/* trPSM3.c prototypes:    */

// Read/Write a register
uint32_t regRead8(MVME_INTERFACE *mvme, DWORD base, int offset);
uint32_t regRead16(MVME_INTERFACE *mvme, DWORD base, int offset);
uint32_t regRead32(MVME_INTERFACE *mvme, DWORD base, int offset);
void regWrite8(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);
void regWrite16(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);
void regWrite32(MVME_INTERFACE *mvme, DWORD base, int offset, uint32_t value);

// Read/Write control registers
INT psmRegRead32(MVME_INTERFACE *mvme, const DWORD base_addr, DWORD reg_offset);
INT psmRegRead16(MVME_INTERFACE *mvme, const DWORD base_addr, DWORD reg_offset);
INT psmRegRead8(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset);
INT psmRegWrite32(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,const DWORD value);

INT psmRegWrite16(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset,const WORD value);
INT psmRegWrite8(MVME_INTERFACE *mvme,  const DWORD base_addr, DWORD reg_offset, const BYTE value);
// Read/Write Freq Sweep DM (32 bit)
INT psmRegReadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset);
INT psmRegWriteFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, const DWORD reg_offset,const DWORD value);
// Read/Write IQ Memory (16 bit)
INT psmRegReadIQDM( MVME_INTERFACE *mvme, DWORD base_addr,  DWORD base_iq_addr,  DWORD reg_offset, INT *I, INT *Q);
INT psmRegWriteIQDM( MVME_INTERFACE *mvme, DWORD base_addr,  const DWORD base_iq_addr, const DWORD reg_offset, INT I, INT Q);
INT psmLoadIQfile ( MVME_INTERFACE *mvme, DWORD base_addr, DWORD base_iq_addr, char *file,  INT *nvalues,  INT *first_I, INT *first_Q);
INT psmLoadFreqFile( MVME_INTERFACE *mvme, DWORD base_addr, char * file , DWORD *nfreq, DWORD *first_freq);
INT psmLoadFreqPtr( MVME_INTERFACE *mvme, DWORD base_addr, INT *pfreq , INT nfreq, DWORD *first_freq);
INT psmReadFreqDM( MVME_INTERFACE *mvme, DWORD base_addr, DWORD nfreq, DWORD offset);
INT psmFreqSweepStrobe( MVME_INTERFACE *mvme, DWORD base_addr);
void dump(MVME_INTERFACE *mvme,  DWORD base_addr);
INT psmFreqSweepAddrReset( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmFreqSweepAddrPreset( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmWrite_reference_freq_hex( MVME_INTERFACE *mvme, DWORD base_addr, DWORD offset, const DWORD freq);
INT psmWrite_reference_freq_Hz( MVME_INTERFACE *mvme, DWORD base_addr, DWORD offset, const DWORD freq_Hz);
INT psmWriteGateControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT gate_code);
INT psmReadGateControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel);
INT psmReadEndSweepControl(MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT display);
INT psmWriteEndSweepControl( MVME_INTERFACE *mvme, DWORD base_addr, BOOL freq_sweep, INT selected_channel, INT code);
INT psmWriteAncilIOControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT code);
INT psmReadAncilIOControl( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel );
INT psmWriteAncilOutput( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel, INT code);
INT psmReadAncilIO( MVME_INTERFACE *mvme, DWORD base_addr, int offset, INT selected_channel );
INT psmVMEReset( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmSetScaleFactor ( MVME_INTERFACE *mvme,  DWORD base_addr, INT selected_channel,  DWORD value);
INT psmWriteBufFactor( MVME_INTERFACE *mvme, DWORD base_addr,  INT selected_channel, INT data);
DWORD get_hex(DWORD freq_Hz);
DWORD get_Hz(DWORD freq_hex);
INT   psmWriteRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr, float Vtrip);
float psmReadRFpowerTripThresh( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmReadRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmClearRFpowerTrip( MVME_INTERFACE *mvme, DWORD base_addr);
INT psmLoadIdleIQ ( MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel,  INT I, INT Q);
INT psmWriteChanReg( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset, INT nbits, INT channel_index, INT data);
INT psmWriteChannelReg( MVME_INTERFACE *mvme, DWORD base_addr, DWORD reg_offset, INT nbits, INT selected_channel,INT data);
INT psmWriteIQLen( MVME_INTERFACE *mvme, DWORD base_addr,  INT selected_channel, INT data);
INT TwosComp_convert(INT value, BOOL twos_comp);
INT get_IQ_BaseAddr(INT chan);
INT select_channel(void);
INT  select_FC0_channel(void);
void show_selected_channel(INT *selected_channel);
INT check_selected_channel(INT selected_channel);
void show_gate_bits(INT data);
INT show_bits(INT data, INT nvb, INT nb, INT size, INT *array);
INT getbitpat(INT data, INT nvb, INT nb, INT chan, INT code);
char *gate_info(INT n);
char *endsweep_info(INT n);
char *channel_name(INT n);
INT bitset(INT data, INT bit, INT val);
INT  getIQpair(DWORD data, INT *I, INT *Q);
INT check_IQ_readback(INT IQ, INT iq);
INT check_IQ_match(INT IQ, INT iq);
INT check_offset(INT offset);
void show_data_array(INT selected_channel);
void psmTest1( MVME_INTERFACE *mvme, DWORD base_addr, INT max_chan, INT *times_ms);
INT psmInit(MVME_INTERFACE *mvme, DWORD base_addr, INT selected_channel);
void calc_freq_conversion_factors(void);
#endif /* ifndef  _PSMIII_INCLUDE_ */
