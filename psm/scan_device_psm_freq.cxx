#include "psm/scan_device_psm_freq.h"
#include "midas.h"
#include "core/scannable_device.h"
#include "core/scan_utils.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <cstring>
#include "math.h"
#include "mrpc.h"

#define PSM_HOTLINKS_DIR "/Scanning/PSM/Hotlinks"
#define PSM_SETTINGS_DIR "/Scanning/PSM/Settings"
#define PSM_COMPUTED_DIR "/Scanning/PSM/Computed"
#define PSM_STATUS_DIR "/Scanning/PSM/Status"

void hotlink_callback(INT a, INT b, void* data) {
  ScanDevicePSMFreq* obj = (ScanDevicePSMFreq*)data;
  obj->hotlinks_touched();
}

ScanDevicePSMFreq::ScanDevicePSMFreq(HNDLE _hDB, PSM* _psm) : ScannableDevice(_hDB) {
  hDB = _hDB;
  psm = _psm;
  most_recent_x_step = -1;
  most_recent_y_step = -1;
}

ScanDevicePSMFreq::~ScanDevicePSMFreq() {
}

INT ScanDevicePSMFreq::check_records() {
  char global_path[256], hotlinks_path[256], freqs_path[256], status_path[256];
  sprintf(global_path, "%s/%s", PSM_SETTINGS_DIR, "Global");
  sprintf(hotlinks_path, "%s", PSM_HOTLINKS_DIR);
  sprintf(freqs_path,  "%s/%s", PSM_SETTINGS_DIR, "Frequency");
  sprintf(status_path, "%s", PSM_STATUS_DIR);

  INT status;

  status = db_check_record(hDB, 0, global_path, PSM_GLOBAL_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", global_path);
    return status;
  }

  status = db_check_record(hDB, 0, hotlinks_path, PSM_HOTLINKS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", hotlinks_path);
    return status;
  }

  status = db_check_record(hDB, 0, freqs_path, PSM_FREQ_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", freqs_path);
    return status;
  }

  status = db_check_record(hDB, 0, status_path, PSM_STATUS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", status_path);
    return status;
  }

  std::vector<std::string> chan_names = psm->get_channel_names();
  channel_settings.clear();
  channel_computed.clear();

  for (auto it = chan_names.begin(); it != chan_names.end(); it++) {
    char settings_path[256], computed_path[256];
    sprintf(settings_path, "%s/%s", PSM_SETTINGS_DIR, it->c_str());
    sprintf(computed_path, "%s/%s", PSM_COMPUTED_DIR, it->c_str());

    status = db_check_record(hDB, 0, settings_path, PSM_CHANNEL_SETTINGS_STR, TRUE);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", settings_path);
      return status;
    }

    status = db_check_record(hDB, 0, computed_path, PSM_CHANNEL_COMPUTED_STR, TRUE);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", computed_path);
      return status;
    }

    channel_settings[*it] = PSM_CHANNEL_SETTINGS();
    channel_computed[*it] = PSM_CHANNEL_COMPUTED();
  }

  // Set the PSM version
  char odb_path[256];
  sprintf(odb_path, "%s/%s", global_path, "PSM version");
  INT version = psm->get_version();
  db_set_value(hDB, 0, odb_path, &version, sizeof(version), 1, TID_INT);

  // Set the max buffer factor this PSM supports
  sprintf(odb_path, "%s/%s", global_path, "Max buffer factor");
  INT max_buf_factor = psm->get_max_buffer_factor();
  db_set_value(hDB, 0, odb_path, &max_buf_factor, sizeof(max_buf_factor), 1, TID_INT);

  return SUCCESS;
}

INT ScanDevicePSMFreq::open_records() {
  char global_path[256], hotlinks_path[256], freqs_path[256], status_path[256];
  sprintf(global_path, "%s/%s", PSM_SETTINGS_DIR, "Global");
  sprintf(hotlinks_path, "%s", PSM_HOTLINKS_DIR);
  sprintf(freqs_path,  "%s/%s", PSM_SETTINGS_DIR, "Frequency");
  sprintf(status_path, "%s", PSM_STATUS_DIR);

  INT status;
  HNDLE h_global, h_hotlinks, h_freqs, h_status;
  INT size_global = sizeof(global_settings);
  INT size_hotlinks = sizeof(hotlink_settings);
  INT size_freqs = sizeof(freq_settings);
  INT size_status = sizeof(rf_status);

  db_find_key(hDB, 0, global_path, &h_global);
  db_find_key(hDB, 0, hotlinks_path, &h_hotlinks);
  db_find_key(hDB, 0, freqs_path, &h_freqs);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_get_record(hDB, h_global, &global_settings, &size_global, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", global_path);
    return status;
  }

  status = db_open_record(hDB, h_global, &global_settings, size_global, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open record for %s", global_path);
    return status;
  }

  status = db_get_record(hDB, h_hotlinks, &hotlink_settings, &size_hotlinks, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", hotlinks_path);
    return status;
  }

  status = db_open_record(hDB, h_hotlinks, &hotlink_settings, size_hotlinks, MODE_READ, hotlink_callback, this);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open record for %s", hotlinks_path);
    return status;
  }

  status = db_get_record(hDB, h_freqs, &freq_settings, &size_freqs, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", freqs_path);
    return status;
  }

  status = db_open_record(hDB, h_freqs, &freq_settings, size_freqs, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open record for %s", freqs_path);
    return status;
  }

  status = db_get_record(hDB, h_status, &rf_status, &size_status, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", status_path);
    return status;
  }

  status = db_open_record(hDB, h_status, &rf_status, size_status, MODE_WRITE, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open record for %s", status_path);
    return status;
  }

  std::vector<std::string> chan_names = psm->get_channel_names();

  for (auto it = chan_names.begin(); it != chan_names.end(); it++) {
    char settings_path[256], computed_path[256];
    sprintf(settings_path, "%s/%s", PSM_SETTINGS_DIR, it->c_str());
    sprintf(computed_path, "%s/%s", PSM_COMPUTED_DIR, it->c_str());

    HNDLE h_settings, h_computed;
    INT size_settings = sizeof(channel_settings[*it]);
    INT size_computed = sizeof(channel_computed[*it]);

    db_find_key(hDB, 0, settings_path, &h_settings);
    db_find_key(hDB, 0, computed_path, &h_computed);

    status = db_get_record(hDB, h_settings, &channel_settings[*it], &size_settings, 0);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", settings_path);
      return status;
    }

    status = db_open_record(hDB, h_settings, &channel_settings[*it], size_settings, MODE_READ, NULL, NULL);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to open record for %s", settings_path);
      return status;
    }

    status = db_get_record(hDB, h_computed, &channel_computed[*it], &size_computed, 0);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", computed_path);
      return status;
    }

    status = db_open_record(hDB, h_computed, &channel_computed[*it], size_computed, MODE_READ, NULL, NULL);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to open record for %s", computed_path);
      return status;
    }
  }

  return SUCCESS;
}

INT ScanDevicePSMFreq::close_records() {
  char global_path[256], hotlinks_path[256], freqs_path[256], status_path[256];
  sprintf(global_path, "%s/%s", PSM_SETTINGS_DIR, "Global");
  sprintf(hotlinks_path, "%s", PSM_HOTLINKS_DIR);
  sprintf(freqs_path,  "%s/%s", PSM_SETTINGS_DIR, "Frequency");
  sprintf(status_path, "%s", PSM_STATUS_DIR);

  INT status;
  HNDLE h_global, h_hotlinks, h_freqs, h_status;
  INT size_global = sizeof(global_settings);
  INT size_hotlinks = sizeof(hotlink_settings);
  INT size_freqs = sizeof(freq_settings);
  INT size_status = sizeof(status);

  db_find_key(hDB, 0, global_path, &h_global);
  db_find_key(hDB, 0, hotlinks_path, &h_hotlinks);
  db_find_key(hDB, 0, freqs_path, &h_freqs);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_close_record(hDB, h_global);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close %s", global_path);
    return status;
  }

  status = db_close_record(hDB, h_hotlinks);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close %s", hotlinks_path);
    return status;
  }

  status = db_close_record(hDB, h_freqs);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close %s", freqs_path);
    return status;
  }

  status = db_close_record(hDB, h_status);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close %s", status_path);
    return status;
  }

  std::vector<std::string> chan_names = psm->get_channel_names();

  for (auto it = chan_names.begin(); it != chan_names.end(); it++) {
    char settings_path[256], computed_path[256];
    sprintf(settings_path, "%s/%s", PSM_SETTINGS_DIR, it->c_str());
    sprintf(computed_path, "%s/%s", PSM_COMPUTED_DIR, it->c_str());

    HNDLE h_settings, h_computed;

    db_find_key(hDB, 0, settings_path, &h_settings);
    db_find_key(hDB, 0, computed_path, &h_computed);

    status = db_close_record(hDB, h_settings);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to close %s", settings_path);
      return status;
    }

    status = db_close_record(hDB, h_computed);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to close %s", computed_path);
      return status;
    }
  }

  return SUCCESS;
}

void ScanDevicePSMFreq::hotlinks_touched() {
  psm->set_rf_power_trip_threshold(hotlink_settings.rf_power_trip_threshold_V);
}

bool ScanDevicePSMFreq::is_rf_tripped() {
  if (hotlink_settings.check_rf_power_trip) {
    rf_status.rf_tripped = psm->is_rf_tripped();
  } else {
    rf_status.rf_tripped = false;
  }

  return rf_status.rf_tripped;
}

void ScanDevicePSMFreq::reset_rf_trip() {
  psm->reset_rf_trip();
}

INT ScanDevicePSMFreq::begin_of_run(char* error) {
  INT status = SUCCESS;
  parametric_precomputed_freqs.clear();
  chan_freqs.clear();
  most_recent_x_step = -1;
  most_recent_y_step = -1;
  rf_status.scan_freq_Hz = -1;

  for (int i = 0; i < MAX_PARAMETERIC_FUNCTIONS; i++) {
    rf_status.scan_parametric_Hz[i] = -1;
  }

  // Init PSM. Defaults to disabling all channels.
  status = psm->init(global_settings.use_cw_mode);

  if (status != SUCCESS) {
    snprintf(error, 255, "%s", "Failed to initialize PSM hardware. See Messages for more.");
    return status;
  }

  if (!global_settings.enabled) {
    cm_msg(MINFO, __FUNCTION__, "Disabling PSM as RF not needed");
    psm->disable();
    return SUCCESS;
  }

  // Global settings
  psm->set_rf_power_trip_threshold(hotlink_settings.rf_power_trip_threshold_V);

  // Per-channel settings.
  for (auto it = channel_settings.begin(); it != channel_settings.end(); it++) {
    std::string chan_name = it->first;
    PSM_CHANNEL_SETTINGS& settings = it->second;

    if (settings.enabled) {
      scan_utils::ts_printf("Setting PSM channel %s to amplitude %d\n", chan_name.c_str(), settings.amplitude);

      status = psm->set_channel_config(chan_name,
                                       settings.gate_control,
                                       settings.amplitude,
                                       settings.modulation_idle_iq_at_end);

      if (status != SUCCESS) {
        snprintf(error, 255, "Failed to set config for PSM channel %s. See Messages for more.", chan_name.c_str());
        return status;
      }

      status = load_iq_pairs(chan_name);

      if (status != SUCCESS) {
        snprintf(error, 255, "Failed to load IQ pairs for PSM channel %s. See Messages for more.", chan_name.c_str());
        return status;
      }
    } else {
      scan_utils::ts_printf("PSM channel %s is disabled\n", chan_name.c_str());
    }
  }

  // Check mutually-exclusive frequency settings
  int num_set = 0;

  if (freq_settings.scan_x) {
    num_set++;
  }
  if (freq_settings.scan_y) {
    num_set++;
  }
  if (freq_settings.scan_within_ppg_loop) {
    num_set++;
  }
  if (freq_settings.no_scan_only_idle_freq) {
    num_set++;
  }

  if (num_set > 1) {
    snprintf(error, 255, "%s", "At most one of scan X/Y/in-ppg-loop/none must be set.");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  if (freq_settings.no_scan_only_idle_freq && freq_settings.use_parametric_fns) {
    snprintf(error, 255, "%s", "Doesn't make sense to enable parametric fns when only using idle freq");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  if (psm->is_using_cw_mode() && freq_settings.scan_within_ppg_loop) {
    snprintf(error, 255, "%s", "Doesn't make sense to use CW mode with an in-ppg-loop of frequencies");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_ODB;
  }

  // Fill parametric_precomputed_freqs if needed
  if (freq_settings.use_parametric_fns) {
    status = precompute_parametric_values();

    if (status != SUCCESS) {
      snprintf(error, 255, "%s", "Failed to pre-compute PSM parameteric frequencies. See Messages for more.");
      return status;
    }
  }

  // Load single frequencies, or get ready for loading
  // lists of frequencies.
  std::vector<std::string> chan_names = psm->get_channel_names_for_loading_freqs();

  for (auto it = chan_names.begin(); it != chan_names.end(); it++) {
    std::string chan_name = *it;

    if (psm->is_channel_enabled(chan_name)) {
      if (psm->is_reference_channel(chan_name)) {
        // Load reference frequency
        scan_utils::ts_printf("Setting PSM reference channel (%s) freq to %f Hz\n", chan_name.c_str(), freq_settings.reference_freq_Hz);
        psm->load_single_frequency(chan_name, freq_settings.reference_freq_Hz);
      } else if (freq_settings.no_scan_only_idle_freq) {
        // Just a single frequency to load. Send internal strobe as well.
        scan_utils::ts_printf("Setting PSM %s freq to %f Hz\n", chan_name.c_str(), freq_settings.reference_freq_Hz);
        psm->load_single_frequency(chan_name, freq_settings.idle_Hz, true);
      } else {
        // Initialize vector for storing frequencies.
        scan_utils::ts_printf("Will load PSM %s frequencies later\n", chan_name.c_str());
        chan_freqs[chan_name] = std::vector<float>();
      }
    }
  }

  return status;
}

INT ScanDevicePSMFreq::end_of_run() {
  return psm->disable();
}

INT ScanDevicePSMFreq::precompute_parametric_values() {
  std::vector<std::string> channel_names = psm->get_channel_names_for_loading_freqs();

  int chan_i = 0;

  for (auto it = channel_names.begin(); it != channel_names.end(); it++, chan_i++) {
    std::string chan_name = *it;

    if (psm->is_reference_channel(chan_name)) {
      continue;
    }

    if (chan_i >= MAX_PARAMETERIC_FUNCTIONS) {
      cm_msg(MERROR, __FUNCTION__, "Coding error. Too many channels for the number of defined parameteric functions in ODB. Channel %s is index %d, but there are only %d functions.", chan_name.c_str(), chan_i, MAX_PARAMETERIC_FUNCTIONS);
      return FE_ERR_DRIVER;
    }

    if (psm->is_channel_enabled(chan_name)) {
      if (!strlen(freq_settings.parametric_fns[chan_i])) {
        cm_msg(MERROR, __FUNCTION__, "No parametric function for PSM channel %s", chan_name.c_str());
        return FE_ERR_ODB;
      }

      int num_steps = -1;

      if (freq_settings.scan_x) {
        num_steps = nX;
      } else if (freq_settings.scan_y) {
        num_steps = nY;
      } else if (freq_settings.scan_within_ppg_loop) {
        num_steps = freq_settings.in_ppg_loop_n_steps;
      }

      num_steps += 1;

      // Call a python frontend to evaluate the parametric function for us
      std::stringstream args;
      args << "{";
      args << "\"x_start\": " << freq_settings.start_Hz << ", ";
      args << "\"x_end\": " << freq_settings.end_Hz << ", ";
      args << "\"num_x\": " << num_steps << ", ";
      args << "\"y_const\": " << freq_settings.parametric_y_Hz << ", ";
      args << "\"func\": \"" << freq_settings.parametric_fns[chan_i] << "\"";
      args << "}";

      INT client_handle;
      INT status = cm_connect_client(freq_settings.parametric_computation_fe_name, &client_handle);

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Failed to connect to %s", freq_settings.parametric_computation_fe_name);
        return CM_NO_CLIENT;
      }

      int bufsize = 10000;
      char retstr_buf[bufsize];
      status = rpc_client_call(client_handle, RPC_JRPC, "eval_parametric", args.str().c_str(), retstr_buf, bufsize);
      cm_disconnect_client(client_handle, FALSE);

      if (debug) {
        scan_utils::ts_printf("Parametric computation returned '%s'\n", retstr_buf);
      }

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Got return value %d from calling parametric computation", status);
        return FE_ERR_DRIVER;
      }

      // Expect return value to be of form {"code": 1, "msg": "4.32 5 6.9 7"}
      char freq_list[bufsize];
      int code = 0;
      status = scan_utils::extract_msg_from_rpc_response(retstr_buf, code, freq_list);

      if (status != SUCCESS) {
        return status;
      }

      if (code != 1) {
        cm_msg(MERROR, __FUNCTION__, "Unexpected code %d from parametric computation, with message: %s", code, retstr_buf);
        return FE_ERR_DRIVER;
      }

      // Parse the output by reading the space-separated list of numbers
      std::istringstream parser(freq_list);
      float val, freq, x;
      int written_num_freqs = 0;
      int iword = 0;

      while (parser >> val) {
        if (iword % 2 == 0) {
          x = val;
        } else {
          freq = val;

          // We have now read x value and frequency for that x value
          if (debug) {
            scan_utils::ts_printf("Parametric freq %s[%d] = %f\n", chan_name.c_str(), written_num_freqs, freq);
          }

          if (parametric_precomputed_freqs.size() <= written_num_freqs) {
            // First channel; build the ParametricFreqPoint
            ParametricFreqPoint point;
            point.x_Hz = x;
            point.chan_freq[chan_name] = freq;
            parametric_precomputed_freqs.push_back(point);
          } else {
            ParametricFreqPoint& point = parametric_precomputed_freqs[written_num_freqs];

            if (fabs(x - point.x_Hz) > 1e-3) {
              cm_msg(MERROR, __FUNCTION__, "Channels disagree about X value %d: %f for first chan and %f for %s", written_num_freqs, point.x_Hz, x, chan_name.c_str());
              return FE_ERR_DRIVER;
            }

            point.chan_freq[chan_name] = freq;
          }

          written_num_freqs++;
        }

        iword++;
      }

      if (written_num_freqs != num_steps) {
        cm_msg(MERROR, __FUNCTION__, "Calculator gave %d freqs for channel %s but we expected %d", written_num_freqs, chan_name.c_str(), num_steps);
        return FE_ERR_DRIVER;
      }
    }
  }

  return SUCCESS;
}

INT ScanDevicePSMFreq::setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) {
  if (!global_settings.enabled) {
    return SUCCESS;
  }

  bool have_new_value = false;
  bool have_new_list = false;

  if (x_enabled && new_x && freq_settings.scan_x) {
    have_new_value = true;
    most_recent_x_step = x_step;
  }

  if (y_enabled && new_y && freq_settings.scan_y) {
    have_new_value = true;
    most_recent_y_step = y_step;
  }

  if (freq_settings.scan_within_ppg_loop && rep == 0) {
    have_new_list = true;
    DirectionType order = freq_settings.in_ppg_loop_randomize_order ? DirRandom : DirIncreasing;
    compute_sweep(order, freq_settings.in_ppg_loop_n_steps);
  }

  // Load new frequencies
  INT status = SUCCESS;
  std::vector<std::string> channel_names = psm->get_channel_names_for_loading_freqs();
  int chan_idx = 0;

  for (auto it = channel_names.begin(); it != channel_names.end(); it++, chan_idx++) {
    std::string chan_name = *it;

    if (psm->is_channel_enabled(chan_name) && !psm->is_reference_channel(chan_name)) {
      if (have_new_value) {
        int step = freq_settings.scan_x ? x_step : y_step;
        float freq = chan_freqs[chan_name][step];

        scan_utils::ts_printf("Setting PSM %s to %f Hz\n", chan_name.c_str(), freq);
        status = psm->load_single_frequency(chan_name, freq, true);

        if (chan_name == PSM_ALL_CHANS) {
          rf_status.scan_freq_Hz = freq;
        } else if (chan_idx < MAX_PARAMETERIC_FUNCTIONS) {
          rf_status.scan_parametric_Hz[chan_idx] = freq;
        }
      } else if (have_new_list) {
        scan_utils::ts_printf("Loading new frequency list of length %lu to PSM %s\n", chan_freqs[chan_name].size(), chan_name.c_str());
        status = psm->load_frequency_list(chan_name, chan_freqs[chan_name]);

        if (freq_settings.scan_within_ppg_loop && freq_settings.in_ppg_end_sweep_load_idle && status == SUCCESS) {
          double idle = freq_settings.in_ppg_load_first_sweep_val_as_idle ? chan_freqs[chan_name][0] : freq_settings.idle_Hz;
          scan_utils::ts_printf("Setting PSM %s idle to %f Hz\n", chan_name.c_str(), idle);
          status = psm->load_single_frequency(chan_name, idle);
        }
      }
    }

    if (status != SUCCESS) {
      return status;
    }
  }

  return SUCCESS;
}

bool comparator(const std::pair<float, int>& l, std::pair<float, int>& r) {
  return l.first < r.first;
}

INT ScanDevicePSMFreq::get_sorted_freq_order_idx(std::vector<int>& sorted_idxs, char* error) {
  // Used to get a mapping from random_idx to sorted_idx.
  // E.g. if freq order is {3,5,1}, sorted_idxs will be {1,2,0}.
  if (chan_freqs.find(PSM_ALL_CHANS) == chan_freqs.end()) {
    snprintf(error, 255, "%s", "Can't call get_sorted_freq_order_idx when channels have different frequency lists.");
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return FE_ERR_DRIVER;
  }

  int num_freqs = chan_freqs[PSM_ALL_CHANS].size();

  // "sorted_pairs" will contain pairs of <frequency, random_order_idx>
  // Initially e.g. {<3,0>, <5,1>, <1,2>}
  std::vector<std::pair<float, int> > sorted_pairs(num_freqs);

  for (int i = 0; i < num_freqs; i++) {
    sorted_pairs[i] = std::make_pair(chan_freqs[PSM_ALL_CHANS][i], i);
  }

  // Now sort by frequency e.g. {<1,2>, <3,0>, <5,1>}
  std::sort(sorted_pairs.begin(), sorted_pairs.end(), comparator);

  // Now build list saying how to map from applied (random) freq order
  // to sorted frequency order e.g. {1,2,0}
  sorted_idxs.resize(sorted_pairs.size());

  for (unsigned int i = 0; i < sorted_pairs.size(); i++) {
    sorted_idxs[sorted_pairs[i].second] = i;
  }

  return SUCCESS;
}

void ScanDevicePSMFreq::compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) {
  if (!global_settings.enabled) {
    return;
  }

  if (freq_settings.scan_x) {
    compute_sweep(direction_type_x, nX);
  } else if (freq_settings.scan_y) {
    compute_sweep(direction_type_y, nY);
  }
}

void ScanDevicePSMFreq::compute_sweep(DirectionType direction_type, int n_steps) {
  if (freq_settings.use_parametric_fns) {
    if (debug) {
      scan_utils::ts_printf("Re-ordering parametric frequencies\n");
    }

    std::vector<int> x_indexes;

    for (unsigned int i = 0; i < parametric_precomputed_freqs.size(); i++) {
      x_indexes.push_back(i);
    }

    reorder_sweep(direction_type, x_indexes);

    parametric_sorted_x.clear();

    for (unsigned int i = 0; i < x_indexes.size(); i++) {
      parametric_sorted_x.push_back(parametric_precomputed_freqs[x_indexes[i]].x_Hz);
    }

    for (auto it = chan_freqs.begin(); it != chan_freqs.end(); it++) {
      it->second.clear();

      for (unsigned int i = 0; i < x_indexes.size(); i++) {
        it->second.push_back(parametric_precomputed_freqs[x_indexes[i]].chan_freq[it->first]);
      }
    }
  } else {
    if (debug) {
      scan_utils::ts_printf("Re-computing frequency sweep (%d steps)\n", n_steps);
    }

    for (auto it = chan_freqs.begin(); it != chan_freqs.end(); it++) {
      it->second = compute_sweep_float(direction_type, freq_settings.start_Hz, freq_settings.end_Hz, n_steps);
    }
  }
}

INT ScanDevicePSMFreq::fill_end_of_cycle_banks(char* pevent) {
  if (!global_settings.enabled) {
    return SUCCESS;
  }

  if (freq_settings.scan_x || freq_settings.scan_y || freq_settings.scan_within_ppg_loop) {
    // Format - if we're doing a normal scan, one value per enabled channel.
    // If doing an "in-ppg-loop" scan, the entire frequency list per channel.
    float* pdata;
    bk_create(pevent, "PSMF", TID_FLOAT, (void**) &pdata);

    for (auto it = chan_freqs.begin(); it != chan_freqs.end(); it++) {
      if (freq_settings.scan_x) {
        *pdata++ = it->second[most_recent_x_step];
      } else if (freq_settings.scan_y) {
        *pdata++ = it->second[most_recent_y_step];
      } else if (freq_settings.scan_within_ppg_loop) {
        for (auto freq_it = it->second.begin(); freq_it != it->second.end(); freq_it++) {
          *pdata++ = *freq_it;
        }
      }
    }

    bk_close(pevent, pdata);
  }

  return SUCCESS;
}

INT ScanDevicePSMFreq::load_iq_pairs(std::string chan_name) {
  INT status = SUCCESS;

  PSM_CHANNEL_SETTINGS& settings = channel_settings[chan_name];
  PSM_CHANNEL_COMPUTED& computed = channel_computed[chan_name];

  if (computed.num_iq < 1) {
    cm_msg(MERROR, __FUNCTION__, "Unexpected 0 IQ pairs for channel %s", chan_name.c_str());
    return FE_ERR_ODB;
  } else if (computed.num_iq == 1) {
    // Single IQ
    status = psm->load_single_iq(chan_name, computed.i[0], computed.q[0]);
  } else {
    status = psm->load_iq_pairs(chan_name, computed.num_iq, computed.i, computed.q);

    if (status != SUCCESS) {
      return status;
    }

    if (settings.modulation_idle_iq_at_end) {
      status = psm->load_idle_iq(chan_name, settings.modulation_idle_i, settings.modulation_idle_q);
    }
  }

  if (status != SUCCESS) {
    return status;
  }

  status = psm->set_buffer_factor(chan_name, computed.buf_factor);

  if (status != SUCCESS) {
    return status;
  }

  if (psm->supports_cic_rate()) {
    status = psm->set_cic_rate(chan_name, computed.cic_rate);
  }

  return status;
}


float ScanDevicePSMFreq::get_current_parametric_x_Hz() {
  return parametric_sorted_x[most_recent_x_step];
}

float ScanDevicePSMFreq::get_current_freq_Hz() {
  return chan_freqs.begin()->second[most_recent_x_step];
}
