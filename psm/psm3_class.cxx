#include "midas.h"
#include "psm/psm3_class.h"

#ifndef DUMMY_VME
extern "C" {
  #include "psm/trPSM3.h"
}
#endif

PSM3Base::PSM3Base() {
  using_cw_mode = false;

#ifdef DUMMY_VME
  chan_name_driver_idx["f0ch1"] = 1;
  chan_name_driver_idx["f0ch2"] = 2;
  chan_name_driver_idx["f0ch3"] = 3;
  chan_name_driver_idx["f0ch4"] = 4;
  chan_name_driver_idx["f1"] = 5;

  chan_name_offset_idx["f0ch1"] = 0;
  chan_name_offset_idx["f0ch2"] = 1;
  chan_name_offset_idx["f0ch3"] = 2;
  chan_name_offset_idx["f0ch4"] = 3;
  chan_name_offset_idx["f1"] = 4;

  chan_name_base_iq_addr["f0ch1"] = 0;
  chan_name_base_iq_addr["f0ch2"] = 0;
  chan_name_base_iq_addr["f0ch3"] = 0;
  chan_name_base_iq_addr["f0ch4"] = 0;
  chan_name_base_iq_addr["f1"] = 0;
#else
  chan_name_driver_idx["f0ch1"] = CH1;
  chan_name_driver_idx["f0ch2"] = CH2;
  chan_name_driver_idx["f0ch3"] = CH3;
  chan_name_driver_idx["f0ch4"] = CH4;
  chan_name_driver_idx["f1"] = F1;

  chan_name_offset_idx["f0ch1"] = 0;
  chan_name_offset_idx["f0ch2"] = 1;
  chan_name_offset_idx["f0ch3"] = 2;
  chan_name_offset_idx["f0ch4"] = 3;
  chan_name_offset_idx["f1"] = 4;

  chan_name_base_iq_addr["f0ch1"] = BASE_DM_F0_CH1_IQ;
  chan_name_base_iq_addr["f0ch2"] = BASE_DM_F0_CH2_IQ;
  chan_name_base_iq_addr["f0ch3"] = BASE_DM_F0_CH3_IQ;
  chan_name_base_iq_addr["f0ch4"] = BASE_DM_F0_CH4_IQ;
  chan_name_base_iq_addr["f1"] = BASE_DM_F1_IQ;
#endif
}

std::string PSM3Base::get_reference_channel_name() {
  return "f1";
}

std::vector<std::string> PSM3Base::get_channel_names() {
  std::vector<std::string> retval;

  for (auto it = chan_name_driver_idx.begin(); it != chan_name_driver_idx.end(); it++) {
    retval.push_back(it->first);
  }

  return retval;
}

std::vector<std::string> PSM3Base::get_channel_names_for_loading_freqs() {
  std::vector<std::string> retval;

  if (using_cw_mode) {
    // Individual freqs for all channels
    retval = get_channel_names();
  } else {
    // One normal freq and one reference freq
    retval.push_back(PSM_ALL_CHANS);
    retval.push_back(get_reference_channel_name());
  }

  return retval;
}

bool PSM3Base::is_reference_channel(std::string channel_name) {
  return channel_name == get_reference_channel_name();
}

int PSM3Base::channel_name_to_driver_index(std::string name) {
  if (chan_name_driver_idx.find(name) == chan_name_driver_idx.end()) {
    return -1;
  }

  return chan_name_driver_idx[name];
}

int PSM3Base::channel_name_to_offset_index(std::string name) {
  if (chan_name_offset_idx.find(name) == chan_name_offset_idx.end()) {
    return -1;
  }

  return chan_name_offset_idx[name];
}

int PSM3Base::get_max_buffer_factor() {
  return 4096;
}

bool PSM3Base::is_using_cw_mode() {
  return using_cw_mode;
}


#ifndef DUMMY_VME
PSM3::PSM3(MVME_INTERFACE* _myvme, DWORD _vme_base) : PSM3Base() {
  myvme = _myvme;
  vme_base = _vme_base;

  reset();
}

PSM3::~PSM3() {
}

void PSM3::reset() {
  psmVMEReset(myvme, vme_base);
}

INT PSM3::init(bool use_cw_mode) {
  reset_enabled_channel_list();

  using_cw_mode = use_cw_mode;

  INT status = SUCCESS;
  INT mode = use_cw_mode ? CW_OPERATING_MODE : BNMR_OPERATING_MODE;
  INT data = psmRegWrite8(myvme, vme_base,  MODULE_OPERATING_MODE, mode);

  if ((data & 3) != mode) {
    cm_msg(MERROR, __FUNCTION__, "Error setting PSM to operating mode %d. Read back mode %d.", mode, (data & 3));
    return FE_ERR_DRIVER;
  }

  psmClearRFpowerTrip(myvme, vme_base);
  psmWriteGateControl(myvme, vme_base, ALLCH, FP_GATE_DISABLED); // disable gates on all channels
  psmReadGateControl(myvme, vme_base, ALLCH); // display data
  psmSetScaleFactor(myvme, vme_base, ALLCH, 0);  // 0 scale factor

  status = disable();

  if (status != SUCCESS) {
    return status;
  }

  return SUCCESS;
}

INT PSM3::disable() {
  psmWriteGateControl(myvme, vme_base, ALLCH, FP_GATE_DISABLED);  // disable gates on all channels
  psmSetScaleFactor(myvme, vme_base, ALLCH, 0);  // 0 scale factor
  return SUCCESS;
}

INT PSM3::set_channel_config(std::string chan_name, INT gate_control, INT scale_factor, BOOL end_iq_sweep_jump_to_idle) {
  note_channel_enabled(chan_name);

  switch (gate_control) {
    case FP_GATE_DISABLED:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Front Panel Gate is Disabled. Use only for testing");
      break;
    case GATE_NORMAL_MODE:
      break;
    case GATE_PULSE_INVERTED:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Front Panel Gate Pulse Inverted. Use only for testing");
      break;
    case INTERNAL_GATE:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Front Panel Gate Pulse Ignored and Gates are ALWAYS ON. Use only for testing");
      break;
    default:
      cm_msg(MERROR, __FUNCTION__, "WARNING: Illegal gate parameter for channel %s (%d)", chan_name, gate_control);
      return FE_ERR_ODB;
  }

  if (scale_factor < 0 || scale_factor > 255) {
    cm_msg(MERROR, __FUNCTION__, "Invalid scale factor %d", scale_factor);
    return FE_ERR_ODB;
  }

  int selected_channel = channel_name_to_driver_index(chan_name);

  if (selected_channel == -1) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  psmWriteGateControl(myvme, vme_base, selected_channel, gate_control);
  psmWriteEndSweepControl(myvme, vme_base, FALSE, selected_channel, end_iq_sweep_jump_to_idle);

  INT read_scale_factor = 0;

  if (using_cw_mode) {
    if (is_reference_channel(chan_name)) {
      read_scale_factor = psmSetScaleFactor(myvme, vme_base, selected_channel, scale_factor);
    } else {
      INT offset = FCW_SCALE_FACTOR + channel_name_to_offset_index(chan_name);
      read_scale_factor = psmRegWrite8(myvme, vme_base, offset, scale_factor);
    }
  } else {
    read_scale_factor = psmSetScaleFactor(myvme, vme_base, selected_channel, scale_factor);
  }

  if (read_scale_factor != scale_factor) {
    cm_msg(MERROR, __FUNCTION__, "Failed to set correct scale factor for chan %s. Set %d, read %d. CW mode=%d", chan_name.c_str(), scale_factor, read_scale_factor, using_cw_mode);
    return FE_ERR_DRIVER;
  }

  return SUCCESS;
}

INT PSM3::set_buffer_factor(std::string chan_name, INT buf_factor) {
  int selected_channel = channel_name_to_driver_index(chan_name);

  if (selected_channel < 0) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  psmWriteBufFactor(myvme, vme_base, selected_channel, buf_factor);
  return SUCCESS;
}

INT PSM3::set_rf_power_trip_threshold(float thresh) {
  psmWriteRFpowerTripThresh(myvme, vme_base, thresh);
  return SUCCESS;
}

bool PSM3::is_rf_tripped() {
  return psmReadRFpowerTrip(myvme, vme_base) & 1;
}

void PSM3::reset_rf_trip() {
  psmClearRFpowerTrip(myvme, vme_base);
}

INT PSM3::load_iq_pairs(std::string channel_name, int num_iq, int* i_arr, int* q_arr) {
  int selected_channel = channel_name_to_driver_index(channel_name);

  if (selected_channel < 0) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", channel_name.c_str());
    return FE_ERR_DRIVER;
  }

  // Load the actual I, Q pairs
  int base_iq_addr = chan_name_base_iq_addr[channel_name];

  for (int n = 0; n < num_iq; n++) {
    // 2's complement and mask to 10 bits
    INT i_load = (~i_arr[n] + 1) & I_Q_DATA_MASK;
    INT q_load = (~q_arr[n] + 1) & I_Q_DATA_MASK;
    int offset = n * 4;

    DWORD data = psmRegWriteIQDM(myvme, vme_base, base_iq_addr, offset, i_load, q_load);

    INT i_read = (data >> 16) & I_Q_DATA_MASK;
    INT q_read = data & I_Q_DATA_MASK;

    if (i_load != i_read || q_load != q_read) {
      cm_msg(MERROR, __FUNCTION__, "Incorrect i,q readback for position %d. Set (%d,%d), read (%d,%d)", n, i_load, q_load, i_read, q_read);
      return FE_ERR_DRIVER;
    }
  }

  // Set the IQ length register to Zero
  INT status = psmWriteIQLen(myvme, vme_base, selected_channel, num_iq);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Error writing IQ length = %u for Idle I,Q pair for channel %s (index=%d)", num_iq, channel_name.c_str(), selected_channel);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT PSM3::load_idle_iq(std::string chan_name, int i, int q) {
  int selected_channel = channel_name_to_driver_index(chan_name);

  if (selected_channel < 0) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  // Load the I,Q pair
  INT status = psmLoadIdleIQ(myvme, vme_base, selected_channel, i, q);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Error writing idle I,Q pair as %d,%d for channel %s (selected_channel %d)", i, q, chan_name.c_str(), selected_channel);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT PSM3::load_single_iq(std::string chan_name, int i, int q) {
  int selected_channel = channel_name_to_driver_index(chan_name);

  if (selected_channel < 0) {
    cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", chan_name.c_str());
    return FE_ERR_DRIVER;
  }

  // Load the IQ
  INT status = load_idle_iq(chan_name, i, q);

  if (status != SUCCESS) {
    return status;
  }

  // Set the IQ length register to Zero
  status = psmWriteIQLen(myvme, vme_base, selected_channel, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Error writing IQ length = 0 for Idle I,Q pair for channel %s (index=%d)", chan_name.c_str(), selected_channel);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT PSM3::load_frequency_list(std::string channel_name, std::vector<float> freqs_Hz) {
  if (is_reference_channel(channel_name)) {
    cm_msg(MERROR, __FUNCTION__, "Can't load a list of reference frequencies");
    return FE_ERR_DRIVER;
  }

  int n_freq = freqs_Hz.size();

  if (n_freq == 0) {
    cm_msg(MERROR, __FUNCTION__, "Can't load a list of frequencies of length 0");
    return FE_ERR_DRIVER;
  }

  int freq_arr[n_freq];

  for (int i = 0; i < n_freq; i++) {
    freq_arr[i] = freqs_Hz[i];
  }

  DWORD first_freq = 0;

  // Load the actual frequencies
  INT status = psmLoadFreqPtr(myvme, vme_base, freq_arr, n_freq, &first_freq);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to load frequency list");
    return FE_ERR_DRIVER;
  }

  // Set the number of frequencies in the list.
  reset_freq_sweep_addr();

  ss_sleep(50);

  psmRegWrite16(myvme, vme_base, FREQ_SWEEP_LENGTH, n_freq);
  psmFreqSweepAddrPreset(myvme, vme_base);

  ss_sleep(50);

  INT value = (psmRegRead16(myvme, vme_base, FREQ_SWEEP_ADDRS)) & ELEVEN_BIT_MASK;

  if (value != 0) {
    cm_msg(MERROR, __FUNCTION__, "After preset, expect frequency sweep address to be 0 not 0x%x or %d", value,value);
    return  FE_ERR_HW;
  }

  return SUCCESS;
}

INT PSM3::load_single_frequency(std::string channel_name, float freq_Hz, bool arm) {
  INT status = SUCCESS;

  if (is_reference_channel(channel_name)) {
    psmWrite_reference_freq_Hz(myvme, vme_base, F1_TUNING_FREQ, freq_Hz);
  } else if (using_cw_mode) {
    int offset_idx = channel_name_to_driver_index(channel_name);

    if (offset_idx == -1) {
      cm_msg(MERROR, __FUNCTION__, "Unknown channel %s", channel_name.c_str());
      return FE_ERR_DRIVER;
    }

    INT reg_offset = FCW_TUNING_FREQ + (4 * offset_idx);
    psmWrite_reference_freq_Hz(myvme, vme_base, reg_offset, freq_Hz);
  } else {
    // In BNMR mode, only 1 reg for idle freq, not 1 per chan!
    DWORD freq_hex = get_hex(freq_Hz);
    psmRegWriteFreqDM(myvme, vme_base, IDLE_FREQ_OFFSET, freq_hex);
  }

  if (arm) {
    // Send internal strobe
    reset_freq_sweep_addr();
    psmFreqSweepStrobe(myvme, vme_base);
  }

  return SUCCESS;
}

INT PSM3::reset_freq_sweep_addr() {
  psmFreqSweepAddrReset(myvme, vme_base);
  return SUCCESS;
}
#endif

