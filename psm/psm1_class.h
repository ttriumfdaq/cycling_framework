#ifndef PSM1_CLASS_H
#define PSM1_CLASS_H

#include "psm/psm_class.h"
#include <map>
#include <string>

// Object-oriented interfce for the PSM version 1.
// See the PSM base class for most documentation.
//
// Specific features of this version:
// * Channel names are 1f/3f/5f/fREF
// * Does not support CW mode
// * Supports a CIC rate
class PSM1 : public PSM {
  public:
    PSM1(MVME_INTERFACE* _myvme, DWORD _vme_base);
    virtual ~PSM1();

    int get_version() {
      return 1;
    }

    int get_max_buffer_factor();
    void reset();
    INT init(bool use_cw_mode);
    bool is_using_cw_mode();
    INT disable();
    INT set_rf_power_trip_threshold(float thresh);
    bool is_rf_tripped();
    void reset_rf_trip();
    INT load_iq_pairs(std::string channel_name, int num_iq, int* i_arr, int* q_arr);
    INT load_idle_iq(std::string channel_name, int i, int q);
    INT load_single_iq(std::string channel_name, int i, int q);
    INT load_frequency_list(std::string channel_name, std::vector<float> freqs_Hz);
    INT load_single_frequency(std::string channel_name, float freq_Hz, bool arm=false);
    INT reset_freq_sweep_addr();
    std::vector<std::string> get_channel_names();
    std::vector<std::string> get_channel_names_for_loading_freqs();
    std::string get_reference_channel_name();
    bool is_reference_channel(std::string channel_name);
    INT set_channel_config(std::string chan_name, INT gate_control, INT scale_factor, BOOL end_iq_sweep_jump_to_idle);
    INT set_buffer_factor(std::string chan_name, INT buf_factor);
    bool supports_cic_rate();
    INT set_cic_rate(std::string chan_name, INT Ncic);

  private:
    bool channel_exists(std::string channel_name);

    MVME_INTERFACE* myvme;
    DWORD vme_base;

    std::vector<std::string> chan_names;
    std::map<std::string, int> chan_name_control_addr;
    std::map<std::string, int> chan_name_base_iq_addr;

};

#endif
