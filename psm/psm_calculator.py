import midas
import midas.frontend
import collections
import json
import ctypes
import traceback
import os
import os.path
import math
import datetime

# Logger provided by FE framework will be configured
# based on whether the user specifies -d flag on the
# command line.
logger = midas.frontend.logger

class PSMCalculator:
    """
    Helper functions for running PSM calculations.
    """
    def __init__(self, client):
        self.client = client
        self.psm_settings_dir = "/Scanning/PSM/Settings"
        self.psm_computed_dir = "/Scanning/PSM/Computed"
        self.max_niq_odb = 4096
        
        self.modulation_modes = {
            "SingleTone": 0,
            "LnSech" : 1,
            "Hermite": 2,
            "Wurst": 3
        }
        
    def get_global_setting(self, key=None):
        if key is None:
            return self.client.odb_get("%s/%s" % (self.psm_settings_dir, "Global"))
        else:
            return self.client.odb_get("%s/%s/%s" % (self.psm_settings_dir, "Global", key))
    
    def get_frequency_settings(self):
            return self.client.odb_get("%s/%s" % (self.psm_settings_dir, "Frequency"))
        
    def get_channel_list(self):
        keys = self.client.odb_get(self.psm_settings_dir, recurse_dir=False)
        return [k for k in keys if k not in ["Global", "Frequency"]]
        
    def get_channel_settings(self, chan):
        return self.client.odb_get("%s/%s" % (self.psm_settings_dir, chan))
    
    def set_channel_computed(self, chan, computed):
        self.client.odb_set("%s/%s" % (self.psm_computed_dir, chan), computed)
        
    def compute_iqs(self, with_image=False, image_base_path=None):
        psm_enabled = self.get_global_setting("Enabled")
        psm_version = self.get_global_setting("PSM version")
        max_buf_factor = self.get_global_setting("Max buffer factor")
        use_wurst_params = self.get_global_setting("Use wurst params")
        wurst_bandwidth = None
        Tpc_computed = None
        any_enabled_gate_normal = False
        
        if psm_version not in [1, 3]:
            raise NotImplementedError("PSM version %d not implemented" % psm_version)
            
        if psm_version == 1 and use_wurst_params:
            raise ValueError("PSM version 1 does not support CW/Wurst mode")
            
        for chan in self.get_channel_list():
            t_start = datetime.datetime.now()
            
            chan_settings = self.get_channel_settings(chan)
            i_vec = [0] * self.max_niq_odb
            q_vec = [0] * self.max_niq_odb
            Ncic = 0 # Only relevant for PSM1
            Niq = 0
            buf_factor = 1
                
            if psm_enabled and chan_settings["Enabled"]:
                # Sanity check Wurst bandwidth - must be identical for all Wurst channels
                if chan_settings["Modulation mode"] == self.modulation_modes["Wurst"]:
                    if wurst_bandwidth is None:
                        wurst_bandwidth = chan_settings["Modulation bandwidth Hz"]
                    elif wurst_bandwidth != chan_settings["Modulation bandwidth Hz"]:
                        raise ValueError("Bandwidth must be the same for all enabled channels in Wurst mode")
                
                if chan_settings["Amplitude"] > 0 and chan_settings["Gate control"] == 1:
                    any_enabled_gate_normal = True
            
                this_Tpc_computed = 0

                if chan_settings["Modulation mode"] == self.modulation_modes["SingleTone"]:
                    i_vec[0] = chan_settings["Modulation idle i"]
                    q_vec[0] = chan_settings["Modulation idle q"]
                    Niq = 1
                    Nc1 = 1
                else:
                    # Using quadrature modulation
                    # If setting constant values, check they  are reasonable
                    constant_i_is_zero = False;
            
                    if chan_settings["Modulation use constant i"]:
                        if chan_settings["Modulation constant i"] == 0:
                            constant_i_is_zero = True;
                    
                            if chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
                                # Hermite: all Q values are zero
                                raise ValueError("Modulating with constant I=0 will result in no output for Hermite math.since Hermite Q values are also zero", is_error=True)
            
                        logger.info("Using constant i: %d" % chan_settings["Modulation constant i"])
                    
                    if chan_settings["Modulation use constant q"]:
                        if chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
                            if chan_settings["Modulation constant q"] != 0:
                                # Hermite; all Q values are zero
                                self.client.msg("Modulating with constant Q=%d has no effect math.since Q=0 always for Hermite", chan_settings.modulation_constant_q)
                        else:
                            if chan_settings["Modulation constant q"] == 0 and constant_i_is_zero:
                                raise ValueError("Modulating with constant I,Q pair=(0,0) will result in no output")
                    
                        logger.info("Using constant q: %d" % chan_settings["Modulation constant q"])

                    # Now do the actual calculations
                    if psm_version == 3:
                        (i_vec, q_vec, Niq, buf_factor, this_Tpc_computed) = self.compute_iq_psm3(chan, chan_settings, max_buf_factor, use_wurst_params)
                    elif psm_version == 1:
                        (i_vec, q_vec, Niq, buf_factor, Ncic) = self.compute_iq_psm1(chan, chan_settings, max_buf_factor)
    
                    if use_wurst_params:
                        if Tpc_computed is None:
                            Tpc_computed = this_Tpc_computed
                        elif Tpc_computed != this_Tpc_computed:
                            raise ValueError("Different Tpc times computed for different channels (%s vs %s)" % (Tpc_computed, this_Tpc_computed))
                
            if buf_factor > max_buf_factor:
                raise RuntimeError("Calculated buffer factor for channel %s is %s: greater than max %s", chan, buf_factor, max_buf_factor)
            
            computed = collections.OrderedDict([("Buffer factor", buf_factor),
                                                ("Num iq", Niq),
                                                ("CIC rate", Ncic),
                                                ("i", i_vec),
                                                ("q", q_vec)])
            
            self.set_channel_computed(chan, computed)
            
            t_end = datetime.datetime.now()
            print("IQ calculation for channel %s took %ss" % (chan, (t_end - t_start).total_seconds()))
            
            if with_image == True or (isinstance(with_image, list) and chan in with_image):
                if image_base_path is None:
                    raise ValueError("Specify image_base_path as the directory to save in")
                
                if chan_settings["Modulation mode"] == self.modulation_modes["SingleTone"]:
                    idle_at_end = False
                else:
                    idle_at_end = chan_settings["Modulation idle iq at end"]
                    
                idle_i = chan_settings["Modulation idle i"]
                idle_q = chan_settings["Modulation idle q"]
                image_filepath = os.path.join(image_base_path, "%s.png" % chan)
                self.plot_iqs(chan, psm_enabled and chan_settings["Enabled"], Niq, i_vec, q_vec, idle_at_end, idle_i, idle_q, image_filepath)

                t_img = datetime.datetime.now()
                print("IQ images for channel %s took %ss" % (chan, (t_img - t_end).total_seconds()))
        
        if psm_enabled and not use_wurst_params and not any_enabled_gate_normal:
            raise ValueError("At least one PSM channel must be enabled and with normal gate control")
                
        return Tpc_computed
    
    def compute_iq_psm1(self, chan_name, chan_settings, max_buf_factor):
        if chan_name not in ["1f", "fRef"]:
            raise ValueError("Channel/profile '%s' not supported for loading IQ pairs in PSM version 1" % chan_name)

        if chan_settings["Modulation mode"] == self.modulation_modes["LnSech"]:
            alpha = 5
            A = 0.1
        elif chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
            alpha = 2.2
            A = 0.39714
        else:
            raise ValueError("Invalid modulation mode %d for channel %s" % (chan_settings["Modulation mode"], chan_name))
            
        if max_buf_factor < 1 or max_buf_factor > 32:
            raise ValueError("Illegal max_buf_factor %d (outside range 1-32)" % max_buf_factor)
  
        d_w_max = alpha * 1e7 / (A * 512.0) # rad/s
        d_w_min = d_w_max / (128 * max_buf_factor) # rad/s
        d_nu_min = d_w_min / (2*math.pi) # Hz
        d_nu_max = d_w_max / (2*math.pi) # Hz
        
        d_nu = chan_settings["Modulation bandwidth Hz"]
        
        if d_nu < d_nu_min or d_nu > d_nu_max:
            raise ValueError("Specified bandwidth %lf Hz is outside allowed range %lf-%lf Hz" % (d_nu, d_nu_min, d_nu_max))

        d_w_req = 2 * math.pi * d_nu 
        d_const = alpha * 2e7 / A # this is 10**9 for A=0.1 and alpha=5

        Ntiqtemp = math.floor(d_const / d_w_req) # nearest smallest integer

        if Ntiqtemp < 1024 or Ntiqtemp >= 129024 * max_buf_factor:
            raise ValueError("Calculated Ntiqtemp %d is outside range %d-%d" % (Ntiqtemp, 1024, 129024 * max_buf_factor))
   
        if Ntiqtemp < 4096:
            Ncic = 2
        elif Ntiqtemp < (4096<<1):
            Ncic = 4
        elif Ntiqtemp < (4096<<2):
            Ncic = 8
        elif Ntiqtemp < (4096<<3):
            Ncic = 16
        elif Ntiqtemp < (4096<<4):
            Ncic = 32
        else:
            Ncic = 63
        
        if Ntiqtemp < 129024:
            Nc = 1
        elif Ntiqtemp < (129024<<1):
            Nc = 2
        elif Ntiqtemp < (129024<<2):
            Nc = 4
        elif Ntiqtemp < (129024<<3):
            Nc = 8
        elif Ntiqtemp < (129024<<4):
            Nc = 16
        elif Ntiqtemp < (129024<<5):
            Nc = 32
        else:
            raise ValueError("Ntiqtemp too large (%d > %d)" % (Ntiqtemp, (129024<<5)))

        Niq = math.ceil(Ntiqtemp / (Ncic * Nc))
        Ntiq = Niq * Nc * Ncic

        if Niq < 512 or Niq > 2048:
            raise ValueError("Niq is %d, outside range 512-2048" % Niq)
        
        if Ntiq > 129024 * max_buf_factor:
            raise ValueError("Ntiq is too great: %d > %d" % (Niq, 129024 * max_buf_factor))
          
        return (i_vec, q_vec, Niq, Nc, Ncic)      

    def compute_iq_psm3(self, chan_name, chan_settings, max_buf_factor, use_wurst_params):
        Tpc = None
        d_nu_khz = chan_settings["Modulation bandwidth Hz"] / 1000
        u = 5
        
        if use_wurst_params:
            Niq_min = 1024
            Niq_max = 2048
            
            freq = self.get_frequency_settings()
            global_settings = self.get_global_setting()
            
            f0_khz = freq["Idle Hz"] / 1000
            f1_khz = global_settings["Wurst precession freq Hz"] / 1000
            q0 = global_settings["Wurst q0"]
            Tp = global_settings["Wurst Tp ms"]
            
            if chan_settings["Modulation mode"] == self.modulation_modes["LnSech"]:
                if f1_khz <= 0:
                    raise ValueError("Precession frequency must be > 0")
                
                b = 5.3;
                Tp_min = (q0 * b / (2 * math.pi)) * (abs(d_nu_khz) / (f1_khz * f1_khz)) # ms
                d_nu_min = 0.0512 * 2 * math.pi * (f1_khz * f1_khz) / (q0 * b)     # kHz minimum bandwidth
                d_nu_max = 419.4304 * 2 * math.pi * (f1_khz * f1_khz) / (q0 * b)   # kHz maximum bandwidth
            elif chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
                b = 2.5;
                Tp_min = 4.69 / abs(d_nu_khz)
                d_nu_min = 0.01118 # kHz  minimum bandwidth
                d_nu_max = 91.6    # kHz  maximum bandwidth
            elif chan_settings["Modulation mode"] == self.modulation_modes["Wurst"]:
                if f1_khz <= 0:
                    raise ValueError("Precession frequency must be > 0")
                
                b = 1;
                # assuming f1= 0.63 kHz; larger values will give higher max, smaller values a lower min
                
                Tp_min = (q0 * b / (2 * math.pi)) * (abs(d_nu_khz) / (f1_khz * f1_khz)) # ms
                d_nu_min = 0.0512 * 2 * math.pi * (f1_khz * f1_khz) / q0     # kHz minimum bandwidth
                d_nu_max = 419.4304 * 2 * math.pi * (f1_khz * f1_khz) / q0   # kHz maximum bandwidth
            else:
                raise ValueError("Invalid modulation mode %d for channel %s" % (chan_settings["Modulation mode"], chan_name))
            
            factor = Tp / (100 * 1e-6 * 2048)
            Nc1 = math.ceil(factor)
            factor = (Tp - 1e-6) /(100 * 1e-6 * Nc1) # subtract 1ns to remove rounding errors was 50
            Niq = math.ceil(factor) # next highest integer
            
            Tpc = 100 * 1e-6 * Nc1 * Niq # ms was 50
            MaxTpc = (4096 * 100 * 1e-6 * 2048) #=  419.4304 ms   was 50
            MinTpc = (1 * 100 * 1024 * 1e-6 ) # = 0.5012 ms was 50
            
            #logger.debug("build_iq_table_psm3_2w: calculated num IQ pairs=%d; pulsewidth Tpc = %f ms\n" % (Niq, Tpc))
        else:
            # Normal (not-2w) mode
            Niq_max = 4096
            Tp_min = 0
            
            if chan_settings["Modulation mode"] == self.modulation_modes["LnSech"]:
                b = 5.2983;
                factor =  b * u / math.pi # 8.4325
                d_nu_min = 40 / 1e3 # kHz
                d_nu_max = 164688 / 1e3 # kHz
                Niq_min = 1024
            elif chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
                b = 2.5;
                factor =  4 * math.sqrt(-math.log(.5)) * b / math.pi # 2.64
                d_nu_min = 12.6  /1e3 # kHz
                d_nu_max = 51562  /1e3# kHz
                Niq_min = 256;
            else:
                raise ValueError("Invalid modulation mode %d for channel %s" % (chan_settings["Modulation mode"], chan_name))
            
            Tp = factor/d_nu_khz   # Tp in ms
            
            f1 =  Tp / (50 * 1e-6 * 2048) # delay is in increments of 50ns
            Nc1 = math.ceil(f1) # next highest integer
            f2 = Tp / (50 * 1e-6 * Nc1);
            Niq = math.ceil(f2) # next highest integer
            
            Tpc = 50 * math.pow(10,-6) * Nc1 * Niq # ms
            MaxTpc = 2048 * 50 * 1e-6 * 2048 # in ms
            MinTpc = 1 * 50 * 1024 * 1e-6 # in ms
        
        if Tp < Tp_min:
            raise ValueError("Specified Tp %lf ms is less than minimum Tp %lf ms" % (Tp, Tp_min))
        
        if abs(d_nu_khz) < d_nu_min or abs(d_nu_khz) > d_nu_max:
            raise ValueError("Specified bandwidth %lf kHz is outside allowed range %lf-%lf kHz" % (d_nu_khz, d_nu_min, d_nu_max))
        
        if Tpc < MinTpc or Tpc >= MaxTpc:
            raise ValueError("Calculated Tpc %lf ms is outside allowed range %lf-%lf ms" % (Tpc, MinTpc, MaxTpc))
        
        if Niq < Niq_min or Niq > Niq_max:
            raise ValueError("Calculated Niq %d is outside allowed range %d-%d" % (Niq, Niq_min, Niq_max))
        
        buf_factor = Nc1
        i_vec = [0] * self.max_niq_odb
        q_vec = [0] * self.max_niq_odb
        
        for n in range(1, Niq + 1):
            if use_wurst_params:
                phi_radians = chan_settings["Modulation wurst phase corr deg"] * 2 * math.pi / 360
                
                if chan_settings["Modulation mode"] == self.modulation_modes["LnSech"]:
                    x = b*2*(2*n-Niq-1)/(2*(Niq-1))
                    sech_x = 1/math.cosh(x)
                    di = 511 * (1.01*sech_x-0.01) * math.cos(d_nu_khz*2*(math.pi/(4*5.3))*Tpc*math.log(sech_x) + phi_radians)
                    dq = 511 * (1.01*sech_x-0.01) * math.sin(d_nu_khz*2*(math.pi/(4*5.3))*Tpc*math.log(sech_x) + phi_radians)
                elif chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
                    x = b*2*(2*n-Niq-1)/(2*(Niq-1))
                    di = 511 * (0.99 * (1-math.pow(x,2))+0.01) * math.exp(-(math.pow(x,2)))
                    dq = 0
                elif chan_settings["Modulation mode"] == self.modulation_modes["Wurst"]:
                    wurst_n = global_settings["Wurst N freq scans (40 or 80)"]
                    x = b*2*(2*n-Niq-1)/(2*(Niq-1))
                    di = 511 * (1-math.pow(abs(math.sin(math.pi*x/2)),wurst_n))*math.cos(d_nu_khz*2*(math.pi/8)*Tpc*x*x + phi_radians)
                    dq = 511 * (1-math.pow(abs(math.sin(math.pi*x/2)),wurst_n))*math.sin(d_nu_khz*2*(math.pi/8)*Tpc*x*x + phi_radians)
            else:
                if chan_settings["Modulation mode"] == self.modulation_modes["LnSech"]:
                    x = b*2*(2*n-Niq-1)/(2*(Niq-1))
                    sech_x = 1/math.cosh(x)
                    di = 511*sech_x * math.cos(u*math.log(sech_x))
                    dq = 511*sech_x * math.sin(u*math.log(sech_x))
                elif chan_settings["Modulation mode"] == self.modulation_modes["Hermite"]:
                    x = b*2*(2*n-Niq-1)/(2*(Niq-1))
                    di = 511 * (1-math.pow(x,2)) * math.exp(-(math.pow(x,2)))
                    dq = 0
        
            if chan_settings["Modulation use constant i"]:
                i_vec[n-1] = chan_settings["Modulation constant i"]
            else:
                i_vec[n-1] = round(di) # closest integer
            
                # Check amplitude value when N=Niq/2
                if n == Niq/2:
                  k = math.floor(math.sqrt((di*di) + (dq*dq)))
                  
                  if k != 510 and k != 511:
                    raise RuntimeError("Unexpected amplitude for n=Niq/2=%d is %d; expect 510 or 511" % (n, k))
        
            if chan_settings["Modulation use constant q"]:
                q_vec[n-1] = chan_settings["Modulation constant q"]
            else:
                q_vec[n-1] = round(dq) # closest integer
        
            if use_wurst_params and (n == 1 or n == Niq):
                if chan_settings["Modulation use constant i"] or chan_settings["Modulation use constant q"]:
                    logger.debug("Not checking I,Q values at begin/end of file because constant I or Q is set")
                else:
                    # Add a check to make sure I=Q=0 at beginning and end of file
                    k = math.floor((di*di) + (dq*dq));
        
                    if k != 0:
                        raise RuntimeError("Unexpected amplitude for n=%d is %d; math.expect 0" % (n, k))
        
        return (i_vec, q_vec, Niq, Nc1, Tpc)

    def plot_iqs(self, chan_name, chan_enabled, Niq, i_arr, q_arr, idle_at_end, idle_i, idle_q, image_filepath):
        import matplotlib
        
        # Try to use a non-GUI backend to speed up matplotlib
        non_gui = matplotlib.rcsetup.non_interactive_bk
        
        if 'agg' in non_gui:
            matplotlib.use('agg')
        elif len(non_gui):
            matplotlib.use(non_gui[0])
            
        import matplotlib.pyplot as plt
        import matplotlib.cm
        import numpy
        
        px_x, px_y = 800, 800
        
        fig = plt.figure()
        dpi = plt.gcf().get_dpi()
        plt.gcf().set_size_inches(px_x/dpi, px_y/dpi)
        
        if not chan_enabled:
            title = "Channel %s disabled\nGenerated at %s" % (chan_name, datetime.datetime.now().replace(microsecond=0))
            fig.suptitle(title)
        elif Niq <= 1:
            title = "%s IQ pairs for channel %s\nGenerated at %s" % (Niq, chan_name, datetime.datetime.now().replace(microsecond=0))
            
            if Niq == 0:
                title += "\n\n\nZERO I,Q PAIRS!\n\n\n"
            else:
                title += "\n\n\nI=%s, Q=%s\n\n\n" % (i_arr[0], q_arr[0])
        
            fig.suptitle(title)
            
            gs = fig.add_gridspec(1, 1)
            ax0 = fig.add_subplot(gs[0, 0])
            
            plt.subplots_adjust(left=0.15, right=0.9, top=0.85, bottom=0.1)
            
            ax0.scatter(i_arr[:1], q_arr[:1], s=15, color='black')
            ax0.set_xlabel('i')
            ax0.set_ylabel('q')
            ax0.set_xlim(-520, 520)
            ax0.set_ylim(-520, 520)
        else:
            gs = fig.add_gridspec(4, 2)
            ax0 = fig.add_subplot(gs[:2, 0])
            ax1 = fig.add_subplot(gs[:2, 1])
            ax2 = fig.add_subplot(gs[2, :])
            ax3 = fig.add_subplot(gs[3, :])
                    
            plt.subplots_adjust(left=0.1, right=0.95, top=0.88, bottom=0.05, hspace=1, wspace=0.25)
    
            x = i_arr[:Niq]
            y = q_arr[:Niq]
            t = list(range(Niq))
            colors = list(matplotlib.cm.rainbow(numpy.linspace(0, 1, Niq)))
            
            if idle_at_end:
                x.append(idle_i)
                y.append(idle_q)
                t.append(Niq)
                colors.append('black')
                
            x_first = x[:int(len(x)/2)]
            y_first = y[:int(len(y)/2)]
            colors_first = colors[:int(len(colors)/2)]
            x_second = x[int(len(x)/2):]
            y_second = y[int(len(y)/2):]
            colors_second = colors[int(len(colors)/2):]
            
            ax0.set_title("First half of I,Q pairs")
            ax0.scatter(x_first, y_first, s=1.3, color=colors_first)
            ax0.set_xlabel('i')
            ax0.set_ylabel('q')
            ax0.set_xlim(-520, 520)
            ax0.set_ylim(-520, 520)
            
            ax1.set_title("Second half of I,Q pairs")
            ax1.scatter(x_second, y_second, s=1.3, color=colors_second)
            ax1.set_xlabel('i')
            ax1.set_ylabel('q')
            ax1.set_xlim(-520, 520)
            ax1.set_ylim(-520, 520)
            
            ax2.set_title("I (t)")
            ax2.scatter(t, x, s=1.3, color=colors)
            ax2.set_xlabel('t')
            ax2.set_ylabel('i')
            ax2.set_ylim(-520, 520)
            
            ax3.set_title("Q (t)")
            ax3.scatter(t, y, s=1.3, color=colors)
            ax3.set_xlabel('t')
            ax3.set_ylabel('q')
            ax3.set_ylim(-520, 520)
            
            if idle_at_end:
                idle_exp = "Jump to idle (black) at end"
            else:
                idle_exp = "No extra idle pair"
        
            fig.suptitle("%s IQ pairs for channel %s; %s\nGenerated at %s" % (Niq, chan_name, idle_exp, datetime.datetime.now().replace(microsecond=0)))
        
        fig.savefig(image_filepath)
        plt.close(fig)
