#include "scan_device_camp.h"
#include "midas.h"
#include "camp_class.h"
#include "core/scannable_device.h"
#include <cmath>
#include <iostream>
#include <sstream>
#include <cstring>
#include "core/scan_utils.h"

#define CAMP_DEVICE_DEFS_PATH "/Scanning/Camp/Definitions"
#define CAMP_SETTINGS_PATH "/Scanning/Camp/Settings"
#define CAMP_STATUS_PATH "/Scanning/Camp/Status"

ScanDeviceCamp::ScanDeviceCamp(HNDLE _hDB) : ScannableDevice(_hDB) {
}

INT ScanDeviceCamp::check_records() {
  char settings_path[256], status_path[256], devices_path[256];
  sprintf(settings_path, CAMP_SETTINGS_PATH);
  sprintf(status_path, CAMP_STATUS_PATH);
  sprintf(devices_path, CAMP_DEVICE_DEFS_PATH);

  INT status;

  status = db_check_record(hDB, 0, settings_path, CAMP_SCAN_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", settings_path);
    return status;
  }

  status = db_check_record(hDB, 0, status_path, CAMP_SCAN_STATUS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", status_path);
    return status;
  }

  status = db_check_record(hDB, 0, devices_path, CAMP_DEVICES_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", devices_path);
    return status;
  }

  return SUCCESS;
}

INT ScanDeviceCamp::open_records() {
  char settings_path[256], status_path[256], devices_path[256];
  sprintf(settings_path, CAMP_SETTINGS_PATH);
  sprintf(status_path, CAMP_STATUS_PATH);
  sprintf(devices_path, CAMP_DEVICE_DEFS_PATH);

  HNDLE h_settings, h_status, h_devs;
  INT size_settings = sizeof(user_settings);
  INT size_status = sizeof(scan_status);
  INT size_defs = sizeof(camp_devices);
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);
  db_find_key(hDB, 0, devices_path, &h_devs);

  status = db_get_record(hDB, h_settings, &user_settings, &size_settings, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", settings_path);
    return status;
  }

  status = db_get_record(hDB, h_status, &scan_status, &size_status, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", status_path);
    return status;
  }

  status = db_get_record(hDB, h_devs, &camp_devices, &size_defs, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", devices_path);
    return status;
  }

  status = db_open_record(hDB, h_settings, &user_settings, size_settings, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open CAMP settings record");
    return status;
  }

  status = db_open_record(hDB, h_status, &scan_status, size_status, MODE_WRITE, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open CAMP status record");
    return status;
  }

  status = db_open_record(hDB, h_devs, &camp_devices, size_defs, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open CAMP defs record");
    return status;
  }

  return status;
}

INT ScanDeviceCamp::close_records() {
  char settings_path[256], status_path[256], devices_path[256];
  sprintf(settings_path, CAMP_SETTINGS_PATH);
  sprintf(status_path, CAMP_STATUS_PATH);
  sprintf(devices_path, CAMP_DEVICE_DEFS_PATH);

  HNDLE h_settings, h_status, h_devs;
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);
  db_find_key(hDB, 0, devices_path, &h_devs);

  status = db_close_record(hDB, h_settings);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close CAMP settings record");
    return status;
  }

  status = db_close_record(hDB, h_status);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close CAMP status record");
    return status;
  }

  status = db_close_record(hDB, h_devs);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close CAMP defs record");
    return status;
  }

  return status;
}

INT ScanDeviceCamp::begin_of_run(char* error) {
  // If we're not scanning any CAMP variables,
  // we don't even need to connect to the CAMP server.
  bool any_enabled = false;

  for (int i = 0; i < CAMP_SCAN_MAXVARS; i++) {
    if (x_enabled && user_settings.x_dev_idx[i] > -1) {
      any_enabled = true;
      break;
    }
    if (y_enabled && user_settings.y_dev_idx[i] > -1) {
      any_enabled = true;
      break;
    }
  }

  if (!any_enabled) {
    return SUCCESS;
  }

  // We have work to do. Connect to CAMP and validate
  // the instruments we're going to be scanning.
  INT status = camp.connect(user_settings.server_name);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to connect to CAMP server at %s", user_settings.server_name);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  for (int i = 0; i < CAMP_SCAN_MAXVARS; i++) {
    if (x_enabled && user_settings.x_dev_idx[i] > -1) {
      if (debug) {
        scan_utils::ts_printf("Checking CAMP settings for X device %i\n", i);
      }

      int dev_idx = user_settings.x_dev_idx[i];

      if (dev_idx > CAMP_SCAN_MAX_DEVICES) {
        snprintf(error, 255, "Invalid CAMP device index %d for X[%i]", dev_idx, i);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      status = camp.validate_instrument_settings(camp_devices.instrument_path[dev_idx],
                                                 camp_devices.setpoint_path[dev_idx],
                                                 camp_devices.units[dev_idx],
                                                 camp_devices.instrument_type[dev_idx]);

      if (status != SUCCESS) {
        snprintf(error, 255, "Invalid settings for CAMP device idx %d (%s)", dev_idx, camp_devices.setpoint_path[dev_idx]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      x_conversion_factors[i] = camp_devices.integer_conversion_factor[dev_idx];
    }

    if (y_enabled && user_settings.y_dev_idx[i] > -1) {
      if (debug) {
        scan_utils::ts_printf("Checking CAMP settings for Y device %i\n", i);
      }

      int dev_idx = user_settings.y_dev_idx[i];

      if (dev_idx > CAMP_SCAN_MAX_DEVICES) {
        snprintf(error, 255, "Invalid CAMP device index %d for Y[%i]", dev_idx, i);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      status = camp.validate_instrument_settings(camp_devices.instrument_path[dev_idx],
                                                 camp_devices.setpoint_path[dev_idx],
                                                 camp_devices.units[dev_idx],
                                                 camp_devices.instrument_type[dev_idx]);

      if (status != SUCCESS) {
        snprintf(error, 255, "Invalid settings for CAMP device idx %d (%s)", dev_idx, camp_devices.setpoint_path[dev_idx]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return FE_ERR_ODB;
      }

      y_conversion_factors[i] = camp_devices.integer_conversion_factor[dev_idx];
    }
  }

  return SUCCESS;
}

INT ScanDeviceCamp::end_of_run() {
  camp.disconnect();
  return SUCCESS;
}

INT ScanDeviceCamp::setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) {
  INT status = SUCCESS;

  // Compute new values and set in ODB
  for (int i = 0; i < CAMP_SCAN_MAXVARS; i++) {
    if (x_enabled && new_x && user_settings.x_dev_idx[i] >= 0) {
      scan_status.x_values[i] = sweep_x[i][x_step];
      int dev_idx = user_settings.x_dev_idx[i];

      status = camp.set(camp_devices.instrument_path[dev_idx], camp_devices.setpoint_path[dev_idx], scan_status.x_values[i]);

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Failed to set %s to %f", camp_devices.setpoint_path[dev_idx], scan_status.x_values[i]);
        return status;
      }
    } else {
      scan_status.x_values[i] = 0;
    }

    if (y_enabled && new_y && user_settings.y_dev_idx[i] >= 0) {
      scan_status.y_values[i] = sweep_y[i][y_step];
      int dev_idx = user_settings.y_dev_idx[i];

      status = camp.set(camp_devices.instrument_path[dev_idx], camp_devices.setpoint_path[dev_idx], scan_status.y_values[i]);

      if (status != SUCCESS) {
        cm_msg(MERROR, __FUNCTION__, "Failed to set %s to %f", camp_devices.setpoint_path[dev_idx], scan_status.y_values[i]);
        return status;
      }
    } else {
      scan_status.y_values[i] = 0;
    }

  }

  return status;
}

INT ScanDeviceCamp::is_next_step_ready(bool& ok) {
  ok = true;
  return SUCCESS;
}

void ScanDeviceCamp::compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) {
  for (int i = 0; i < CAMP_SCAN_MAXVARS; i++) {
    if (direction_type_x != DirNoRecomputation) {
      if (x_enabled && user_settings.x_dev_idx[i] >= 0) {
        sweep_x[i] = compute_sweep_float(direction_type_x, user_settings.x_start[i], user_settings.x_end[i], nX);
        print_sweep(sweep_x[i], true, i);
      } else {
        sweep_x[i].clear();
      }
    }

    if (direction_type_y != DirNoRecomputation) {
      if (y_enabled && user_settings.y_dev_idx[i] >= 0) {
        sweep_y[i] = compute_sweep_float(direction_type_y, user_settings.y_start[i], user_settings.y_end[i], nY);
        print_sweep(sweep_y[i], false, i);
      } else {
        sweep_y[i].clear();
      }
    }
  }
}

INT ScanDeviceCamp::fill_end_of_cycle_banks(char* pevent) {
  if (x_enabled) {
    float* pdata_x;
    bk_create(pevent, "XCMP", TID_FLOAT, (void**) &pdata_x);

    for (int i = 0; i < CAMP_SCAN_MAXVARS; i++) {
      *pdata_x++ = scan_status.x_values[i];
    }

    bk_close(pevent, pdata_x);
  }

  if (y_enabled) {
    float* pdata_y;
    bk_create(pevent, "YCMP", TID_FLOAT, (void**) &pdata_y);

    for (int i = 0; i < CAMP_SCAN_MAXVARS; i++) {
      *pdata_y++ = scan_status.y_values[i];
    }

    bk_close(pevent, pdata_y);
  }

  return SUCCESS;
}

void ScanDeviceCamp::print_sweep(std::vector<float>& values, bool is_x, int idx) {
  int dev_idx = -1;
  std::cout << "Sweep values for CAMP ";

  if (is_x) {
    std::cout << "X";
    dev_idx = user_settings.x_dev_idx[idx];
  } else {
    std::cout << "Y";
    dev_idx = user_settings.y_dev_idx[idx];
  }

  std::cout << " param #" << idx << " (" << camp_devices.setpoint_path[dev_idx] << ") are: ";

  for (unsigned int i = 0; i < values.size(); i++) {
    if (i > 0) {
      std::cout << ", ";
    }

    std::cout << values[i];
  }

  std::cout << std::endl;
}

int ScanDeviceCamp::get_x_value_converted(int index) {
  if (index < 0 || index >= CAMP_SCAN_MAXVARS) {
    return -99999;
  }

  return (scan_status.x_values[index] * x_conversion_factors[index]) + 0.49;
}

int ScanDeviceCamp::get_y_value_converted(int index) {
  if (index < 0 || index >= CAMP_SCAN_MAXVARS) {
    return -99999;
  }

  return (scan_status.y_values[index] * y_conversion_factors[index]) + 0.49;
}
