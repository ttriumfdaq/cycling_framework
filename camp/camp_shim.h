#ifndef CAMP_SHIM_H
#define CAMP_SHIM_H

#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/time.h>

// This shim is required because CAMP is pure C, to the extent that
// camp.h is not valid C++.
//
// camp.h cannot therefore be #included (directly or indirectly) from any
// file that is going to be compiled as C++.
//
// So we create camp_shim.c, which will be compiled as C, but has a camp_shim.h
// that is valid C and C++. All the C++ calling code can then happily #include
// camp_shim.h, and call these functions.
//
// Sadly, this means that we do have to create our own version of some structs
// that are defined in camp.h

struct CampShimVarDetails {
  // From CAMP_VAR_CORE in camp.h
  char* path;
  bool is_numeric;
  u_long status;
  char* status_msg;
  char* log_action;

  // From CAMP_NUMERIC in camp.h
  double val;
  struct timeval time_started;
  u_long num;
  double low;
  double hi;
  double sum;
  double sumSquares;
  double sumCubes;
  double offset;
  char* units;
};

typedef struct CampShimVarDetails CampShimVarDetails;

void camp_shim_cleanup(CampShimVarDetails* details);

int camp_shim_connect(char* server, char* err_msg, int err_msg_len);
int camp_shim_disconnect();

int camp_shim_validate_instrument_settings(char* instrument_path, char* set_path, char* units, char* instrument_type, int max_retries, char* err_msg, int err_msg_len);

int camp_shim_set(char* instrument_path, char* set_path, float value, int max_retries, char* err_msg, int err_msg_len);
int camp_shim_get(char* set_path, float* value, int max_retries, char* err_msg, int err_msg_len);

void populate_err_msg(const char* function, char* prefix, char* err_msg, int err_msg_len);
int camp_shim_ensure_online(char* instrument_path, int max_retries, char* err_msg, int err_msg_len);

// loggable should be array length max_loggable of null pointers. Actual vars will be created.
int camp_shim_get_all_loggable_vars(bool td, CampShimVarDetails** loggable, int max_loggable, char* err_msg, int err_msg_len);

#endif
