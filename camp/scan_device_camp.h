#ifndef SCAN_DEVICE_CAMP_H
#define SCAN_DEVICE_CAMP_H

#include "camp_class.h"
#include "midas.h"
#include "core/scannable_device.h"

#define CAMP_SCAN_MAXVARS 5
#define CAMP_SCAN_MAX_DEVICES 100

#define EMPTY_STR_80_10 "[80] \n\
[80] \n\
[80] \n\
[80] \n\
[80] \n\
[80] \n\
[80] \n\
[80] \n\
[80] \n\
[80] \n"

#define EMPTY_STR_80_100 EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10 \
EMPTY_STR_80_10

#define EMPTY_STR_32_10 "[32] \n\
[32] \n\
[32] \n\
[32] \n\
[32] \n\
[32] \n\
[32] \n\
[32] \n\
[32] \n\
[32] \n"

#define EMPTY_STR_32_100 EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10 \
EMPTY_STR_32_10

#define EMPTY_STR_10_10 "[10] \n\
[10] \n\
[10] \n\
[10] \n\
[10] \n\
[10] \n\
[10] \n\
[10] \n\
[10] \n\
[10] \n"

#define EMPTY_STR_10_100 EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10 \
EMPTY_STR_10_10


#define CAMP_DEVICES_STR "\
Setpoint path = STRING[100] : \n" \
EMPTY_STR_80_100 "\
Instrument path = STRING[100] : \n" \
EMPTY_STR_32_100 "\
Instrument type = STRING[100] : \n" \
EMPTY_STR_32_100 "\
Units = STRING[100] : \n" \
EMPTY_STR_10_100 "\
Integer conversion factor = INT[100]\n\
[0] 1\n\
[1] 1\n\
[2] 1\n\
[3] 1\n\
[4] 1\n\
[5] 1\n\
[6] 1\n\
[7] 1\n\
[8] 1\n\
[9] 1\n\
[10] 1\n\
[11] 1\n\
[12] 1\n\
[13] 1\n\
[14] 1\n\
[15] 1\n\
[16] 1\n\
[17] 1\n\
[18] 1\n\
[19] 1\n\
[20] 1\n\
[21] 1\n\
[22] 1\n\
[23] 1\n\
[24] 1\n\
[25] 1\n\
[26] 1\n\
[27] 1\n\
[28] 1\n\
[29] 1\n\
[30] 1\n\
[31] 1\n\
[32] 1\n\
[33] 1\n\
[34] 1\n\
[35] 1\n\
[36] 1\n\
[37] 1\n\
[38] 1\n\
[39] 1\n\
[40] 1\n\
[41] 1\n\
[42] 1\n\
[43] 1\n\
[44] 1\n\
[45] 1\n\
[46] 1\n\
[47] 1\n\
[48] 1\n\
[49] 1\n\
[50] 1\n\
[51] 1\n\
[52] 1\n\
[53] 1\n\
[54] 1\n\
[55] 1\n\
[56] 1\n\
[57] 1\n\
[58] 1\n\
[59] 1\n\
[60] 1\n\
[61] 1\n\
[62] 1\n\
[63] 1\n\
[64] 1\n\
[65] 1\n\
[66] 1\n\
[67] 1\n\
[68] 1\n\
[69] 1\n\
[70] 1\n\
[71] 1\n\
[72] 1\n\
[73] 1\n\
[74] 1\n\
[75] 1\n\
[76] 1\n\
[77] 1\n\
[78] 1\n\
[79] 1\n\
[80] 1\n\
[81] 1\n\
[82] 1\n\
[83] 1\n\
[84] 1\n\
[85] 1\n\
[86] 1\n\
[87] 1\n\
[88] 1\n\
[89] 1\n\
[90] 1\n\
[91] 1\n\
[92] 1\n\
[93] 1\n\
[94] 1\n\
[95] 1\n\
[96] 1\n\
[97] 1\n\
[98] 1\n\
[99] 1"

typedef struct {
  char setpoint_path[CAMP_SCAN_MAX_DEVICES][80];
  char instrument_path[CAMP_SCAN_MAX_DEVICES][32];
  char instrument_type[CAMP_SCAN_MAX_DEVICES][32];
  char units[CAMP_SCAN_MAX_DEVICES][10];
  INT integer_conversion_factor[CAMP_SCAN_MAX_DEVICES];
} CAMP_SCAN_DEVICES_STRUCT;

#define CAMP_SCAN_SETTINGS_STR "\
Server name = STRING : [200] \n\
X device index = INT[5] : \n\
[0] : -1\n\
[1] : -1\n\
[2] : -1\n\
[3] : -1\n\
[4] : -1\n\
X start = FLOAT[5] : \n\
[0] : 0\n\
[1] : 0\n\
[2] : 0\n\
[3] : 0\n\
[4] : 0\n\
X end = FLOAT[5] : \n\
[0] : 0\n\
[1] : 0\n\
[2] : 0\n\
[3] : 0\n\
[4] : 0\n\
Y device index = INT[5] : \n\
[0] : -1\n\
[1] : -1\n\
[2] : -1\n\
[3] : -1\n\
[4] : -1\n\
Y start = FLOAT[5] : \n\
[0] : 0\n\
[1] : 0\n\
[2] : 0\n\
[3] : 0\n\
[4] : 0\n\
Y end = FLOAT[5] : \n\
[0] : 0\n\
[1] : 0\n\
[2] : 0\n\
[3] : 0\n\
[4] : 0"

typedef struct {
  char server_name[200];
  INT x_dev_idx[CAMP_SCAN_MAXVARS];
  float x_start[CAMP_SCAN_MAXVARS];
  float x_end[CAMP_SCAN_MAXVARS];
  INT y_dev_idx[CAMP_SCAN_MAXVARS];
  float y_start[CAMP_SCAN_MAXVARS];
  float y_end[CAMP_SCAN_MAXVARS];
} CAMP_SCAN_SETTINGS_STRUCT;

#define CAMP_SCAN_STATUS_STR "\
X value = FLOAT[5] : \n\
[0] : 0\n\
[1] : 0\n\
[2] : 0\n\
[3] : 0\n\
[4] : 0\n\
Y value = FLOAT[5] : \n\
[0] : 0\n\
[1] : 0\n\
[2] : 0\n\
[3] : 0\n\
[4] : 0"

typedef struct {
  float x_values[CAMP_SCAN_MAXVARS];
  float y_values[CAMP_SCAN_MAXVARS];
} CAMP_SCAN_STATUS_STRUCT;

// Wrapper for scanning CAMP-controlled devices in the cycling framework.
// It supports scanning in 2D, with up to 5 devices in X and 5 in Y.
//
// The user specifies long-term "definitions" of devices in /Scanning/Camp/Definitions,
// then just states which device definitions to use when scanning.
class ScanDeviceCamp: public ScannableDevice {
  public:
    ScanDeviceCamp(HNDLE _hDB);
    virtual ~ScanDeviceCamp() {};

    // Functions from the ScannableDevice interface.
    virtual INT check_records() override;
    virtual INT open_records() override;
    virtual INT close_records() override;

    virtual INT begin_of_run(char* error) override;
    virtual INT end_of_run() override;

    virtual INT setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) override;
    virtual INT is_next_step_ready(bool& ok) override;
    virtual void compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) override;

    virtual INT fill_end_of_cycle_banks(char* pevent) override;

    // Custom functions for this implementation.

    // Users can specify a conversion factor for converting between the
    // true float value and an integer representation. These functions
    // return the converted value.
    int get_x_value_converted(int index);
    int get_y_value_converted(int index);

  private:
    void print_sweep(std::vector<float>& values, bool is_x, int idx);

    Camp camp;

    CAMP_SCAN_DEVICES_STRUCT camp_devices;
    CAMP_SCAN_SETTINGS_STRUCT user_settings;
    CAMP_SCAN_STATUS_STRUCT scan_status;

    std::vector<float> sweep_x[CAMP_SCAN_MAXVARS];
    std::vector<float> sweep_y[CAMP_SCAN_MAXVARS];

    INT x_conversion_factors[CAMP_SCAN_MAXVARS];
    INT y_conversion_factors[CAMP_SCAN_MAXVARS];
};


#endif
