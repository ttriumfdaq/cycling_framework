#include "midas.h"

#ifndef DUMMY_CAMP
extern "C" {
  #include "camp_shim.h"
}
#endif

#include "camp_class.h"
#include "core/scan_utils.h"
#include <string>
#include <cstring>

Camp::Camp() {
  connected = false;
  max_retries = 10;
  err_msg_len = 1024;
  c_loggable = NULL;
}

Camp::~Camp() {
  if (connected) {
    disconnect();
  }
}

INT Camp::connect(char *server) {
#ifdef DUMMY_CAMP
  INT status = SUCCESS;
#else
  INT status = camp_shim_connect(server, err_msg, err_msg_len);
#endif

  if (status == SUCCESS) {
    connected = true;
  } else {
    cm_msg(MERROR, __FUNCTION__, "%s", err_msg);
  }

  return status;
}

INT Camp::disconnect() {
#ifndef DUMMY_CAMP
  if (connected) {
    camp_shim_disconnect();
  }
#endif
  connected = false;
  return SUCCESS;
}

INT Camp::validate_instrument_settings(char *instrument_path, char *set_path, char *units, char *instrument_type) {
  if (!connected) {
    cm_msg(MERROR, __FUNCTION__, "Call connect() before validate_instrument_settings()");
    return FE_ERR_DRIVER;
  }

#ifdef DUMMY_CAMP
  INT status = SUCCESS;
#else
  INT status = camp_shim_validate_instrument_settings(instrument_path, set_path, units, instrument_type, max_retries, err_msg, err_msg_len);
#endif

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "%s", err_msg);
  }

  return status;
}

INT Camp::set(char *instrument_path, char *set_path, float value) {
  if (!connected) {
    cm_msg(MERROR, __FUNCTION__, "Call connect() before set()");
    return FE_ERR_DRIVER;
  }

#ifdef DUMMY_CAMP
  INT status = SUCCESS;
#else
  INT status = camp_shim_set(instrument_path, set_path, value, max_retries, err_msg, err_msg_len);
#endif

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "%s", err_msg);
  }

  return status;
}

INT Camp::get(char *set_path, float &value) {
  if (!connected) {
    cm_msg(MERROR, __FUNCTION__, "Call connect() before get()");
    return FE_ERR_DRIVER;
  }

#ifdef DUMMY_CAMP
  INT status = SUCCESS;
#else
  INT status = camp_shim_get(set_path, &value, max_retries, err_msg, err_msg_len);
#endif

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "%s", err_msg);
  }

  return status;
}

INT Camp::get_list_of_loggable_params(bool td, std::vector<CampShimVarDetails*>& loggable, int max_loggable) {
  free_vars(loggable);

  if (c_loggable) {
    free(c_loggable);
    c_loggable = NULL;
  }

  loggable.clear();

  c_loggable = (CampShimVarDetails**) calloc(max_loggable, sizeof(CampShimVarDetails*));

#ifdef DUMMY_CAMP
  INT status = SUCCESS;
#else
  INT status = camp_shim_get_all_loggable_vars(td, c_loggable, max_loggable, err_msg, err_msg_len);
#endif

  for (int i = 0; i < max_loggable; i++) {
    if (c_loggable[i]) {
      loggable.push_back(c_loggable[i]);
    }
  }

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "%s", err_msg);
  }

  return status;
}

void Camp::free_vars(std::vector<CampShimVarDetails*>& var_list) {
  for (auto it = var_list.begin(); it != var_list.end(); it++) {
    camp_shim_cleanup(*it);
    free(*it);
  }
}

