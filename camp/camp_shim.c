#include "camp_shim.h"
#include "camp_clnt.h"
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>

#define FE_ERR_ODB                  602
#define FE_ERR_HW                   603
#define FE_ERR_DRIVER               605
#define SUCCESS 1


void camp_shim_cleanup(CampShimVarDetails* details) {
  free(details->path);
  details->path = NULL;
  free(details->status_msg);
  details->status_msg = NULL;
  free(details->log_action);
  details->log_action = NULL;
  free(details->units);
  details->units = NULL;
}

int camp_shim_connect(char *server, char* err_msg, int err_msg_len) {
  long timeout = 20;
  int status = camp_clntInit(server, timeout);

  if (_failure(status)) {
    char prefix[255];
    sprintf(prefix, "Failed camp_clntInit");
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_DRIVER;
  }

  status = camp_clntUpdate();

  if (status != CAMP_SUCCESS) {
    char prefix[255];
    sprintf(prefix, "Failed camp_clntUpdate");
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_DRIVER;
  }

  return SUCCESS;
}

int camp_shim_disconnect() {
  camp_clntEnd();
  return SUCCESS;
}

int camp_shim_validate_instrument_settings(char *instrument_path, char *set_path, char *units, char *instrument_type, int max_retries, char* err_msg, int err_msg_len) {
  // See if instrument already exists.
  CAMP_VAR* pVar = camp_varGetp(instrument_path);
  int status;

  if (pVar == NULL) {
    printf("CAMP: adding instrument %s to CAMP...\n", instrument_path);
  } else if (strcmp(instrument_type, pVar->spec.CAMP_VAR_SPEC_u.pIns->typeIdent) != 0) {
    // Delete existing instrument as it's of the wrong type.
    printf("CAMP: replacing instrument '%s' in CAMP (changing type from %s to %s)\n", instrument_path, pVar->spec.CAMP_VAR_SPEC_u.pIns->typeIdent, instrument_type);

    status = campSrv_insDel(instrument_path);

    if (status != CAMP_SUCCESS) {
      char prefix[255];
      sprintf(prefix, "Failed to remove old device %s from CAMP", instrument_path);
      populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
      return FE_ERR_DRIVER;
    }

    pVar = NULL;
  }

  if (pVar == NULL) {
    // Add instrument to CAMP if needed.
    status = campSrv_insAdd(instrument_type, instrument_path);

    if (status != CAMP_SUCCESS) {
      char prefix[255];
      sprintf(prefix, "Failed to add new device %s to CAMP", instrument_path);
      populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
      return FE_ERR_DRIVER;
    }

    status = camp_clntUpdate();

    if (status != CAMP_SUCCESS) {
      char prefix[255];
      sprintf(prefix, "Failed camp_clntUpdate after adding new instrument %s", instrument_path);
      populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
      return FE_ERR_DRIVER;
    }

    pVar = camp_varGetp(instrument_path);

    if (pVar == NULL) {
      snprintf(err_msg, err_msg_len, "Tried to add instrument %s to CAMP, but it still doesn't exist", instrument_path);
      return FE_ERR_HW;
    } else {
      printf("Init_Sweep_Device: instrument '%s'  is ready\n", instrument_path);
    }
  }

  // Make sure there's a default interface definition that we will modify.
  if (pVar->spec.CAMP_VAR_SPEC_u.pIns->pIF == NULL) {
    snprintf(err_msg, err_msg_len, "In %s: CAMP instrument '%s' has invalid default interface definition", __FUNCTION__, instrument_path);
    return ( CAMP_INVAL_INS);
  }

  // Ensure device is online.
  status = camp_shim_ensure_online(instrument_path, max_retries, err_msg, err_msg_len);

  if (status != SUCCESS) {
    return status;
  }

  // Make sure that the set path actually contains the instrument path.
  char set_prefix[20], instr_prefix[20];
  camp_pathGetFirst(set_path, set_prefix, sizeof(set_prefix));
  camp_pathGetFirst(instrument_path, instr_prefix, sizeof(instr_prefix));

  if (!camp_pathCompare(set_prefix, instr_prefix)) {
    snprintf(err_msg, err_msg_len, "In %s: set path (%s) does not contain instrument path (%s)", __FUNCTION__, set_path, instrument_path);
    return FE_ERR_ODB;
  }

  // Check the units are correct.
  // Non-fatal error if not.
  char read_units[80];
  bool got_units = false;

  for (int i = 0; i < max_retries; i++) {
    status = camp_varNumGetUnits(set_path, read_units, sizeof(read_units));
    if (_success(status)) {
      got_units = true;
      break;
    }
  }

  if (!got_units) {
    sprintf(read_units, "X");
    char prefix[255];
    sprintf(prefix, "Failed to read units of %s", set_path);
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_DRIVER;
  }

  // Convert CAMP & ODB units to lower-case
  char odb_units[80];
  sprintf(odb_units, "%s", units);

  for (int j = 0; j < strlen(odb_units); j++) {
    odb_units[j] = tolower(units[j]);
  }
  for (int j = 0; j < strlen(read_units); j++) {
    read_units[j] = tolower(read_units[j]);
  }

  if (strncmp(odb_units, read_units, strlen(read_units)) != 0) {
    snprintf(err_msg, err_msg_len, "In %s: Units of CAMP device %s specified in ODB (%s) disagree with units in CAMP (%s).", __FUNCTION__, set_path, units, read_units);
    return FE_ERR_ODB;
  }

  // Try to read the current value.
  float tmp_read_val = 0;

  return camp_shim_get(set_path, &tmp_read_val, max_retries, err_msg, err_msg_len);
}

int camp_shim_set(char *instrument_path, char *set_path, float value, int max_retries, char* err_msg, int err_msg_len) {
  double dvalue = value;
  bool ok = false;
  printf("CAMP: Setting %s to %f\n", set_path, value);

  for (int i = 0; i < max_retries; i++) {
    int status = campSrv_varNumSetVal(set_path, dvalue);
    if (status == CAMP_SUCCESS) {
      ok = true;
      break;
    }
    usleep(100000);
  }

  if (!ok) {
    printf("CAMP: sleeping 5s before trying to set device online\n");
    sleep(5);

    int status = camp_shim_ensure_online(instrument_path, max_retries, err_msg, err_msg_len);

    if (status != SUCCESS) {
      return status;
    }

    // Try again to set value
    for (int i = 0; i < max_retries; i++) {
      status = campSrv_varNumSetVal(set_path, dvalue);
      if (status == CAMP_SUCCESS) {
        ok = true;
        break;
      }
      usleep(100000);
    }

    if (!ok) {
      char prefix[255];
      sprintf(prefix, "Failed to set %s to %f", set_path, value);
      populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
      return FE_ERR_HW;
    }
  }

  return SUCCESS;
}

int camp_shim_ensure_online(char* instrument_path, int max_retries, char* err_msg, int err_msg_len) {
  // Try this because device may go offline if lots of errors
  bool_t online = 0;
  int status = camp_insGetLine(instrument_path, &online);

  if (!online) {
    printf("CAMP: Now turning instrument online\n");
    for (int i = 1; i < max_retries; i++) {
      status = campSrv_insLine(instrument_path, TRUE);
      if (_success(status)) {
        online = true;
        break;
      }
    }
  }

  if (!online) {
    char prefix[255];
    sprintf(prefix, "Failed to turn instrument online (%s)", instrument_path);
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

int camp_shim_get(char *set_path, float *value, int max_retries, char* err_msg, int err_msg_len) {
  int status = campSrv_varGet(set_path, CAMP_XDR_NO_CHILD);

  if (status != CAMP_SUCCESS) {
    char prefix[255];
    sprintf(prefix, "Failed camp_varGet for variable %s", set_path);
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_HW;
  }

  double readback = 0;
  bool success = false;

  for (int i = 0; i < max_retries; i++) {
    status = camp_varNumGetVal(set_path, &readback);
    if (status == CAMP_SUCCESS) {
      success = true;
      break;
    }
  }

  if (success) {
    *value = readback;
    return SUCCESS;
  } else {
    char prefix[255];
    sprintf(prefix, "Failed to read CAMP variable %s (failed %d times)", set_path, max_retries);
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_HW;
  }
}

void camp_shim_camp_var_to_shim_var(CAMP_VAR* camp_var, CampShimVarDetails* shim_var) {
  shim_var->path = strdup(camp_var->core.path);
  shim_var->is_numeric = (camp_var->spec.varType & CAMP_VAR_TYPE_NUMERIC);
  shim_var->status = camp_var->core.status;
  shim_var->status_msg = strdup(camp_var->core.statusMsg);
  shim_var->log_action = strdup(camp_var->core.logAction);

  if (shim_var->is_numeric) {
    CAMP_NUMERIC* camp_num = camp_var->spec.CAMP_VAR_SPEC_u.pNum;
    shim_var->val = camp_num->val;
    shim_var->time_started = camp_num->timeStarted;
    shim_var->num = camp_num->num;
    shim_var->low = camp_num->low;
    shim_var->hi = camp_num->hi;
    shim_var->sum = camp_num->sum;
    shim_var->sumSquares = camp_num->sumSquares;
    shim_var->sumCubes = camp_num->sumCubes;
    shim_var->offset = camp_num->offset;
    shim_var->units = strdup(camp_num->units);
  }
}

bool camp_shim_is_loggable(CAMP_VAR* pVar, bool td) {
  if (!(pVar->core.status & CAMP_VAR_ATTR_LOG)) {
    return 0;
  }

  if (streq(pVar->core.logAction, "log_mdarc")) {
    return 1;
  }

  if (td && (streq( pVar->core.logAction, "log_td_musr" ) || streq( pVar->core.logAction, "log_mdarc_td" ))) {
    return 1;
  }

  if (!td && (streq( pVar->core.logAction, "log_i_musr" ) || streq( pVar->core.logAction, "log_mdarc_i" ))) {
    return 1;
  }

  return 0;
}

void camp_shim_recurse_get_loggable_var(bool td, CAMP_VAR* pVarStart, CampShimVarDetails** loggable, int max_loggable, int* curr) {
  for(CAMP_VAR* pVar = pVarStart; pVar != NULL; pVar = pVar->pNext) {
    if( pVar->pChild != NULL ) {
      camp_shim_recurse_get_loggable_var(td, pVar->pChild, loggable, max_loggable, curr);
    }

    if (camp_shim_is_loggable(pVar, td) && *curr < max_loggable - 1) {
      loggable[*curr] = malloc(sizeof(CampShimVarDetails));
      camp_shim_camp_var_to_shim_var(pVar, loggable[*curr]);
      (*curr)++;
    }
  }
}

int camp_shim_get_all_loggable_vars(bool td, CampShimVarDetails** loggable, int max_loggable, char* err_msg, int err_msg_len) {
  int status = camp_clntUpdate();

  if (status != CAMP_SUCCESS) {
    char prefix[255];
    sprintf(prefix, "Failed camp_clntUpdate before getting list of loggable params");
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_DRIVER;
  }

  status = campSrv_varGet("/", CAMP_XDR_ALL);

  if (status != CAMP_SUCCESS) {
    char prefix[255];
    sprintf(prefix, "Failed campSrv_varGet when trying to get list of loggable params");
    populate_err_msg(__FUNCTION__, prefix, err_msg, err_msg_len);
    return FE_ERR_DRIVER;
  }

  int curr = 0;
  camp_shim_recurse_get_loggable_var(td, pVarList, loggable, max_loggable, &curr);

  return SUCCESS;
}


void populate_err_msg(const char *function, char *prefix, char* err_msg, int err_msg_len) {
  char *msg = camp_getMsg();

  if (msg && strlen(msg)) {
    snprintf(err_msg, err_msg_len, "In %s: %s: %s", function, prefix, msg);
  } else {
    snprintf(err_msg, err_msg_len, "In %s: %s", function, prefix);
  }
}

