#include "camp_class.h"
#include "camp_shim.h"
#include <stdio.h>
#include <string>
#include <map>
#include <vector>
#include "core/scan_utils.h"

// Simple program to read from CAMP using the `Camp` class-based
// infrastructure in camp_class.cxx. Comparing this to low-level
// camp programs can be useful for debugging access/programming issues.

void usage() {
  printf("Usage: camp_test.exe <server> get <path>\n");
  printf("       camp_test.exe <server> get loggable\n");
  printf("Possible sever is bnmrvw.triumf.ca\n");
  exit(0);
}

int main(int argc, char* argv[]) {
  if (argc != 4) {
    usage();
  }

  std::string cmd(argv[2]);

#ifdef DUMMY_CAMP
  printf("***************************************************************************\n");
  printf("EXECUTABLE WAS COMPILED IN DUMMY MODE - NOT CONNECTING TO REAL CAMP SERVER!\n");
  printf("***************************************************************************\n");
#endif

  Camp camp;

  unsigned long int t1 = scan_utils::current_time_ms();
  INT status = camp.connect(argv[1]);
  unsigned long int t2 = scan_utils::current_time_ms();

  if (status != SUCCESS) {
   printf("Couldn't connect to CAMP server on %s\n", argv[1]);
   exit(status);
  }

  if (cmd == "get") {
    if (argc != 4) {
      usage();
    }

    std::string dev(argv[3]);

    if (dev == "loggable") {
      std::vector<CampShimVarDetails*> loggable;

      unsigned long int t3 = scan_utils::current_time_ms();
      status = camp.get_list_of_loggable_params(true, loggable, 100);
      unsigned long int t4 = scan_utils::current_time_ms();

      printf("> Connect took %d ms\n", t2-t1);
      printf("> Loggable search took %d ms\n", t4-t3);

      if (status != SUCCESS) {
        printf("Couldn't get list of loggable vars\n");
        exit(status);
      } else if (loggable.size() == 0) {
        printf("No loggable vars found\n");
      } else {
        for (auto it = loggable.begin(); it != loggable.end(); it++) {
          printf("%s = %f\n", (*it)->path, (*it)->val);
        }
      }
    } else {
      float val = 0;

      unsigned long int t3 = scan_utils::current_time_ms();
      status = camp.get(argv[3], val);
      unsigned long int t4 = scan_utils::current_time_ms();

      printf("> Connect took %d ms\n", t2-t1);
      printf("> Get request took %d ms\n", t4-t3);

      if (status != SUCCESS) {
        printf("Couldn't get var\n");
        exit(status);
      } else {
        printf("%f\n", val);
      }
    }
  }
}
