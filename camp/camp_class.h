#ifndef CAMP_CLASS_H
#define CAMP_CLASS_H

#include "midas.h"
#include "camp_shim.h"

// Object-oriented interface to CAMP, which is a server-based
// system used by CMMS experiments for slow control devices.
//
// The CAMP API is pure C (and invalid C++), so this class mostly
// just calls functions in camp_shim.h (which is pure C and valid C++),
// that handles actually dealing with the CAMP API.
class Camp {
  public:
    Camp();
    ~Camp();

    // Connect to a given CAMP server.
    INT connect(char* server);

    // Disconnect from the CAMP server.
    INT disconnect();

    // Check that an instrument exists in CAMP (creating it if needed),
    // validate that the instrument is online, that the setting path
    // exists, and that the instrument's units and type match what the
    // user expects.
    INT validate_instrument_settings(char* instrument_path, char* set_path, char* units, char* instrument_type);

    // Set a new value for a CAMP-controlled device.
    INT set(char* instrument_path, char* set_path, float value);

    // Read the current value of a CAMP-controlled device.
    INT get(char* set_path, float& value);

    // Get a list of all parameters on the CAMP server that are
    // set to loggable for the current experiment type.
    // td specifies whether this is a time-differential or integral experiment.
    //
    // See camp_shim.h for the definition of the CampShimVarDetails struct.
    INT get_list_of_loggable_params(bool td, std::vector<CampShimVarDetails*>& loggable, int max_loggable);

    // Free any memory that was allocated for the char* members of each
    // CampShimVarDetails struct.
    void free_vars(std::vector<CampShimVarDetails*>& var_list);

  private:
    bool connected;
    int max_retries;

    // These are passed to camp shim functions so that CAMP error messages can be
    // reported back to us.
    int err_msg_len;
    char err_msg[1024];

    CampShimVarDetails** c_loggable;
};

#endif
