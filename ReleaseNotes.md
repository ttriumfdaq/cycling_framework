Contact: Ben Smith <bsmith@triumf.ca>

Release v0.1.1
==============

### Overview

Allow 32-bit and 64-bit builds to be built in the same compilation run.

### Backwards-incompatible changes

* EPICS_BASE and EPICS_HOST_ARCH must now be specified in parent CMakeLists.txt.

### New features and bug fixes

* New program for testing CAMP access (camp_test.exe)
* Improved documentation.


Release v0.1.0
==============

### Overview

Initial implementation of cycling framework, with features necessary for BNMR, BNQR, EBIT, CPET and MPET.

### Backwards-incompatible changes

N/A

### New features and bug fixes

N/A
