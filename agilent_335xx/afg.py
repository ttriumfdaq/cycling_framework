"""
Tools for talking to an Agilent frequency generator.
A frontend that adds a midas interface around this class is available 
in afg_fe.py
"""
import telnetlib
import time

class Afg:
    """
    Designed to talk to Agilent 335xx frequency generators.
    Works by sending commands over telnet.
    
    Members:
        
    * logger (`logging.Logger`) - Optional logger for writing debug messages.
    * telnet (`telnetlib.Telnet`) - The acual telnet connection to the device.
    * response_regexes (list of bytestring) - Text we expect to receive when
        reading responses from the device.
    """
    def __init__(self, host, port, logger=None):
        """
        Connect to the specified device.
        
        Args:
            
        * host (str)
        * port (int)
        * logger (`logging.Logger`)
        """
        self.logger = logger
        
        if self.logger:
            self.logger.debug("Opening connection to %s:%s" % (host, port))
        
        self.telnet = telnetlib.Telnet(host, port, 9999)
        self.response_regexes = [b'33521A>', b'33500>']
        
        # Read from telnet; if connection failed, an exception will be raised
        self.read()
        
    def write(self, command):
        """
        Write a command to the device.
        
        Args:
            
        * command (str)
        
        Returns:
            list of str - From comma-separating the first line of response
        """
        if self.logger:
            self.logger.debug("Writing '%s'" % command)
            
        self.telnet.write(b"%s\r" % command.encode('utf-8'))
        
        # We MUST sleep before reading from the device.
        # It is annoying that the sleep is so long, but any shorter
        # seems to lead to the device not responding any further.
        time.sleep(0.1)
        return self.read()
        
    def read(self, parsed=True):
        """
        Read from the device.
        
        Args:
            
        * parsed (bool) - If True, return the first line only, and split at commas.
            If False, return the raw text.
            
        Returns:
            list of str or str
        """
        (regex_idx, dummy, text) = self.telnet.expect(self.response_regexes)
        
        text = text.decode('utf-8')
        
        if self.logger:
            self.logger.debug("Read '%s'" % text)
        
        if regex_idx == -1:
            raise RuntimeError("Unexpected response. Text read was '%s'" % text)
        
        if parsed:
            lines = text.split("\r")
            return lines[0].strip().split(",")
        else:
            return text

    def clear(self):
        """
        Abort the current frequency list sweep, and clear device errors.
        """
        self.write("ABOR")
        self.write("*CLS")

    def reset_trigger(self):
        """
        Clear and initialise.
        """
        self.clear()
        self.write("INIT:CONT ON")

    def set_sine(self):
        """
        Set waveform shape to sine wave.
        """
        self.write("FUNC SIN")

    def set_mode_list(self):
        """
        State that we're going to provide a custom list of frequencies.
        """
        self.write("FREQ:MODE LIST")

    def set_output_load(self, load="INF"):
        """
        Set the output load.
        """
        self.write("OUTP:LOAD %s" % load)

    def set_trigger_source(self, source="EXT"):
        """
        Set the trigger source (by default to external trigger).
        """
        self.write("TRIG:SOUR %s" % source)

    def turn_output_on(self):
        """
        Enable output.
        """
        self.write("OUTP ON")

    def turn_output_off(self):
        """
        Disable output.
        """
        self.write("OUTP OFF")

    def toggle_output(self):
        """
        Check whether output is currently enabled, then flip the state.
        """
        state = self.write("OUTP?")
        if state[0] == '0':
            self.turn_output_on()
        else:
            self.turn_output_off()
            
    def set_rf_v(self, rf_v):
        """
        Set the output amplitude (in Volts).
        """
        self.write("VOLT %s" % rf_v)

    def set_freq_list(self, freq_list):
        """
        Load the frequencies to be stepped through.
        
        Args:
            
        * freq_list (list of float)
        """
        if len(freq_list) > 128:
            raise ValueError("Maximum frequency list exceeded. Max list length 128, supplied list %s." % len(freq_list))
        
        self.write("LIST:FREQ %s" % ", ".join("%f" % freq for freq in freq_list))


