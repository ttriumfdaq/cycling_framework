"""
Frontend for interacting with Agilent 335xx frequency generators.

Main features are to program the AFGs during begin-of-run, and
respond to RPC requests from custom webpage (e.g. showing which 
frequencies we'll program).

Users map specify custom frequency lists, or we'll calculate based
on other ODB parameters.

OBD settings are in /Equipment/AFG/Settings. Computed values are
stored in /Equipment/AFG/Computed.
"""
import midas
import midas.frontend
import cycling_framework.agilent_335xx.afg
import cycling_framework.agilent_335xx.calc_freq
import json

# Logger provided by midas FE system. Will be configured already
# based on whether user specified -d flag on command line.
logger = midas.frontend.logger

class AfgEquipment(midas.frontend.EquipmentBase):
    """
    Only one equipment in this frontend.
    
    Members:
        
    * max_rf_v (float) - Maximum voltage we can program.
    * min_rf_v (float) - Minumum voltage we can program.
    * devices (list of str) - Friendly names for the AFG device(s) we'll talk to.
        We'll create an ODB sub-directory for each.
    * odb_computed_dir (str) - Base ODB directory where we'll write the results
        of any computations we do.
    * ppg_dir (str) - Where in the ODB to find PPG timing information. Used to
        find how long the RF will be on for. Currently a bit hacky. Would be
        better to use an RPC request to talk to the PPG frontend directly, rather
        than relying on us knowing the exact PPG ODB structure.
    """
    def __init__(self, client, devices):
        equip_name = "AFG"

        # Maximum input voltage is 5V. Take 4.8V so that we are
        # far away from any distortions.
        self.max_rf_v = 4.8
        self.min_rf_v = 0.002
        
        self.devices = devices
        default_common = midas.frontend.InitialEquipmentCommon()
        default_common.equip_type = midas.EQ_PERIODIC
        default_common.buffer_name = ""
        default_common.trigger_mask = 0
        default_common.event_id = 148
        default_common.period_ms = 1000
        default_common.read_when = 0
        
        default_settings = {
            "PPG loop name": "begin_ramp",
            "Calibration": {
                "RF excitation time (ms)": 100,
                "RF amplitude (V)": 0.2662,
                "Reference ion": "1Na23",
                "Reference charge": 1,
                "Reference frequency": 2470782.7,
                "Reference frequency error": 0.5,
                "Freq minus (Hz)": 6111.3,
                "Filename NUBASE": "/home/mpet/Aaron/TitanMidasScripts/ElectronBindingData/NUBASE2012_formatted.dat",
                "Filename binding energies": "/home/mpet/Aaron/TitanMidasScripts/ElectronBindingData/ElectronBindingEnergies.dat"
            },
            "Species comment": "1K39 etc; can be semi-colon-separated list",
            "Species": "",
            "Charge comment": "1 etc; can be semi-colon-separated list",
            "Charge": ""
        }
        
        default_computed = {}

        for dev in devices:
            if dev.lower() == "quad":
                pulse_name = "Pulse_QUAD1"
            elif dev.lower() == "dipole":
                pulse_name = "Pulse_DPL1"
            else:
                pulse_name = "Pulse_%s1" % dev.upper()
            
            default_settings[dev] = {
                "Enabled": True,
                "Host": "mpet%s" % dev.lower(),
                "Port": "5024",
                "Use custom frequency list": False,
                "Custom frequencies": "",
                "Centre frequency comment": "freqc - cyclotron freqs for species; freqp - dipole freqs for species; custom - custom freqs; can be semi-colon-separated list",
                "Centre frequency": "",
                "Custom centre frequency": "",
                "Frequency deviation comment": "Frequency modulation in Hz; can be semi-colon-separated list",
                "Frequency deviation": "",
                "PPG pulse name": pulse_name,
                "Custom amplitude (V)": 0.1,
                "Use custom amplitude": False
            }
            
            default_computed[dev] = {
                "Num freq steps": 0,
                "Start frequency (Hz)": 0.1,
                "End frequency (Hz)": 0.1,
                "Amplitude (V)": 0.1,
                "Excitation time (ms)": 0.1,
                "Frequency list": [0.1, 0.1]
            }
        
        midas.frontend.EquipmentBase.__init__(self, client, equip_name, default_common, default_settings)
        
        self.odb_computed_dir = self.odb_settings_dir.replace("Settings", "Computed")
        self.client.odb_set(self.odb_computed_dir, default_computed, update_structure_only=True)
                              
        self.ppg_dir = "/Equipment/PPGCompiler/Programming"
        self.settings_changed_func()

    def settings_changed_func(self):
        """
        When settings are changed, update the calibration constants used
        for frequency calculations.
        """
        cal_sett = self.settings["Calibration"]
        self.calc_freq = cycling_framework.agilent_335xx.calc_freq.CalcFreq(cal_sett["Filename NUBASE"],
                                                                  cal_sett["Filename binding energies"],
                                                                  cal_sett["Reference ion"],
                                                                  cal_sett["Reference frequency"],
                                                                  cal_sett["Reference charge"],
                                                                  cal_sett["Reference frequency error"])

    def begin_of_run(self):
        """
        At begin-of-run, for each device that's enabled, calculate the list
        of frequencies/voltage, then program the AFG.
        """
        enabled_devs = []

        for dev in self.devices:
            if self.settings[dev]["Enabled"]:
                enabled_devs.append(dev)
            else:
                self.client.msg("Not programming %s AFG as it is disabled in ODB" % dev)

        if len(enabled_devs) == 0:
            return midas.status_codes["SUCCESS"]
                
        for dev in enabled_devs:
            (rf_v, exc_time) = self.calculate_voltage(dev)
            freq_list = self.calculate_frequency_list(dev)
            
            start_freq = freq_list[0] if len(freq_list) else 0
            end_freq = freq_list[-1] if len(freq_list) else 0
            self.client.odb_set("%s/%s/Num freq steps" % (self.odb_computed_dir, dev), len(freq_list))
            self.client.odb_set("%s/%s/Start frequency (Hz)" % (self.odb_computed_dir, dev), start_freq)
            self.client.odb_set("%s/%s/End frequency (Hz)" % (self.odb_computed_dir, dev), end_freq)
            self.client.odb_set("%s/%s/Amplitude (V)" % (self.odb_computed_dir, dev), rf_v)
            self.client.odb_set("%s/%s/Frequency list" % (self.odb_computed_dir, dev), freq_list)
        
            afg = cycling_framework.agilent_335xx.afg.Afg(self.settings[dev]["Host"], self.settings[dev]["Port"], logger)
            afg.clear()
            afg.set_sine()
            afg.set_mode_list()
            afg.set_rf_v(rf_v)
            afg.set_freq_list(freq_list)
            afg.reset_trigger()   
            self.client.msg("Programmed %s AFG with freq list %s" % (dev, freq_list))
        
        return midas.status_codes["SUCCESS"]
        
    def calculate_voltage(self, dev):
        """
        Calculate the RF voltage that should be used, based on user's settings.
        
        Args:
            
        * dev (str) - Device name
        """
        exc_time = self.get_exc_time_ms(dev)

        if self.settings[dev]["Use custom amplitude"]:
            rf_v = self.settings[dev]["Custom amplitude (V)"]
        else:
            cal_rf_time = self.settings["Calibration"]["RF excitation time (ms)"]
            cal_rf_v = self.settings["Calibration"]["RF amplitude (V)"]
            
            self.client.odb_set("%s/%s/Excitation time (ms)" % (self.odb_computed_dir, dev), exc_time)
            
            rf_v = cal_rf_v * cal_rf_time / exc_time
            
        if rf_v > self.max_rf_v:
            rf_v = self.max_rf_v
            
        if rf_v < self.min_rf_v:
            rf_v = self.min_rf_v
            
        return (rf_v, exc_time)
            
    def get_exc_time_ms(self, dev):
        """
        Get the excitation time of the specified device, in ms.

        Args:
            
        * dev (str) - Device name
        
        Returns:
            float
        """
        pulse_name = self.settings[dev]["PPG pulse name"]
        return self.client.odb_get("%s/%s/pulse width (ms)" % (self.ppg_dir, pulse_name))
    
    def split_odb_string(self, odb_string):
        """
        Some ODB settings can be lists specified as comma- or semicolon-separated
        strings. This functions splits such strings into lists of strings.
        
        Args:
            
        * odb_string (str)
        
        Returns:
            list of str
        """
        return [x.strip() for x in odb_string.replace(",", ";").split(";")]
    
    def get_frequency_centre(self, dev):
        """
        Get user input from the ODB and calculate the respective centre
        frequencies using the "calcFreq" module, in Hz.

        Expected Midas Variable Structure:
            semi-colon delimited and equal-length lists.

        If any list is too short, the last value in the list is appended
        so that each list is the same length.
        
        Args:
            
        * dev (str) - Device name
        
        Returns:
            list of float
        """
        freq_c = self.split_odb_string(self.settings[dev]["Centre frequency"])
        freq_custom = self.split_odb_string(self.settings[dev]["Custom centre frequency"])
        species = self.split_odb_string(self.settings["Species"])
        charge = self.split_odb_string(self.settings["Charge"])

        if "custom" not in freq_c:
            freq_custom = ["0"]

        check_lists = {"Centre frequency": freq_c, 
                       "Species": species, 
                       "Charge": charge, 
                       "Custom centre frequency": freq_custom}
        
        listmax = max(len(x) for x in check_lists.values())
        
        for list_name, mylist in check_lists.items():
            if mylist == [""]:
                raise Exception("ODB variable %s is empty!" % list_name)
            
            while len(mylist) < listmax:
                mylist.append(mylist[-1])
        
        for i in range(len(species)):
            if freq_c[i].lower() == 'freqc':
                freq_c[i] = self.calc_freq.getFreqC(species[i], float(charge[i]))
            elif freq_c[i].lower() == 'freqp':
                freq_c[i] = self.calc_freq.getFreqP(species[i], float(charge[i]))
            elif freq_c[i].lower() == 'custom':
                freq_c[i] = float(freq_custom[i])
                
        return freq_c

    def get_frequency_modulation(self, dev):
        """
        Get the frequency modulation from the ODB (in Hz).

        If the variable is empty, set the frequency modulation to zero.
        
        Args:
            
        * dev (str) - Device name
        
        Returns:
            list of float
        """
        freq_m = self.settings[dev]["Frequency deviation"]
        
        try:
            freq_m = [float(x) for x in self.split_odb_string(freq_m)]
        except ValueError:
            freq_m = [0.0]
            
        return freq_m

    def get_num_points(self):
        """
        Get the number of frequencies that will be used.
        Based on the PPG loop length.
        
        Returns:
            int
        """
        block_name = self.settings["PPG loop name"]
        return self.client.odb_get("%s/%s/loop count" % (self.ppg_dir, block_name))
    
    def calculate_frequency_list(self, dev):
        """
        Given a semi-colon delimited list of center frequencies and
        modulations from the odb generate a frequency list.
        
        The number of points defined in the odb should be a multiple
        of the number of frequencies.  If not the 'extra' points are
        included in the last frequency to be scanned.
        
        If the frequency modulation list is shorter than the center
        frequency list then the last modulation listed is repeated
        until the lists are the same length.
        
        If the Frequency Modulation list is longer than the center
        frequency list then the 'extra' elements are ignored.
        
        Args:
            
        * dev (str) - Device name
        
        Returns:
            list of float
        """
        freqlist_Hz = []
        num_p = self.get_num_points()

        if self.settings[dev]["Use custom frequency list"]:
            freqlist_Hz = [float(f) for f in self.split_odb_string(self.settings[dev]["Custom frequencies"])]
            
            if len(freqlist_Hz) != num_p:
                raise ValueError("Invalid number of custom frequencies. %d provided, expected %d" % (len(freqlist_Hz), num_p))
        else:
            freq_c = self.get_frequency_centre(dev)
            freq_mod = self.get_frequency_modulation(dev)
            
            while(len(freq_mod) < len(freq_c)):
                freq_mod.append(freq_mod[-1])
            
            num_per_freq = int(num_p / len(freq_c))
            n = [num_per_freq] * len(freq_c)
            
            # Add the left-overs to the last element
            n[-1] += num_p % num_per_freq
            
            for i in range(len(freq_c)):
                fstart = freq_c[i] - freq_mod[i]
                df = 2. * freq_mod[i] / float(n[i] - 1)
                
                for j in range(n[i]):
                    freq = (fstart + j * df)
                    freqlist_Hz.append(freq)
                
        return freqlist_Hz


class AfgFrontend(midas.frontend.FrontendBase):
    """
    Frontend class. Only has one Equipment.
    """
    def __init__(self, devices=["AFG"]):
        midas.frontend.FrontendBase.__init__(self, "AfgFrontend")
        self.add_equipment(AfgEquipment(self.client, devices))
                
        self.client.register_jrpc_callback(self.rpc_handler, True)

        # Ensure we run after initial PPG compilation but before C frontends
        self.client.set_transition_sequence(midas.TR_START, 350)
        
    def begin_of_run(self, run_number):
        return self.equipment["AFG"].begin_of_run()

    def rpc_handler(self, client, cmd, args, max_len):
        """
        JRPC handler we register with midas. This will generally be called when
        a user clicks a button on the associated webpage.
        
        Args:
            
        * client (`midas.client.MidasClient`)
        * cmd (str) - The command the user wants us to do.
        * args (str) - Stringified JSON of any arguments for this command.
        * max_len (int) - Maximum length of response the user will accept.
        
        Returns:
            2-tuple of (int, str) for (status code, message)
            
        Accepted commands:
            
        * compute_voltage - Compute the voltage we'll apply for a device.
        * compute_freq_list - Compute the frequencies we'll set for a device.
        * compute_all - Compute voltage and frequencies for all devices.
        """
        ret_int = midas.status_codes["SUCCESS"]
        ret_str = ""
        
        logger.debug("Handling RPC command %s" % cmd)
        equip = self.equipment["AFG"]

        if cmd == "compute_voltage":
            # Args:
            # * dev (str)
            jargs = json.loads(args)
            dev = jargs.get("dev")
            
            ret_int = midas.status_codes["SUCCESS"]
            ret_str = json.dumps({"voltage": equip.calculate_voltage(dev)[0]})
        elif cmd == "compute_freq_list":
            # Args:
            # * dev (str)
            jargs = json.loads(args)
            dev = jargs.get("dev")
            
            ret_int = midas.status_codes["SUCCESS"]
            ret_str = json.dumps({"freq_list": equip.calculate_frequency_list(dev)})
        elif cmd == "compute_all":
            ret = {}
            
            for dev in equip.devices:
                if equip.settings[dev]["Enabled"]:
                    (voltage, exc_time) = equip.calculate_voltage(dev)
                    freq_list = equip.calculate_frequency_list(dev)
                else:
                    exc_time = "Disabled"
                    voltage = "Disabled"
                    freq_list = ["Disabled"]
                    
                ret[dev] = {"exc_time": exc_time,
                            "voltage": voltage,
                            "freq_list": freq_list}
            
            ret_int = midas.status_codes["SUCCESS"]
            ret_str = json.dumps(ret)
        else:
            ret_int = midas.status_codes["FE_ERR_DRIVER"]
            ret_str = "Unknown command '%s'" % cmd     
            
        logger.debug("RPC status is %s: %s" % (ret_int, ret_str))
            
        return (ret_int, ret_str)

if __name__ == "__main__":
    parser = midas.frontend.parser
    parser.add_argument("--oneshot", action="store_true", help="Just program the AFGs then quit")
    args = midas.frontend.parse_args()

    # Quad/dipole are MPET devices.
    # Other experiment using this frontend can make a trivial extra file that
    # defines the devices for that experiment.
    fe = AfgFrontend(["Quad", "Dipole"])
    
    if args.oneshot:
        # Just program the AFGs
        fe.equipment["AFG"].begin_of_run()
    else:
        # Run as a frontend
        fe.run()

