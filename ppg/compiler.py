"""
Allow a PPG laodfile to be created based on a structure specified in the ODB.
"""
import cycling_framework.ppg
import collections
import functools
import copy

class Block:
    """
    Base class that all PPG operations inherit from.
    
    Members:
        
    * name (str)
    * tref (str) - What the time of this block is relative to.
    * tref_offset_ms (float) - Offset of this block's time relative
        to the time of tref.
    * width_ms (float) - Time that this operation takes. Generally 0,
        unless a pulse (which has a user-specified duration).
    * our_total_width_ms (float) - Total time of this block. If this is
        a loop, it includes the time for all children to be completed too.
    * start_tref_name (str) - The tref that other blocks can use to be 
        referenced against the start of this block.
    * end_tref_name (str) - The tref that other blocks can use to be 
        referenced against the end of this block.
    * initial_block_order (int) - The index of this block in the user's
        original ODB specification.
    * time_offset_from_t0_ms (float) - Time offset of this block
        from the start of the PPG program.
    * time_offset_from_loop_start_ms (float) - If in a loop, the time
        offset of this block from the start of the loop.
    """
    def __init__(self, name, tref, tref_offset_ms, initial_block_order):
        self.name = name
        self.tref = tref.upper() if tref is not None else tref
        self.tref_offset_ms = tref_offset_ms
        self.width_ms = 0
        self.our_total_width_ms = None
        self.start_tref_name = None
        self.end_tref_name = None
        self.initial_block_order = initial_block_order
        self.time_offset_from_t0_ms = None
        self.time_offset_from_loop_start_ms = None
    
    def base_str(self):
        try:
            toff = " (%10.6fms)" % self.time_offset_from_t0_ms
        except TypeError:
            toff = ""
            
        return "%s %12.6fms after %s%s:" % (self.start_tref_name.ljust(30), self.tref_offset_ms, self.tref.ljust(25), toff)
    
class Transition(Block):
    """
    Block that toggles the state of  a channel (on/off).
    
    Extra members:
        
    * channel (str) - The channel to work on (e.g. 'CH3').
    """
    def __init__(self, channel, name=None, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.channel = channel.lower()
        self.start_tref_name = "_T%s" % name
        self.end_tref_name = "_T%s" % name
        
    def __str__(self):
        return "%s toggle %s" % (self.base_str(), self.channel)
        
class TurnOn(Block):
    """
    Block that turns a channel on (unless it is already on, in which case it does nothing).
    
    Extra members:
        
    * channel (str) - The channel to work on (e.g. 'CH3').
    """
    def __init__(self, channel, name=None, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.channel = channel.lower()
        self.start_tref_name = "_T%s" % name
        self.end_tref_name = "_T%s" % name
        
    def __str__(self):
        return "%s turn on %s" % (self.base_str(), self.channel)
        
class TurnOff(Block):
    """
    Block that turns a channel off (unless it is already on, in which case it does nothing).
    
    Extra members:
        
    * channel (str) - The channel to work on (e.g. 'CH3').
    """
    def __init__(self, channel, name=None, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.channel = channel.lower()
        self.start_tref_name = "_T%s" % name
        self.end_tref_name = "_T%s" % name
        
    def __str__(self):
        return "%s turn off %s" % (self.base_str(), self.channel)
        
class Pulse(Block):
    """
    Block that turns a channel on/off for a specified duration.

    Extra members:
        
    * channel (str) - The channel to work on (e.g. 'CH3').
    """
    def __init__(self, channel, width_ms, name=None, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.channel = channel.lower()
        self.width_ms = width_ms
        self.start_tref_name = "_TSTART_%s" % name
        self.end_tref_name = "_TEND_%s" % name

    def __str__(self):
        return "%s pulse %s for %12.6fms" % (self.base_str(), self.channel, self.width_ms)
        
class Pattern(Block):
    """
    Block that sets all channels to a specific state.
    
    Extra members:
        
    * pattern (int) - bitmask of which channels to turn on.
    """
    def __init__(self, pattern, name=None, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.pattern = pattern
        self.start_tref_name = "_T%s" % name
        self.end_tref_name = "_T%s" % name

    def __str__(self):
        return "%s change to bitpattern %s" % (self.base_str(), self.pattern)

class Delay(Block):
    """
    Block that doesn't affect the channel state, but may
    be useful for setting other time references. Has a 
    defined width.
    """
    def __init__(self, name=None, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.start_tref_name = "__delay__"
        self.end_tref_name = "_T%s" % name

    def __str__(self):
        return "%s delay (end time is %s)" % (self.base_str(), self.end_tref_name)
        
class Tref(Block):
    """
    Block that doesn't affect the channel state, but may
    be useful for setting other time references. No
    defined width.
    """
    def __init__(self, name, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.start_tref_name = "_T%s" % name
        self.end_tref_name = "_T%s" % name

    def __str__(self):
        return "%s tref" % (self.base_str())
        
class Loop(Block):
    """
    Block that can contain child blocks, and can be
    repeated multiple times.

    Extra members:
        
    * num_loops (int) - Number of times to repeat the loop.
    * children (list of Block) - Children of this loop.
    """
    def __init__(self, num_loops, name, tref=None, tref_offset_ms=0, initial_block_order=-1):
        Block.__init__(self, name, tref, tref_offset_ms, initial_block_order)
        self.num_loops = num_loops
        self.children = []
        self.start_tref_name = "_TBEG%s" % name
        self.end_tref_name = "_TEND%s" % name

    def __str__(self):
        return "%s start looping %s times" % (self.base_str(), self.num_loops)
    
    def end_str(self):
        return "%s" % self.end_tref_name

    def add(self, block):
        self.children.append(block)

class Program(Loop):
    """
    Top-level block for containing the entire PPG program.
    """
    def __init__(self):
        Loop.__init__(self, 1, "_____MAIN____", "T0", 0, -1)
        self.start_tref_name = "T0"
        self.end_tref_name = None
        self.time_offset_from_t0_ms = 0
        self.time_offset_from_loop_start_ms = 0
        
    def populate_from_odb(self, odb_blocks, std_width_ms):
        """
        Populate the program from a ODB structure.
        
        Args:
            
        * odb_blocks (`collections.OrderedDict`)
        """
        self.children = []
        prev_block = self
        current_loops = [self]
        skipping_depth = 0
        first_block_of_loop = True
        
        for block_num, (name, sett) in enumerate(odb_blocks.items()):
            block_name = name.upper()
            
            tref = sett.get("time reference", "")
            toff = sett.get("time offset (ms)", 0)
            new_block = None
            
            if tref == "":
                if first_block_of_loop:
                    tref = prev_block.start_tref_name
                else:
                    tref = prev_block.end_tref_name
            
            if skipping_depth > 0 and not block_name.startswith("UNSKIP"):
                continue
            
            first_block_of_loop = False
            
            if block_name.startswith("PUL"):
                channel = sett["ppg signal name"]
                width_ms = sett.get("pulse width (ms)", 0)
                new_block = Pulse(channel, width_ms, block_name, tref, toff, block_num)
            elif block_name.startswith("STD"):
                channel = sett["ppg signal name"]
                width_ms = std_width_ms
                new_block = Pulse(channel, width_ms, block_name, tref, toff, block_num)
            elif block_name.startswith("TRA"):
                channel = sett["ppg signal name"]
                new_block = Transition(channel, block_name, tref, toff, block_num)
            elif block_name.startswith("TURNON"):
                channel = sett["ppg signal name"]
                new_block = TurnOn(channel, block_name, tref, toff, block_num)
            elif block_name.startswith("TURNOFF"):
                channel = sett["ppg signal name"]
                new_block = TurnOff(channel, block_name, tref, toff, block_num)
            elif block_name.startswith("DEL"):
                new_block = Delay(block_name, tref, toff, block_num)
            elif block_name.startswith("TIM"):
                new_block = Tref(block_name, tref, toff, block_num)
            elif block_name.startswith("PAT"):
                patt = sett["bit pattern"]
                new_block = Pattern(patt, block_name, tref, toff, block_num)
            elif block_name.startswith("BEGIN"):
                loop_name = block_name.replace("BEGIN", "")
                if loop_name.startswith("_"):
                    loop_name = loop_name[1:]
                num_loops = sett["loop count"]
                new_block = Loop(num_loops, loop_name, tref, toff, block_num)
                first_block_of_loop = True
            elif block_name.startswith("END"):
                loop_name = block_name.replace("END", "")
                if loop_name.startswith("_"):
                    loop_name = loop_name[1:]
                    
                if current_loops[-1].name != loop_name:
                    raise ValueError("Loop %s ended before loop %s ended" % (loop_name, current_loops[-1].name))
                
                if tref != prev_block.end_tref_name or toff != 0:
                    current_loops[-1].children.append(Delay("DELAY_FOR_END_LOOP_%s" % loop_name, tref, toff, block_num))
                
                prev_block = current_loops[-1]
                current_loops = current_loops[:-1]
                first_block_of_loop = False
            elif block_name.startswith("SKIP"):
                skipping_depth += 1
            elif block_name.startswith("UNSKIP"):
                skipping_depth -= 1
            elif block_name in ["NAMES", "IMAGE"]:
                continue
            else:
                raise ValueError("Unhandled block %s" % block_name)
                
            if new_block is not None:
                current_loops[-1].children.append(new_block)
                prev_block = new_block
                
                if isinstance(new_block, Loop):
                    current_loops.append(new_block)
                    
        if len(current_loops) != 1:
            raise ValueError("Not all loops were ended")
    
    def _convert_loop_to_odb(self, loop, odb_blocks):
        """
        Convert a loop of a program to ODB structure.
        Recurses down to child loops as needed.
        
        Args:
            
        * loop (`Loop` or `Program`) - the loop to analyze
        * odb_blocks (`collections.OrderedDict`) - the object to append to
        """
        for child in loop.children:
            obj = {}
            
            if child.tref != None and child.tref != "":
                obj["time reference"] = child.tref
            
            if child.tref_offset_ms != None and child.tref_offset_ms != 0:
                obj["time offset (ms)"] = child.tref_offset_ms
            
            if isinstance(child, Pulse):
                obj_prefix = "pulse_"
                obj["ppg signal name"] = child.channel
                obj["pulse width (ms)"] = child.width_ms
            elif isinstance(child, Transition):
                obj_prefix = "trans_"
                obj["ppg signal name"] = child.channel
            elif isinstance(child, TurnOn):
                obj_prefix = "turnon_"
                obj["ppg signal name"] = child.channel
            elif isinstance(child, TurnOff):
                obj_prefix = "turnoff_"
                obj["ppg signal name"] = child.channel
            elif isinstance(child, Delay):
                obj_prefix = "delay_"
            elif isinstance(child, Tref):
                obj_prefix = "time_"
            elif isinstance(child, Pattern):
                obj_prefix = "pat_"
                obj["bit pattern"] = child.pattern
            elif isinstance(child, Loop):
                obj_prefix = "begin_"
                obj["loop count"] = child.num_loops
                
            if child.name is None:
                obj_name = "%s%d" % (obj_prefix, len(odb_blocks))
            else:
                obj_name = "%s%s" % (obj_prefix, child.name)
                
            if obj_name in odb_blocks:
                raise ValueError("Duplicate block name %s" % obj_name)
            
            odb_blocks[obj_name] = obj
            
            if isinstance(child, Loop):
                self._convert_loop_to_odb(child, odb_blocks)
                odb_blocks[obj_name.replace("begin", "end")] = {}
                
    def convert_to_odb(self):
        """
        Convert this program to a structure suitable for saving in the ODB.
        
        Returns: `collections.OrderedDict`
        """
        odb_blocks = collections.OrderedDict()
        self._convert_loop_to_odb(self, odb_blocks)
        return odb_blocks
        
class OperationBase:
    """
    Base class for all operations we can program the PPG with.
    Blocks are converted to operations as part of the compilation.
    
    Members:
        
    * hold_ms (float) - How long to wait before starting the operation
        after this one.
    * channel_state (list of bool) - The new state of each channel.
    """
    def __init__(self, hold_ms, channel_state):
        self.hold_ms = hold_ms
        self.channel_state = copy.copy(channel_state)

class OperationTransition:
    """
    Operation that changes the state of channels (on/off).
    """
    def __init__(self, hold_ms, channel_state):
        OperationBase.__init__(self, hold_ms, channel_state)

    def __str__(self):
        state_int = [int(x) for x in self.channel_state]
        return "Transition:  hold %10.6fms with channel state %s" % (self.hold_ms, state_int)
        
class OperationStartLoop:
    """
    Operation that starts a loop.
    
    Extra members:
        
    * num_loops (int) - Number of repetitions.
    * loop_name (str) - Human-readable name of the loop. Will be stored as a comment in the loadfile.
    """
    def __init__(self, hold_ms, num_loops, loop_name, channel_state):
        OperationBase.__init__(self, hold_ms, channel_state)
        self.num_loops = num_loops
        self.loop_name = loop_name
        
    def __str__(self):
        state_int = [int(x) for x in self.channel_state]
        return "Start loop:  hold %10.6fms with channel state %s, start looping %s times" % (self.hold_ms, state_int, self.num_loops)

class OperationEndLoop:
    """
    Operation that ends a loop.
    """
    def __init__(self, hold_ms, channel_state):
        OperationBase.__init__(self, hold_ms, channel_state)
        
    def __str__(self):
        state_int = [int(x) for x in self.channel_state]
        return "End loop:    hold %10.6fms with channel state %s" % (self.hold_ms, state_int)
    
class OperationHalt:
    """
    Operation that halts the PPG.
    """
    def __init__(self):
        OperationBase.__init__(self, 0, 0)
        
    def __str__(self):
        return "Halt: end of program"

class PPGCompiler:
    """
    Main class that converts ODB entries to text that can be loaded onto the PPG.
    
    Compilation flow is ODB -> Blocks -> Operations -> text.
    
    Members:
        
    * freq_Hz (int/float) - PPG clock frequency in Hz.
    * std_width_ms (float) - If a Pulse doesn't specify a width, what width to use.
    * num_channels (int) - Max number of channels this PPG supports.
    """
    def __init__(self):
        self.freq_Hz = 1e8 # Can be updated by frontend
        self.std_width_ms = 0.001 # Can be updated by frontend
        self.num_channels = cycling_framework.ppg.max_channels_triumf # Will update when compile_from_odb_blocks() called.
        
    def odb_to_structure(self, odb_blocks):
        """
        Convert ODB to Blocks. ODB is a flat list, return
        value has structure of nested loops etc.
        
        Args:
            
        * odb_blocks (`collections.OrderedDict`)
        
        Returns:
            `Program` - The main loop, with a start time of T0.
        """
        prog = Program()
        prog.populate_from_odb(odb_blocks, self.std_width_ms)
        return prog
    
    def validate_trefs(self, loop):
        """
        Check that all blocks in this loop reference another block
        in this loop, or the start of the loop. Nonsensical to support
        referencing any other times.
        
        Raises a ValueError if there is a problem.
        
        Args:
            
        * loop (`Loop`)
        """
        child_loops = [b for b in loop.children if isinstance(b, Loop)]
        child_blocks = [b for b in loop.children if not isinstance(b, Loop)]
        
        # Loops can only reference Trefs within their own loop
        for block in child_loops:
            self.validate_trefs(block)
            
        # Build list of defined references
        tref_name_list = [loop.start_tref_name]

        for block in loop.children:
            if not isinstance(block, Loop):
                # Don't allow people to reference the start time of a loop
                # outside of that loop
                tref_name_list.append(block.start_tref_name)
                
            tref_name_list.append(block.end_tref_name)
        
        # Check all blocks reference other time references at the same level
        for block in child_blocks:
            if block.tref not in tref_name_list:
                raise ValueError("Block %s references undefined time reference %s, not in list %s" % (block.name, block.tref, tref_name_list))
    
    def compute_in_loop_time_offsets(self, loop):
        """
        For a given loop, populate time_offset_from_loop_start_ms for all
        children. Also populate width_ms for the loop itself.
        
        Args:
            
        * loop (`Loop`)
        """
        child_loops = [b for b in loop.children if isinstance(b, Loop)]
        
        # Calculate the widths of all child loops first
        for block in child_loops:
            # But these need to know start times of their parent loops...
            block.time_offset_from_loop_start_ms = 0
            loop_len_ms = self.compute_in_loop_time_offsets(block)
            block.width_ms = loop_len_ms * block.num_loops
            
        tref_times = {loop.start_tref_name: loop.time_offset_from_loop_start_ms}
        uncomputed = [b for b in loop.children]
        
        while len(uncomputed):
            computed_this_time = []
            
            for block in uncomputed:
                if block.tref in tref_times:
                    block.time_offset_from_loop_start_ms = tref_times[block.tref] + block.tref_offset_ms
                    tref_times[block.start_tref_name] = block.time_offset_from_loop_start_ms
                    tref_times[block.end_tref_name] = block.time_offset_from_loop_start_ms + block.width_ms
                    computed_this_time.append(block.name)
            
            if len(computed_this_time) == 0:
                raise ValueError("Unable to compute start times for %s" % (",".join("%s %s" % (type(b).__name__, b.name) for b in uncomputed)))   
            
            uncomputed = [b for b in uncomputed if b.name not in computed_this_time]
    
        loop_len_ms = max(b.time_offset_from_loop_start_ms + b.width_ms for b in loop.children) 
        return loop_len_ms

    def add_loop_start_offsets(self, loop, total_offset_ms=0):
        """
        For children in a loop, compute time_offset_from_t0_ms from
        the time offset of the block relative to the loop and the
        time offset of the loop from T0 (the start of the PPG program). 

        Args:
            
        * loop (`Loop`)
        * total_offset_ms (float) - Loop's time offset
        """
        for block in loop.children:
            block.time_offset_from_t0_ms = block.time_offset_from_loop_start_ms + total_offset_ms
            
            if isinstance(block, Loop):
                self.add_loop_start_offsets(block, block.time_offset_from_t0_ms)
                
    def compute_t0_offsets(self, loop):
        """
        Wrapper around `compute_in_loop_time_offsets()` and `add_loop_start_offsets()`.
        
        Args:
            
        * loop (`Loop`)
        """
        self.compute_in_loop_time_offsets(loop)
        self.add_loop_start_offsets(loop)
        
        total_len_ms = max(b.time_offset_from_t0_ms + b.width_ms for b in loop.children) 
        return total_len_ms
        
    def validate_t0_offsets(self, loop):
        """
        Sanity-check the time offsets a user has specified - we don't
        allow any out-of-loop operations to happen while a loop is
        supposed to be running.
        
        Raises a ValueError if a problem is detected.

        Args:
            
        * loop (`Loop`)
        """
        # Check that no other transitions happen while a loop should be running
        loops_active = {}
        child_loops = [b for b in loop.children if isinstance(b, Loop)]
        
        for block in child_loops:
            # Check sub-loops first
            self.validate_t0_offsets(block)
        
        # Now this loop - get time when subloops are running
        for block in child_loops:
            loops_active[block.name] = (block.time_offset_from_t0_ms, block.time_offset_from_t0_ms + block.width_ms)
            
        # Check if any other items overlap with subloops running
        for block in loop.children:
            start = block.time_offset_from_t0_ms
            end = start + block.width_ms
            
            for subloop_name, (subloop_start, subloop_end) in loops_active.items():
                if subloop_name == block.name:
                    continue
                    
                if end <= (subloop_start + 1e-8) or start >= (subloop_end - 1e-8):
                    continue
                
                raise ValueError("Block %s overlaps with loop %s - not allowed. Times are %f-%f and %f-%f" % (block.name, subloop_name, start, end, subloop_start, subloop_end))
    
    def covert_pulses_to_transitions(self, loop):
        """
        Convert any Pulse object to two Transition objects.
        After this, all objects in a loop are "zero-width".

        Args:
            
        * loop (`Loop`)
        """
        child_loops = [b for b in loop.children if isinstance(b, Loop)]
        child_pulses = [b for b in loop.children if isinstance(b, Pulse)]
        
        for block in child_loops:
            self.covert_pulses_to_transitions(block)
        
        for block in child_pulses:
            if block.width_ms == 0:
                continue
            
            tr_start = Transition(block.channel, "TRANS_FROM_START_%s" % block.name, block.tref, block.tref_offset_ms, block.initial_block_order)
            tr_start.start_tref_name = block.start_tref_name
            tr_end = Transition(block.channel, "TRANS_FROM_END_%s" % block.name, block.tref, block.tref_offset_ms + block.width_ms, block.initial_block_order + 0.5)
            tr_end.start_tref_name = block.end_tref_name
            tr_start.time_offset_from_t0_ms = block.time_offset_from_t0_ms
            tr_end.time_offset_from_t0_ms = block.time_offset_from_t0_ms + block.width_ms
            
            loop.children.append(tr_start)
            loop.children.append(tr_end)
        
            loop.children = [c for c in loop.children if c not in child_pulses]
    
    def sort_loop(self, loop):
        """
        Sort the blocks within a loop to be in increasing time offset from
        the start of the loop.

        Args:
            
        * loop (`Loop`)
        """
        child_loops = [b for b in loop.children if isinstance(b, Loop)]
        
        for block in child_loops:
            self.sort_loop(block)
        
        def t0_compare(a, b):
            if abs(a.time_offset_from_t0_ms - b.time_offset_from_t0_ms) < 1e-7:
                return a.initial_block_order - b.initial_block_order
            else:
                return a.time_offset_from_t0_ms - b.time_offset_from_t0_ms
        
        loop.children.sort(key=functools.cmp_to_key(t0_compare))
        
    def channel_name_to_index(self, ch_name):
        """
        Convert a channel name like "CH3" to a 0-based channel index (2).
        
        Args:
            
        * ch_name (str)
        
        Returns:
            int
        """
        idx = int(ch_name.lower().replace("ch", "")) - 1
        
        if idx < 0 or idx >= self.num_channels:
            raise ValueError("Invalid channel name, valid range is 0-%s" % self.num_channels)
        
        return idx
        
    def get_first_non_tref_block(self, block_list):
        """
        Get the first block in the list that isn't a `Tref` block.
        
        Args:
            
        * block_list (list of `Block`)
        
        Returns:
            `Block`
        """
        
        for block in block_list:
            if not isinstance(block, Tref):
                return block
        
        return None
            
    def compute_operations(self, loop, op_list=None, curr_state=None, add_halt=False, loop_depth=0):
        """
        Convert Blocks to Operations.
        
        Args:
            
        * loop (`Loop`)
        * op_list (list of `OperationBase`) - Will be added to. Will be created if needed.
        * curr_state (list of bool) - Current on/off state of each channel.
        * add_halt (bool) - Whether to automatically add an OperationHalt at the end of this loop.
        * loop_depth (int) - How nested our loop structure is. PPGs only allow them to be 16-deep.
        
        Returns:
            list of `OperationBase`
        """
        if loop_depth > 16:
            raise ValueError("Maximum loop depth exceeded - restructure your PPG program")
        
        if op_list is None:
            curr_state = [False] * self.num_channels
            op_list = []
            
            first_child = self.get_first_non_tref_block(loop.children)
            
            if first_child is None:
                first_delay = 0
            else:
                first_delay = first_child.time_offset_from_t0_ms
            
            if first_delay > 0:
                op_list.append(OperationTransition(first_delay, curr_state))
        
        # Blocks are sorted by start time already.
        for i, block in enumerate(loop.children):
            if i == len(loop.children) - 1:
                # End of loop
                hold_ms = 0
            elif isinstance(block, Loop):
                # Starting new loop.
                # Hold state until first operation within loop.
                first_child = self.get_first_non_tref_block(block.children)
                
                if first_child is None:
                    hold_ms = 0
                else:
                    hold_ms = first_child.time_offset_from_t0_ms - block.time_offset_from_t0_ms
            else:
                # Normal operation within loop.
                next_block = self.get_first_non_tref_block(loop.children[i+1:])
                
                if next_block is None:
                    hold_ms = 0
                else:
                    hold_ms = next_block.time_offset_from_t0_ms - block.time_offset_from_t0_ms
            
            if hold_ms < 0:
                if hold_ms > -1e-7:
                    hold_ms = 0
                else:
                    raise ValueError("Negative hold time encountered for block %s: %d ms" % (block.name, hold_ms))
            
            if isinstance(block, Loop):
                # Number of loop iterations is 20-bit field for both PulseBlaster and TRIUMF PPGs
                # Write loop content multiple times if more iterations required.
                num_loops_left = block.num_loops
                max_loops = 0xFFFFF
                new_loop_depth = loop_depth + 1
                
                while num_loops_left > 0:
                    this_loops = min(num_loops_left, max_loops)
                    num_loops_left -= this_loops
                    
                    op_list.append(OperationStartLoop(hold_ms, this_loops, block.name, curr_state))
                    self.compute_operations(block, op_list, curr_state, loop_depth=new_loop_depth)
                    op_list.append(OperationEndLoop(0, curr_state))
                    
                    if num_loops_left == 0 and i < len(loop.children) - 1:
                        next_block = self.get_first_non_tref_block(loop.children[i+1:])
                        post_end_loop_hold_ms = next_block.time_offset_from_t0_ms - block.time_offset_from_t0_ms - block.width_ms
                        op_list.append(OperationTransition(post_end_loop_hold_ms, curr_state))
                    
            elif isinstance(block, (Transition, TurnOn, TurnOff, Pattern, Delay)):
                if isinstance(block, Transition):
                    ch_idx = self.channel_name_to_index(block.channel)
                    curr_state[ch_idx] = not curr_state[ch_idx]
                elif isinstance(block, TurnOn):
                    ch_idx = self.channel_name_to_index(block.channel)
                    curr_state[ch_idx] = True
                elif isinstance(block, TurnOff):
                    ch_idx = self.channel_name_to_index(block.channel)
                    curr_state[ch_idx] = False
                elif isinstance(block, Delay):
                    pass
                elif isinstance(block, Pattern):
                    for ch_idx in range(len(curr_state)):
                        curr_state[ch_idx] = (self.bitpattern & (1 << ch_idx)) > 0
                
                op_list.append(OperationTransition(hold_ms, curr_state))
                        
            elif isinstance(block, Tref):
                # Nothing to do for a time ref block
                pass
            else:
                raise TypeError("Unexpected block type %s" % type(block))
        
        if add_halt:
            op_list.append(OperationHalt())
        
        return op_list
        
    def optimize_operations(self, op_list, print_debug, iteration=0):
        """
        Optimize the PPG operations to be performed.
        
        Args:
            
        * op_list (list of `OperationBase`)
        
        Returns:
            list of `OperationBase`
        """
        new_op_list = []
        ignore_next = False
        
        for i, op in enumerate(op_list):
            if ignore_next:
                ignore_next = False
                continue
            
            if i < len(op_list) - 1:
                next_op = op_list[i+1]
                this_op_zero_len = abs(op.hold_ms) < 1e-10
                next_op_zero_len = abs(next_op.hold_ms) < 1e-10
                
                if isinstance(op, OperationTransition) and next_op_zero_len and isinstance(next_op, OperationEndLoop):
                    # Only do if same?
                    next_op.channel_state = op.channel_state
                    next_op.hold_ms = op.hold_ms
                    continue
                    
                if isinstance(op, OperationTransition) and next_op_zero_len and isinstance(next_op, OperationTransition) and op.channel_state == next_op.channel_state:
                    # Merge this transition with the next transition
                    next_op.hold_ms = op.hold_ms
                    continue

                if this_op_zero_len:
                    if isinstance(op, OperationTransition) and not isinstance(next_op, OperationHalt):
                        # Simply merge transitions that happen at the same time
                        continue
                    
                    if isinstance(op, OperationStartLoop) and isinstance(next_op, OperationTransition):
                        # Merge the start loop and the first transition if they're at the same time
                        ignore_next = True
                        op.hold_ms = next_op.hold_ms
                        op.channel_state = next_op.channel_state
                        
            new_op_list.append(op)
        
        # See if we made any change to the operation list this time.
        # If so, we'll run the optimizer again.
        if len(op_list) == len(new_op_list):
            if print_debug:
                print("\nOptimization complete")
        elif iteration > 100:
            print("\nQuitting optimization after %d iterations" % iteration)
        else:
            if print_debug:
                print("\nOptimised operation list after iteration %d:" % iteration)
                self.print_oplist(new_op_list)
            
            new_op_list = self.optimize_operations(new_op_list, print_debug, iteration + 1)
        
        return new_op_list
        
    def min_hold_cycles(self, for_pulseblaster):
        """
        Get the minimum delay length allowed for the PPG.
        
        Args:
        * for_pulseblaster (bool) - Minimum delay varies based on
            which type of PPG we have.
        """
        return 2 if for_pulseblaster else 0
        
    def hold_ms_to_cycles(self, hold_ms, for_pulseblaster):
        """
        Convert a delay in ms to number of cycles.
        
        Args:
            
        * hold_ms (float)
        * for_pulseblaster (bool) - Minimum delay varies based on
            which type of PPG we have.
        """
        base = round(self.freq_Hz * hold_ms / 1000.)
        minimum = self.min_hold_cycles(for_pulseblaster)
        
        if for_pulseblaster:
            # Min delay 2, actual delay N + 3 cycles
            return max(minimum, base - 3)
        else:
            return max(minimum, base)
        
    def opcode_data_pulseblaster(self, op, loop_instr_ids):
        """
        Get the opcode to use in a pulseblaster PPG.
        
        Args:
            
        * op (`OperationBase`)
        * loop_instr_ids (list of int) - List of instruction IDs of 
            loops that are currently open.
            
        Returns:
            2-tuple of (int, int) for (op_code, data)
        """
        if isinstance(op, OperationStartLoop):
            return (2, op.num_loops - 1)
        elif isinstance(op, OperationEndLoop):
            return (3, loop_instr_ids[-1])
        elif isinstance(op, OperationTransition):
            return (0, 0)
        elif isinstance(op, OperationHalt):
            return (1, 0)

    def opcode_triumf(self, op, loop_instr_ids):
        """
        Get the opcode to use in a TRIUMF PPG.
        This PPG version puts data and opcode into single value
        
        Args:
            
        * op (`OperationBase`)
        * loop_instr_ids (list of int) - List of instruction IDs of 
            loops that are currently open.
            
        Returns:
            int
        """
        if isinstance(op, OperationStartLoop):
            return (2 << 20) + op.num_loops
        elif isinstance(op, OperationEndLoop):
            return (3 << 20) + loop_instr_ids[-1]
        elif isinstance(op, OperationTransition):
            return (1 << 20)
        elif isinstance(op, OperationHalt):
            return 0
    
    def add_instr_line(self, instr_lines, bit_set, hold, op, loop_instr_ids, extra_comment, for_pulseblaster):
        """
        Convert an Operation to a text line, and add it to `instr_lines`.
        
        Args:
            
        * instr_lines (list of str) - Instructions we'll add to.
        * bit_set (int) - Channels currently on.
        * hold (float) - Delay in cycles.
        * op (`OperationBase`) - The new operation to perform.
        * loop_instr_ids (list of int) - List of instruction IDs of loops that are currently open.
        * extra_comment (str) - Any extra comment we should add to this line.
        * for_pulseblaster (bool) - PPG type
        """
        instr_id = len(instr_lines)
        
        if for_pulseblaster:
            # opcode, data, delay, ch_set
            opcode_int, data_int = self.opcode_data_pulseblaster(op, loop_instr_ids)
            opcode_str = ("%X" % opcode_int).rjust(4)
            data_str = ("%X" % data_int).rjust(20)
            hold_str = ("%X" % hold).rjust(32)
            ch_set_str = "%04X" % bit_set
            instr_lines.append("%s\t%s\t%s\t%s" % (opcode_str, data_str, hold_str, ch_set_str))
        else:
            # Instr ID, ch_set, ch_clear, delay, opcode
            opcode = self.opcode_triumf(op, loop_instr_ids)
            bit_clr = 0xffffffff - bit_set
            comment = "# %s; %s" % (bin(bit_set)[2:].rjust(self.num_channels,'0'), extra_comment)
            instr_lines.append("%03d 0x%08x 0x%08x  0x%08x 0x%06x %s" % (instr_id, bit_set, bit_clr, hold, opcode, comment))

    def create_ppg_loadfile_content(self, op_list, for_pulseblaster):
        """
        Create content that would be written to ppgload.dat (for TRIUMF PPG) or
        bytecode.dat (for pulseblaster).
        
        Args:
            
        * op_list (list of `OperationBase`)
        * for_pulseblaster (bool)
        
        Returns:
            (str, {int: str}) - loadfile content; names of loops starting at an instruction ID
        """
        instr_lines = []
        loop_instr_ids = []
        known_loop_names = {}
        prev_bit_set = 0
        max_hold = 0xffffffff
        
        for i, op in enumerate(op_list):
            extra_comment = ""
            hold = self.hold_ms_to_cycles(op.hold_ms, for_pulseblaster)
            
            while hold > max_hold:
                # Handle 32-bit rollover delays
                bonus_op = OperationTransition(0, None)
                extra_comment = "Delay auto-added to avoid 32-bit rollover"
                self.add_instr_line(instr_lines, prev_bit_set, max_hold, bonus_op, loop_instr_ids, extra_comment, for_pulseblaster)
                hold -= max_hold

            bit_set = 0
            
            if not isinstance(op, OperationHalt):
                for ch_idx, ch_state in enumerate(op.channel_state):
                    if ch_state:
                        bit_set |= (1 << ch_idx)
                        
                prev_bit_set = bit_set
                
            if isinstance(op, OperationStartLoop):
                extra_comment = op.loop_name
                
            self.add_instr_line(instr_lines, bit_set, hold, op, loop_instr_ids, extra_comment, for_pulseblaster)

            if isinstance(op, OperationStartLoop):
                loop_instr_ids.append(len(instr_lines) - 1)
                known_loop_names[len(instr_lines) - 1] = op.loop_name
            elif isinstance(op, OperationEndLoop):
                loop_instr_ids = loop_instr_ids[:-1]

        retval = []
        
        if for_pulseblaster:
            retval.append("Op_Code Size 4")
            retval.append("Branch Size 20")
            retval.append("Delay Size 32")
            retval.append("Flag Size 24")
            retval.append("Instruction Lines %s" % len(instr_lines))
            retval.append("Port Address 8000")
            retval.append("")
        else:
            retval.append("#Created by python PPG compiler")
            retval.append("#Ins 0=halt 1=cont 2=loop 3=endloop")
            retval.append("#Note: PC is decimal; bitpats,delay,ins/data are hex")
            retval.append("#PC set bitpat clr bitpat      delay  ins/data")
            retval.append("Num Instruction Lines = %03d           # IDDDDD   set bitpattern: bits %s to 0      loopname" % (len(instr_lines), self.num_channels-1))
        
        retval.extend(instr_lines)
        text = "\n".join(retval)
        
        return (text, known_loop_names)

    def print_blocks(self, loop, indent=0):
        """
        Print Blocks to screen.
        
        Args:
            
        * loop (`Loop`)
        * indent (int) - Number of spaces to prepend.
        """
        for block in loop.children:
            print((" " * indent) + "%s" % block)
            
            if isinstance(block, Loop):
                self.print_blocks(block, indent+2)
                print((" " * indent) + block.end_str())
    
    def print_oplist(self, op_list):
        """
        Print Operations to screen.
        
        Args:
            
        * op_list (list of `OperationBase`).
        """
        for op in op_list:
            print(op)

    def compile_from_odb_blocks(self, odb_blocks, for_pulseblaster=False, print_debug=False):
        """
        Main function that marshals the data from ODB -> Blocks -> Operations -> text.
        
        Args:
            
        * odb_blocks (`collections.OrderedDict`)
        * for_pulseblaster (bool) - PPG type
        * print_debug (bool) - Whether to print debug information to screen.
        
        Returns:
            str - The text that should be loaded onto the PPG by C/C++ code.
        """
        self.num_channels = cycling_framework.ppg.max_channels_pulseblaster if for_pulseblaster else cycling_framework.ppg.max_channels_triumf
        
        # Build basic structure
        main_loop = self.odb_to_structure(odb_blocks)

        if print_debug:
            print("Basic structure:")
            self.print_blocks(main_loop)
        
        # Ensure all blocks reference time references that have been defined
        self.validate_trefs(main_loop)
        
        # Compute time offsets from t0
        # - expanding loops as needed to get end of loop time offset
        # - computing just 1st loop for ones within loop
        self.compute_t0_offsets(main_loop)
        
        # Ensure no pulses overlap with loops
        self.validate_t0_offsets(main_loop)
        
        # Convert pulses to two transitions to make future logic simpler
        self.covert_pulses_to_transitions(main_loop)
        
        # Sort transitions according to time offset from t0
        self.sort_loop(main_loop)
        
        if print_debug:
            print("\nSorted blocks:")
            self.print_blocks(main_loop)
        
        # Compute transitions and delays
        add_halt = not for_pulseblaster
        op_list = self.compute_operations(main_loop, add_halt=add_halt)
        
        if print_debug:
            print("\nOperation list:")
            self.print_oplist(op_list)
        
        # Compactify any operations that can be optimised
        op_list = self.optimize_operations(op_list, print_debug)
            
        # Create byte code
        (loadtext, known_loop_names) = self.create_ppg_loadfile_content(op_list, for_pulseblaster)
        
        if print_debug:
            print("\nLoadfile content:")
            print(loadtext)
        
        return (loadtext, known_loop_names)
        