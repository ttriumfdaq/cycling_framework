import unittest
import cycling_framework.ppg.compiler
import cycling_framework.ppg.simulator
import collections
import os.path

class TestPpgCompiler(unittest.TestCase):
    def setUp(self):
        self.compiler = cycling_framework.ppg.compiler.PPGCompiler()
        
    def testBasic(self):
        pulse_1 = {"time reference": "T0", "time offset (ms)": 300, "ppg signal name": "CH1", "pulse width (ms)": 10}
        pulse_2 = {"time reference": "_TEND_PULSE_1", "time offset (ms)": 400, "ppg signal name": "CH2", "pulse width (ms)": 10}
        
        odb_blocks = collections.OrderedDict([("Pulse_2", pulse_2), ("Pulse_1", pulse_1)])

        for for_pb in [False, True]:
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            sim = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, for_pulseblaster=for_pb)
            
            self.assertFalse(sim.ch_on_at_time_ms(0, 295))
            self.assertTrue(sim.ch_on_at_time_ms(0, 305))
            self.assertFalse(sim.ch_on_at_time_ms(0, 315))
            self.assertFalse(sim.ch_on_at_time_ms(0, 705))
            self.assertFalse(sim.ch_on_at_time_ms(0, 715))
            self.assertFalse(sim.ch_on_at_time_ms(0, 725))
            
            self.assertFalse(sim.ch_on_at_time_ms(1, 295))
            self.assertFalse(sim.ch_on_at_time_ms(1, 305))
            self.assertFalse(sim.ch_on_at_time_ms(1, 315))
            self.assertFalse(sim.ch_on_at_time_ms(1, 705))
            self.assertTrue(sim.ch_on_at_time_ms(1, 715))
            self.assertFalse(sim.ch_on_at_time_ms(1, 725))
        
    def testInverted(self):
        pulse_1 = {"time reference": "T0", "time offset (ms)": 300, "ppg signal name": "CH1", "pulse width (ms)": 10}
        pulse_2 = {"time reference": "_TEND_PULSE_1", "time offset (ms)": 400, "ppg signal name": "CH2", "pulse width (ms)": 10}
        
        odb_blocks = collections.OrderedDict([("Pulse_2", pulse_2), ("Pulse_1", pulse_1)])

        for for_pb in [False, True]:
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            sim = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, for_pulseblaster=for_pb, invert_mask=0x1)
            
            self.assertTrue(sim.ch_on_at_time_ms(0, 295))
            self.assertFalse(sim.ch_on_at_time_ms(0, 305))
            self.assertTrue(sim.ch_on_at_time_ms(0, 315))
            self.assertTrue(sim.ch_on_at_time_ms(0, 705))
            self.assertTrue(sim.ch_on_at_time_ms(0, 715))
            self.assertTrue(sim.ch_on_at_time_ms(0, 725))
            
            self.assertFalse(sim.ch_on_at_time_ms(1, 295))
            self.assertFalse(sim.ch_on_at_time_ms(1, 305))
            self.assertFalse(sim.ch_on_at_time_ms(1, 315))
            self.assertFalse(sim.ch_on_at_time_ms(1, 705))
            self.assertTrue(sim.ch_on_at_time_ms(1, 715))
            self.assertFalse(sim.ch_on_at_time_ms(1, 725))
        
    def testLoop(self):
        pulse_1 = {"time reference": "T0", "time offset (ms)": 100, "ppg signal name": "CH1", "pulse width (ms)": 10}
        loop_start = {"time reference": "_TEND_PULSE_1", "time offset (ms)": 400, "loop count": 3}
        pulse_a = {"time reference": "_TBEGLOOP", "time offset (ms)": 10, "ppg signal name": "CH2", "pulse width (ms)": 5}
        loop_end = {}
        pulse_2 = {"time reference": "_TENDLOOP", "time offset (ms)": 400, "ppg signal name": "CH2", "pulse width (ms)": 10}
        
        odb_blocks = collections.OrderedDict([("Pulse_1", pulse_1), 
                                              ("begin_loop", loop_start),
                                              ("Pulse_A", pulse_a),
                                              ("end_loop", loop_end),
                                              ("Pulse_2", pulse_2)])

        for for_pb in [False, True]:
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            
            sim_1loop = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, expand_loops=False, for_pulseblaster=for_pb)
    
            self.assertFalse(sim_1loop.ch_on_at_time_ms(0, 95))
            self.assertTrue(sim_1loop.ch_on_at_time_ms(0, 105))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(0, 115))
            
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 105))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 515))
            self.assertTrue(sim_1loop.ch_on_at_time_ms(1, 522))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 920))
            self.assertTrue(sim_1loop.ch_on_at_time_ms(1, 930))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 940))
    
            sim_3loop = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, expand_loops=True, for_pulseblaster=for_pb)
        
            self.assertFalse(sim_3loop.ch_on_at_time_ms(0, 95))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(0, 105))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(0, 115))
            
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 105))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 515))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 522))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 530))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 537))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 545))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 552))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 950))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 960))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 970))
            
    def testEndLoopDelay(self):
        pulse_1 = {"time reference": "T0", "time offset (ms)": 100, "ppg signal name": "CH1", "pulse width (ms)": 10}
        loop_start = {"time reference": "_TEND_PULSE_1", "time offset (ms)": 400, "loop count": 3}
        pulse_a = {"time reference": "_TBEGLOOP", "time offset (ms)": 10, "ppg signal name": "CH2", "pulse width (ms)": 5}
        loop_end = {"time offset (ms)": 300}
        pulse_2 = {"time reference": "_TENDLOOP", "time offset (ms)": 400, "ppg signal name": "CH2", "pulse width (ms)": 10}
        
        odb_blocks = collections.OrderedDict([("Pulse_1", pulse_1), 
                                              ("begin_loop", loop_start),
                                              ("Pulse_A", pulse_a),
                                              ("end_loop", loop_end),
                                              ("Pulse_2", pulse_2)])

        for for_pb in [False, True]:
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            
            sim_1loop = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, expand_loops=False, for_pulseblaster=for_pb)
    
            self.assertFalse(sim_1loop.ch_on_at_time_ms(0, 95))
            self.assertTrue(sim_1loop.ch_on_at_time_ms(0, 105))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(0, 115))
            
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 105))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 515))
            self.assertTrue(sim_1loop.ch_on_at_time_ms(1, 522))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 1220))
            self.assertTrue(sim_1loop.ch_on_at_time_ms(1, 1230))
            self.assertFalse(sim_1loop.ch_on_at_time_ms(1, 1240))
    
            sim_3loop = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, expand_loops=True, for_pulseblaster=for_pb)
            
            self.assertFalse(sim_3loop.ch_on_at_time_ms(0, 95))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(0, 105))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(0, 115))
            
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 105))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 515))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 522))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 530))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 832))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 837))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 845))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 1152))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 1850))
            self.assertTrue(sim_3loop.ch_on_at_time_ms(1, 1860))
            self.assertFalse(sim_3loop.ch_on_at_time_ms(1, 1870))
            

    def testLongDelay(self):
        pulse_1 = {"time reference": "T0", "time offset (ms)": 600000, "ppg signal name": "CH1", "pulse width (ms)": 10}
        odb_blocks = collections.OrderedDict([("Pulse_1", pulse_1)])
        
        for for_pb in [False, True]:
    
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            
            sim = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, for_pulseblaster=for_pb)
            
            self.assertFalse(sim.ch_on_at_time_ms(0, 600000-1))
            self.assertTrue(sim.ch_on_at_time_ms(0, 600000+1))
            self.assertFalse(sim.ch_on_at_time_ms(0, 600000+11))

    def testTref(self):
        tref = {"time reference": "T0", "time offset (ms)": 300}
        pulse = {"time reference": "_TTIME_T1", "time offset (ms)": 400, "ppg signal name": "CH1", "pulse width (ms)": 10}
        
        odb_blocks = collections.OrderedDict([("TIME_T1", tref), ("Pulse_1", pulse)])

        for for_pb in [False, True]:
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            sim = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, for_pulseblaster=for_pb)
            
            self.assertFalse(sim.ch_on_at_time_ms(0, 695))
            self.assertTrue(sim.ch_on_at_time_ms(0, 705))
            self.assertFalse(sim.ch_on_at_time_ms(0, 715))


    def testPlot(self):
        pulse_1 = {"time reference": "T0", "time offset (ms)": 100, "ppg signal name": "CH1", "pulse width (ms)": 10}
        loop_start = {"time reference": "_TEND_PULSE_1", "time offset (ms)": 400, "loop count": 3}
        pulse_a = {"time reference": "_TBEGLOOP", "time offset (ms)": 10, "ppg signal name": "CH2", "pulse width (ms)": 5}
        loop_b_start = {"time reference": "_TEND_PULSE_A", "time offset (ms)": 100, "loop count": 4}
        pulse_b = {"time reference": "_TBEGLOOPB", "time offset (ms)": 20, "ppg signal name": "CH3", "pulse width (ms)": 50}
        loop_b_end = {}
        loop_end = {"time reference": "_TENDLOOPB", "time offset (ms)": 1000}
        pulse_2 = {"time reference": "_TENDLOOP", "time offset (ms)": 400, "ppg signal name": "CH2", "pulse width (ms)": 10}
        
        odb_blocks = collections.OrderedDict([("Pulse_1", pulse_1), 
                                              ("begin_loop", loop_start),
                                              ("Pulse_A", pulse_a),
                                              ("begin_loopb", loop_b_start),
                                              ("Pulse_B", pulse_b),
                                              ("end_loopb", loop_b_end),
                                              ("end_loop", loop_end),
                                              ("Pulse_2", pulse_2)])

        for for_pb in [False, True]:
            img_suffix = "pulseblaster" if for_pb else "triumf"
            (ppgload, loop_names) = self.compiler.compile_from_odb_blocks(odb_blocks, for_pulseblaster=for_pb)
            
            test_image = "test_image_1loop_%s.png" % img_suffix
            
            if os.path.exists(test_image):
                os.unlink(test_image)
            
            sim = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, for_pulseblaster=for_pb)
            sim.plot(test_image)
            
            self.assertTrue(os.path.exists(test_image))
            
            test_image = "test_image_3loop_%s.png" % img_suffix
            
            if os.path.exists(test_image):
                os.unlink(test_image)
            
            names = ["Equip 1", "Equipment 2", "Third equip"]
            sim = cycling_framework.ppg.simulator.PPGSimulator(text=ppgload, expand_loops=True, for_pulseblaster=for_pb)
            sim.plot(test_image, names)
            
            self.assertTrue(os.path.exists(test_image))

if __name__ == '__main__':
    unittest.main()
