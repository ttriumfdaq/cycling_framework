"""
Tool to simulate the operation of a PPG. 
"""

import cycling_framework.ppg
import datetime

class PPGSimulator:
    """
    The class simulates a PPG, by parsing a loadfile and telling
    you when certain channels would be on/off.
    
    It is useful for testing PPG compilation logic, and for helping
    to create images displaying the pattern (using matplotlib).
    
    Members:
        
    * for_pulseblaster (bool) - Whether we're simulating a PulseBlaster or TRIUMF PPG.
    * num_channels (int) - Number of channels to simulate.
    * freq_Hz (int/float) - Clock frequency.
    * invert (int) - Bitmask of which channels to invert.
    * instructions (list of lists) - Instructions parsed (format of inner list depends on PPG type).
    * loop_times_ms (dict of {int: list of [float, float]}) - Start/end times of each 
        iteration of each loop, keyed by instruction ID.
    * channel_on_times_ms (dict of {int: list of (float, float)}) - Start/end times of each channel
        being on, keyed by channel ID.
    * end_time_ms (float) - Overall time that PPG program will take to run.
    * expanded_loops (bool) - Whether we expanded loops for the most recent simiulation.
    """
    def __init__(self, filename=None, text=None, expand_loops=False, for_pulseblaster=False, freq_Hz=1e8, invert_mask=0x0, known_loop_names={}, exclude_expand_loop_names=[]):
        self.for_pulseblaster = for_pulseblaster
        self.num_channels = cycling_framework.ppg.max_channels_pulseblaster if for_pulseblaster else cycling_framework.ppg.max_channels_triumf
        self.freq_Hz = freq_Hz
        self.invert = [invert_mask & (1<<i) for i in range(self.num_channels)]
        
        self.instructions = []
        self.loop_times_ms = {}
        self.true_loop_counts = {}
        self.sim_loop_counts = {}
        self.channel_on_times_ms = {i: [] for i in range(self.num_channels)}
        self.end_time_ms = None
        self.expanded_loops = None
        
        self.reset()
        
        self.loop_names = known_loop_names

        if filename is not None:
            self._load_from_file(filename)
            
        if text is not None:
            self._load_from_text(text)
            
        self.simulate(expand_loops, exclude_expand_loop_names)
    
    def ch_on_at_time_ms(self, ch_idx, time_ms):
        """
        Whether a channel will be on a certain time.
        
        Args:
            
        * ch_idx (int) - 0-based channel index.
        * time_ms (float)
        
        Returns:
            bool
        """
        if ch_idx not in self.channel_on_times_ms:
            return False
        
        on_regions = self.channel_on_times_ms[ch_idx]
        
        for region in on_regions:
            if region[0] < time_ms and region[1] > time_ms:
                return not self.invert[ch_idx]
            
        return self.invert[ch_idx]
    
    def reset(self):
        """
        Reset the state of our simulation (forget we parsed a file).
        """
        self.channel_on_times_ms = {i: [] for i in range(self.num_channels)}
        self.instructions = []
        self.loop_times_ms = {}
        self.true_loop_counts = {}
        self.sim_loop_counts = {}
        self.end_time_ms = None
        self.expanded_loops = None
        self.loop_names = {}
        
    def _load_from_file(self, filename):
        """
        Load and parse a file that would be loaded onto the PPG.
        
        
        Args:
            
        * filename (str)
        """
        with open(filename) as f:
            lines = f.readlines()
            self._load_from_text(lines)
            
    def _load_from_text(self, text):
        """
        Parse text that would be loaded onto the PPG.
        Fill `self.instructions`.
        
        Args:
            
        * text (str)
        """
        if not isinstance(text, list):
            text = text.splitlines()
        
        # Number of instructions we expect to load
        num_instr = None
        
        # Number of items specified for each instruction line
        expected_info_len = 4 if self.for_pulseblaster else 5
        
        for line in text:
            # Strip out comments
            if line.find("#") == -1:
                comment = ""
            else:
                comment = line[line.find("#"):]
                line = line[:line.find("#")]
            
            line = line.strip()
            
            if len(line) == 0:
                # Just a comment line
                continue
            
            if self.for_pulseblaster:
                if line.lower().startswith("inst"):
                    num_instr = int(line.lower().replace("instruction lines", ""))
                    continue
                if line.lower().find("size") != -1 or line.lower().find("port") != -1:
                    continue
            else:
                if line.lower().startswith("num"):
                    num_instr = int(line.lower().replace("num instruction lines =", ""))
                    continue
                
            bits = line.split()
            
            if len(bits) != expected_info_len:
                raise ValueError("Unexpected format (%s words instead of %s) in line '%s'" % (len(bits), expected_info_len, line))
            
            bits.append(comment)
            
            self.instructions.append(bits)
                
        if num_instr is None:
            raise ValueError("Didn't find line saying how many instructions to expect")
        
        if num_instr != len(self.instructions):
            raise ValueError("Didn't extract expected number of instructions")
        
    def simulate(self, expand_loops=False, exclude_expand_loop_names=[]):
        """
        Actually simulate the operation of a PPG, using the content of `self.instructions`.
        Main members that are filled are self.channel_on_times_ms and self.end_time_ms.
        
        Args:
            
        * expand_loops (bool) - Whether to pretend we're only running each loop once,
            or actually run them all (up to some maximum number).
        """
        time_now_ms = 0
        ch_on_time_ms = {i: None for i in range(self.num_channels)}
        ch_on_bitpat = 0
        instr_id = 0
        loops_remaining = {}
        eval_count = 0
        max_expanded = 10000
        self.expanded_loops = expand_loops
        
        exclude_expand_loop_names = [s.upper() for s in exclude_expand_loop_names]
        
        opcode_tr = 0 if self.for_pulseblaster else 1 # Transition
        opcode_sl = 2 # Start loop
        opcode_el = 3 # End loop
        opcode_halt = 1 if self.for_pulseblaster else 0 # Halt program
        
        while instr_id < len(self.instructions):
            if self.for_pulseblaster:
                (opcode_hex, data_hex, delay_hex, set_hex, comment) = self.instructions[instr_id]
                opcode = int(opcode_hex, 16)
                data = int(data_hex, 16)
                delay_cyc = int(delay_hex, 16) + 3
                set_bitpat = int(set_hex, 16)
                clr_bitpat = ~set_bitpat
            else:
                (this_instr_id, set_hex, clr_hex, delay_hex, inst_data_hex, comment) = self.instructions[instr_id]
                inst_data = int(inst_data_hex, 16)
                opcode = inst_data >> 20
                data = inst_data & 0xFFFFF
                delay_cyc = int(delay_hex, 16)
                set_bitpat = int(set_hex, 16)
                clr_bitpat = int(clr_hex, 16)
            
                if int(this_instr_id) != instr_id:
                    raise ValueError("Unexpected instruction ID - %s != %s" % (this_instr_id, instr_id))
            
            delay_ms = delay_cyc / (self.freq_Hz / 1000)
            
            # Transition
            ch_on_bitpat |= set_bitpat
            ch_on_bitpat &= ~clr_bitpat
                    
            for i in range(self.num_channels):
                if ch_on_bitpat & (1 << i):
                    # Channel is now on
                    if ch_on_time_ms[i] is None:
                        # Channel just turned on
                        ch_on_time_ms[i] = time_now_ms
                    else:
                        # Nothing to do - channel was already on
                        pass
                else:
                    # Channel is off
                    if ch_on_time_ms[i] is None:
                        # Nothing to do - channel was already off
                        pass
                    else:
                        # Channel just turned off - record pulse in global list
                        self.channel_on_times_ms[i].append((ch_on_time_ms[i], time_now_ms))
                        ch_on_time_ms[i] = None

            if opcode == opcode_tr:
                # Just a transition                      
                instr_id += 1
            elif opcode == opcode_sl:
                # Start of loop
                if instr_id in loops_remaining:
                    # Continuing loop
                    self.loop_times_ms[instr_id].append([time_now_ms, None])
                else:
                    # New loop
                    num_loops = data + 1 if self.for_pulseblaster else data
                    self.true_loop_counts[instr_id] = num_loops
                    
                    if instr_id not in self.loop_names:
                        comment_bits = comment.split(";")
                        
                        if len(comment_bits) > 1:
                            loop_name = comment_bits[1].strip()
                        else:
                            loop_name = chr(65 + len(self.loop_names))
                            
                        self.loop_names[instr_id] = loop_name
                    
                    if instr_id not in self.loop_times_ms:
                        self.loop_times_ms[instr_id] = []
    
                    if expand_loops and self.loop_names[instr_id].upper() not in exclude_expand_loop_names:
                        self.sim_loop_counts[instr_id] = min(num_loops, max_expanded)
                    else:
                        self.sim_loop_counts[instr_id] = 1
                
                    loops_remaining[instr_id] = self.sim_loop_counts[instr_id]
                    self.loop_times_ms[instr_id].append([time_now_ms, None])
                
                instr_id += 1
            elif opcode == opcode_el:
                # End of loop
                loop_start_instr_id = data
                loops_remaining[loop_start_instr_id] -= 1
                self.loop_times_ms[loop_start_instr_id][-1][1] = time_now_ms + delay_ms
                
                if loops_remaining[loop_start_instr_id] == 0:
                    # Break out of loop
                    instr_id += 1
                    del loops_remaining[loop_start_instr_id]
                else:
                    # Return to start of loop
                    instr_id = loop_start_instr_id
            elif opcode == opcode_halt:
                break
            else:
                raise ValueError("Unsupported opcode %s" % opcode)
                    
            time_now_ms += delay_ms

            # Sanity check we're not in an infinite loop
            eval_count += 1
            if eval_count > 1e6:
                raise RuntimeError("Appear to be stuck or in a crazy loop - breaking after evaluating %s instructions" % eval_count)
            
        # Check if any channels are still on after all instructions are finished                
        for i in range(self.num_channels):
            if ch_on_time_ms[i] is not None:
                self.channel_on_times_ms[i].append((ch_on_time_ms[i], time_now_ms))
                
        self.end_time_ms = time_now_ms
                
    def plot(self, image_filepath, channel_names=[], include_loop_durations=True):
        """
        Create an image showing when each channel is on/off.
        
        This function requires matplotlib to be installed.
        
        Args:
            
        * image_filepath (str) - Where to write to. The extension of this file will
            determine the filetype written (png/jpg etc).
        * channel_names (list of str) - Human-readable names of each channel.
        * include_loop_durations (bool) - Whether to include extra rows showing the
            extent of each loop.
        """
        import matplotlib
        
        # Try to use a non-GUI backend to speed up matplotlib
        non_gui = matplotlib.rcsetup.non_interactive_bk
        
        if 'agg' in non_gui:
            matplotlib.use('agg')
        elif len(non_gui):
            matplotlib.use(non_gui[0])
            
        import matplotlib.pyplot as plt
        
        px_x, px_y = 1200, 600
        
        fig, ax1 = plt.subplots(1,1)
        dpi = plt.gcf().get_dpi()
        plt.gcf().set_size_inches(px_x/dpi, px_y/dpi)
        plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.1)
        ever_on_chans = [k for k,v in self.channel_on_times_ms.items() if len(v)]
        inverted_chans = [k for k,v in enumerate(self.invert) if v]
        ever_on_chans.extend(inverted_chans)
        show_chans = list(set(ever_on_chans))
        yticklabels = []
        ytickcolors = []
        line_num = 0
        
        def y_on(line, inverted=False):
            return -line - 0.5 if not inverted else self.y_off(line)
        
        def y_off(line, inverted=False):
            return -line - 1 if not inverted else self.y_on(line)
        
        def y_tick(line):
            return (y_off(line) + y_on(line)) / 2
        
        for chan_idx in sorted(show_chans):
            inverted = self.invert[chan_idx]
            label = "CH%s" % (chan_idx + 1)
            
            if channel_names and chan_idx < len(channel_names) and len(channel_names[chan_idx]):
                if len(channel_names[chan_idx]) > 20:
                    label += "\n%s..." % channel_names[chan_idx][:17]
                else:
                    label += "\n%s" % channel_names[chan_idx]
            
            yticklabels.append(label)
            ytickcolors.append("C%s" % line_num)
            on_times = self.channel_on_times_ms[chan_idx]
            x = [0]
            y = [y_off(line_num, inverted)]
            
            for pulse in on_times:
                x.append(pulse[0])
                x.append(pulse[1])
                y.append(y_on(line_num, inverted))
                y.append(y_off(line_num, inverted))
                
            x.append(self.end_time_ms)
            y.append(y_off(line_num, inverted))
            plt.step(x, y, where='post')
            line_num += 1
        
        if include_loop_durations:
            for instr in sorted(self.loop_times_ms.keys()):
                # Make 2 regions so each loop iteration is distinct
                x_evens = [0]
                y_evens = [y_off(line_num)]
                x_odds = [0]
                y_odds = [y_off(line_num)]
                
                for i, (start, end) in enumerate(self.loop_times_ms[instr]):
                    if i % 2 == 0:
                        x_evens.extend([start, end])
                        y_evens.extend([y_on(line_num), y_off(line_num)])
                    else:
                        x_odds.extend([start, end])
                        y_odds.extend([y_on(line_num), y_off(line_num)])
                    
                x_evens.append(self.end_time_ms)
                y_evens.append(y_off(line_num))
                x_odds.append(self.end_time_ms)
                y_odds.append(y_off(line_num))
                
                if self.expanded_loops and self.true_loop_counts[instr] == self.sim_loop_counts[instr]:
                    truly = "Expanded all "
                else:
                    truly = "Showing %d/" % self.sim_loop_counts[instr]
                     
                yticklabels.append("Loop '%s'\n%s%s reps" % (self.loop_names[instr], truly, self.true_loop_counts[instr]))
                ytickcolors.append("C%s" % line_num)
                plt.fill_between(x_evens, y_evens, y_off(line_num), step='post', alpha=0.4, facecolor="C%s" % line_num)
                plt.fill_between(x_odds, y_odds, y_off(line_num), step='post', alpha=0.2, facecolor="C%s" % line_num)
                line_num += 1
        
        ax1.set_yticks([y_tick(y) for y in range(len(yticklabels))])
        ax1.set_yticklabels(yticklabels, minor=False, linespacing=0.8)
        
        for i, col in enumerate(ytickcolors):
            ax1.get_yticklabels()[i].set_color(col)
        
        ax1.xaxis.grid(True)
        
        plt.xlabel('Time (ms)')
        loop_str = ""
        
        if len(self.loop_times_ms):
            if self.expanded_loops:
                loop_str = " - showing all iterations of loops"
            else:
                loop_str = " - showing 1 iteration of each loop"
                
        fig.suptitle("PPG timing diagram%s\nGenerated at %s" % (loop_str, datetime.datetime.now().replace(microsecond=0)))
        fig.savefig(image_filepath)
        plt.close(fig)