"""
Frontend for creating code that can be loaded onto a PPG,
based on a configuration a user supplies in the ODB.

See the `cycling_framework.ppg.compiler` module for definition 
of the expected ODB structure.

Note that actually loading the PPG is done in C/C++. This
python code just simplifies the work needed to create the
file that should be loaded.
"""
import midas
import midas.frontend
import cycling_framework.ppg.compiler
import cycling_framework.ppg.simulator
import collections
import json
import ctypes
import traceback
import os
import os.path
import datetime

# Logger provided by FE framework will be configured
# based on whether the user specifies -d flag on the
# command line.
logger = midas.frontend.logger

class PPGCompilerEquipment(midas.frontend.EquipmentBase):
    """
    Equipment for running the PPG compiler.
    
    Members:
        
    * odb_prog_dir (str) - Path in the ODB where user will configure
        the PPG program.
    * compiler (`cycling_framework.ppg.compiler.PPGCompiler`)
    """
    def __init__(self, client):
        equip_name = "PPGCompiler"
        
        default_common = midas.frontend.InitialEquipmentCommon()
        default_common.equip_type = midas.EQ_PERIODIC
        default_common.buffer_name = ""
        default_common.trigger_mask = 0
        default_common.event_id = 147
        default_common.period_ms = 1000
        default_common.read_when = 0
        
        data_path = client.odb_get("/Logger/Data dir")
        if data_path.endswith("/"):
            data_path = data_path[:-1]
        
        default_settings = {"Standard pulse width (ms)": 0.001,
                            "PPG clock (MHz)": 100.0,
                            "Use external trigger": True,
                            "Use external clock": True,
                            "Inverted channels bitmask": ctypes.c_uint32(0),
                            "Image path": "%s/ppg.png" % data_path,
                            "Expanded image path": "%s/ppg_expanded.png" % data_path,
                            "Loadfile path": "%s/ppg.dat" % data_path,
                            "PPG uses pulseblaster": False,
                            "Compile at BOR": False,
                            "Debug": False}
        
        midas.frontend.EquipmentBase.__init__(self, client, equip_name, default_common, default_settings)
        
        self.odb_prog_dir = self.odb_settings_dir.replace("Settings", "Programming")
        
        if not self.client.odb_exists(self.odb_prog_dir):
            print("Creating default basic PPG cycle")
            self.client.odb_set(self.odb_prog_dir, self.create_basic_cycle()) 
                
        self.compiler = cycling_framework.ppg.compiler.PPGCompiler()
        self.settings_changed_func()

    def create_basic_cycle(self):
        """
        If this is the first time running the FE, we create a basic program
        in the ODB to show off the features. This function returns the
        ODB structure to use.
        
        Returns:
            `collections.OrderedDict`
        """
        pulse_1 = {"time reference": "T0", "time offset (ms)": 100, "ppg signal name": "CH1", "pulse width (ms)": 10}
        loop_start = {"time reference": "_TEND_PULSE_1", "time offset (ms)": 400, "loop count": 3}
        pulse_a = {"time reference": "_TBEGLOOP", "time offset (ms)": 10, "ppg signal name": "CH2", "pulse width (ms)": 5}
        loop_end = {}
        pulse_2 = {"time reference": "_TENDLOOP", "time offset (ms)": 400, "ppg signal name": "CH2", "pulse width (ms)": 10}
        names_list = ["First channel", "Second channel"]
        names_list.extend([""] * 30)
        names = {"names": names_list}
        
        return collections.OrderedDict([("Pulse_1", pulse_1), 
                                        ("begin_loop", loop_start),
                                        ("Pulse_A", pulse_a),
                                        ("end_loop", loop_end),
                                        ("Pulse_2", pulse_2),
                                        ("names", names)])

    def settings_changed_func(self):
        """
        When certain settings are changed, we need to tell the compiler.
        """
        self.compiler.freq_Hz = self.settings["PPG clock (MHz)"] * 1e6
        self.compiler.std_width_ms = self.settings["Standard pulse width (ms)"]
        
    def readout_func(self):
        """
        No actual data readout from this equipment.
        """
        return None
    
    def compile_at_bor(self):
        """
        Whether we should compile the PPG program at begin-of-run.
        In some experiments, other frontends call us by RPC, so we
        don't need to run the compilation automatically at BOR ourselves.
        """
        return self.settings["Compile at BOR"]
    
    def _get_end_loop_block_name(self, ppg_blocks, start_block_name):
        """
        Helper function to get the name of the end-of-loop block that
        matches the specified begin-of-loop block. Accounts for case
        differences.
        
        Args:
            
        * ppg_blocks (list of `compiler.Block`)
        * start_block_name (str)
        
        Returns:
            str, or None if not found
        """
        loop_name = start_block_name.upper().replace("BEGIN", "", 1).strip("_")
        
        for block_name in ppg_blocks.keys():
            stripped = block_name.upper().replace("END", "", 1).strip("_")
            logger.debug("%s vs %s" % (stripped, loop_name))
            if stripped == loop_name:
                return block_name
    
    def add_block(self, block_type, block_name, toff_ms, tref, width_ms, channel_name, loop_count, position):
        """
        Add a new block to the ODB. If this is a begin-of-loop block, we'll automatically
        add an end-of-loop block too.
        
        Args:
            
        * block_type (str) - pulse/begin/trans/...
        * block_name (str) - Name of the ODB subdirectory to create
        * toff_ms (float) - Offset time in ms
        * tref (str) - Time that toff_ms is in reference to
        * width_ms (float) - Pulse width for pulse blocks
        * channel_name (str) - Channel name for pulse/transition blocks
        * loop_count (int) - Number of iterations for loop blocks
        * position (int) - Where in the list of blocks to place this new block
        """
        if toff_ms is None:
            toff_ms = 0
            
        if tref is None:
            tref = ctypes.create_string_buffer(32)
        
        if width_ms is None and block_type == "pulse":
            width_ms = 0
        
        block = {"time reference": tref,
                 "time offset (ms)": ctypes.c_double(toff_ms)}
        
        if width_ms is not None:
            block["pulse width (ms)"] = ctypes.c_double(width_ms)
            
        if channel_name is not None:
            block["ppg signal name"] = channel_name
            
        if block_type == "begin":
            block["loop count"] = loop_count
            
        self.client.odb_set("%s/%s" % (self.odb_prog_dir, block_name), block)
        retval = self.reorder_block(block_name, position)
        
        if block_type == "begin":
            end_name = block_name.upper().replace("BEGIN", "END", 1)
            self.client.odb_set("%s/%s" % (self.odb_prog_dir, end_name), {})
            retval = self.reorder_block(end_name, position + 1)
        
        return retval

    def delete_block(self, block_name):
        """
        Delete a PPG block and any references to it.
        
        Args:
            
        * block_name (str)
        """
        # Delete the block itself.
        self.client.odb_delete(self.odb_prog_dir + "/" + block_name)

        # Now any references in the other blocks
        upper_name = block_name.upper()
        time_prefixes = ["_T", "_TSTART_", "_TEND_", "_TBEG", "_TEND"]
        time_suffix = block_name.upper()
        
        if upper_name.startswith("BEGIN_"):
            time_suffix = time_suffix.replace("BEGIN_", "", 1)
        
        if upper_name.startswith("END_"):
            time_suffix = time_suffix.replace("END_", "", 1)
                
        blocks = self.client.odb_get(self.odb_prog_dir)
        
        for bn, block in blocks.items():
            tref = block.get("time reference", "")
            for pref in time_prefixes:
                allowed_tref = "%s%s" % (pref, time_suffix)
                
                if tref == allowed_tref:
                    odb_path = "%s/%s/time reference" % (self.odb_prog_dir, bn)
                    self.client.odb_set(odb_path, "")
                    
        # Now any references in scan parameters
        if self.client.odb_exists("/Scanning/PPG/Settings"):
            scan = self.client.odb_get("/Scanning/PPG/Settings")
            
            for dim in ["X", "Y"]:
                keyname = "%s paths" % dim
                arr = scan[keyname]
                
                for i, val in enumerate(arr):
                    if val.find("/%s/" % block_name) != -1:
                        arr[i] = ""
                        
                self.client.odb_set("/Scanning/PPG/Settings/%s" % keyname, arr)
            
        # If a loop, update the end of the loop as well    
        if upper_name.startswith("BEGIN"):
            all_blocks = self.client.odb_get(self.odb_prog_dir)
            end_name = self._get_end_loop_block_name(all_blocks, block_name)
            logger.debug("Deleted start of loop %s, also try to delete %s" % (block_name, end_name))
            if end_name is not None:
                return self.delete_block(end_name)
        
        return (midas.status_codes["SUCCESS"], "OK")    
    
    def rename_block(self, old_name, new_name):
        """
        Re-name a PPG block. We need to update the name of the block itself, 
        any references to it from other blocks, and any usages of it in the 
        Scanning section.
        
        Args:
            
        * old_name (str)
        * new_name (str)
        """
        # Sanity-check the requested name change
        upper_old = old_name.upper()
        upper_new = new_name.upper()
        
        if upper_old[:3] != upper_new[:3]:
            raise ValueError("Don't change the first 3 characters of a block name!")
        
        if upper_old.startswith("BEGIN_") != upper_new.startswith("BEGIN_"):
            raise ValueError("Don't strip BEGIN_ from a block name!")
            
        if upper_old.startswith("END_") != upper_new.startswith("END_"):
            raise ValueError("Don't strip END_ from a block name!")
        
        # Name the block itself
        self.client.odb_rename(self.odb_prog_dir + "/" + old_name, new_name)
        
        # Now any references in the other blocks
        time_prefixes = ["_T", "_TSTART_", "_TEND_", "_TBEG", "_TEND"]
        time_suffix_old = old_name.upper()
        time_suffix_new = new_name.upper()
        
        if upper_old.startswith("BEGIN_"):
            time_suffix_old = time_suffix_old.replace("BEGIN_", "", 1)
            time_suffix_new = time_suffix_new.replace("BEGIN_", "", 1)
        
        if upper_old.startswith("END_"):
            time_suffix_old = time_suffix_old.replace("END_", "", 1)
            time_suffix_new = time_suffix_new.replace("END_", "", 1)
                
        blocks = self.client.odb_get(self.odb_prog_dir)
        
        for block_name, block in blocks.items():
            tref = block.get("time reference", "")
            for pref in time_prefixes:
                allowed_tref = "%s%s" % (pref, time_suffix_old)
                
                if tref.upper() == allowed_tref.upper():
                    odb_path = "%s/%s/time reference" % (self.odb_prog_dir, block_name)
                    new_tref = "%s%s" % (pref, time_suffix_new)
                    self.client.odb_set(odb_path, new_tref)
                    
        # Now any references in scan parameters
        if self.client.odb_exists("/Scanning/PPG/Settings"):
            scan = self.client.odb_get("/Scanning/PPG/Settings")
            
            for dim in ["X", "Y"]:
                keyname = "%s paths" % dim
                arr = scan[keyname]
                
                for i, val in enumerate(arr):
                    old_dir = "/%s/" % old_name
                    new_dir = "/%s/" % new_name
                    
                    if val.find(old_dir) != -1:
                        arr[i] = val.replace(old_dir, new_dir)
                        
                self.client.odb_set("/Scanning/PPG/Settings/%s" % keyname, arr)
            
        # If a loop, update the end of the loop as well    
        if upper_old.startswith("BEGIN"):
            all_blocks = self.client.odb_get(self.odb_prog_dir)
            end_old = self._get_end_loop_block_name(all_blocks, old_name)
            end_new = upper_new.replace("BEGIN", "END", 1)
            return self.rename_block(end_old, end_new)
        
        return (midas.status_codes["SUCCESS"], "OK")    
    
    def reorder_block(self, block_name, new_idx):
        """
        Re-order a block to be in a new position in the list.
        
        Args:
            
        * block_name (str)
        * new_idx (int)
        """
        odb = self.client.odb_get(self.odb_prog_dir)
        old_len = len(odb)
        
        # odb is an OrderedDict; order is determined by when
        # each item was added. We re-add the block to be moved,
        # then re-add all the blocks that should follow it.
        odb[block_name] = odb.pop(block_name)
        key_list = list(odb.keys())
        
        for i, k in enumerate(key_list):
            if k != block_name and i > new_idx:
                odb[k] = odb.pop(k)
        
        if len(odb) != old_len:
            raise RuntimeError("Error re-ordering ODB blocks. Old length %s, new length %s" % (old_len, len(odb)))
        
        self.client.odb_set(self.odb_prog_dir, odb)
        
        return (midas.status_codes["SUCCESS"], "OK")
    
    def _get_pulses_for_channel_in_loop(self, channel_name, loop, pulse_names):
        """
        Helper function to get the names of Pulse objects within a loop that
        act on a specific channel.
        Recurses down to daughter loops if necessary too.
        
        Args:
            
        * channel_name (str)
        * loop (`ppg.compiler.Loop`)
        * pulse_names (list of str) - This will be filled by this function.
        """
        for child in loop.children:
            if isinstance(child, cycling_framework.ppg.compiler.Pulse) and child.channel.lower() == channel_name:
                pulse_names.append(child.name)
            elif isinstance(child, cycling_framework.ppg.compiler.Loop):
                self._get_pulses_for_channel_in_loop(channel_name, child, pulse_names)
    
    def list_pulses_for_channel(self, channel_name):
        """
        Get the names of all pulses that act on a specific channel.
        
        Args:
            
        * channel_name (str)
        
        Returns:
            2-tuple of (int, str) for (status_code, message). Message
            is comma-spearated list of pulse names on SUCCESS. Message
            is the error message on failure.
        """
        try:
            odb = self.client.odb_get(self.odb_prog_dir)
        except KeyError:
            err = "ODB path '%s/%s' doesn't exist" % (self.odb_prog_dir)
            self.client.msg(err, True)
            return (midas.status_codes["FE_ERR_ODB"], err)
        
        main_loop = self.compiler.odb_to_structure(odb)
        pulse_names = []
        
        self._get_pulses_for_channel_in_loop(channel_name.lower(), main_loop, pulse_names)
        
        return (midas.status_codes["SUCCESS"], ",".join(p.lower() for p in pulse_names))
    
    def compile(self, with_image=True, with_loadfile=True, return_loadfile_in_response=False, image_exclude_expand_loop_names=[]):
        """
        Compile the current ODB structure.
        
        Args:
            
        * with_image (bool) - Create PNG images displaying the structure.
        * with_loadfile (bool) - Write file to disk that can be loaded onto PPG.
        * return_loadfile_in_response (bool) - Return the content of the loadfile
            as part of the response.
        * image_exclude_expand_loop_names (list of str) - When making images, which
            loops to not bother expanding (e.g. if there are 100000 iterations)
            
        Returns:
            2-tuple of (int, str) for (status_code, message).
            On success, message is just "OK", unless return_loadfile_in_response
            is True.
        """
        t_start = datetime.datetime.now()
        for_pulseblaster = self.settings["PPG uses pulseblaster"]
        freq_Hz = self.settings["PPG clock (MHz)"] * 1e6
        debug = self.settings["Debug"]
        
        self.client.msg("Compiling PPG program")
        
        try:
            odb = self.client.odb_get(self.odb_prog_dir)
        except KeyError:
            err = "ODB path '%s/%s' doesn't exist" % (self.odb_prog_dir)
            self.client.msg(err, True)
            return (midas.status_codes["FE_ERR_ODB"], err)
        
        try:
            channel_names = odb["names"]["names"]
        except KeyError:
            channel_names = None
            
        try:
            (loadtext, known_loop_names) = self.compiler.compile_from_odb_blocks(odb, for_pulseblaster, debug)
        except Exception as e:
            traceback.print_exc()
            err = "PPG compilation failed: %s" % str(e)
            self.client.msg(err, True)
            return (midas.status_codes["FE_ERR_ODB"], err)
        
        t_compile_end = datetime.datetime.now()
        print("Compilation took %ss" % (t_compile_end - t_start).total_seconds())
        
        if with_loadfile:
            try:
                with open(self.settings["Loadfile path"], "w") as f:
                    f.write(loadtext)
                self.client.msg("Wrote PPG loadfile to %s" % self.settings["Loadfile path"])
            except Exception as e:
                traceback.print_exc()
                err = "Writing loadfile failed: %s" % str(e)
                self.client.msg(err, True)
                return (midas.status_codes["FE_ERR_ODB"], err)
        
        if with_image:
            try:
                invert_mask = self.settings["Inverted channels bitmask"]
                simulator = cycling_framework.ppg.simulator.PPGSimulator(text=loadtext, freq_Hz=freq_Hz, for_pulseblaster=for_pulseblaster, invert_mask=invert_mask, known_loop_names=known_loop_names, exclude_expand_loop_names=image_exclude_expand_loop_names)
                simulator.plot(self.settings["Image path"], channel_names)
                self.client.msg("Wrote PPG image to %s" % self.settings["Image path"])
                
                exp_simulator = cycling_framework.ppg.simulator.PPGSimulator(text=loadtext, freq_Hz=freq_Hz, expand_loops=True, for_pulseblaster=for_pulseblaster, invert_mask=invert_mask, known_loop_names=known_loop_names, exclude_expand_loop_names=image_exclude_expand_loop_names)
                exp_simulator.plot(self.settings["Expanded image path"], channel_names)
                self.client.msg("Wrote expanded PPG image to %s" % self.settings["Expanded image path"])
                
                t_image_end = datetime.datetime.now()
                print("Generating images took %ss" % (t_image_end - t_compile_end).total_seconds())
            except Exception as e:
                traceback.print_exc()
                err = "Plot creation failed: %s" % str(e)
                self.client.msg(err, True)
                return (midas.status_codes["FE_ERR_ODB"], err)
        
        msg = "OK"
        
        if return_loadfile_in_response:
            msg = loadtext
            
        return (midas.status_codes["SUCCESS"], msg)

class PPGCompilerFrontend(midas.frontend.FrontendBase):
    """
    Frontend contains only 1 equipment.
    """
    def __init__(self):
        midas.frontend.FrontendBase.__init__(self, "PPGCompilerFrontend")
        self.add_equipment(PPGCompilerEquipment(self.client))
        
        self.client.register_jrpc_callback(self.rpc_handler, True)
        
        # Ensure we compile the PPG file before C frontend tries to load it
        self.client.set_transition_sequence(midas.TR_START, 300)
        
    def begin_of_run(self, run_number):
        """
        Optionally compile the PPG loadfile.
        """
        if self.equipment["PPGCompiler"].compile_at_bor():
            return self.compile()
        else:
            return midas.status_codes["SUCCESS"]

    def compile(self, with_image=True, with_loadfile=True, return_loadfile_in_response=False, image_exclude_expand_loop_names=[]):
        """
        See `PPGCompilerEquipment.compile()`.
        """
        return self.equipment["PPGCompiler"].compile(with_image, with_loadfile, return_loadfile_in_response, image_exclude_expand_loop_names=image_exclude_expand_loop_names)

    def rpc_handler(self, client, cmd, args, max_len):
        """
        JRPC handler we register with midas. This will generally be called when
        a user clicks a button on the associated webpage.
        
        Args:
            
        * client (`midas.client.MidasClient`)
        * cmd (str) - The command the user wants us to do.
        * args (str) - Stringified JSON of any arguments for this command.
        * max_len (int) - Maximum length of response the user will accept.
        
        Returns:
            2-tuple of (int, str) for (status code, message)
            
        Accepted commands:
           
        * compile_now - Compile PPG loadfile now
        * rename_block - Rename an ODB block (and references to it)
        * add_block - Add a new block to the ODB
        * delete_block - Delete a block from the ODB
        * reorder_block - Reorder a block's position
        * list_pulses_for_channel - Return pulse names for a channel
        """
        ret_int = midas.status_codes["SUCCESS"]
        ret_str = ""
        
        logger.debug("Handling RPC command %s" % cmd)

        if cmd == "compile_now":
            # Re-compile the .dat file, optionally re-creating the image.
            # Image creation is slow, so we can skip it during an in-run
            # scan, for example, to improve performance.
            # We can also decide to not write the ppgload.dat file to disk,
            # and/or return the content we would write to that file as part
            # of the return message.
            # Args:
            # * with_image (bool) - optional, defaults to False
            # * with_loadfile (bool) - optional, defaults to True
            # * return_loadfile_in_response (bool) - optional, defaults to False
            with_image = False
            
            if len(args):
                jargs = json.loads(args)
                with_image = jargs.get("with_image", False)
                with_loadfile = jargs.get("with_loadfile", True)
                image_exclude_expand_loop_names = jargs.get("image_exclude_expand_loop_names", [])
                return_loadfile_in_response = jargs.get("return_loadfile_in_response", False)
            
            (ret_int, ret_str) = self.compile(with_image, with_loadfile, return_loadfile_in_response, image_exclude_expand_loop_names)
        elif cmd == "rename_block":
            # Rename an ODB block and any references to it.
            # Args:
            # * old_name (str)
            # * new_name (str)
            jargs = json.loads(args)
            old_name = jargs["old_name"]
            new_name = jargs["new_name"]
            (ret_int, ret_str) = self.equipment["PPGCompiler"].rename_block(old_name, new_name)
        elif cmd == "add_block":
            # Add a new ODB block.
            # Args:
            # * block_type (str)
            jargs = json.loads(args)
            block_type = jargs["block_type"]
            block_name = jargs["block_name"]
            toff_ms = jargs.get("toff", None)
            tref = jargs.get("tref", None)
            width_ms = jargs.get("width", None)
            chan_name = jargs.get("channel", None)
            loop_count = jargs.get("loop_count", None)
            pos = int(jargs["position"])
            (ret_int, ret_str) = self.equipment["PPGCompiler"].add_block(block_type, block_name, toff_ms, tref, width_ms, chan_name, loop_count, pos)
        elif cmd == "delete_block":
            # Delete an ODB block and any references to it.
            # Args:
            # * block_name (str)
            jargs = json.loads(args)
            (ret_int, ret_str) = self.equipment["PPGCompiler"].delete_block(jargs["block_name"])
        elif cmd == "reorder_block":
            # Change position of an ODB block.
            # Args:
            # * block_name (str)
            # * new_idx (int)
            jargs = json.loads(args)
            (ret_int, ret_str) = self.equipment["PPGCompiler"].reorder_block(jargs["block_name"], jargs["new_idx"])
        elif cmd == "list_pulses_for_channel":
            # Get the list of pulses that use the specified channel.
            # Args:
            # * channel (str) - like CH1
            jargs = json.loads(args)
            (ret_int, ret_str) = self.equipment["PPGCompiler"].list_pulses_for_channel(jargs["channel"])
        else:
            ret_int = midas.status_codes["FE_ERR_DRIVER"]
            ret_str = "Unknown command '%s'" % cmd     
            
        logger.debug("RPC status is %s: %s" % (ret_int, ret_str))
            
        return (ret_int, ret_str)
    

if __name__ == "__main__":
    parser = midas.frontend.parser
    parser.add_argument("--oneshot", action="store_true", help="One-shot compilation")
    args = midas.frontend.parse_args()
    
    fe = PPGCompilerFrontend()
    
    if args.oneshot:
        fe.compile()
    else:
        fe.run()