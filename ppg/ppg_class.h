#ifndef PPG_CLASS_H
#define PPG_CLASS_H

#include <sys/time.h>
#include <string>
#include "midas.h"
#include "mvmestd.h"

// Base class for all the different versions of the PPG we might encounter.
class PPG {
  public:
    PPG() {}
    virtual ~PPG() {};

    // Set up the PPG with a specific configuration:
    // - whether to use an external (hardware) trigger or internal (software) trigger
    // - whether to invert the state of some channels (so they default to on instead of off)
    // - whether to use an external or internal (on-board) clock.
    virtual INT init(bool external_trigger, DWORD polarity_mask, bool external_clock) { return SUCCESS; }

    // Load a sequence onto the PPG. The PPG compiler and the code in this
    // function must agree on what the content of the file should be.
    virtual INT load_file(std::string path) { return SUCCESS; }

    // Start the PPG sequence (via software signal).
    virtual INT start_sequencer() { return SUCCESS; }

    // Stop the PPG sequence.
    virtual INT stop_sequencer() { return SUCCESS; }

    // Report whether the PPG sequence is currently running.
    virtual bool is_sequencer_running() { return false; }

    // Print the current state of the PPG to screen.
    virtual void print_status() {}

    // Report the current state of the PPG clock.
    virtual void get_clock_state(bool& is_external, bool& external_is_good) {}

    // Set the PPG to use an external trigger source.
    virtual void enable_external_trigger() {}

    // Report whether PPG is currently using external trigger.
    virtual bool is_external_trigger() { return false; }
};

#ifndef DUMMY_VME
#include "mvmestd.h"

// Implementation of the PulseBlaster-based PPG.
// This version does not support an external clock.
class PulseBlasterPPG : public PPG {
  public:
    PulseBlasterPPG(MVME_INTERFACE* _myvme, DWORD _vme_base = 0x00100000) {
      myvme = _myvme;
      PPG_BASE = _vme_base;
    }

    INT init(bool external_trigger, DWORD polarity_mask, bool external_clock);
    INT load_file(std::string path);
    INT start_sequencer();
    INT stop_sequencer();
    bool is_sequencer_running();
    void print_status();
    void get_clock_state(bool& is_external, bool& external_is_good);
    void enable_external_trigger();
    bool is_external_trigger();

    // Special function for setting polarization.
    void set_polz(bool state);
    bool read_polz();

  private:
    MVME_INTERFACE* myvme;
    DWORD PPG_BASE;
};

// Implementation of the TRIUMF PPG.
class TriumfPPG : public PPG {
  public:
    TriumfPPG(MVME_INTERFACE* _myvme, DWORD _vme_base = 0x00100000) {
      myvme = _myvme;
      PPG_BASE = _vme_base;
    }

    INT init(bool external_trigger, DWORD polarity_mask, bool external_clock);
    INT load_file(std::string path);
    INT start_sequencer();
    INT stop_sequencer();
    bool is_sequencer_running();
    void print_status();
    void get_clock_state(bool& is_external, bool& external_is_good);
    void enable_external_trigger();
    bool is_external_trigger();

  private:
    MVME_INTERFACE* myvme;
    DWORD PPG_BASE;
};

#endif // DUMMY_VME

// Implementation of a dummy PPG for testing.
// This version just pretends that it's running a sequence of a specified
// duration.
class DummyPPG : public PPG {
  public:
    DummyPPG(float _fake_cycle_len_ms) {
      fake_cycle_len_ms = _fake_cycle_len_ms;
      cycle_start.tv_sec = 0;
      cycle_start.tv_usec = 0;
      ext_trig = false;
      polz_state = false;
    }

    INT init(bool external_trigger, DWORD polarity_mask, bool external_clock);
    INT load_file(std::string path);
    INT start_sequencer();
    INT stop_sequencer();
    bool is_sequencer_running();
    void print_status();
    void get_clock_state(bool& is_external, bool& external_is_good);
    void enable_external_trigger();
    bool is_external_trigger();

    void set_polz(bool state) {polz_state = state;};
    bool read_polz() { return polz_state; };

  private:
    bool ext_trig;
    float fake_cycle_len_ms;
    struct timeval cycle_start;

    bool polz_state;
};

#endif
