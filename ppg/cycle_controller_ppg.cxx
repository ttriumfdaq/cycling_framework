#include "ppg_class.h"
#include "midas.h"
#include "mrpc.h"
#include "core/cycle_controller.h"
#include "cycle_controller_ppg.h"
#include "core/scan_utils.h"
#include <algorithm>
#include <iostream>


INT CycleControllerPPG::begin_of_run(char* error) {
  ppg_started = false;

  char ext_trig_odb_path[256];
  sprintf(ext_trig_odb_path, "/Equipment/PPGCompiler/Settings/Use external trigger");
  INT sz = sizeof(ext_trig);
  INT status = db_get_value(hDB, 0, ext_trig_odb_path, &ext_trig, &sz, TID_BOOL, 0);

  return status;
}

CycleState CycleControllerPPG::get_cycle_progress() {
  // Assumes that get_cycle_progress() will be called at least
  // once while the PPG is running.
  bool running = ppg->is_sequencer_running();

  if (running) {
    ppg_started = true;
    return Running;
  } else if (ppg_started) {
    return Finished;
  } else {
#ifdef DUMMY_VME
    // Pretend that something external started the PPG sequence
    if (ext_trig) {
      ppg->start_sequencer();
    }
#endif

    return NotStarted;
  }
}

INT CycleControllerPPG::start_cycle(bool is_new_supercycle, bool is_new_loop) {
  if (ext_trig) {
    scan_utils::ts_printf("Re-enabling external trigger on PPG.\n");
    ppg_started = false;
    ppg->enable_external_trigger();
    return SUCCESS;
  }

  scan_utils::ts_printf("Starting PPG.\n");
  INT status = ppg->start_sequencer();

  if (status == SUCCESS) {
    ppg_started = true;
  } else {
    cm_msg(MERROR, __FUNCTION__, "Failed to start PPG");
  }

  ss_sleep(10);

  return status;
}

INT CycleControllerPPG::stop_cycle() {
  scan_utils::ts_printf("Stopping PPG.\n");
  INT status = ppg->stop_sequencer();

  if (status == SUCCESS) {
    ppg_started = false;
  }

  return status;
}
