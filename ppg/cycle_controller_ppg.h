#ifndef CYCLE_CONTROLLER_PPG_H
#define CYCLE_CONTROLLER_PPG_H

#include "ppg_class.h"
#include "midas.h"
#include "core/cycle_controller.h"

class CycleControllerPPG: public CycleController {
  public:
    CycleControllerPPG(HNDLE _hDB, PPG* _ppg) : CycleController(_hDB) {
      ppg = _ppg;
      ppg_started = false;
      ext_trig = 0;
    }

    virtual ~CycleControllerPPG() {};

    virtual INT check_records() override { return SUCCESS; };
    virtual INT open_records() override { return SUCCESS; };
    virtual INT close_records() override { return SUCCESS; };

    virtual INT begin_of_run(char* error) override;

    virtual CycleState get_cycle_progress() override;
    virtual INT start_cycle(bool is_new_supercycle, bool is_new_loop) override;
    virtual INT stop_cycle() override;

  protected:

    PPG* ppg;
    bool ppg_started;
    BOOL ext_trig;

};

#endif
