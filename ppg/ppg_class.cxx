#include "ppg_class.h"
#include <string>
#include <sys/time.h>
#include "midas.h"
#include "core/scan_utils.h"

#ifndef DUMMY_VME
#include "mvmestd.h"

extern "C" {
#include "tppg.h" // TPPG family of driver functions for TRIUMF PPG
#include "vppg.h" // VPPG family of driver functions for PulseBlaster PPG (from midas drivers)
}

// PulseBlaster does not support external clock
INT PulseBlasterPPG::init(bool external_trigger, DWORD polarity_mask, bool external_clock) {
  INT status = 0;

  // Basic init
  VPPGStopSequencer(myvme, PPG_BASE);
  VPPGInit(myvme, PPG_BASE); // PPG controls POL output (helicity), disable external trig, beam off (VME control)
  VPPGBeamOff(myvme, PPG_BASE);

  // Set up internal / external clock source
    if (external_clock) {
      cm_msg(MERROR, __FUNCTION__, "This PPG version doesn't support external clock");
      return FE_ERR_HW;
    }

  // Set up internal / external trigger source
  if (external_trigger) {
    scan_utils::ts_printf("Setting PPG with external start\n");
    status = VPPGEnableExtTrig(myvme, PPG_BASE);
  } else {
    scan_utils::ts_printf("Setting PPG with internal start\n");
    status = VPPGDisableExtTrig(myvme, PPG_BASE);
  }

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't set PPG internal/external start signal source");
    return status;
  }

  // Set up polarity
  scan_utils::ts_printf("Setting PPG polarity mask 0x%x\n", polarity_mask);

  // make sure PPG controls these outputs
  VPPGPolzCtlPPG(myvme, PPG_BASE);
  VPPGBeamCtlPPG(myvme, PPG_BASE);
  DWORD read_pol_mask = VPPGPolmskWrite(myvme, PPG_BASE, polarity_mask);

  if (read_pol_mask != polarity_mask) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't set PPG polarity mask - set %d, read %d", polarity_mask, read_pol_mask);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT PulseBlasterPPG::load_file(std::string path) {
  // You should probably call init() again after loading the new file,
  // as we stop the sequencer (and disable any external trigger) as part
  // of the loading process.
  int status = stop_sequencer();

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't stop the PPG, so unable to load new file");
    return FE_ERR_HW;
  }

  char* c_path = strdup(path.c_str());
  status = VPPGLoad(myvme, PPG_BASE, c_path);
  free(c_path);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't load PPG file %s - status %d", path.c_str(), status);
    return FE_ERR_HW;
  }

  cm_msg(MINFO, __FUNCTION__, "PPG file %s successfully loaded", path.c_str());
  return SUCCESS;
}

INT PulseBlasterPPG::start_sequencer() {
  VPPGStartSequencer(myvme, PPG_BASE);
  return SUCCESS;
}

INT PulseBlasterPPG::stop_sequencer() {
  VPPGDisableExtTrig(myvme, PPG_BASE); // Stop external triggers from restarting PPG
  VPPGStopSequencer(myvme, PPG_BASE);
  return SUCCESS;
}

bool PulseBlasterPPG::is_sequencer_running() {
  return VPPGRegRead(myvme, PPG_BASE, VPPG_VME_READ_STAT_REG) & 0x1;
}

void PulseBlasterPPG::print_status() {
  VPPGExtTrigRegRead(myvme, PPG_BASE, 1);
}

void PulseBlasterPPG::get_clock_state(bool& is_external, bool& external_is_good) {
  // PulseBlaster does not support external clock
  is_external = false;
  external_is_good = false;
}

void PulseBlasterPPG::enable_external_trigger() {
  VPPGEnableExtTrig(myvme, PPG_BASE);
}

bool PulseBlasterPPG::is_external_trigger() {
  return VPPGExtTrigRegRead(myvme, PPG_BASE, 0);
}

void PulseBlasterPPG::set_polz(bool state) {
  VPPGPolzSet(myvme, PPG_BASE, state);
}

bool PulseBlasterPPG::read_polz() {
  return VPPGPolzRead(myvme, PPG_BASE);
}

INT TriumfPPG::init(bool external_trigger, DWORD polarity_mask, bool external_clock) {
  INT status = 0;

  // Basic init
  TPPGDisableExtTrig(myvme, PPG_BASE); // stop new external triggers from restarting the PPG
  TPPGReset(myvme, PPG_BASE); // Halt pgm and stops (even if in long delay)
  TPPGInit(myvme, PPG_BASE, 0); // internal trig,  set pol mask to zero, PC back to zero, sets outputs to zero  clears TEST MODE bit. Does not change ext/internal clock.

  // Set up internal / external clock source
  bool current_clock_is_ext = false;
  bool current_ext_clock_good = false;
  get_clock_state(current_clock_is_ext, current_ext_clock_good);

  if (external_clock && !current_ext_clock_good) {
    cm_msg(MERROR, __FUNCTION__, "Can't set PPG to external clock as external clock signal is BAD");
    return FE_ERR_HW;
  }

  if (external_clock == current_clock_is_ext) {
    scan_utils::ts_printf("Keeping current state of clock source\n");
  } else {
    // Toggle clock and set divide-down (call with TRUE)
    scan_utils::ts_printf("Toggling state of clock source\n");
    status = TPPGToggleExtClock(myvme, PPG_BASE, TRUE);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Couldn't toggle PPG internal/external clock signal");
      return status;
    }
  }

  // Set up internal / external trigger source
  if (external_trigger) {
    scan_utils::ts_printf("Setting PPG with external start\n");
    status = TPPGEnableExtTrig(myvme, PPG_BASE);
  } else {
    scan_utils::ts_printf("Setting PPG with internal start\n");
    status = TPPGDisableExtTrig(myvme, PPG_BASE);
  }

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't set PPG internal/external start signal source");
    return status;
  }

  // Set up polarity
  scan_utils::ts_printf("Setting PPG polarity mask 0x%x\n", polarity_mask);
  DWORD read_pol_mask = TPPGPolmskWrite(myvme, PPG_BASE, polarity_mask);

  if (read_pol_mask != polarity_mask) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't set PPG polarity mask - set %d, read %d", polarity_mask, read_pol_mask);
    return FE_ERR_HW;
  }

  return SUCCESS;
}

INT TriumfPPG::load_file(std::string path) {
  // You should probably call init() again after loading the new file,
  // as we stop the sequencer (and disable any external trigger) as part
  // of the loading process.
  int status = stop_sequencer();

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't stop the PPG, so unable to load new file");
    return FE_ERR_HW;
  }

  char* c_path = strdup(path.c_str());
  status = TPPGLoad(myvme, PPG_BASE, 0, c_path);
  free(c_path);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Couldn't load PPG file %s - status %d", path.c_str(), status);
    return FE_ERR_HW;
  }

  cm_msg(MINFO, __FUNCTION__, "PPG file %s successfully loaded", path.c_str());
  return SUCCESS;
}

INT TriumfPPG::start_sequencer() {
  TPPGStartSequencer(myvme, PPG_BASE);
  return SUCCESS;
}

INT TriumfPPG::stop_sequencer() {
  // Stop the PPG and make sure outputs are set to required pattern even if stopped mid-cycle
  TPPGDisableExtTrig(myvme, PPG_BASE); // Stop external triggers from restarting PPG
  TPPGReset(myvme, PPG_BASE); // Stops program even if in long delay
  TPPGStopSequencer(myvme, PPG_BASE); // Halt pgm and stop
  ss_sleep(500);
  TPPGZeroAll(myvme, PPG_BASE); // zero all the PPG outputs
  return SUCCESS;
}

bool TriumfPPG::is_sequencer_running() {
  return TPPGRegRead(myvme, PPG_BASE, TPPG_CSR_REG) & 0x1;
}

void TriumfPPG::print_status() {
  TPPGStatusRead(myvme,PPG_BASE);
}

void TriumfPPG::get_clock_state(bool& is_external, bool& external_is_good) {
  // Read the state of the ppg clock from the status register
  scan_utils::ts_printf("ppg_get_clock_info: Reading Status Register of PPG...\n");
  DWORD data = TPPGStatusRead(myvme, PPG_BASE);

  is_external = (data & 0x10000);
  external_is_good = (data & 0x20000);
}

void TriumfPPG::enable_external_trigger() {
  TPPGEnableExtTrig(myvme, PPG_BASE);
}

bool TriumfPPG::is_external_trigger() {
  return TPPGExtTrigRead(myvme, PPG_BASE);
}

#endif // DUMMY_VME

INT DummyPPG::init(bool external_trigger, DWORD polarity_mask, bool external_clock) {
  cycle_start.tv_sec = 0;
  cycle_start.tv_usec = 0;
  ext_trig = external_trigger;
  return SUCCESS;
}

INT DummyPPG::load_file(std::string path) {
  cm_msg(MINFO, __FUNCTION__, "Pretending to load PPG file to dummy PPG");
  return SUCCESS;
}

INT DummyPPG::start_sequencer() {
  gettimeofday(&cycle_start, NULL);
  scan_utils::ts_printf("Dummy sequencer starting at %ld.%d\n", cycle_start.tv_sec, cycle_start.tv_usec);
  return SUCCESS;
}

INT DummyPPG::stop_sequencer() {
  cycle_start.tv_sec = 0;
  cycle_start.tv_usec = 0;
  return SUCCESS;
}

bool DummyPPG::is_sequencer_running() {
  bool started = cycle_start.tv_sec > 0;

  if (!started) {
    return false;
  }

  struct timeval now;
  gettimeofday(&now, NULL);

  float elapsed_ms = (now.tv_sec - cycle_start.tv_sec) * 1000;
  elapsed_ms += (now.tv_usec - cycle_start.tv_usec) / 1000;
  return elapsed_ms < fake_cycle_len_ms;
}

void DummyPPG::print_status() {
  if (is_sequencer_running()) {
    scan_utils::ts_printf("Dummy sequencer is 'running'\n");
  } else {
    scan_utils::ts_printf("Dummy sequencer is not 'running'\n");
  }
}

void DummyPPG::get_clock_state(bool& is_external, bool& external_is_good) {
  is_external = false;
  external_is_good = true;
}

void DummyPPG::enable_external_trigger() {
  ext_trig = true;
}

bool DummyPPG::is_external_trigger() {
  return ext_trig;
}
