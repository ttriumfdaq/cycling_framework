#include "ppg_class.h"
#include "midas.h"
#include "mrpc.h"
#include "core/scannable_device.h"
#include "scan_device_ppg.h"
#include "core/scan_utils.h"
#include <algorithm>
#include <iostream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>

#define PPG_SETTINGS_PATH "/Scanning/PPG/Settings"
#define PPG_STATUS_PATH "/Scanning/PPG/Status"

INT ScanDevicePPG::check_records() {
  char settings_path[256], status_path[256];
  sprintf(settings_path, PPG_SETTINGS_PATH);
  sprintf(status_path, PPG_STATUS_PATH);

  INT status;

  status = db_check_record(hDB, 0, settings_path, PPG_SCAN_SETTINGS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", settings_path);
    return status;
  }

  status = db_check_record(hDB, 0, status_path, PPG_SCAN_STATUS_STR, TRUE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to check record for %s", status_path);
    return status;
  }

  return SUCCESS;
}

INT ScanDevicePPG::open_records() {
  char settings_path[256], status_path[256];
  sprintf(settings_path, PPG_SETTINGS_PATH);
  sprintf(status_path, PPG_STATUS_PATH);

  HNDLE h_settings, h_status;
  INT size_settings = sizeof(user_settings);
  INT size_status = sizeof(scan_status);
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_get_record(hDB, h_settings, &user_settings, &size_settings, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", settings_path);
    return status;
  }

  status = db_get_record(hDB, h_status, &scan_status, &size_status, 0);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to get record for %s", status_path);
    return status;
  }

  status = db_open_record(hDB, h_settings, &user_settings, size_settings, MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open PPG settings record");
    return status;
  }

  status = db_open_record(hDB, h_status, &scan_status, size_status, MODE_WRITE, NULL, NULL);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open PPG status record");
    return status;
  }

  return status;
}


INT ScanDevicePPG::close_records() {
  char settings_path[256], status_path[256];
  sprintf(settings_path, PPG_SETTINGS_PATH);
  sprintf(status_path, PPG_STATUS_PATH);

  HNDLE h_settings, h_status;
  INT status;

  db_find_key(hDB, 0, settings_path, &h_settings);
  db_find_key(hDB, 0, status_path, &h_status);

  status = db_close_record(hDB, h_settings);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close PPG settings record");
    return status;
  }

  status = db_close_record(hDB, h_status);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to close PPG status record");
    return status;
  }

  return status;
}

INT ScanDevicePPG::begin_of_run(char* error) {
  INT status;

  status = validate_ppg_scan_paths(error);

  if (status != SUCCESS) {
    return status;
  }

  status = read_ppg_clock_and_trigger_settings(error);

  if (status != SUCCESS) {
    return status;
  }

  return SUCCESS;
}

INT ScanDevicePPG::validate_ppg_scan_paths(char* error) {
  INT status;
  HNDLE key;

  if (x_enabled) {
    for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
      if (strlen(user_settings.x_paths[i]) == 0) {
        continue;
      }

      status = db_find_key(hDB, 0, user_settings.x_paths[i], &key);

      if (status != SUCCESS) {
        snprintf(error, 255, "Failed to find expected path (PPG X scan var #%d) %s", i, user_settings.x_paths[i]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return status;
      }
    }
  }

  if (y_enabled) {
    for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
      if (strlen(user_settings.y_paths[i]) == 0) {
        continue;
      }

      status = db_find_key(hDB, 0, user_settings.y_paths[i], &key);

      if (status != SUCCESS) {
        snprintf(error, 255, "Failed to find expected path (PPG Y scan var #%d) %s", i, user_settings.y_paths[i]);
        cm_msg(MERROR, __FUNCTION__, "%s", error);
        return status;
      }
    }
  }

  return SUCCESS;
}

INT ScanDevicePPG::fill_end_of_cycle_banks(char* pevent) {
  if (x_enabled) {
    double* pdata_xval;
    bk_create(pevent, "XPPG", TID_DOUBLE, (void**) &pdata_xval);

    for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
      *pdata_xval++ = scan_status.x_values[i];
    }

    bk_close(pevent, pdata_xval);
  }

  if (y_enabled) {
    double* pdata_yval;
    bk_create(pevent, "YPPG", TID_DOUBLE, (void**) &pdata_yval);

    for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
      *pdata_yval++ = scan_status.y_values[i];
    }

    bk_close(pevent, pdata_yval);
  }

  return SUCCESS;
}

INT ScanDevicePPG::compile_ppg(char* ppgfile) {
  // Does not load PPG, just calls the PPG compiler via RPC.
  // Originally the PPG compiler wrote a file to disk (ppgload.dat), which we then loaded.
  // However the PPG compiler may run on a different machine, and NFS caching can make
  // it very slow for this machine to see the new file. So we now get the content
  // that would be written to that file, and write the file ourselves.
  // Ideally we would just work on the string directly, but there's a lot of legacy code
  // that explicitly parses a file when loading the PPG, which would be painful to rewrite.

  // First talk to the PPG compiler
  INT client_handle;
  INT status = cm_connect_client("PPGCompilerFrontend", &client_handle);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Failed to connect to PPGCompilerFrontend");
    return CM_NO_CLIENT;
  }

  int bufsize = 100000;
  char retstr_buf[bufsize];
  status = rpc_client_call(client_handle, RPC_JRPC, "compile_now", "{\"with_image\":false, \"with_loadfile\":false, \"return_loadfile_in_response\":true}", retstr_buf, bufsize);
  cm_disconnect_client(client_handle, FALSE);

  if (status != SUCCESS) {
    cm_msg(MERROR, __FUNCTION__, "Got return value %d from PPGCompilerFrontend", status);
    return DB_TIMEOUT;
  }

  // Parse the response from the PPG compiler
  int code = 0;
  char loadfile_content[bufsize];
  status = scan_utils::extract_msg_from_rpc_response(retstr_buf, code, loadfile_content);

  if (status != SUCCESS) {
    return status;
  }

  if (code != 1) {
    cm_msg(MERROR, __FUNCTION__, "Unexpected return value from PPGCompilerFrontend: %s", retstr_buf);
    return FE_ERR_DRIVER;
  }

  // Write the new content to the file
  FILE *fptr = fopen(ppgfile, "w");

  if (!fptr) {
    cm_msg(MERROR, __FUNCTION__, "Failed to open PPG loadfile (%s) for writing", ppgfile);
    return FE_ERR_DRIVER;
  }

  fprintf(fptr, "%s", loadfile_content);
  fclose(fptr);

  return SUCCESS;
}

void ScanDevicePPG::print_sweep(std::vector<double>& values, char* ppg_path, bool is_x, int idx) {
  std::cout << "Sweep values for PPG ";

  if (is_x) {
    std::cout << "X";
  } else {
    std::cout << "Y";
  }

  std::cout << " param #" << idx << " (" << ppg_path << ") are: ";

  for (unsigned int i = 0; i < values.size(); i++) {
    if (i > 0) {
      std::cout << ", ";
    }

    std::cout << values[i];
  }

  std::cout << std::endl;
}

void ScanDevicePPG::compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) {
  for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
    if (direction_type_x != DirNoRecomputation) {
      if (strlen(user_settings.x_paths[i]) == 0 || !x_enabled) {
        sweep_x[i].clear();
      } else {
        sweep_x[i] = compute_sweep_double(direction_type_x, user_settings.x_start[i], user_settings.x_end[i], nX);
        print_sweep(sweep_x[i], user_settings.x_paths[i], true, i);
      }
    }

    if (direction_type_y != DirNoRecomputation) {
      if (strlen(user_settings.y_paths[i]) == 0 || !y_enabled) {
        sweep_y[i].clear();
      } else {
        sweep_y[i] = compute_sweep_double(direction_type_y, user_settings.y_start[i], user_settings.y_end[i], nY);
        print_sweep(sweep_y[i], user_settings.y_paths[i], false, i);
      }
    }
  }
}

INT ScanDevicePPG::setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) {
  INT status = SUCCESS;
  bool need_new_loadfile = bor;

  // Compute new values and set in ODB
  if (new_x) {
    for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
      if (sweep_x[i].size() == 0 || !x_enabled) {
        scan_status.x_values[i] = 0;
      } else {
        need_new_loadfile = true;
        scan_status.x_values[i] = sweep_x[i][x_step];
        status = db_set_value(hDB, 0, user_settings.x_paths[i], &(scan_status.x_values[i]), sizeof(double), 1, TID_DOUBLE);

        if (status != SUCCESS) {
          cm_msg(MERROR, __FUNCTION__, "Failed to set %s to %f", user_settings.x_paths[i], scan_status.x_values[i]);
          return status;
        }
      }
    }
  }

  if (new_y) {
    for (int i = 0; i < PPG_SCAN_MAXVARS; i++) {
      if (sweep_y[i].size() == 0 || !y_enabled) {
        scan_status.y_values[i] = 0;
      } else {
        need_new_loadfile = true;
        scan_status.y_values[i] = sweep_y[i][y_step];
        status = db_set_value(hDB, 0, user_settings.y_paths[i], &(scan_status.y_values[i]), sizeof(double), 1, TID_DOUBLE);

        if (status != SUCCESS) {
          cm_msg(MERROR, __FUNCTION__, "Failed to set %s to %f", user_settings.y_paths[i], scan_status.y_values[i]);
          return status;
        }
      }
    }
  }

  if (need_new_loadfile) {
    // Check where we're going to write PPG file to, so we can ensure it's written correctly.
    char ppgfile[256];
    INT size_ppgfile = sizeof(ppgfile);

    status = db_get_value(hDB, 0, "/Equipment/PPGCompiler/Settings/Loadfile path", ppgfile, &size_ppgfile, TID_STRING, FALSE);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "ODB entry for PPG loadfile path not found");
      return status;
    }

    // Compile the new loadfile
    status = compile_ppg(ppgfile);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to compile new PPG loadfile");
      return status;
    }

    // Arm the PPG (will be started when start_cycle() called).
    // Disable external trigger by default (cycle controller can enable it if desired).
    status = ppg->init(false, ppg_polarity_mask, ppg_external_clock);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to init PPG");
      return status;
    }

    // Load the file onto the PPG itself
    status = ppg->load_file(ppgfile);

    if (status != SUCCESS) {
      cm_msg(MERROR, __FUNCTION__, "Failed to load new PPG loadfile");
      return status;
    }
  }

  return status;
}


INT ScanDevicePPG::read_ppg_clock_and_trigger_settings(char* error) {
  char ext_clock_odb_path[256], uses_pb_odb_path[256], pol_mask_path[256];
  sprintf(ext_clock_odb_path, "/Equipment/PPGCompiler/Settings/Use external clock");
  sprintf(uses_pb_odb_path, "/Equipment/PPGCompiler/Settings/PPG uses pulseblaster");
  sprintf(pol_mask_path, "/Equipment/PPGCompiler/Settings/Inverted channels bitmask");

  INT status;
  HNDLE ext_clock_hkey, pol_msk_hkey;

  status = db_find_key(hDB, 0, ext_clock_odb_path, &ext_clock_hkey);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to find key %s", ext_clock_odb_path);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  status = db_open_record(hDB, ext_clock_hkey, &ppg_external_clock, sizeof(ppg_external_clock), MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to open record for %s", ext_clock_odb_path);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  status = db_find_key(hDB, 0, pol_mask_path, &pol_msk_hkey);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to find key %s", pol_mask_path);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  status = db_open_record(hDB, pol_msk_hkey, &ppg_polarity_mask, sizeof(ppg_polarity_mask), MODE_READ, NULL, NULL);

  if (status != SUCCESS) {
    snprintf(error, 255, "Failed to open record for %s", pol_mask_path);
    cm_msg(MERROR, __FUNCTION__, "%s", error);
    return status;
  }

  return SUCCESS;
}
