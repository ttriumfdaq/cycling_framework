#ifndef SCAN_DEVICE_PPG_H
#define SCAN_DEVICE_PPG_H

#include "ppg_class.h"
#include "midas.h"
#include "core/scannable_device.h"

#define PPG_SCAN_MAXVARS 20

#define PPG_SCAN_SETTINGS_STR "\
X paths = STRING[20] : \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
Y paths = STRING[20] : \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
[200] \n\
X start = DOUBLE[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y start = DOUBLE[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
X end = DOUBLE[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y end = DOUBLE[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
"

#define PPG_SCAN_STATUS_STR "\
X values = DOUBLE[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
Y values = DOUBLE[20] : \n\
[0] 0\n\
[1] 0\n\
[2] 0\n\
[3] 0\n\
[4] 0\n\
[5] 0\n\
[6] 0\n\
[7] 0\n\
[8] 0\n\
[9] 0\n\
[10] 0\n\
[11] 0\n\
[12] 0\n\
[13] 0\n\
[14] 0\n\
[15] 0\n\
[16] 0\n\
[17] 0\n\
[18] 0\n\
[19] 0\n\
"

typedef struct {
  char x_paths[PPG_SCAN_MAXVARS][200];
  char y_paths[PPG_SCAN_MAXVARS][200];
  double x_start[PPG_SCAN_MAXVARS];
  double y_start[PPG_SCAN_MAXVARS];
  double x_end[PPG_SCAN_MAXVARS];
  double y_end[PPG_SCAN_MAXVARS];
} PPG_SCAN_SETTINGS_STRUCT;

typedef struct {
  double x_values[PPG_SCAN_MAXVARS];
  double y_values[PPG_SCAN_MAXVARS];
} PPG_SCAN_STATUS_STRUCT;

class ScanDevicePPG: public ScannableDevice {
  public:
    ScanDevicePPG(HNDLE _hDB, PPG* _ppg) : ScannableDevice(_hDB) {
      ppg = _ppg;
      ppg_polarity_mask = 0;
      ppg_external_clock = false;
    }

    virtual ~ScanDevicePPG() {};

    virtual INT check_records() override;
    virtual INT open_records() override;
    virtual INT close_records() override;

    virtual INT begin_of_run(char* error) override;

    virtual INT setup_next_step(bool new_x, bool new_y, int x_step, int y_step, int rep, bool bor) override;
    virtual void compute_values_for_next_sweep(DirectionType direction_type_x, DirectionType direction_type_y) override;

    virtual INT fill_end_of_cycle_banks(char* pevent) override;

  private:
    INT compile_ppg(char* ppgfile);
    INT validate_ppg_scan_paths(char* error);
    INT read_ppg_clock_and_trigger_settings(char* error);
    void print_sweep(std::vector<double>& values, char* ppg_path, bool is_x, int idx);

    PPG* ppg;
    DWORD ppg_polarity_mask;
    BOOL ppg_external_clock;

    PPG_SCAN_SETTINGS_STRUCT user_settings;
    PPG_SCAN_STATUS_STRUCT scan_status;

    std::vector<double> sweep_x[PPG_SCAN_MAXVARS];
    std::vector<double> sweep_y[PPG_SCAN_MAXVARS];

};

#endif
